      subroutine begin(name, jnm, d1, c1, timlmt) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(out) :: timlmt 
      character  :: name*80 
      character , intent(out) :: jnm*8 
      character  :: d1*8 
      character  :: c1*8 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
! +++
! +++ get job identification, run-time limit, date and time of day.
! +++
!  open files formerly opened in flip with program statement
! name changed by pauly
      write (0, *) ' begin opening flipsol.txt' 
!      open(unit=6,file='flipsol.txt')
      open(unit=99, file='gdata', position='asis') 
      write (6, *) ' begin opened flipsol.txt' 
      write (0, *) ' begin opened flipsol.txt' 
      jnm = '  pica-1$' 
      name = 'flip   $' 
      read (5, 100) name 
  100 format(a80) 
      write (0, 100) name 
      write (6, 100) name 
!     date, time  is in cftlib on unicos
!     CRAY change 5/25/93
!pcl      call time(c1)
!pcl      call date(d1)
      timlmt = 1.E+20 
! +++
! +++ clear scm cell storage block . . .
! +++
 
      return  
      end subroutine begin 
