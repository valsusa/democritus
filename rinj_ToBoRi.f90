 
      subroutine rinj(isp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
      use emission_com_M
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: isp 
	  logical :: rebalancing
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: iwid, jwid, j, i, ij, ipj, ipjp, ijp, npinj, ip, np, nh 
      real(double) :: sbottom, delz, vtherm, flxrnd, flxdir, fluxin, qbottom, &
         sutop, qtop, sright, delr, qright, rebal, qinj, the, zeta, w1, w2, w3&
         , w4, vmag, ws, th, Omega, Omegadt, cosomdt, sinomdt, up0,x2p,z2p,&
         x2p2,dthere
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: erf, ranf, generate 
!-----------------------------------------------
 
!
!     A subroutine to inject particles
!

      pi = acos(-1.) 
      iwid = 1 
      jwid = nxp 
      nh = mod(ncyc,nhst) 
!
!     rebalance
!
      rebalancing=.false.
      if(rebalancing) then

      sbottom = 0. 
      j = 2 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-yn(ipj)) 
         sbottom = sbottom + vol(ij)/delz 
      end do 
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*vvi(isp)*(1. + erf(vvi(isp)/vtherm)) 
      fluxin = flxrnd + flxdir 
      qbottom = sbottom*fluxin*dt 
 
      sutop = 0. 
      j = ny 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-yn(ipj)) 
         sutop = sutop + vol(ij)/delz 
      end do 
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*(-vvi(isp))*(1. + erf((-vvi(isp))/vtherm)) 
      fluxin = flxrnd + flxdir 
      qtop = sutop*fluxin*dt 
 
      sright = 0. 
      i = nx 
      do j = 2, ny 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delr = 0.5*(xn(ipj)+xn(ipjp)-xn(ij)-xn(ijp)) 
         sright = sright + vol(ij)/delr 
      end do 
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-uvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*(-uvi(isp))*(1. + erf((-uvi(isp))/vtherm)) 
      fluxin = flxrnd + flxdir 
      qright = sright*fluxin*dt 
      rebal = abs(qout(isp)/(qtop+qbottom+qright)) 

      else

	      rebal=1.d0

      endif
      
      if(.not.periodic) then
!
!     BOTTOM
!
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*vvi(isp)*(1. + erf(vvi(isp)/vtherm)) 
      fluxin = (flxrnd + flxdir)*rebal*rhr(isp) 
 
      j = 2 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-yn(ipj)) 
      !   if(qom(isp)>0) then
         npinj = int(fluxin*dt*npcel(isp)/rhr(isp)/delz) 
         npinj=max(1,npinj)
      !   write(*,*)'bottom',npinj,fluxin*dt*npcel(isp)/rhr(isp)/delz
      !         else
      !   npinj=sqrt(dble(npcel(isp)))
         !npinj=1
      !   end if
         if (npinj == 0) then 
          if(ranf().lt.fluxin*dt*npcel(isp)/rhr(isp)/delz) then
           npinj=1
           qinj=rhr(isp)*vol(ij)/npcel(isp)
           endif
!            npinj = 1 
            !qinj = fluxin*vol(ij)/delz*dt 
         else 
            !qinj = fluxin*vol(ij)/delz*dt 
                     qinj = fluxin*vol(ij)/delz*dt 
         endif 
         

         qinj = qinj*sign(1.,qom(isp)) 
         
 !        write(*,*)'Bottom injection: Np=',npinj,'   Qinj=',qinj
 

         do ip = 1, npinj 
            np = iphead(1) 
 
            if (np > 0) then 
 
               iphead(1) = link(np) 
               link(np) = iphead(ij) 
               iphead(ij) = np 
 
               the = ranf() 
               zeta = 0D0 
!      ranf()*fluxin*dt/delz
               xp(np) = i + the 
               yp(np) = j + zeta 
               w1 = the*(1 - zeta) 
               w2 = the*zeta 
               w3 = (1 - the)*zeta 
               w4 = (1 - the)*(1 - zeta) 
 
               xpf(np) = w1*xn(ipj) + w2*xn(ipjp) + w3*xn(ijp) + w4*xn(ij) 
               ypf(np) = w1*yn(ipj) + w2*yn(ipjp) + w3*yn(ijp) + w4*yn(ij) 
 
               qpar(np) = qinj/npinj 
               ico(np) = isp 
               diepar(np) = abs(qpar(np))*diem(isp) 
               vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
               ws = vmag*siep(isp) 
               th = 2.*pi*ranf() 
               up(np) = uvi(isp) + ws*cos(th) 
!        vp(np)=abs(vvi(isp)+ws*sin(th))
               wp(np) = ws*sin(th) 
!        vmag = sqrt(-2.*log(1.-.999999*ranf()))
!        ws=vmag*siep(isp)
!        th = 2*pi*ranf()
!        wp(np)=ws*cos(th)
               vp(np) = generate(vvi(isp),siep(isp)*sqrt(2.))
! Advance the particle a random portion of dt 
               ypf(np) = ypf(np) + vp(np)*dt*ranf()
! (Leo)
               dtsub(np) = dt
               
               flux_inject_bottom(nh,isp)=flux_inject_bottom(nh,isp)+qpar(np)
            else 
 
               write (*, *) 'WARNING: Not Enough Particles' 
               stop  
            endif 
 
         end do 
 
      end do 
 
 
 
!
!     TOP
!
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*(-vvi(isp))*(1. + erf((-vvi(isp))/vtherm)) 
      fluxin = (flxrnd + flxdir)*rebal*rhr(isp) 

      j = ny 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-yn(ipj)) 
     !    if(qom(isp)>0) then
         npinj = int(fluxin*dt*npcel(isp)/rhr(isp)/delz) 
                  npinj=max(1,npinj)
      !   npinj=max(1,npinj)
          !write(*,*)'top',npinj,fluxin*dt*npcel(isp)/rhr(isp)/delz
     !     else
     !    npinj=sqrt(dble(npcel(isp)))
         !npinj=1
     !    end if
                  
         if (npinj == 0) then 
        if(ranf().lt.fluxin*dt*npcel(isp)/rhr(isp)/delz) then
           npinj=1
           qinj=rhr(isp)*vol(ij)/npcel(isp)
           endif
!            npinj = 1 
!            qinj = fluxin*vol(ij)/delz*dt 
         else 
!            qinj = fluxin*vol(ij)/delz*dt 
        qinj = fluxin*vol(ij)/delz*dt
         endif 
 
 
         qinj = qinj*sign(1.,qom(isp)) 
 
         do ip = 1, npinj 
            np = iphead(1) 
 
            if (np > 0) then 
 
               iphead(1) = link(np) 
               link(np) = iphead(ij) 
               iphead(ij) = np 
 
               the = ranf() 
               zeta = 1D0 
!      ranf()*fluxin*dt/delz
               xp(np) = i + the 
               yp(np) = j + zeta 
               w1 = the*(1 - zeta) 
               w2 = the*zeta 
               w3 = (1 - the)*zeta 
               w4 = (1 - the)*(1 - zeta) 
 
               xpf(np) = w1*xn(ipj) + w2*xn(ipjp) + w3*xn(ijp) + w4*xn(ij) 
               ypf(np) = w1*yn(ipj) + w2*yn(ipjp) + w3*yn(ijp) + w4*yn(ij) 
 
               qpar(np) = qinj/npinj 
               ico(np) = isp 
               diepar(np) = abs(qpar(np))*diem(isp) 
               vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
               ws = vmag*siep(isp) 
               th = 2.*pi*ranf() 
               up(np) = uvi(isp) + ws*cos(th) 
!        vp(np)=-abs(vvi(isp)+ws*sin(th))
               wp(np) = ws*sin(th) 
!        vmag = sqrt(-2.*log(1.-.999999*ranf()))
!        ws=vmag*siep(isp)
!        th = 2*pi*ranf()
!        wp(np)=ws*cos(th)
               vp(np) = -generate((-vvi(isp)),siep(isp)*sqrt(2.)) 
!      write(*,*)'top',xpf(np),ypf(np),vp(np)
               ypf(np) = ypf(np) + vp(np)*dt*ranf() 

!(Leo)
               dtsub(np) = dt
               flux_inject_top(nh,isp)=flux_inject_top(nh,isp)+qpar(np)
            else 
 
               write (*, *) 'WARNING: Not Enough Particles' 
               stop  
            endif 
 
         end do 
 
      end do 
      
      end if

!
!     RIGHT
!
     if(.not.reflctr) then
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-uvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*(-uvi(isp))*(1. + erf((-uvi(isp))/vtherm)) 
      fluxin = (flxrnd + flxdir)*rebal*rhr(isp)
 
      i = nx 
      do j = 2, ny 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         !delr = 0.5*(xn(ipj)+xn(ipjp)-xn(ij)-xn(ijp)) 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-yn(ipj))
         
    !     if(qom(isp)>0) then
         npinj = int(fluxin*dt*npcel(isp)*delz/rhr(isp)/vol(ij)) 
         npinj=max(1,npinj)
          !write(*,*)'right',npinj,fluxin*dt*npcel(isp)*delz/rhr(isp)/vol(ij)
     !     else
     !    npinj=sqrt(dble(npcel(isp)))
     !    end if
         !npinj=1
         if (npinj == 0) then 
            if(ranf().lt.fluxin*dt*npcel(isp)/vol(ij)/rhr(isp)*delz) then
               npinj=1
               !qinj=rhr(isp)*vol(ij)/npcel(isp)
            endif
 !            npinj = 1 
             !qinj = fluxin*vol(ij)/delr*dt 
         else 
            !qinj = fluxin*vol(ij)/delr*dt 
         endif
         
 		 qinj=fluxin*delz*dt
         qinj = qinj*sign(1.,qom(isp)) 
 
         ip=1
         do while(ip.le.npinj)
            np = iphead(1) 
 
            if (np > 0) then 
 
               iphead(1) = link(np) 
               link(np) = iphead(ij) 
               iphead(ij) = np 
 
111            continue
               if (ip.gt.npinj) exit
               ip=ip+1

               the = 1.D0 
               zeta = ranf() 
 
               xp(np) = i + the 
               yp(np) = j + zeta 
               w1 = the*(1 - zeta) 
               w2 = the*zeta 
               w3 = (1 - the)*zeta 
               w4 = (1 - the)*(1 - zeta) 
 
 
               xpf(np) = w1*xn(ipj) + w2*xn(ipjp) + w3*xn(ijp) + w4*xn(ij) 
               ypf(np) = w1*yn(ipj) + w2*yn(ipjp) + w3*yn(ijp) + w4*yn(ij) 
 
               qpar(np) = qinj/npinj 
               ico(np) = isp 
               diepar(np) = abs(qpar(np))*diem(isp) 
               vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
               ws = vmag*siep(isp) 
               th = 2.*pi*ranf() 
               up(np) = -generate((-uvi(isp)),siep(isp)*sqrt(2.)) 
               wp(np) = ws*sin(th) 
 
               vp(np) = vvi(isp) + ws*cos(th) 
 
!(Leo)
               Omega=-qom(isp)*by0
               dthere=dt*ranf()

               dtsub(np) = dthere
               flux_inject_right(nh,isp)=flux_inject_right(nh,isp)+qpar(np)

               if(abs(Omega).le.0.01/dthere) then
                  xpf(np) = xpf(np) + up(np)*dthere
                  ypf(np)=ypf(np)+vp(np)*dthere
                  if (ypf(np).lt.0.or.ypf(np).gt.yn(ny*jwid+nx+iwid)) then
                     goto 111
                  endif
! If the magnetic field is too large, we need to advance the particles even
! in the reinjection time step, to avoid unphysical accumulation on the right
! hand of the computational domain
               else
                  ypf(np)=ypf(np)+vp(np)*dthere
                  if (ypf(np).lt.0.or.ypf(np).gt.yn(ny*jwid+nx+iwid)) then
                     goto 111
                  endif
                  Omegadt=Omega*dthere
                  if (abs(Omegadt).ge.2*pi)  goto 111
 
                  cosomdt=cos(Omegadt)
                  sinomdt=sin(Omegadt)
                  if (cyl.eq.0) then
                     x2p2=xpf(np)+(wp(np)-wp(np)*cosomdt+up(np)*sinomdt)/Omega
                  else
                     x2p=xpf(np)+(wp(np)-wp(np)*cosomdt+up(np)*sinomdt)/Omega
                     z2p=0+(-up(np)+up(np)*cosomdt+wp(np)*sinomdt)/Omega
                     x2p2=sqrt(x2p**2+z2p**2+1E-10)

                     up0=up(np)
                     up(np)=up(np)*cosomdt+wp(np)*sinomdt
                     wp(np)=wp(np)*cosomdt-up0*sinomdt
                     up0=up(np)
                     up(np) = up(np)*x2p/x2p2 + wp(np)*z2p/x2p2 
                     wp(np) = (-up0*z2p/x2p2) + wp(np)*x2p/x2p2
                  endif

                  if(x2p2.le.xpf(np)) then
                     xpf(np)=x2p2
                  else
                     goto 111
                  endif

                  
               endif
               
            else 
 
               write (*, *) 'WARNING: Not Enough Particles' 
               stop  
            endif 
 
         end do 

      end do 
 
      end if

      return  
      end subroutine rinj 

!-------------------------------------------------------------------
!(Leo) Error function

      real(double) function erf(X)
      USE vast_kind_param, ONLY:  double
      implicit none 
      real(double) :: X,Z,ERFCC,T
      Z=ABS(X)      
      T=1./(1.+0.5*Z)
      ERFCC=T*EXP(-Z*Z-1.26551223+T*(1.00002368+T*(.37409196&
+T*(.09678418+T*(-.18628806+T*(.27886807+T*(-1.13520398+&
T*(1.48851587+T*(-.82215223+T*.17087277)))))))))
      IF (X.LT.0.) ERFCC=2.-ERFCC
      erf=1-ERFCC
      return
      end function erf
