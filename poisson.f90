      subroutine poisson 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: istart, istop, jstart, jstop, nm, j, i, ij, ibdytst, imj, imjm&
         , imjp, ipj, ijp, ijm, ipjm, itsav, ipjp 
      real(double) :: wsl, wst, wsr, wsb, error, xratio, yratio, xnorm, ynorm, dnorm 
!-----------------------------------------------

!
!      ***************************************************************
!
!      POISSON solves the equation:
!
!      -DIV(COEF*GRAD(PHI0))=SRCE
!
!      ***************************************************************
!       _________________ 
!		| c  |  d  |  e |
!       -----------------
!       | bf |  a  |  b |
!       -----------------
!       | ef |  df | cf |
!       -----------------
!


      nxp = nx + 1 
      nyp = ny + 1     
      phi0=0d0
!
!
!     set potential in ghost cells to specified boundary value
!
!
!     note: this call differs from subsequent call
!     to bccphi where phi is recalculated to give
!     correct boundary value
!
      call bccphi (nx, ny, phi0, wb, wt, wl, wr, t, omfield, phibdy, periodic, &
         yb, yt, y, amptud, modey) 
!
      istart = 2 
      istop = nx 
      jstart = 2 
      jstop = ny 
!
      if (wl == 0) istart = 1 
      if (wr == 0) istop = nx + 1 
      if (.not.periodic) then 
         if (wb == 0) jstart = 1 
         if (wt == 0) jstop = ny + 1 
      endif 
!
      nxm = istop - istart + 1 
      nym = jstop - jstart + 1 
!
      nm = 1 
      do j = jstart, jstop 
         do i = istart, istop 
            ij = (j - 1)*nxp + i 
			ipj = ij + 1 
            ipjp = ij + nxp +1 
            ijp = ij + nxp

            srce(nm) = qtilde(ij) 
            soln(nm) = phi0(ij) 
			if(max(max(diegrid(ij),diegrid(ipj)),max(diegrid(ijp),diegrid(ipjp))).lt.-1d0) then
!			if(diegrid(ij).lt.-1d0.or.diegrid(ipj).lt.-1d0.or.diegrid(ijp).lt.-1d0.or.diegrid(ipjp).lt.-1d0) then
			srce(nm) = -(diegrid(ij)+diegrid(ipj)+diegrid(ijp)+diegrid(ipjp))*.25d0*phiring
			end if
            nm = nm + 1 
         end do 
      end do 
!
      call stencil (1, nxp, istart, istop, jstart, jstop, istart, istop, jstart&
         , jstop, nxm, tinyi, x, y, bx, by, bz, diegrid, coef, coeftrns, coefpar, a, b, &
         c, d, e, af, bf, cf, df, ef, ag, bg, cg, dg, eg, ah, bh, ch, dh, eh, &
         cx1, cx2, cx3, cx4, cr1, cr2, cr3, cr4, cy1, cy2, cy3, cy4) 
!
!     boundary conditions
!
!     left boundary
!
      wsl = wl 
!
!     temp change 4/3/89 jub
!
!
!     if Neumann conditions, set coefficients to zero
!
      if (wl == 0) then 
         nm = 1 
         do j = jstart, jstop 
            a(nm) = a(nm) !- 2.*tinyi 
!      write(*,*)a(nm),tinyi
            d(nm) = d(nm) + c(nm) 
            a(nm) = a(nm) + bf(nm) 
            df(nm) = df(nm) + ef(nm) 
            c(nm) = 0.0 
            bf(nm) = 0.0 
            ef(nm) = 0.0 
            !srce(nm) = 0.0 
            nm = nm + nxm 
         end do 
      endif 
!
      if (wl == 1) then 
!     apply dirichlet conditions on left
         wsl = wl 
         ibdytst = wl + wr + wb + wt 
         if (ibdytst == 0) wsl = 1.E-3 
         imj = nxp + 1 
         imjm = imj - nxp 
         imjp = imj + nxp 
         nm = 1 
         do j = 2, ny 
            srce(nm) = srce(nm) - wsl*(c(nm)*phi0(imjp)+bf(nm)*phi0(imj)+ef(nm)&
               *phi0(imjm)) 
            bf(nm) = 0.0 
            ef(nm) = 0.0 
            c(nm) = 0.0 
            imj = imj + nxp 
            imjm = imjm + nxp 
            imjp = imjp + nxp 
            nm = nm + nxm 
         end do 
      endif 
!
!     top boundary
!
      if (.not.periodic) then 
         if (wt == 1) then 
!     apply dirichlet conditions on top
            wst = wt 
            ij = ny*nxp + 2 
            imj = ij - 1 
            ipj = ij + 1 
            nm = (nym - 1)*nxm + 1 
            do i = 2, nx 
               srce(nm) = srce(nm) - wst*(c(nm)*phi0(imj)+d(nm)*phi0(ij)+e(nm)*&
                  phi0(ipj)) 
               c(nm) = 0.0 
               d(nm) = 0.0 
               e(nm) = 0.0 
               nm = nm + 1 
               imj = imj + 1 
               ij = ij + 1 
               ipj = ipj + 1 
            end do 
         endif 
      endif 
!
!     right boundary
!
!
!     if Neumann conditions, set coefficients to zero
!
      if (wr == 0) then 
!
         nm = nxm 
         do j = jstart, jstop 
            a(nm) = a(nm) !- 2.*tinyi 
!      write(*,*)a(nm),tinyi
            e(nm) = 0.0 
            b(nm) = 0.0 
            cf(nm) = 0.0 
            srce(nm) = srce(nm) 
            nm = nm + nxm 
         end do 
!
      endif 
!
      if (wr == 1) then 
!     apply dirichlet conditions on left
         wsr = wr 
         if (ibdytst == 0) wsr = 1.E-3 
         nm = nxm 
         ij = 2*nxp 
         ijp = ij + nxp 
         ijm = ij - nxp 
         do j = 2, ny 
            srce(nm) = srce(nm) - wsr*(b(nm)*phi0(ij)+e(nm)*phi0(ijp)+cf(nm)*&
               phi0(ijm)) 
            !d(nm)=d(nm)-e(nm)
            !a(nm)=a(nm)-b(nm)
            !df(nm)=df(nm)-cf(nm)
            b(nm) = 0.0 
            e(nm) = 0.0 
            cf(nm) = 0.0 
            ij = ij + nxp 
            ijm = ijm + nxp 
            ijp = ijp + nxp 
            nm = nm + nxm 
         end do 
      endif 
      if (wb == 1) then 
!     apply dirichlet conditions on bottom
!
!     bottom boundary
!
         if (.not.periodic) then 
            wsb = wb 
            ijm = 2 
            imjm = 1 
            ipjm = 3 
            nm = 1 
            do i = 2, nx 
!
               srce(nm) = srce(nm) - wsb*(ef(nm)*phi0(imjm)+df(nm)*phi0(ijm)+cf&
                  (nm)*phi0(ipjm)) 
               ef(nm) = 0.0 
               df(nm) = 0.0 
               cf(nm) = 0.0 
               nm = nm + 1 
               ijm = ijm + 1 
               imjm = imjm + 1 
               ipjm = ipjm + 1 
            end do 
         endif 
      endif 
!
      itmax = 5000
!      itmax = 200 
      itsav = itmax 
      error = 1.d-4
      dnorm=sum(srce(1:nxm*nym)**2)
      if(dnorm.lt.1d-10) then 
        phi0=0d0
        return
      end if
!
!      write(*,*)'calling solver in poisson, source norm = ',dnorm
      if (symmetry .or. estatic) then 
         call cgitj (a, b, c, d, e, af, bf, cf, df, ef, nxm, nym, soln, srce, &
            residu, sc1, sc2, itmax, error, xratio, yratio, xnorm, ynorm) 
            
      else 
!
         call ilucgj (ef, df, cf, bf, a, b, c, d, e, eh, dh, ch, bh, ag, bg, cg&
            , dg, eg, nxm, nym, soln, srce, residu, sc1, sc2, itmax, error, &
            xratio, yratio, xnorm, ynorm) 
      endif 
      numit = itmax 
      if (numit >= itsav) write (*, *) 'Poisson: iteration failed to converge, itmax=',itmax 
!
!     transfer soln to phi0
!
      nm = 1 
      do j = jstart, jstop 
         do i = istart, istop 
            ij = (j - 1)*nxp + i 
            phi0(ij) = soln(nm) 
            nm = nm + 1 
         end do 
      end do 
!
      call bccphi (nx, ny, phi0, 2*wb, 2*wt, 2*wl, 2*wr, t, omfield, phibdy, &
         periodic, yb, yt, y, amptud, modey) 
!
!
!     *****************************************************
!
!     calculate the error in the solution of Poisson's eq.
!
!     *****************************************************
!
      call stencil (1, nxp, istart, istop, jstart, jstop, istart, istop, jstart&
         , jstop, nxm, tinyi, x, y, bx, by, bz, diegrid, coef, coeftrns, coefpar, a, b, &
         c, d, e, af, bf, cf, df, ef, ag, bg, cg, dg, eg, ah, bh, ch, dh, eh, &
         cx1, cx2, cx3, cx4, cr1, cr2, cr3, cr4, cy1, cy2, cy3, cy4) 
!
      nm = 1 
      do j = jstart, jstop 
         do i = istart, istop 
!
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
            imjp = ij + nxp - 1 
            imj = ij - 1 
            imjm = ij - nxp - 1 
            ijm = ij - nxp 
            ipjm = ij - nxp + 1 
!
            pkntc(ij) = a(nm)*phi0(ij) + b(nm)*phi0(ipj) + bf(nm)*phi0(imj) + d&
               (nm)*phi0(ijp) + df(nm)*phi0(ijm) + e(nm)*phi0(ipjp) + ef(nm)*&
               phi0(imjm) + c(nm)*phi0(imjp) + cf(nm)*phi0(ipjm) - qtilde(ij) 
!
            nm = nm + 1 
         end do 
      end do 
!
!
      return  
      end subroutine poisson 
