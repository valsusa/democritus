      subroutine bincrea 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: is, iv 
!-----------------------------------------------
      do is = 1, nsp 
         do iv = 1, ndistr 
            vd(iv) = 2.*siep(is)*3.*(iv - 1)/float(ndistr - 1) - siep(is)*3. 
            ud(iv,is) = 0. 
         end do 
      end do 
      iviaver = 0 
      iviaspl = 0 
      iviacoa = 0 
      return  
      end subroutine bincrea 
