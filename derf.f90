      REAL(KIND(0.0D0)) FUNCTION DERF (X) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  DOUBLE 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      REAL(DOUBLE) , INTENT(IN) :: X 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER :: NTERF 
      REAL(DOUBLE), DIMENSION(21) :: ERFCS 
      REAL(DOUBLE) :: SQEPS, SQRTPI, XBIG, Y 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      INTEGER , EXTERNAL :: INITDS 
      REAL(DOUBLE) , EXTERNAL :: D1MACH, DCSEVL, DERFC 
!-----------------------------------------------
!***BEGIN PROLOGUE  DERF
!***DATE WRITTEN   770701   (YYMMDD)
!***REVISION DATE  840425   (YYMMDD)
!***CATEGORY NO.  C8A,L5A1E
!***KEYWORDS  DOUBLE PRECISION,ERROR FUNCTION,SPECIAL FUNCTION
!***AUTHOR  FULLERTON, W., (LANL)
!***PURPOSE  Computes the d.p. error function, ERF, of X.
!***DESCRIPTION
!
! DERF(X) calculates the double precision error function for double
! precision argument X.
!
! Series for ERF        on the interval  0.          to  1.00000E+00
!                                        with weighted error   1.28E-32
!                                         log weighted error  31.89
!                               significant figures required  31.05
!                                    decimal places required  32.55
!***REFERENCES  (NONE)
!***ROUTINES CALLED  D1MACH,DCSEVL,DERFC,INITDS
!***END PROLOGUE  DERF
      DATA ERFCS(1)/  - .49046121234691808039984544033376D-1/  
      DATA ERFCS(2)/  - .14226120510371364237824741899631D+0/  
      DATA ERFCS(3)/  + .10035582187599795575754676712933D-1/  
      DATA ERFCS(4)/  - .57687646997674847650827025509167D-3/  
      DATA ERFCS(5)/  + .27419931252196061034422160791471D-4/  
      DATA ERFCS(6)/  - .11043175507344507604135381295905D-5/  
      DATA ERFCS(7)/  + .38488755420345036949961311498174D-7/  
      DATA ERFCS(8)/  - .11808582533875466969631751801581D-8/  
      DATA ERFCS(9)/  + .32334215826050909646402930953354D-10/  
      DATA ERFCS(10)/  - .79910159470045487581607374708595D-12/  
      DATA ERFCS(11)/  + .17990725113961455611967245486634D-13/  
      DATA ERFCS(12)/  - .37186354878186926382316828209493D-15/  
      DATA ERFCS(13)/  + .71035990037142529711689908394666D-17/  
      DATA ERFCS(14)/  - .12612455119155225832495424853333D-18/  
      DATA ERFCS(15)/  + .20916406941769294369170500266666D-20/  
      DATA ERFCS(16)/  - .32539731029314072982364160000000D-22/  
      DATA ERFCS(17)/  + .47668672097976748332373333333333D-24/  
      DATA ERFCS(18)/  - .65980120782851343155199999999999D-26/  
      DATA ERFCS(19)/  + .86550114699637626197333333333333D-28/  
      DATA ERFCS(20)/  - .10788925177498064213333333333333D-29/  
      DATA ERFCS(21)/  + .12811883993017002666666666666666D-31/  
      DATA SQRTPI/ 1.77245385090551602729816748334115D0/  
      DATA NTERF, XBIG, SQEPS/ 0, 2*0.D0/  
!***FIRST EXECUTABLE STATEMENT  DERF
      IF (NTERF == 0) THEN 
         NTERF = INITDS(ERFCS,21,0.1*SNGL(D1MACH(3))) 
         XBIG = DSQRT((-DLOG(SQRTPI*D1MACH(3)))) 
         SQEPS = DSQRT(2.0D0*D1MACH(3)) 
      ENDIF 
!
      Y = DABS(X) 
      IF (Y <= 1.D0) THEN 
         IF (Y <= SQEPS) THEN 
!
! ERF(X) = 1.0 - ERFC(X)  FOR  -1.0 .LE. X .LE. 1.0
!
            DERF = 2.0D0*X/SQRTPI 
         ELSE 
            DERF = X*(1.0D0 + DCSEVL(2.D0*X*X - 1.D0,ERFCS,NTERF)) 
         ENDIF 
         RETURN  
      ENDIF 
!
! ERF(X) = 1.0 - ERFC(X) FOR ABS(X) .GT. 1.0
!
      IF (Y <= XBIG) DERF = DSIGN(1.0D0 - DERFC(Y),X) 
      IF (Y > XBIG) DERF = DSIGN(1.0D0,X) 
!
      RETURN  
      END FUNCTION DERF 


      REAL(KIND(0.0D0)) FUNCTION DERFC (X) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  DOUBLE 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      REAL(DOUBLE) , INTENT(IN) :: X 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER :: NTERF, NTERFC, NTERC2 
      REAL :: ETA 
      REAL(DOUBLE), DIMENSION(21) :: ERFCS 
      REAL(DOUBLE), DIMENSION(59) :: ERFCCS 
      REAL(DOUBLE), DIMENSION(49) :: ERC2CS 
      REAL(DOUBLE) :: SQEPS, SQRTPI, XMAX, XSML, Y 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      INTEGER , EXTERNAL :: INITDS 
      REAL(DOUBLE) , EXTERNAL :: D1MACH, DCSEVL 
!-----------------------------------------------
!***BEGIN PROLOGUE  DERFC
!***DATE WRITTEN   770701   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  C8A,L5A1E
!***KEYWORDS  COMPLEMENTARY ERROR FUNCTION,DOUBLE PRECISION,
!             SPECIAL FUNCTION
!***AUTHOR  FULLERTON, W., (LANL)
!***PURPOSE  Computes the d.p. complementary error function, ERFC.
!***DESCRIPTION
!
! DERFC(X) calculates the double precision complementary error function
! for double precision argument X.
!
! Series for ERF        on the interval  0.          to  1.00000E+00
!                                        with weighted Error   1.28E-32
!                                         log weighted Error  31.89
!                               significant figures required  31.05
!                                    decimal places required  32.55
!
! Series for ERC2       on the interval  2.50000E-01 to  1.00000E+00
!                                        with weighted Error   2.67E-32
!                                         log weighted Error  31.57
!                               significant figures required  30.31
!                                    decimal places required  32.42
!
! Series for ERFC       on the interval  0.          to  2.50000E-01
!                                        with weighted error   1.53E-31
!                                         log weighted error  30.82
!                               significant figures required  29.47
!                                    decimal places required  31.70
!***REFERENCES  (NONE)
!***ROUTINES CALLED  D1MACH,DCSEVL,INITDS,XERROR
!***END PROLOGUE  DERFC
      DATA ERFCS(1)/  - .49046121234691808039984544033376D-1/  
      DATA ERFCS(2)/  - .14226120510371364237824741899631D+0/  
      DATA ERFCS(3)/  + .10035582187599795575754676712933D-1/  
      DATA ERFCS(4)/  - .57687646997674847650827025509167D-3/  
      DATA ERFCS(5)/  + .27419931252196061034422160791471D-4/  
      DATA ERFCS(6)/  - .11043175507344507604135381295905D-5/  
      DATA ERFCS(7)/  + .38488755420345036949961311498174D-7/  
      DATA ERFCS(8)/  - .11808582533875466969631751801581D-8/  
      DATA ERFCS(9)/  + .32334215826050909646402930953354D-10/  
      DATA ERFCS(10)/  - .79910159470045487581607374708595D-12/  
      DATA ERFCS(11)/  + .17990725113961455611967245486634D-13/  
      DATA ERFCS(12)/  - .37186354878186926382316828209493D-15/  
      DATA ERFCS(13)/  + .71035990037142529711689908394666D-17/  
      DATA ERFCS(14)/  - .12612455119155225832495424853333D-18/  
      DATA ERFCS(15)/  + .20916406941769294369170500266666D-20/  
      DATA ERFCS(16)/  - .32539731029314072982364160000000D-22/  
      DATA ERFCS(17)/  + .47668672097976748332373333333333D-24/  
      DATA ERFCS(18)/  - .65980120782851343155199999999999D-26/  
      DATA ERFCS(19)/  + .86550114699637626197333333333333D-28/  
      DATA ERFCS(20)/  - .10788925177498064213333333333333D-29/  
      DATA ERFCS(21)/  + .12811883993017002666666666666666D-31/  
      DATA ERC2CS(1)/  - .6960134660230950112739150826197D-1/  
      DATA ERC2CS(2)/  - .4110133936262089348982212084666D-1/  
      DATA ERC2CS(3)/  + .3914495866689626881561143705244D-2/  
      DATA ERC2CS(4)/  - .4906395650548979161280935450774D-3/  
      DATA ERC2CS(5)/  + .7157479001377036380760894141825D-4/  
      DATA ERC2CS(6)/  - .1153071634131232833808232847912D-4/  
      DATA ERC2CS(7)/  + .1994670590201997635052314867709D-5/  
      DATA ERC2CS(8)/  - .3642666471599222873936118430711D-6/  
      DATA ERC2CS(9)/  + .6944372610005012589931277214633D-7/  
      DATA ERC2CS(10)/  - .1371220902104366019534605141210D-7/  
      DATA ERC2CS(11)/  + .2788389661007137131963860348087D-8/  
      DATA ERC2CS(12)/  - .5814164724331161551864791050316D-9/  
      DATA ERC2CS(13)/  + .1238920491752753181180168817950D-9/  
      DATA ERC2CS(14)/  - .2690639145306743432390424937889D-10/  
      DATA ERC2CS(15)/  + .5942614350847910982444709683840D-11/  
      DATA ERC2CS(16)/  - .1332386735758119579287754420570D-11/  
      DATA ERC2CS(17)/  + .3028046806177132017173697243304D-12/  
      DATA ERC2CS(18)/  - .6966648814941032588795867588954D-13/  
      DATA ERC2CS(19)/  + .1620854541053922969812893227628D-13/  
      DATA ERC2CS(20)/  - .3809934465250491999876913057729D-14/  
      DATA ERC2CS(21)/  + .9040487815978831149368971012975D-15/  
      DATA ERC2CS(22)/  - .2164006195089607347809812047003D-15/  
      DATA ERC2CS(23)/  + .5222102233995854984607980244172D-16/  
      DATA ERC2CS(24)/  - .1269729602364555336372415527780D-16/  
      DATA ERC2CS(25)/  + .3109145504276197583836227412951D-17/  
      DATA ERC2CS(26)/  - .7663762920320385524009566714811D-18/  
      DATA ERC2CS(27)/  + .1900819251362745202536929733290D-18/  
      DATA ERC2CS(28)/  - .4742207279069039545225655999965D-19/  
      DATA ERC2CS(29)/  + .1189649200076528382880683078451D-19/  
      DATA ERC2CS(30)/  - .3000035590325780256845271313066D-20/  
      DATA ERC2CS(31)/  + .7602993453043246173019385277098D-21/  
      DATA ERC2CS(32)/  - .1935909447606872881569811049130D-21/  
      DATA ERC2CS(33)/  + .4951399124773337881000042386773D-22/  
      DATA ERC2CS(34)/  - .1271807481336371879608621989888D-22/  
      DATA ERC2CS(35)/  + .3280049600469513043315841652053D-23/  
      DATA ERC2CS(36)/  - .8492320176822896568924792422399D-24/  
      DATA ERC2CS(37)/  + .2206917892807560223519879987199D-24/  
      DATA ERC2CS(38)/  - .5755617245696528498312819507199D-25/  
      DATA ERC2CS(39)/  + .1506191533639234250354144051199D-25/  
      DATA ERC2CS(40)/  - .3954502959018796953104285695999D-26/  
      DATA ERC2CS(41)/  + .1041529704151500979984645051733D-26/  
      DATA ERC2CS(42)/  - .2751487795278765079450178901333D-27/  
      DATA ERC2CS(43)/  + .7290058205497557408997703680000D-28/  
      DATA ERC2CS(44)/  - .1936939645915947804077501098666D-28/  
      DATA ERC2CS(45)/  + .5160357112051487298370054826666D-29/  
      DATA ERC2CS(46)/  - .1378419322193094099389644800000D-29/  
      DATA ERC2CS(47)/  + .3691326793107069042251093333333D-30/  
      DATA ERC2CS(48)/  - .9909389590624365420653226666666D-31/  
      DATA ERC2CS(49)/  + .2666491705195388413323946666666D-31/  
      DATA ERFCCS(1)/  + .715179310202924774503697709496D-1/  
      DATA ERFCCS(2)/  - .265324343376067157558893386681D-1/  
      DATA ERFCCS(3)/  + .171115397792085588332699194606D-2/  
      DATA ERFCCS(4)/  - .163751663458517884163746404749D-3/  
      DATA ERFCCS(5)/  + .198712935005520364995974806758D-4/  
      DATA ERFCCS(6)/  - .284371241276655508750175183152D-5/  
      DATA ERFCCS(7)/  + .460616130896313036969379968464D-6/  
      DATA ERFCCS(8)/  - .822775302587920842057766536366D-7/  
      DATA ERFCCS(9)/  + .159214187277090112989358340826D-7/  
      DATA ERFCCS(10)/  - .329507136225284321486631665072D-8/  
      DATA ERFCCS(11)/  + .722343976040055546581261153890D-9/  
      DATA ERFCCS(12)/  - .166485581339872959344695966886D-9/  
      DATA ERFCCS(13)/  + .401039258823766482077671768814D-10/  
      DATA ERFCCS(14)/  - .100481621442573113272170176283D-10/  
      DATA ERFCCS(15)/  + .260827591330033380859341009439D-11/  
      DATA ERFCCS(16)/  - .699111056040402486557697812476D-12/  
      DATA ERFCCS(17)/  + .192949233326170708624205749803D-12/  
      DATA ERFCCS(18)/  - .547013118875433106490125085271D-13/  
      DATA ERFCCS(19)/  + .158966330976269744839084032762D-13/  
      DATA ERFCCS(20)/  - .472689398019755483920369584290D-14/  
      DATA ERFCCS(21)/  + .143587337678498478672873997840D-14/  
      DATA ERFCCS(22)/  - .444951056181735839417250062829D-15/  
      DATA ERFCCS(23)/  + .140481088476823343737305537466D-15/  
      DATA ERFCCS(24)/  - .451381838776421089625963281623D-16/  
      DATA ERFCCS(25)/  + .147452154104513307787018713262D-16/  
      DATA ERFCCS(26)/  - .489262140694577615436841552532D-17/  
      DATA ERFCCS(27)/  + .164761214141064673895301522827D-17/  
      DATA ERFCCS(28)/  - .562681717632940809299928521323D-18/  
      DATA ERFCCS(29)/  + .194744338223207851429197867821D-18/  
      DATA ERFCCS(30)/  - .682630564294842072956664144723D-19/  
      DATA ERFCCS(31)/  + .242198888729864924018301125438D-19/  
      DATA ERFCCS(32)/  - .869341413350307042563800861857D-20/  
      DATA ERFCCS(33)/  + .315518034622808557122363401262D-20/  
      DATA ERFCCS(34)/  - .115737232404960874261239486742D-20/  
      DATA ERFCCS(35)/  + .428894716160565394623737097442D-21/  
      DATA ERFCCS(36)/  - .160503074205761685005737770964D-21/  
      DATA ERFCCS(37)/  + .606329875745380264495069923027D-22/  
      DATA ERFCCS(38)/  - .231140425169795849098840801367D-22/  
      DATA ERFCCS(39)/  + .888877854066188552554702955697D-23/  
      DATA ERFCCS(40)/  - .344726057665137652230718495566D-23/  
      DATA ERFCCS(41)/  + .134786546020696506827582774181D-23/  
      DATA ERFCCS(42)/  - .531179407112502173645873201807D-24/  
      DATA ERFCCS(43)/  + .210934105861978316828954734537D-24/  
      DATA ERFCCS(44)/  - .843836558792378911598133256738D-25/  
      DATA ERFCCS(45)/  + .339998252494520890627359576337D-25/  
      DATA ERFCCS(46)/  - .137945238807324209002238377110D-25/  
      DATA ERFCCS(47)/  + .563449031183325261513392634811D-26/  
      DATA ERFCCS(48)/  - .231649043447706544823427752700D-26/  
      DATA ERFCCS(49)/  + .958446284460181015263158381226D-27/  
      DATA ERFCCS(50)/  - .399072288033010972624224850193D-27/  
      DATA ERFCCS(51)/  + .167212922594447736017228709669D-27/  
      DATA ERFCCS(52)/  - .704599152276601385638803782587D-28/  
      DATA ERFCCS(53)/  + .297976840286420635412357989444D-28/  
      DATA ERFCCS(54)/  - .126252246646061929722422632994D-28/  
      DATA ERFCCS(55)/  + .539543870454248793985299653154D-29/  
      DATA ERFCCS(56)/  - .238099288253145918675346190062D-29/  
      DATA ERFCCS(57)/  + .109905283010276157359726683750D-29/  
      DATA ERFCCS(58)/  - .486771374164496572732518677435D-30/  
      DATA ERFCCS(59)/  + .152587726411035756763200828211D-30/  
      DATA SQRTPI/ 1.77245385090551602729816748334115D0/  
      DATA NTERF, NTERFC, NTERC2, XSML, XMAX, SQEPS/ 3*0, 3*0.D0/  
!***FIRST EXECUTABLE STATEMENT  DERFC
      IF (NTERF == 0) THEN 
         ETA = 0.1*SNGL(D1MACH(3)) 
         NTERF = INITDS(ERFCS,21,ETA) 
         NTERFC = INITDS(ERFCCS,59,ETA) 
         NTERC2 = INITDS(ERC2CS,49,ETA) 
!
         XSML = -DSQRT((-DLOG(SQRTPI*D1MACH(3)))) 
         XMAX = DSQRT((-DLOG(SQRTPI*D1MACH(1)))) 
         XMAX = XMAX - 0.5D0*DLOG(XMAX)/XMAX - 0.01D0 
         SQEPS = DSQRT(2.0D0*D1MACH(3)) 
      ENDIF 
!
      IF (X <= XSML) THEN 
!
! ERFC(X) = 1.0 - ERF(X)  FOR  X .LT. XSML
!
         DERFC = 2.0D0 
         RETURN  
      ENDIF 
!
      IF (X <= XMAX) THEN 
         Y = DABS(X) 
         IF (Y <= 1.0D0) THEN 
            IF (Y < SQEPS) THEN 
!
! ERFC(X) = 1.0 - ERF(X)  FOR ABS(X) .LE. 1.0
!
               DERFC = 1.0D0 - 2.0D0*X/SQRTPI 
            ELSE 
               DERFC = 1.0D0 - X*(1.0D0 + DCSEVL(2.D0*X*X - 1.D0,ERFCS,NTERF)) 
            ENDIF 
            RETURN  
         ENDIF 
!
! ERFC(X) = 1.0 - ERF(X)  FOR  1.0 .LT. ABS(X) .LE. XMAX
!
         Y = Y*Y 
         IF (Y <= 4.D0) DERFC = DEXP((-Y))/DABS(X)*(0.5D0 + DCSEVL((8.D0/Y - &
            5.D0)/3.D0,ERC2CS,NTERC2)) 
         IF (Y > 4.D0) DERFC = DEXP((-Y))/DABS(X)*(0.5D0 + DCSEVL(8.D0/Y - 1.D0&
            ,ERFCCS,NTERFC)) 
         IF (X < 0.D0) DERFC = 2.0D0 - DERFC 
         RETURN  
      ENDIF 
!
      CALL XERROR ('DERFC   X SO BIG ERFC UNDERFLOWS', 32, 1, 1) 
      DERFC = 0.D0 
      RETURN  
!
      END FUNCTION DERFC 


      INTEGER FUNCTION INITDS (DOS, NOS, ETA) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  DOUBLE 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(IN) :: NOS 
      REAL , INTENT(IN) :: ETA 
      REAL(DOUBLE) , INTENT(IN) :: DOS(NOS) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER :: II, I 
      REAL :: ERR 
!-----------------------------------------------
!***BEGIN PROLOGUE  INITDS
!***DATE WRITTEN   770601   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  C3A2
!***KEYWORDS  CHEBYSHEV,DOUBLE PRECISION,INITIALIZE,
!             ORTHOGONAL POLYNOMIAL,SERIES,SPECIAL FUNCTION
!***AUTHOR  FULLERTON, W., (LANL)
!***PURPOSE  Initializes the d.p. properly normalized orthogonal
!            polynomial series to determine the number of terms needed
!            for specific accuracy.
!***DESCRIPTION
!
! Initialize the double precision orthogonal series DOS so that INITDS
! is the number of terms needed to insure the error is no larger than
! ETA.  Ordinarily ETA will be chosen to be one-tenth machine precision
!
!             Input Arguments --
! DOS    dble prec array of NOS coefficients in an orthogonal series.
! NOS    number of coefficients in DOS.
! ETA    requested accuracy of series.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  XERROR
!***END PROLOGUE  INITDS
!
!***FIRST EXECUTABLE STATEMENT  INITDS
      IF (NOS < 1) CALL XERROR ('INITDS  NUMBER OF COEFFICIENTS LT 1', 35, 2, 2&
         ) 
!
      ERR = 0. 
      DO II = 1, NOS 
         I = NOS + 1 - II 
         ERR = ERR + ABS(SNGL(DOS(I))) 
         IF (ERR <= ETA) CYCLE  
         EXIT  
      END DO 
!
      IF (I == NOS) CALL XERROR ('INITDS  ETA MAY BE TOO SMALL', 28, 1, 2) 
      INITDS = I 
!
      RETURN  
      END FUNCTION INITDS 


      SUBROUTINE XERROR(MESSG, NMESSG, NERR, LEVEL) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER  :: NMESSG 
      INTEGER  :: NERR 
      INTEGER  :: LEVEL 
      CHARACTER  :: MESSG*(*) 
!-----------------------------------------------
!***BEGIN PROLOGUE  XERROR
!***DATE WRITTEN   790801   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  R3C
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Processes an error (diagnostic) message.
!***DESCRIPTION
!     Abstract
!        XERROR processes a diagnostic message, in a manner
!        determined by the value of LEVEL and the current value
!        of the library error control flag, KONTRL.
!        (See subroutine XSETF for details.)
!
!     Description of Parameters
!      --Input--
!        MESSG - the Hollerith message to be processed, containing
!                no more than 72 characters.
!        NMESSG- the actual number of characters in MESSG.
!        NERR  - the error number associated with this message.
!                NERR must not be zero.
!        LEVEL - error category.
!                =2 means this is an unconditionally fatal error.
!                =1 means this is a recoverable error.  (I.e., it is
!                   non-fatal if XSETF has been appropriately called.)
!                =0 means this is a warning message only.
!                =-1 means this is a warning message which is to be
!                   printed at most once, regardless of how many
!                   times this call is executed.
!
!     Examples
!        CALL XERROR('SMOOTH -- NUM WAS ZERO.',23,1,2)
!        CALL XERROR('INTEG  -- LESS THAN FULL ACCURACY ACHIEVED.',
!                    43,2,1)
!        CALL XERROR('ROOTER -- ACTUAL ZERO OF F FOUND BEFORE INTERVAL F
!    1ULLY COLLAPSED.',65,3,0)
!        CALL XERROR('EXP    -- UNDERFLOWS BEING SET TO ZERO.',39,1,-1)
!
!     Latest revision ---  19 MAR 1980
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  XERRWV
!***END PROLOGUE  XERROR
!***FIRST EXECUTABLE STATEMENT  XERROR
      CALL XERRWV (MESSG, NMESSG, NERR, LEVEL, 0, 0, 0, 0, 0., 0.) 
      RETURN  
      END SUBROUTINE XERROR 


      SUBROUTINE XERRWV(MESSG, NMESSG, NERR, LEVEL, NI, I1, I2, NR, R1, R2) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER  :: NMESSG 
      INTEGER  :: NERR 
      INTEGER  :: LEVEL 
      INTEGER , INTENT(IN) :: NI 
      INTEGER , INTENT(IN) :: I1 
      INTEGER , INTENT(IN) :: I2 
      INTEGER , INTENT(IN) :: NR 
      REAL , INTENT(IN) :: R1 
      REAL , INTENT(IN) :: R2 
      CHARACTER  :: MESSG*(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER , DIMENSION(5) :: LUN 
      INTEGER :: LKNTRL, MAXMES, KDUMMY, JUNK, KOUNT, LMESSG, LERR, LLEVEL, &
         MKNTRL, NUNIT, ISIZEI, ISIZEF, KUNIT, IUNIT, I, IFATAL 
      CHARACTER :: LFIRST*20, FORM*37 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      INTEGER , EXTERNAL :: J4SAVE, I1MACH 
!-----------------------------------------------
!***BEGIN PROLOGUE  XERRWV
!***DATE WRITTEN   800319   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  R3C
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Processes error message allowing 2 integer and two real
!            values to be included in the message.
!***DESCRIPTION
!     Abstract
!        XERRWV processes a diagnostic message, in a manner
!        determined by the value of LEVEL and the current value
!        of the library error control flag, KONTRL.
!        (See subroutine XSETF for details.)
!        In addition, up to two integer values and two real
!        values may be printed along with the message.
!
!     Description of Parameters
!      --Input--
!        MESSG - the Hollerith message to be processed.
!        NMESSG- the actual number of characters in MESSG.
!        NERR  - the error number associated with this message.
!                NERR must not be zero.
!        LEVEL - error category.
!                =2 means this is an unconditionally fatal error.
!                =1 means this is a recoverable error.  (I.e., it is
!                   non-fatal if XSETF has been appropriately called.)
!                =0 means this is a warning message only.
!                =-1 means this is a warning message which is to be
!                   printed at most once, regardless of how many
!                   times this call is executed.
!        NI    - number of integer values to be printed. (0 to 2)
!        I1    - first integer value.
!        I2    - second integer value.
!        NR    - number of real values to be printed. (0 to 2)
!        R1    - first real value.
!        R2    - second real value.
!
!     Examples
!        CALL XERRWV('SMOOTH -- NUM (=I1) WAS ZERO.',29,1,2,
!    1   1,NUM,0,0,0.,0.)
!        CALL XERRWV('QUADXY -- REQUESTED ERROR (R1) LESS THAN MINIMUM (
!    1R2).,54,77,1,0,0,0,2,ERRREQ,ERRMIN)
!
!     Latest revision ---  19 MAR 1980
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  FDUMP,I1MACH,J4SAVE,XERABT,XERCTL,XERPRT,XERSAV,
!                    XGETUA
!***END PROLOGUE  XERRWV
!     GET FLAGS
!***FIRST EXECUTABLE STATEMENT  XERRWV
      LKNTRL = J4SAVE(2,0,.FALSE.) 
      MAXMES = J4SAVE(4,0,.FALSE.) 
!     CHECK FOR VALID INPUT
      IF (.NOT.(NMESSG>0 .AND. NERR/=0 .AND. LEVEL>=(-1) .AND. LEVEL<=2)) THEN 
         IF (LKNTRL > 0) CALL XERPRT ('FATAL ERROR IN...', 17) 
         CALL XERPRT ('XERROR -- INVALID INPUT', 23) 
         IF (LKNTRL > 0) CALL FDUMP 
         IF (LKNTRL > 0) CALL XERPRT ('JOB ABORT DUE TO FATAL ERROR.', 29) 
         IF (LKNTRL > 0) CALL XERSAV (' ', 0, 0, 0, KDUMMY) 
         CALL XERABT ('XERROR -- INVALID INPUT', 23) 
         RETURN  
      ENDIF 
      JUNK = J4SAVE(1,NERR,.TRUE.) 
      CALL XERSAV (MESSG, NMESSG, NERR, LEVEL, KOUNT) 
!     LET USER OVERRIDE
      LFIRST = MESSG 
      LMESSG = NMESSG 
      LERR = NERR 
      LLEVEL = LEVEL 
      CALL XERCTL (LFIRST, LMESSG, LERR, LLEVEL, LKNTRL) 
!     RESET TO ORIGINAL VALUES
      LMESSG = NMESSG 
      LERR = NERR 
      LLEVEL = LEVEL 
      LKNTRL = MAX0(-2,MIN0(2,LKNTRL)) 
      MKNTRL = IABS(LKNTRL) 
!     DECIDE WHETHER TO PRINT MESSAGE
      IF (LLEVEL>=2 .OR. LKNTRL/=0) THEN 
         IF (.NOT.(LLEVEL==(-1) .AND. KOUNT>MIN0(1,MAXMES) .OR. LLEVEL==0&
             .AND. KOUNT>MAXMES .OR. LLEVEL==1 .AND. KOUNT>MAXMES .AND. MKNTRL&
            ==1 .OR. LLEVEL==2 .AND. KOUNT>MAX0(1,MAXMES))) THEN 
            IF (LKNTRL > 0) THEN 
               CALL XERPRT (' ', 1) 
!           INTRODUCTION
               IF (LLEVEL == (-1)) CALL XERPRT (&
                  'WARNING MESSAGE...THIS MESSAGE WILL ONLY BE PRINTED ONCE.', &
                  57) 
               IF (LLEVEL == 0) CALL XERPRT ('WARNING IN...', 13) 
               IF (LLEVEL == 1) CALL XERPRT ('RECOVERABLE ERROR IN...', 23) 
               IF (LLEVEL == 2) CALL XERPRT ('FATAL ERROR IN...', 17) 
            ENDIF 
            CALL XERPRT (MESSG, LMESSG) 
            CALL XGETUA (LUN, NUNIT) 
            ISIZEI = LOG10(FLOAT(I1MACH(9))) + 1.0 
            ISIZEF = LOG10(FLOAT(I1MACH(10))**I1MACH(11)) + 1.0 
            DO KUNIT = 1, NUNIT 
               IUNIT = LUN(KUNIT) 
               IF (IUNIT == 0) IUNIT = I1MACH(4) 
               DO I = 1, MIN(NI,2) 
                  WRITE (FORM, 21) I, ISIZEI 
   21             FORMAT('(11X,21HIN ABOVE MESSAGE, I',I1,'=,I',I2,')   ') 
                  IF (I == 1) WRITE (IUNIT, FORM) I1 
                  IF (I /= 2) CYCLE  
                  WRITE (IUNIT, FORM) I2 
               END DO 
               DO I = 1, MIN(NR,2) 
                  WRITE (FORM, 23) I, ISIZEF + 10, ISIZEF 
   23             FORMAT('(11X,21HIN ABOVE MESSAGE, R',I1,'=,E',I2,'.',I2,')') 
                  IF (I == 1) WRITE (IUNIT, FORM) R1 
                  IF (I /= 2) CYCLE  
                  WRITE (IUNIT, FORM) R2 
               END DO 
               IF (LKNTRL <= 0) CYCLE  
!              ERROR NUMBER
               WRITE (IUNIT, 30) LERR 
   30          FORMAT(' ERROR NUMBER =',I10) 
            END DO 
!        TRACE-BACK
            IF (LKNTRL > 0) CALL FDUMP 
         ENDIF 
      ENDIF 
      IFATAL = 0 
      IF (LLEVEL==2 .OR. LLEVEL==1 .AND. MKNTRL==2) IFATAL = 1 
!     QUIT HERE IF MESSAGE IS NOT FATAL
      IF (IFATAL <= 0) RETURN  
      IF (LKNTRL>0 .AND. KOUNT<=MAX0(1,MAXMES)) THEN 
!        PRINT REASON FOR ABORT
         IF (LLEVEL == 1) CALL XERPRT ('JOB ABORT DUE TO UNRECOVERED ERROR.', &
            35) 
         IF (LLEVEL == 2) CALL XERPRT ('JOB ABORT DUE TO FATAL ERROR.', 29) 
!        PRINT ERROR SUMMARY
         CALL XERSAV (' ', -1, 0, 0, KDUMMY) 
      ENDIF 
      IF (LLEVEL==2 .AND. KOUNT>MAX0(1,MAXMES)) LMESSG = 0 
      CALL XERABT (MESSG, LMESSG) 
      RETURN  
      END SUBROUTINE XERRWV 


      SUBROUTINE XERSAV(MESSG, NMESSG, NERR, LEVEL, ICOUNT) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(IN) :: NMESSG 
      INTEGER , INTENT(IN) :: NERR 
      INTEGER , INTENT(IN) :: LEVEL 
      INTEGER , INTENT(OUT) :: ICOUNT 
      CHARACTER , INTENT(IN) :: MESSG*(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER , DIMENSION(5) :: LUN 
      INTEGER , DIMENSION(10) :: NERTAB, LEVTAB, KOUNT 
      INTEGER :: KOUNTX, NUNIT, KUNIT, IUNIT, I, II 
      CHARACTER , DIMENSION(10) :: MESTAB*20 
      CHARACTER :: MES*20 

      SAVE MESTAB, NERTAB, LEVTAB, KOUNT, KOUNTX 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      INTEGER , EXTERNAL :: I1MACH 
!-----------------------------------------------
!***BEGIN PROLOGUE  XERSAV
!***DATE WRITTEN   800319   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  Z
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Records that an error occurred.
!***DESCRIPTION
!     Abstract
!        Record that this error occurred.
!
!     Description of Parameters
!     --Input--
!       MESSG, NMESSG, NERR, LEVEL are as in XERROR,
!       except that when NMESSG=0 the tables will be
!       dumped and cleared, and when NMESSG is less than zero the
!       tables will be dumped and not cleared.
!     --Output--
!       ICOUNT will be the number of times this message has
!       been seen, or zero if the table has overflowed and
!       does not contain this message specifically.
!       When NMESSG=0, ICOUNT will not be altered.
!
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!     Latest revision ---  19 Mar 1980
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  I1MACH,S88FMT,XGETUA
!***END PROLOGUE  XERSAV
!     NEXT TWO DATA STATEMENTS ARE NECESSARY TO PROVIDE A BLANK
!     ERROR TABLE INITIALLY
      DATA KOUNT(1), KOUNT(2), KOUNT(3), KOUNT(4), KOUNT(5), KOUNT(6), KOUNT(7)&
         , KOUNT(8), KOUNT(9), KOUNT(10)/ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0/  
      DATA KOUNTX/ 0/  
!***FIRST EXECUTABLE STATEMENT  XERSAV
      IF (NMESSG <= 0) THEN 
!     DUMP THE TABLE
         IF (KOUNT(1) == 0) RETURN  
!        PRINT TO EACH UNIT
         CALL XGETUA (LUN, NUNIT) 
         DO KUNIT = 1, NUNIT 
            IUNIT = LUN(KUNIT) 
            IF (IUNIT == 0) IUNIT = I1MACH(4) 
!           PRINT TABLE HEADER
            WRITE (IUNIT, 10) 
   10       FORMAT('0          ERROR MESSAGE SUMMARY'/,&
               ' MESSAGE START             NERR     LEVEL     COUNT') 
!           PRINT BODY OF TABLE
            DO I = 1, 10 
               IF (KOUNT(I) == 0) EXIT  
               WRITE (IUNIT, 15) MESTAB(I), NERTAB(I), LEVTAB(I), KOUNT(I) 
   15          FORMAT(1X,A20,3I10) 
            END DO 
!           PRINT NUMBER OF OTHER ERRORS
            IF (KOUNTX /= 0) WRITE (IUNIT, 40) KOUNTX 
   40       FORMAT('0OTHER ERRORS NOT INDIVIDUALLY TABULATED=',I10) 
            WRITE (IUNIT, 50) 
   50       FORMAT(1X) 
         END DO 
         IF (NMESSG < 0) RETURN  
!        CLEAR THE ERROR TABLES
         DO I = 1, 10 
            KOUNT(I) = 0 
         END DO 
         KOUNTX = 0 
         RETURN  
      ENDIF 
      MES = MESSG 
      DO I = 1, 10 
         II = I 
         IF (KOUNT(I) == 0) GO TO 110 
         IF (MES /= MESTAB(I)) CYCLE  
         IF (NERR /= NERTAB(I)) CYCLE  
         IF (LEVEL /= LEVTAB(I)) CYCLE  
         GO TO 100 
      END DO 
!     THREE POSSIBLE CASES...
!     TABLE IS FULL
      KOUNTX = KOUNTX + 1 
      ICOUNT = 1 
      RETURN  
!     MESSAGE FOUND IN TABLE
  100 CONTINUE 
      KOUNT(II) = KOUNT(II) + 1 
      ICOUNT = KOUNT(II) 
      RETURN  
!     EMPTY SLOT FOUND FOR NEW MESSAGE
  110 CONTINUE 
      MESTAB(II) = MES 
      NERTAB(II) = NERR 
      LEVTAB(II) = LEVEL 
      KOUNT(II) = 1 
      ICOUNT = 1 
      RETURN  
      END SUBROUTINE XERSAV 


      SUBROUTINE XGETUA(IUNITA, N) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(OUT) :: N 
      INTEGER , INTENT(OUT) :: IUNITA(5) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER :: I, INDEX 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      INTEGER , EXTERNAL :: J4SAVE 
!-----------------------------------------------
!***BEGIN PROLOGUE  XGETUA
!***DATE WRITTEN   790801   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  R3C
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Returns unit number(s) to which error messages are being
!            sent.
!***DESCRIPTION
!     Abstract
!        XGETUA may be called to determine the unit number or numbers
!        to which error messages are being sent.
!        These unit numbers may have been set by a call to XSETUN,
!        or a call to XSETUA, or may be a default value.
!
!     Description of Parameters
!      --Output--
!        IUNIT - an array of one to five unit numbers, depending
!                on the value of N.  A value of zero refers to the
!                default unit, as defined by the I1MACH machine
!                constant routine.  Only IUNIT(1),...,IUNIT(N) are
!                defined by XGETUA.  The values of IUNIT(N+1),...,
!                IUNIT(5) are not defined (for N .LT. 5) or altered
!                in any way by XGETUA.
!        N     - the number of units to which copies of the
!                error messages are being sent.  N will be in the
!                range from 1 to 5.
!
!     Latest revision ---  19 MAR 1980
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  J4SAVE
!***END PROLOGUE  XGETUA
!***FIRST EXECUTABLE STATEMENT  XGETUA
      N = J4SAVE(5,0,.FALSE.) 
      DO I = 1, N 
         INDEX = I + 4 
         IF (I == 1) INDEX = 3 
         IUNITA(I) = J4SAVE(INDEX,0,.FALSE.) 
      END DO 
      RETURN  
      END SUBROUTINE XGETUA 


      REAL(KIND(0.0D0)) FUNCTION D1MACH (I) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  DOUBLE 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(IN) :: I 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER, DIMENSION(4) :: SMALL, LARGE, RIGHT, DIVER, LOG10 
      REAL(DOUBLE), DIMENSION(5) :: DMACH 
!-----------------------------------------------
!***BEGIN PROLOGUE  D1MACH
!***DATE WRITTEN   750101   (YYMMDD)
!***REVISION DATE  910131   (YYMMDD)
!***CATEGORY NO.  R1
!***KEYWORDS  MACHINE CONSTANTS
!***AUTHOR  FOX, P. A., (BELL LABS)
!           HALL, A. D., (BELL LABS)
!           SCHRYER, N. L., (BELL LABS)
!***PURPOSE  Returns double precision machine dependent constants
!***DESCRIPTION
!
!     This is the CMLIB version of D1MACH, the double precision machine
!     constants subroutine originally developed for the PORT library.
!
!     D1MACH can be used to obtain machine-dependent parameters
!     for the local machine environment.  It is a function
!     subprogram with one (input) argument, and can be called
!     as follows, for example
!
!          D = D1MACH(I)
!
!     where I=1,...,5.  The (output) value of D above is
!     determined by the (input) value of I.  The results for
!     various values of I are discussed below.
!
!  Double-precision machine constants
!  D1MACH( 1) = B**(EMIN-1), the smallest positive magnitude.
!  D1MACH( 2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
!  D1MACH( 3) = B**(-T), the smallest relative spacing.
!  D1MACH( 4) = B**(1-T), the largest relative spacing.
!  D1MACH( 5) = LOG10(B)
!***REFERENCES  FOX P.A., HALL A.D., SCHRYER N.L.,*FRAMEWORK FOR A
!                 PORTABLE LIBRARY*, ACM TRANSACTIONS ON MATHEMATICAL
!                 SOFTWARE, VOL. 4, NO. 2, JUNE 1978, PP. 177-188.
!***ROUTINES CALLED  XERROR
!***END PROLOGUE  D1MACH
!
!
!
      EQUIVALENCE (DMACH(1), SMALL(1)) 
      EQUIVALENCE (DMACH(2), LARGE(1)) 
      EQUIVALENCE (DMACH(3), RIGHT(1)) 
      EQUIVALENCE (DMACH(4), DIVER(1)) 
      EQUIVALENCE (DMACH(5), LOG10(1)) 
      DATA SMALL(1), SMALL(2)/ 1048576, 0/  
      DATA LARGE(1), LARGE(2)/ 2146435071, -1/  
      DATA RIGHT(1), RIGHT(2)/ 1017118720, 0/  
      DATA DIVER(1), DIVER(2)/ 1018167296, 0/  
      DATA LOG10(1), LOG10(2)/ 1070810131, 1352628735/  
!
!     MACHINE CONSTANTS FOR IEEE ARITHMETIC MACHINES AND 8087-BASED
!     MICROS, SUCH AS THE IBM PC AND AT&T 6300, IN WHICH THE LEAST
!     SIGNIFICANT BYTE IS STORED FIRST.
!
! === MACHINE = IEEE.LEAST-SIG-BYTE-FIRST
! === MACHINE = 8087
! === MACHINE = IBM.PC
! === MACHINE = ATT.6300
!      DATA SMALL(1),SMALL(2) /          0,    1048576 /
!      DATA LARGE(1),LARGE(2) /         -1, 2146435071 /
!      DATA RIGHT(1),RIGHT(2) /          0, 1017118720 /
!      DATA DIVER(1),DIVER(2) /          0, 1018167296 /
!      DATA LOG10(1),LOG10(2) / 1352628735, 1070810131 /
!
!     MACHINE CONSTANTS FOR AMDAHL MACHINES.
!
! === MACHINE = AMDAHL
!      DATA SMALL(1),SMALL(2) /    1048576,          0 /
!      DATA LARGE(1),LARGE(2) / 2147483647,         -1 /
!      DATA RIGHT(1),RIGHT(2) /  856686592,          0 /
!      DATA DIVER(1),DIVER(2) /  873463808,          0 /
!      DATA LOG10(1),LOG10(2) / 1091781651, 1352628735 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM.
!
! === MACHINE = BURROUGHS.1700
!      DATA SMALL(1) / ZC00800000 /
!      DATA SMALL(2) / Z000000000 /
!      DATA LARGE(1) / ZDFFFFFFFF /
!      DATA LARGE(2) / ZFFFFFFFFF /
!      DATA RIGHT(1) / ZCC5800000 /
!      DATA RIGHT(2) / Z000000000 /
!      DATA DIVER(1) / ZCC6800000 /
!      DATA DIVER(2) / Z000000000 /
!      DATA LOG10(1) / ZD00E730E7 /
!      DATA LOG10(2) / ZC77800DC0 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 5700 SYSTEM.
!
! === MACHINE = BURROUGHS.5700
!      DATA SMALL(1) / O1771000000000000 /
!      DATA SMALL(2) / O0000000000000000 /
!      DATA LARGE(1) / O0777777777777777 /
!      DATA LARGE(2) / O0007777777777777 /
!      DATA RIGHT(1) / O1461000000000000 /
!      DATA RIGHT(2) / O0000000000000000 /
!      DATA DIVER(1) / O1451000000000000 /
!      DATA DIVER(2) / O0000000000000000 /
!      DATA LOG10(1) / O1157163034761674 /
!      DATA LOG10(2) / O0006677466732724 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 6700/7700 SYSTEMS.
!
! === MACHINE = BURROUGHS.6700
! === MACHINE = BURROUGHS.7700
!      DATA SMALL(1) / O1771000000000000 /
!      DATA SMALL(2) / O7770000000000000 /
!      DATA LARGE(1) / O0777777777777777 /
!      DATA LARGE(2) / O7777777777777777 /
!      DATA RIGHT(1) / O1461000000000000 /
!      DATA RIGHT(2) / O0000000000000000 /
!      DATA DIVER(1) / O1451000000000000 /
!      DATA DIVER(2) / O0000000000000000 /
!      DATA LOG10(1) / O1157163034761674 /
!      DATA LOG10(2) / O0006677466732724 /
!
!     MACHINE CONSTANTS FOR THE CONVEX C-120 (NATIVE MODE)
!     WITH OR WITHOUT -R8 OPTION
!
! === MACHINE = CONVEX.C1
! === MACHINE = CONVEX.C1.R8
!      DATA DMACH(1) / 5.562684646268007D-309 /
!      DATA DMACH(2) / 8.988465674311577D+307 /
!      DATA DMACH(3) / 1.110223024625157D-016 /
!      DATA DMACH(4) / 2.220446049250313D-016 /
!      DATA DMACH(5) / 3.010299956639812D-001 /
!
!     MACHINE CONSTANTS FOR THE CONVEX C-120 (IEEE MODE)
!     WITH OR WITHOUT -R8 OPTION
!
! === MACHINE = CONVEX.C1.IEEE
! === MACHINE = CONVEX.C1.IEEE.R8
!      DATA DMACH(1) / 2.225073858507202D-308 /
!      DATA DMACH(2) / 1.797693134862315D+308 /
!      DATA DMACH(3) / 1.110223024625157D-016 /
!      DATA DMACH(4) / 2.220446049250313D-016 /
!      DATA DMACH(5) / 3.010299956639812D-001 /
!
!     MACHINE CONSTANTS FOR THE CYBER 170/180 SERIES USING NOS (FTN5).
!
! === MACHINE = CYBER.170.NOS
! === MACHINE = CYBER.180.NOS
!      DATA SMALL(1) / O"00604000000000000000" /
!      DATA SMALL(2) / O"00000000000000000000" /
!      DATA LARGE(1) / O"37767777777777777777" /
!      DATA LARGE(2) / O"37167777777777777777" /
!      DATA RIGHT(1) / O"15604000000000000000" /
!      DATA RIGHT(2) / O"15000000000000000000" /
!      DATA DIVER(1) / O"15614000000000000000" /
!      DATA DIVER(2) / O"15010000000000000000" /
!      DATA LOG10(1) / O"17164642023241175717" /
!      DATA LOG10(2) / O"16367571421742254654" /
!
!     MACHINE CONSTANTS FOR THE CDC 180 SERIES USING NOS/VE
!
! === MACHINE = CYBER.180.NOS/VE
!      DATA SMALL(1) / Z"3001800000000000" /
!      DATA SMALL(2) / Z"3001000000000000" /
!      DATA LARGE(1) / Z"4FFEFFFFFFFFFFFE" /
!      DATA LARGE(2) / Z"4FFE000000000000" /
!      DATA RIGHT(1) / Z"3FD2800000000000" /
!      DATA RIGHT(2) / Z"3FD2000000000000" /
!      DATA DIVER(1) / Z"3FD3800000000000" /
!      DATA DIVER(2) / Z"3FD3000000000000" /
!      DATA LOG10(1) / Z"3FFF9A209A84FBCF" /
!      DATA LOG10(2) / Z"3FFFF7988F8959AC" /
!
!     MACHINE CONSTANTS FOR THE CYBER 205
!
! === MACHINE = CYBER.205
!      DATA SMALL(1) / X'9000400000000000' /
!      DATA SMALL(2) / X'8FD1000000000000' /
!      DATA LARGE(1) / X'6FFF7FFFFFFFFFFF' /
!      DATA LARGE(2) / X'6FD07FFFFFFFFFFF' /
!      DATA RIGHT(1) / X'FF74400000000000' /
!      DATA RIGHT(2) / X'FF45000000000000' /
!      DATA DIVER(1) / X'FF75400000000000' /
!      DATA DIVER(2) / X'FF46000000000000' /
!      DATA LOG10(1) / X'FFD04D104D427DE7' /
!      DATA LOG10(2) / X'FFA17DE623E2566A' /
!
!     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES.
!
! === MACHINE = CDC.6000
! === MACHINE = CDC.7000
!      DATA SMALL(1) / 00604000000000000000B /
!      DATA SMALL(2) / 00000000000000000000B /
!      DATA LARGE(1) / 37767777777777777777B /
!      DATA LARGE(2) / 37167777777777777777B /
!      DATA RIGHT(1) / 15604000000000000000B /
!      DATA RIGHT(2) / 15000000000000000000B /
!      DATA DIVER(1) / 15614000000000000000B /
!      DATA DIVER(2) / 15010000000000000000B /
!      DATA LOG10(1) / 17164642023241175717B /
!      DATA LOG10(2) / 16367571421742254654B /
!
!     MACHINE CONSTANTS FOR THE CRAY 1, XMP, 2, AND 3.
!
! === MACHINE = CRAY.46-BIT-INTEGER
! === MACHINE = CRAY.64-BIT-INTEGER
!      DATA SMALL(1) / 201354000000000000000B /
!      DATA SMALL(2) / 000000000000000000000B /
!      DATA LARGE(1) / 577767777777777777777B /
!      DATA LARGE(2) / 000007777777777777776B /
!      DATA RIGHT(1) / 376434000000000000000B /
!      DATA RIGHT(2) / 000000000000000000000B /
!      DATA DIVER(1) / 376444000000000000000B /
!      DATA DIVER(2) / 000000000000000000000B /
!      DATA LOG10(1) / 377774642023241175717B /
!      DATA LOG10(2) / 000007571421742254654B /
!
!     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!
!     NOTE - IT MAY BE APPROPRIATE TO INCLUDE THE FOLLOWING LINE -
!     STATIC DMACH(5)
!
! === MACHINE = DATA_GENERAL.ECLIPSE.S/200
!      DATA SMALL/20K,3*0/,LARGE/77777K,3*177777K/
!      DATA RIGHT/31420K,3*0/,DIVER/32020K,3*0/
!      DATA LOG10/40423K,42023K,50237K,74776K/
!
!     ELXSI 6400
!
! === MACHINE = ELSXI.6400
!      DATA SMALL(1), SMALL(2) / '00100000'X,'00000000'X /
!      DATA LARGE(1), LARGE(2) / '7FEFFFFF'X,'FFFFFFFF'X /
!      DATA RIGHT(1), RIGHT(2) / '3CB00000'X,'00000000'X /
!      DATA DIVER(1), DIVER(2) / '3CC00000'X,'00000000'X /
!      DATA LOG10(1), DIVER(2) / '3FD34413'X,'509F79FF'X /
!
!     MACHINE CONSTANTS FOR THE HARRIS 220
!     MACHINE CONSTANTS FOR THE HARRIS SLASH 6 AND SLASH 7
!
! === MACHINE = HARRIS.220
! === MACHINE = HARRIS.SLASH6
! === MACHINE = HARRIS.SLASH7
!      DATA SMALL(1),SMALL(2) / '20000000, '00000201 /
!      DATA LARGE(1),LARGE(2) / '37777777, '37777577 /
!      DATA RIGHT(1),RIGHT(2) / '20000000, '00000333 /
!      DATA DIVER(1),DIVER(2) / '20000000, '00000334 /
!      DATA LOG10(1),LOG10(2) / '23210115, '10237777 /
!
!     MACHINE CONSTANTS FOR THE HONEYWELL 600/6000 SERIES.
!     MACHINE CONSTANTS FOR THE HONEYWELL DPS 8/70 SERIES.
!
! === MACHINE = HONEYWELL.600/6000
! === MACHINE = HONEYWELL.DPS.8/70
!      DATA SMALL(1),SMALL(2) / O402400000000, O000000000000 /
!      DATA LARGE(1),LARGE(2) / O376777777777, O777777777777 /
!      DATA RIGHT(1),RIGHT(2) / O604400000000, O000000000000 /
!      DATA DIVER(1),DIVER(2) / O606400000000, O000000000000 /
!      DATA LOG10(1),LOG10(2) / O776464202324, O117571775714 /
!
!      MACHINE CONSTANTS FOR THE HP 2100
!      3 WORD DOUBLE PRECISION OPTION WITH FTN4
!
! === MACHINE = HP.2100.3_WORD_DP
!      DATA SMALL(1), SMALL(2), SMALL(3) / 40000B,       0,       1 /
!      DATA LARGE(1), LARGE(2), LARGE(3) / 77777B, 177777B, 177776B /
!      DATA RIGHT(1), RIGHT(2), RIGHT(3) / 40000B,       0,    265B /
!      DATA DIVER(1), DIVER(2), DIVER(3) / 40000B,       0,    276B /
!      DATA LOG10(1), LOG10(2), LOG10(3) / 46420B,  46502B,  77777B /
!
!      MACHINE CONSTANTS FOR THE HP 2100
!      4 WORD DOUBLE PRECISION OPTION WITH FTN4
!
! === MACHINE = HP.2100.4_WORD_DP
!      DATA SMALL(1), SMALL(2) /  40000B,       0 /
!      DATA SMALL(3), SMALL(4) /       0,       1 /
!      DATA LARGE(1), LARGE(2) /  77777B, 177777B /
!      DATA LARGE(3), LARGE(4) / 177777B, 177776B /
!      DATA RIGHT(1), RIGHT(2) /  40000B,       0 /
!      DATA RIGHT(3), RIGHT(4) /       0,    225B /
!      DATA DIVER(1), DIVER(2) /  40000B,       0 /
!      DATA DIVER(3), DIVER(4) /       0,    227B /
!      DATA LOG10(1), LOG10(2) /  46420B,  46502B /
!      DATA LOG10(3), LOG10(4) /  76747B, 176377B /
!
!     HP 9000
!
!      D1MACH(1) = 2.8480954D-306
!      D1MACH(2) = 1.40444776D+306
!      D1MACH(3) = 2.22044605D-16
!      D1MACH(4) = 4.44089210D-16
!      D1MACH(5) = 3.01029996D-1
!
! === MACHINE = HP.9000
!      DATA SMALL(1), SMALL(2) / 00040000000B, 00000000000B /
!      DATA LARGE(1), LARGE(2) / 17737777777B, 37777777777B /
!      DATA RIGHT(1), RIGHT(2) / 07454000000B, 00000000000B /
!      DATA DIVER(1), DIVER(2) / 07460000000B, 00000000000B /
!      DATA LOG10(1), LOG10(2) / 07764642023B, 12047674777B /
!
!     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!     THE XEROX SIGMA 5/7/9, THE SEL SYSTEMS 85/86, AND
!     THE INTERDATA 3230 AND INTERDATA 7/32.
!
! === MACHINE = IBM.360
! === MACHINE = IBM.370
! === MACHINE = XEROX.SIGMA.5
! === MACHINE = XEROX.SIGMA.7
! === MACHINE = XEROX.SIGMA.9
! === MACHINE = SEL.85
! === MACHINE = SEL.86
! === MACHINE = INTERDATA.3230
! === MACHINE = INTERDATA.7/32
!      DATA SMALL(1),SMALL(2) / Z00100000, Z00000000 /
!      DATA LARGE(1),LARGE(2) / Z7FFFFFFF, ZFFFFFFFF /
!      DATA RIGHT(1),RIGHT(2) / Z33100000, Z00000000 /
!      DATA DIVER(1),DIVER(2) / Z34100000, Z00000000 /
!      DATA LOG10(1),LOG10(2) / Z41134413, Z509F79FF /
!
!     MACHINE CONSTANTS FOR THE INTERDATA 8/32
!     WITH THE UNIX SYSTEM FORTRAN 77 COMPILER.
!
!     FOR THE INTERDATA FORTRAN VII COMPILER REPLACE
!     THE Z'S SPECIFYING HEX CONSTANTS WITH Y'S.
!
! === MACHINE = INTERDATA.8/32.UNIX
!      DATA SMALL(1),SMALL(2) / Z'00100000', Z'00000000' /
!      DATA LARGE(1),LARGE(2) / Z'7EFFFFFF', Z'FFFFFFFF' /
!      DATA RIGHT(1),RIGHT(2) / Z'33100000', Z'00000000' /
!      DATA DIVER(1),DIVER(2) / Z'34100000', Z'00000000' /
!      DATA LOG10(1),LOG10(2) / Z'41134413', Z'509F79FF' /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KA PROCESSOR).
!
! === MACHINE = PDP-10.KA
!      DATA SMALL(1),SMALL(2) / "033400000000, "000000000000 /
!      DATA LARGE(1),LARGE(2) / "377777777777, "344777777777 /
!      DATA RIGHT(1),RIGHT(2) / "113400000000, "000000000000 /
!      DATA DIVER(1),DIVER(2) / "114400000000, "000000000000 /
!      DATA LOG10(1),LOG10(2) / "177464202324, "144117571776 /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KI PROCESSOR).
!
! === MACHINE = PDP-10.KI
!      DATA SMALL(1),SMALL(2) / "000400000000, "000000000000 /
!      DATA LARGE(1),LARGE(2) / "377777777777, "377777777777 /
!      DATA RIGHT(1),RIGHT(2) / "103400000000, "000000000000 /
!      DATA DIVER(1),DIVER(2) / "104400000000, "000000000000 /
!      DATA LOG10(1),LOG10(2) / "177464202324, "047674776746 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!     32-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!
! === MACHINE = PDP-11.32-BIT
!      DATA SMALL(1),SMALL(2) /    8388608,           0 /
!      DATA LARGE(1),LARGE(2) / 2147483647,          -1 /
!      DATA RIGHT(1),RIGHT(2) /  612368384,           0 /
!      DATA DIVER(1),DIVER(2) /  620756992,           0 /
!      DATA LOG10(1),LOG10(2) / 1067065498, -2063872008 /
!
!      DATA SMALL(1),SMALL(2) / O00040000000, O00000000000 /
!      DATA LARGE(1),LARGE(2) / O17777777777, O37777777777 /
!      DATA RIGHT(1),RIGHT(2) / O04440000000, O00000000000 /
!      DATA DIVER(1),DIVER(2) / O04500000000, O00000000000 /
!      DATA LOG10(1),LOG10(2) / O07746420232, O20476747770 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
!     16-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!
! === MACHINE = PDP-11.16-BIT
!      DATA SMALL(1),SMALL(2) /    128,      0 /
!      DATA SMALL(3),SMALL(4) /      0,      0 /
!      DATA LARGE(1),LARGE(2) /  32767,     -1 /
!      DATA LARGE(3),LARGE(4) /     -1,     -1 /
!      DATA RIGHT(1),RIGHT(2) /   9344,      0 /
!      DATA RIGHT(3),RIGHT(4) /      0,      0 /
!      DATA DIVER(1),DIVER(2) /   9472,      0 /
!      DATA DIVER(3),DIVER(4) /      0,      0 /
!      DATA LOG10(1),LOG10(2) /  16282,   8346 /
!      DATA LOG10(3),LOG10(4) / -31493, -12296 /
!
!      DATA SMALL(1),SMALL(2) / O000200, O000000 /
!      DATA SMALL(3),SMALL(4) / O000000, O000000 /
!      DATA LARGE(1),LARGE(2) / O077777, O177777 /
!      DATA LARGE(3),LARGE(4) / O177777, O177777 /
!      DATA RIGHT(1),RIGHT(2) / O022200, O000000 /
!      DATA RIGHT(3),RIGHT(4) / O000000, O000000 /
!      DATA DIVER(1),DIVER(2) / O022400, O000000 /
!      DATA DIVER(3),DIVER(4) / O000000, O000000 /
!      DATA LOG10(1),LOG10(2) / O037632, O020232 /
!      DATA LOG10(3),LOG10(4) / O102373, O147770 /
!
!     MACHINE CONSTANTS FOR THE SEQUENT BALANCE 8000
!
! === MACHINE = SEQUENT.BALANCE.8000
!      DATA SMALL(1),SMALL(2) / $00000000,  $00100000 /
!      DATA LARGE(1),LARGE(2) / $FFFFFFFF,  $7FEFFFFF /
!      DATA RIGHT(1),RIGHT(2) / $00000000,  $3CA00000 /
!      DATA DIVER(1),DIVER(2) / $00000000,  $3CB00000 /
!      DATA LOG10(1),LOG10(2) / $509F79FF,  $3FD34413 /
!
!     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES. FTN COMPILER
!
! === MACHINE = UNIVAC.1100
!      DATA SMALL(1),SMALL(2) / O000040000000, O000000000000 /
!      DATA LARGE(1),LARGE(2) / O377777777777, O777777777777 /
!      DATA RIGHT(1),RIGHT(2) / O170540000000, O000000000000 /
!      DATA DIVER(1),DIVER(2) / O170640000000, O000000000000 /
!      DATA LOG10(1),LOG10(2) / O177746420232, O411757177572 /
!
!     MACHINE CONSTANTS FOR VAX 11/780
!     (EXPRESSED IN INTEGER AND HEXADECIMAL)
!    *** THE INTEGER FORMAT SHOULD BE OK FOR UNIX SYSTEMS***
!
! === MACHINE = VAX.11/780
!      DATA SMALL(1), SMALL(2) /        128,           0 /
!      DATA LARGE(1), LARGE(2) /     -32769,          -1 /
!      DATA RIGHT(1), RIGHT(2) /       9344,           0 /
!      DATA DIVER(1), DIVER(2) /       9472,           0 /
!      DATA LOG10(1), LOG10(2) /  546979738,  -805796613 /
!
!    ***THE HEX FORMAT BELOW MAY NOT BE SUITABLE FOR UNIX SYSYEMS***
!      DATA SMALL(1), SMALL(2) / Z00000080, Z00000000 /
!      DATA LARGE(1), LARGE(2) / ZFFFF7FFF, ZFFFFFFFF /
!      DATA RIGHT(1), RIGHT(2) / Z00002480, Z00000000 /
!      DATA DIVER(1), DIVER(2) / Z00002500, Z00000000 /
!      DATA LOG10(1), LOG10(2) / Z209A3F9A, ZCFF884FB /
!
!   MACHINE CONSTANTS FOR VAX 11/780 (G-FLOATING)
!     (EXPRESSED IN INTEGER AND HEXADECIMAL)
!    *** THE INTEGER FORMAT SHOULD BE OK FOR UNIX SYSTEMS***
!
!      DATA SMALL(1), SMALL(2) /         16,           0 /
!      DATA LARGE(1), LARGE(2) /     -32769,          -1 /
!      DATA RIGHT(1), RIGHT(2) /      15552,           0 /
!      DATA DIVER(1), DIVER(2) /      15568,           0 /
!      DATA LOG10(1), LOG10(2) /  1142112243, 2046775455 /
!
!    ***THE HEX FORMAT BELOW MAY NOT BE SUITABLE FOR UNIX SYSYEMS***
!      DATA SMALL(1), SMALL(2) / Z00000010, Z00000000 /
!      DATA LARGE(1), LARGE(2) / ZFFFF7FFF, ZFFFFFFFF /
!      DATA RIGHT(1), RIGHT(2) / Z00003CC0, Z00000000 /
!      DATA DIVER(1), DIVER(2) / Z00003CD0, Z00000000 /
!      DATA LOG10(1), LOG10(2) / Z44133FF3, Z79FF509F /
!
!
!***FIRST EXECUTABLE STATEMENT  D1MACH
      IF (I<1 .OR. I>5) CALL XERROR ('D1MACH -- I OUT OF BOUNDS', 25, 1, 2) 
!
      D1MACH = DMACH(I) 
      RETURN  
!
      END FUNCTION D1MACH 


      REAL(KIND(0.0D0)) FUNCTION DCSEVL (X, A, N) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  DOUBLE 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(IN) :: N 
      REAL(DOUBLE) , INTENT(IN) :: X 
      REAL(DOUBLE) , INTENT(IN) :: A(N) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER :: I, NI 
      REAL(DOUBLE) :: TWOX, B0, B1, B2 
!-----------------------------------------------
!***BEGIN PROLOGUE  DCSEVL
!***DATE WRITTEN   770401   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  C3A2
!***KEYWORDS  CHEBYSHEV,FNLIB,SPECIAL FUNCTION
!***AUTHOR  FULLERTON, W., (LANL)
!***PURPOSE  Evaluate the double precision N-term Chebyshev series A
!            at X.
!***DESCRIPTION
!
! Evaluate the N-term Chebyshev series A at X.  Adapted from
! R. Broucke, Algorithm 446, C.A.C.M., 16, 254 (1973).
! W. Fullerton, C-3, Los Alamos Scientific Laboratory.
!
!       Input Arguments --
! X    double precision value at which the series is to be evaluated.
! A    double precision array of N terms of a Chebyshev series.  In
!      evaluating A, only half of the first coefficient is summed.
! N    number of terms in array A.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  XERROR
!***END PROLOGUE  DCSEVL
!
!***FIRST EXECUTABLE STATEMENT  DCSEVL
      IF (N < 1) CALL XERROR ('DCSEVL  NUMBER OF TERMS LE 0', 28, 2, 2) 
      IF (N > 1000) CALL XERROR ('DCSEVL  NUMBER OF TERMS GT 1000', 31, 3, 2) 
      IF (X<(-1.D0) .OR. X>1.D0) CALL XERROR ('DCSEVL  X OUTSIDE (-1,+1)', 25, &
         1, 1) 
!
      TWOX = 2.0D0*X 
      B1 = 0.D0 
      B0 = 0.D0 
      DO I = 1, N 
         B2 = B1 
         B1 = B0 
         NI = N - I + 1 
         B0 = TWOX*B1 - B2 + A(NI) 
      END DO 
!
      DCSEVL = 0.5D0*(B0 - B2) 
!
      RETURN  
      END FUNCTION DCSEVL 


      SUBROUTINE FDUMP 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!***BEGIN PROLOGUE  FDUMP
!***DATE WRITTEN   790801   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  Z
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Symbolic dump (should be locally written).
!***DESCRIPTION
!        ***Note*** Machine Dependent Routine
!        FDUMP is intended to be replaced by a locally written
!        version which produces a symbolic dump.  Failing this,
!        it should be replaced by a version which prints the
!        subprogram nesting list.  Note that this dump must be
!        printed on each of up to five files, as indicated by the
!        XGETUA routine.  See XSETUA and XGETUA for details.
!
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!     Latest revision ---  23 May 1979
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  FDUMP
!***FIRST EXECUTABLE STATEMENT  FDUMP
      RETURN  
      END SUBROUTINE FDUMP 


      INTEGER FUNCTION I1MACH (I) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(IN) :: I 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER , DIMENSION(16) :: IMACH 
      INTEGER :: OUTPUT 
!-----------------------------------------------
!***BEGIN PROLOGUE  I1MACH
!***DATE WRITTEN   750101   (YYMMDD)
!***REVISION DATE  910131   (YYMMDD)
!***CATEGORY NO.  R1
!***KEYWORDS  MACHINE CONSTANTS
!***AUTHOR  FOX, P. A., (BELL LABS)
!           HALL, A. D., (BELL LABS)
!           SCHRYER, N. L., (BELL LABS)
!***PURPOSE  Returns integer machine dependent constants
!***DESCRIPTION
!
!     This is the CMLIB version of I1MACH, the integer machine
!     constants subroutine originally developed for the PORT library.
!
!     I1MACH can be used to obtain machine-dependent parameters
!     for the local machine environment.  It is a function
!     subroutine with one (input) argument, and can be called
!     as follows, for example
!
!          K = I1MACH(I)
!
!     where I=1,...,16.  The (output) value of K above is
!     determined by the (input) value of I.  The results for
!     various values of I are discussed below.
!
!  I/O unit numbers.
!    I1MACH( 1) = the standard input unit.
!    I1MACH( 2) = the standard output unit.
!    I1MACH( 3) = the standard punch unit.
!    I1MACH( 4) = the standard error message unit.
!
!  Words.
!    I1MACH( 5) = the number of bits per integer storage unit.
!    I1MACH( 6) = the number of characters per integer storage unit.
!
!  Integers.
!    assume integers are represented in the S-digit, base-A form
!
!               sign ( X(S-1)*A**(S-1) + ... + X(1)*A + X(0) )
!
!               where 0 .LE. X(I) .LT. A for I=0,...,S-1.
!    I1MACH( 7) = A, the base.
!    I1MACH( 8) = S, the number of base-A digits.
!    I1MACH( 9) = A**S - 1, the largest magnitude.
!
!  Floating-Point Numbers.
!    Assume floating-point numbers are represented in the T-digit,
!    base-B form
!               sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
!               where 0 .LE. X(I) .LT. B for I=1,...,T,
!               0 .LT. X(1), and EMIN .LE. E .LE. EMAX.
!    I1MACH(10) = B, the base.
!
!  Single-Precision
!    I1MACH(11) = T, the number of base-B digits.
!    I1MACH(12) = EMIN, the smallest exponent E.
!    I1MACH(13) = EMAX, the largest exponent E.
!
!  Double-Precision
!    I1MACH(14) = T, the number of base-B digits.
!    I1MACH(15) = EMIN, the smallest exponent E.
!    I1MACH(16) = EMAX, the largest exponent E.
!
!  To alter this function for a particular environment,
!  the desired set of DATA statements should be activated by
!  removing the C from column 1.  Also, the values of
!  I1MACH(1) - I1MACH(4) should be checked for consistency
!  with the local operating system.
!***REFERENCES  FOX P.A., HALL A.D., SCHRYER N.L.,*FRAMEWORK FOR A
!                 PORTABLE LIBRARY*, ACM TRANSACTIONS ON MATHEMATICAL
!                 SOFTWARE, VOL. 4, NO. 2, JUNE 1978, PP. 177-188.
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  I1MACH
!
      EQUIVALENCE (IMACH(4), OUTPUT) 
      DATA IMACH(1)/ 5/  
      DATA IMACH(2)/ 6/  
      DATA IMACH(3)/ 7/  
      DATA IMACH(4)/ 6/  
      DATA IMACH(5)/ 32/  
      DATA IMACH(6)/ 4/  
      DATA IMACH(7)/ 2/  
      DATA IMACH(8)/ 31/  
      DATA IMACH(9)/ 2147483647/  
      DATA IMACH(10)/ 2/  
      DATA IMACH(11)/ 24/  
      DATA IMACH(12)/ -125/  
      DATA IMACH(13)/ 128/  
      DATA IMACH(14)/ 53/  
      DATA IMACH(15)/ -1021/  
      DATA IMACH(16)/ 1024/  
!
!     MACHINE CONSTANTS FOR AMDAHL MACHINES.
!
! === MACHINE = AMDAHL
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  32 /
!      DATA IMACH( 6) /   4 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /  16 /
!      DATA IMACH(11) /   6 /
!      DATA IMACH(12) / -64 /
!      DATA IMACH(13) /  63 /
!      DATA IMACH(14) /  14 /
!      DATA IMACH(15) / -64 /
!      DATA IMACH(16) /  63 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM.
!
! === MACHINE = BURROUGHS.1700
!      DATA IMACH( 1) /    7 /
!      DATA IMACH( 2) /    2 /
!      DATA IMACH( 3) /    2 /
!      DATA IMACH( 4) /    2 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   33 /
!      DATA IMACH( 9) / Z1FFFFFFFF /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -256 /
!      DATA IMACH(13) /  255 /
!      DATA IMACH(14) /   60 /
!      DATA IMACH(15) / -256 /
!      DATA IMACH(16) /  255 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 5700 SYSTEM.
!
! === MACHINE = BURROUGHS.5700
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  48 /
!      DATA IMACH( 6) /   6 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  39 /
!      DATA IMACH( 9) / O0007777777777777 /
!      DATA IMACH(10) /   8 /
!      DATA IMACH(11) /  13 /
!      DATA IMACH(12) / -50 /
!      DATA IMACH(13) /  76 /
!      DATA IMACH(14) /  26 /
!      DATA IMACH(15) / -50 /
!      DATA IMACH(16) /  76 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 6700/7700 SYSTEMS.
!
! === MACHINE = BURROUGHS.6700
! === MACHINE = BURROUGHS.7700
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  48 /
!      DATA IMACH( 6) /   6 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  39 /
!      DATA IMACH( 9) / O0007777777777777 /
!      DATA IMACH(10) /   8 /
!      DATA IMACH(11) /  13 /
!      DATA IMACH(12) / -50 /
!      DATA IMACH(13) /  76 /
!      DATA IMACH(14) /  26 /
!      DATA IMACH(15) / -32754 /
!      DATA IMACH(16) /  32780 /
!
!     MACHINE CONSTANTS FOR THE CONVEX C-120 (NATIVE MODE)
!
! === MACHINE = CONVEX.C1
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    0 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   53 /
!      DATA IMACH(15) / -1023 /
!      DATA IMACH(16) /  1023 /
!
!     MACHINE CONSTANTS FOR THE CONVEX (NATIVE MODE)
!     WITH -R8 OPTION
!
! === MACHINE = CONVEX.C1.R8
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /     0 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     4 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    53 /
!      DATA IMACH(12) / -1023 /
!      DATA IMACH(13) /  1023 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1023 /
!      DATA IMACH(16) /  1023 /
!
!     MACHINE CONSTANTS FOR THE CONVEX C-120 (IEEE MODE)
!
! === MACHINE = CONVEX.C1.IEEE
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    0 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -125 /
!      DATA IMACH(13) /  128 /
!      DATA IMACH(14) /   53 /
!      DATA IMACH(15) / -1021 /
!      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE CONVEX (IEEE MODE)
!     WITH -R8 OPTION
!
! === MACHINE = CONVEX.C1.IEEE.R8
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /     0 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     4 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    53 /
!      DATA IMACH(12) / -1021 /
!      DATA IMACH(13) /  1024 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1021 /
!      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE CYBER 170/180 SERIES USING NOS (FTN5).
!
! === MACHINE = CYBER.170.NOS
! === MACHINE = CYBER.180.NOS
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   60 /
!      DATA IMACH( 6) /   10 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   48 /
!      DATA IMACH( 9) / O"00007777777777777777" /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   48 /
!      DATA IMACH(12) / -974 /
!      DATA IMACH(13) / 1070 /
!      DATA IMACH(14) /   96 /
!      DATA IMACH(15) / -927 /
!      DATA IMACH(16) / 1070 /
!
!     MACHINE CONSTANTS FOR THE CDC 180 SERIES USING NOS/VE
!
! === MACHINE = CYBER.180.NOS/VE
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /     7 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    64 /
!      DATA IMACH( 6) /     8 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    63 /
!      DATA IMACH( 9) / 9223372036854775807 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    47 /
!      DATA IMACH(12) / -4095 /
!      DATA IMACH(13) /  4094 /
!      DATA IMACH(14) /    94 /
!      DATA IMACH(15) / -4095 /
!      DATA IMACH(16) /  4094 /
!
!     MACHINE CONSTANTS FOR THE CYBER 205
!
! === MACHINE = CYBER.205
!      DATA IMACH( 1) /      5 /
!      DATA IMACH( 2) /      6 /
!      DATA IMACH( 3) /      7 /
!      DATA IMACH( 4) /      6 /
!      DATA IMACH( 5) /     64 /
!      DATA IMACH( 6) /      8 /
!      DATA IMACH( 7) /      2 /
!      DATA IMACH( 8) /     47 /
!      DATA IMACH( 9) / X'00007FFFFFFFFFFF' /
!      DATA IMACH(10) /      2 /
!      DATA IMACH(11) /     47 /
!      DATA IMACH(12) / -28625 /
!      DATA IMACH(13) /  28718 /
!      DATA IMACH(14) /     94 /
!      DATA IMACH(15) / -28625 /
!      DATA IMACH(16) /  28718 /
!
!     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES.
!
! === MACHINE = CDC.6000
! === MACHINE = CDC.7000
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   60 /
!      DATA IMACH( 6) /   10 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   48 /
!      DATA IMACH( 9) / 00007777777777777777B /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   48 /
!      DATA IMACH(12) / -974 /
!      DATA IMACH(13) / 1070 /
!      DATA IMACH(14) /   96 /
!      DATA IMACH(15) / -927 /
!      DATA IMACH(16) / 1070 /
!
!     MACHINE CONSTANTS FOR THE CRAY 1, XMP, 2, AND 3.
!     USING THE 46 BIT INTEGER COMPILER OPTION
!
! === MACHINE = CRAY.46-BIT-INTEGER
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /   102 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    64 /
!      DATA IMACH( 6) /     8 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    46 /
!      DATA IMACH( 9) /  777777777777777777777B /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    47 /
!      DATA IMACH(12) / -8189 /
!      DATA IMACH(13) /  8190 /
!      DATA IMACH(14) /    94 /
!      DATA IMACH(15) / -8099 /
!      DATA IMACH(16) /  8190 /
!
!     MACHINE CONSTANTS FOR THE CRAY 1, XMP, 2, AND 3.
!     USING THE 64 BIT INTEGER COMPILER OPTION
!
! === MACHINE = CRAY.64-BIT-INTEGER
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /   102 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    64 /
!      DATA IMACH( 6) /     8 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    63 /
!      DATA IMACH( 9) /  777777777777777777777B /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    47 /
!      DATA IMACH(12) / -8189 /
!      DATA IMACH(13) /  8190 /
!      DATA IMACH(14) /    94 /
!      DATA IMACH(15) / -8099 /
!      DATA IMACH(16) /  8190 /C
!     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!
! === MACHINE = DATA_GENERAL.ECLIPSE.S/200
!      DATA IMACH( 1) /   11 /
!      DATA IMACH( 2) /   12 /
!      DATA IMACH( 3) /    8 /
!      DATA IMACH( 4) /   10 /
!      DATA IMACH( 5) /   16 /
!      DATA IMACH( 6) /    2 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   15 /
!      DATA IMACH( 9) /32767 /
!      DATA IMACH(10) /   16 /
!      DATA IMACH(11) /    6 /
!      DATA IMACH(12) /  -64 /
!      DATA IMACH(13) /   63 /
!      DATA IMACH(14) /   14 /
!      DATA IMACH(15) /  -64 /
!      DATA IMACH(16) /   63 /
!
!     ELXSI  6400
!
! === MACHINE = ELSXI.6400
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /     6 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     4 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    32 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    24 /
!      DATA IMACH(12) /  -126 /
!      DATA IMACH(13) /   127 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1022 /
!      DATA IMACH(16) /  1023 /
!
!     MACHINE CONSTANTS FOR THE HARRIS 220
!     MACHINE CONSTANTS FOR THE HARRIS SLASH 6 AND SLASH 7
!
! === MACHINE = HARRIS.220
! === MACHINE = HARRIS.SLASH6
! === MACHINE = HARRIS.SLASH7
!      DATA IMACH( 1) /       5 /
!      DATA IMACH( 2) /       6 /
!      DATA IMACH( 3) /       0 /
!      DATA IMACH( 4) /       6 /
!      DATA IMACH( 5) /      24 /
!      DATA IMACH( 6) /       3 /
!      DATA IMACH( 7) /       2 /
!      DATA IMACH( 8) /      23 /
!      DATA IMACH( 9) / 8388607 /
!      DATA IMACH(10) /       2 /
!      DATA IMACH(11) /      23 /
!      DATA IMACH(12) /    -127 /
!      DATA IMACH(13) /     127 /
!      DATA IMACH(14) /      38 /
!      DATA IMACH(15) /    -127 /
!      DATA IMACH(16) /     127 /
!
!     MACHINE CONSTANTS FOR THE HONEYWELL 600/6000 SERIES.
!     MACHINE CONSTANTS FOR THE HONEYWELL DPS 8/70 SERIES.
!
! === MACHINE = HONEYWELL.600/6000
! === MACHINE = HONEYWELL.DPS.8/70
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /   43 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / O377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   63 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR THE HP 2100
!     3 WORD DOUBLE PRECISION OPTION WITH FTN4
!
! === MACHINE = HP.2100.3_WORD_DP
!      DATA IMACH(1) /      5/
!      DATA IMACH(2) /      6 /
!      DATA IMACH(3) /      4 /
!      DATA IMACH(4) /      1 /
!      DATA IMACH(5) /     16 /
!      DATA IMACH(6) /      2 /
!      DATA IMACH(7) /      2 /
!      DATA IMACH(8) /     15 /
!      DATA IMACH(9) /  32767 /
!      DATA IMACH(10)/      2 /
!      DATA IMACH(11)/     23 /
!      DATA IMACH(12)/   -128 /
!      DATA IMACH(13)/    127 /
!      DATA IMACH(14)/     39 /
!      DATA IMACH(15)/   -128 /
!      DATA IMACH(16)/    127 /
!
!     MACHINE CONSTANTS FOR THE HP 2100
!     4 WORD DOUBLE PRECISION OPTION WITH FTN4
!
! === MACHINE = HP.2100.4_WORD_DP
!      DATA IMACH(1) /      5 /
!      DATA IMACH(2) /      6 /
!      DATA IMACH(3) /      4 /
!      DATA IMACH(4) /      1 /
!      DATA IMACH(5) /     16 /
!      DATA IMACH(6) /      2 /
!      DATA IMACH(7) /      2 /
!      DATA IMACH(8) /     15 /
!      DATA IMACH(9) /  32767 /
!      DATA IMACH(10)/      2 /
!      DATA IMACH(11)/     23 /
!      DATA IMACH(12)/   -128 /
!      DATA IMACH(13)/    127 /
!      DATA IMACH(14)/     55 /
!      DATA IMACH(15)/   -128 /
!      DATA IMACH(16)/    127 /
!
!     HP 9000
!
! === MACHINE = HP.9000
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /     6 /
!      DATA IMACH( 4) /     7 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     4 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    32 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    24 /
!      DATA IMACH(12) /  -126 /
!      DATA IMACH(13) /   127 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1015 /
!      DATA IMACH(16) /  1017 /
!
!     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!     THE XEROX SIGMA 5/7/9 AND THE SEL SYSTEMS 85/86 AND
!     THE INTERDATA 3230 AND INTERDATA 7/32.
!
! === MACHINE = IBM.360
! === MACHINE = IBM.370
! === MACHINE = XEROX.SIGMA.5
! === MACHINE = XEROX.SIGMA.7
! === MACHINE = XEROX.SIGMA.9
! === MACHINE = SEL.85
! === MACHINE = SEL.86
! === MACHINE = INTERDATA.3230
! === MACHINE = INTERDATA.7/32
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  32 /
!      DATA IMACH( 6) /   4 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  31 /
!      DATA IMACH( 9) / Z7FFFFFFF /
!      DATA IMACH(10) /  16 /
!      DATA IMACH(11) /   6 /
!      DATA IMACH(12) / -64 /
!      DATA IMACH(13) /  63 /
!      DATA IMACH(14) /  14 /
!      DATA IMACH(15) / -64 /
!      DATA IMACH(16) /  63 /
!
!     MACHINE CONSTANTS FOR THE INTERDATA 8/32
!     WITH THE UNIX SYSTEM FORTRAN 77 COMPILER.
!
!     FOR THE INTERDATA FORTRAN VII COMPILER REPLACE
!     THE Z'S SPECIFYING HEX CONSTANTS WITH Y'S.
!
! === MACHINE = INTERDATA.8/32.UNIX
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   6 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  32 /
!      DATA IMACH( 6) /   4 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  31 /
!      DATA IMACH( 9) / Z'7FFFFFFF' /
!      DATA IMACH(10) /  16 /
!      DATA IMACH(11) /   6 /
!      DATA IMACH(12) / -64 /
!      DATA IMACH(13) /  62 /
!      DATA IMACH(14) /  14 /
!      DATA IMACH(15) / -64 /
!      DATA IMACH(16) /  62 /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KA PROCESSOR).
!
! === MACHINE = PDP-10.KA
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    5 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / "377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   54 /
!      DATA IMACH(15) / -101 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KI PROCESSOR).
!
! === MACHINE = PDP-10.KI
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    5 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / "377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   62 /
!      DATA IMACH(15) / -128 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
!     32-BIT INTEGER ARITHMETIC.
!
! === MACHINE = PDP-11.32-BIT
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   56 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
!     16-BIT INTEGER ARITHMETIC.
!
! === MACHINE = PDP-11.16-BIT
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   16 /
!      DATA IMACH( 6) /    2 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   15 /
!      DATA IMACH( 9) / 32767 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   56 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR THE SEQUENT BALANCE 8000.
!
! === MACHINE = SEQUENT.BALANCE.8000
!      DATA IMACH( 1) /     0 /
!      DATA IMACH( 2) /     0 /
!      DATA IMACH( 3) /     7 /
!      DATA IMACH( 4) /     0 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     1 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    31 /
!      DATA IMACH( 9) /  2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    24 /
!      DATA IMACH(12) /  -125 /
!      DATA IMACH(13) /   128 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1021 /
!      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES. FTN COMPILER
!
! === MACHINE = UNIVAC.1100
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    1 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / O377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   60 /
!      DATA IMACH(15) /-1024 /
!      DATA IMACH(16) / 1023 /
!
!     MACHINE CONSTANTS FOR THE VAX 11/780
!
! === MACHINE = VAX.11/780
!      DATA IMACH(1) /    5 /
!      DATA IMACH(2) /    6 /
!      DATA IMACH(3) /    5 /
!      DATA IMACH(4) /    6 /
!      DATA IMACH(5) /   32 /
!      DATA IMACH(6) /    4 /
!      DATA IMACH(7) /    2 /
!      DATA IMACH(8) /   31 /
!      DATA IMACH(9) /2147483647 /
!      DATA IMACH(10)/    2 /
!      DATA IMACH(11)/   24 /
!      DATA IMACH(12)/ -127 /
!      DATA IMACH(13)/  127 /
!      DATA IMACH(14)/   56 /
!      DATA IMACH(15)/ -127 /
!      DATA IMACH(16)/  127 /
!
!
!***FIRST EXECUTABLE STATEMENT  I1MACH
      IF (I<1 .OR. I>16) CALL XERROR ('I1MACH -- I OUT OF BOUNDS', 25, 1, 2) 
!
      I1MACH = IMACH(I) 
      RETURN  
!
      END FUNCTION I1MACH 


      INTEGER FUNCTION J4SAVE (IWHICH, IVALUE, ISET) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER , INTENT(IN) :: IWHICH 
      INTEGER , INTENT(IN) :: IVALUE 
      LOGICAL , INTENT(IN) :: ISET 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER, DIMENSION(9) :: IPARAM 

      SAVE IPARAM 
!-----------------------------------------------
!***BEGIN PROLOGUE  J4SAVE
!***REFER TO  XERROR
!     Abstract
!        J4SAVE saves and recalls several global variables needed
!        by the library error handling routines.
!
!     Description of Parameters
!      --Input--
!        IWHICH - Index of item desired.
!                = 1 Refers to current error number.
!                = 2 Refers to current error control flag.
!                 = 3 Refers to current unit number to which error
!                    messages are to be sent.  (0 means use standard.)
!                 = 4 Refers to the maximum number of times any
!                     message is to be printed (as set by XERMAX).
!                 = 5 Refers to the total number of units to which
!                     each error message is to be written.
!                 = 6 Refers to the 2nd unit for error messages
!                 = 7 Refers to the 3rd unit for error messages
!                 = 8 Refers to the 4th unit for error messages
!                 = 9 Refers to the 5th unit for error messages
!        IVALUE - The value to be set for the IWHICH-th parameter,
!                 if ISET is .TRUE. .
!        ISET   - If ISET=.TRUE., the IWHICH-th parameter will BE
!                 given the value, IVALUE.  If ISET=.FALSE., the
!                 IWHICH-th parameter will be unchanged, and IVALUE
!                 is a dummy parameter.
!      --Output--
!        The (old) value of the IWHICH-th parameter will be returned
!        in the function value, J4SAVE.
!
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!    Adapted from Bell Laboratories PORT Library Error Handler
!     Latest revision ---  23 MAY 1979
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  J4SAVE
      DATA IPARAM(1), IPARAM(2), IPARAM(3), IPARAM(4)/ 0, 2, 0, 10/  
      DATA IPARAM(5)/ 1/  
      DATA IPARAM(6), IPARAM(7), IPARAM(8), IPARAM(9)/ 0, 0, 0, 0/  
!***FIRST EXECUTABLE STATEMENT  J4SAVE
      J4SAVE = IPARAM(IWHICH) 
      IF (ISET) IPARAM(IWHICH) = IVALUE 
      RETURN  
      END FUNCTION J4SAVE 


      SUBROUTINE XERABT(MESSG, NMESSG) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER  :: NMESSG 
      CHARACTER  :: MESSG*(*) 
!-----------------------------------------------
!***BEGIN PROLOGUE  XERABT
!***DATE WRITTEN   790801   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  R3C
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Aborts program execution and prints error message.
!***DESCRIPTION
!     Abstract
!        ***Note*** machine dependent routine
!        XERABT aborts the execution of the program.
!        The error message causing the abort is given in the calling
!        sequence, in case one needs it for printing on a dayfile,
!        for example.
!
!     Description of Parameters
!        MESSG and NMESSG are as in XERROR, except that NMESSG may
!        be zero, in which case no message is being supplied.
!
!     Written by Ron Jones, with SLATEC Common Math Library Subcommittee
!     Latest revision ---  19 MAR 1980
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  XERABT
!***FIRST EXECUTABLE STATEMENT  XERABT
      STOP  
      END SUBROUTINE XERABT 


      SUBROUTINE XERCTL(MESSG1, NMESSG, NERR, LEVEL, KONTRL) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER  :: NMESSG 
      INTEGER  :: NERR 
      INTEGER  :: LEVEL 
      INTEGER  :: KONTRL 
      CHARACTER  :: MESSG1*20 
!-----------------------------------------------
!***BEGIN PROLOGUE  XERCTL
!***DATE WRITTEN   790801   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  R3C
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Allows user control over handling of individual errors.
!***DESCRIPTION
!     Abstract
!        Allows user control over handling of individual errors.
!        Just after each message is recorded, but before it is
!        processed any further (i.e., before it is printed or
!        a decision to abort is made), a call is made to XERCTL.
!        If the user has provided his own version of XERCTL, he
!        can then override the value of KONTROL used in processing
!        this message by redefining its value.
!        KONTRL may be set to any value from -2 to 2.
!        The meanings for KONTRL are the same as in XSETF, except
!        that the value of KONTRL changes only for this message.
!        If KONTRL is set to a value outside the range from -2 to 2,
!        it will be moved back into that range.
!
!     Description of Parameters
!
!      --Input--
!        MESSG1 - the first word (only) of the error message.
!        NMESSG - same as in the call to XERROR or XERRWV.
!        NERR   - same as in the call to XERROR or XERRWV.
!        LEVEL  - same as in the call to XERROR or XERRWV.
!        KONTRL - the current value of the control flag as set
!                 by a call to XSETF.
!
!      --Output--
!        KONTRL - the new value of KONTRL.  If KONTRL is not
!                 defined, it will remain at its original value.
!                 This changed value of control affects only
!                 the current occurrence of the current message.
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  XERCTL
!***FIRST EXECUTABLE STATEMENT  XERCTL
      RETURN  
      END SUBROUTINE XERCTL 


      SUBROUTINE XERPRT(MESSG, NMESSG) 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      IMPLICIT NONE
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      INTEGER  :: NMESSG 
      CHARACTER , INTENT(IN) :: MESSG*(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      INTEGER , DIMENSION(5) :: LUN 
      INTEGER :: NUNIT, LENMES, KUNIT, IUNIT, ICHAR, LAST 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      INTEGER , EXTERNAL :: I1MACH 
!-----------------------------------------------
!***BEGIN PROLOGUE  XERPRT
!***DATE WRITTEN   790801   (YYMMDD)
!***REVISION DATE  820801   (YYMMDD)
!***CATEGORY NO.  Z
!***KEYWORDS  ERROR,XERROR PACKAGE
!***AUTHOR  JONES, R. E., (SNLA)
!***PURPOSE  Prints error messages.
!***DESCRIPTION
!     Abstract
!        Print the Hollerith message in MESSG, of length NMESSG,
!        on each file indicated by XGETUA.
!     Latest revision ---  19 MAR 1980
!***REFERENCES  JONES R.E., KAHANER D.K., "XERROR, THE SLATEC ERROR-
!                 HANDLING PACKAGE", SAND82-0800, SANDIA LABORATORIES,
!                 1982.
!***ROUTINES CALLED  I1MACH,S88FMT,XGETUA
!***END PROLOGUE  XERPRT
!     OBTAIN UNIT NUMBERS AND WRITE LINE TO EACH UNIT
!***FIRST EXECUTABLE STATEMENT  XERPRT
      CALL XGETUA (LUN, NUNIT) 
      LENMES = LEN(MESSG) 
      DO KUNIT = 1, NUNIT 
         IUNIT = LUN(KUNIT) 
         IF (IUNIT == 0) IUNIT = I1MACH(4) 
         DO ICHAR = 1, LENMES, 72 
            LAST = MIN0(ICHAR + 71,LENMES) 
            WRITE (IUNIT, '(1X,A)') MESSG(ICHAR:LAST) 
         END DO 
      END DO 
      RETURN  
      END SUBROUTINE XERPRT 
