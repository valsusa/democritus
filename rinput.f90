 
      subroutine rinput 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
      use emission_com_M
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: inc, nscale, itpmax, npmax, nnn 
      real(double) :: a0, peps, xi, b0, asq, ron, roi, roin, siein, pap, rho0, &
         donfac, epsuv, usmth, ufrctn, dtf, orthog, xicof, dpcof 
!-----------------------------------------------
!
!
!
!
! +++
! +++ read data deck, compute derived and scalar quantities
! +++
 
      namelist /picin/problem_type, &
         nx, ny, imp, inc, lpr, wb, wl, wr, wt, dx, dy, cyl, dt, &
         dtmax, tlimd, twfilm, twprtr, twfin, om, peps, eps, lambda, mu, xi, gx&
         , gy, a0, b0, asq, ron, gm1, consrv, stabl, roi, siei, uin, vin, roin&
         , siein, pap, grid, in1, in2, in3, in4, in5, in6, iout, onw1, onw2, &
         onw3, eqvol, ntimes, nscale, itpmax, itmax, tau, scal, nummax, rho0, &
         periodic, timing, donfac, ncon, epsuv, usmth, ufrctn, conductive, &
         boris, leonardo, omfield, phibdy, ninner, coll, rmaj, neutral, tinyi, firstime, &
         modey, amptud, symmetry, epsp, dtf, t, coef1, coef2, cntr, maxset, &
         twmovie, hybrid, debug, movie, plots, gravity, mix, fluid, estatic, &
         nsmth, smth, exconst, eyconst, hot, reflctr, reflctl, reflctb, reflctt&
         , ug0, vg0, twhist, qom, bx0, by0, bz0, diem, chimin, friction, &
         frdriftx, frdrifty, nadaptfr, collision, phiring, conductor
         
      namelist /emission/siep_emission, n0_emission, maxwellian,emitted_species
         
      namelist /reg/nrg, nsp_dust, nsp_dust2, anpr, rhr, vinf, rhinf, einf, zvi, rvi, nvg&
         , velinj, fluxinj, uinf, iprd, inj, isqin, npmax, icoinf, icoplt, uvi&
         , vvi, siep, icoi, rix, riy, rex, rey, xcenter, ycenter, thetadim,&
         siepr, siepl, siepb, siept, tscal, xorigin, yorigin, riinj, reinj,&
         romin, romax, npcel,p_inject,nsubmax
 
      namelist /obstacl/nobs, rvo, zvo, icofix 
      namelist /thistry/history, iregp, xpinit, ypinit, nstat, xwindo, ywindo 
      namelist /controllo/lcoal, lsplit, iesp, offset, ndistr, ycut, elle, &
         ntempo, nparticle, ntresh, vtresh, sor, geometric_object

      write (*, *) ' rinput:  setting defaults' 
!     set default values of the logical variables
!
      problem_type=1
      debug = .FALSE. 
      movie = .FALSE. 
      plots = .FALSE. 
      hybrid = .FALSE. 
      timing = .FALSE. 
      history = .FALSE. 
      periodic = .FALSE. 
	  boris = .TRUE.
	  leonardo = .TRUE.
      hot = .FALSE. 
      reflctr = .TRUE. 
      reflctl = .TRUE. 
      reflctb = .TRUE. 
      reflctt = .TRUE. 
      
      ycut=1d0
      sor=1.d0
      geometric_object=.true.
      
      exconst = 0.0 
      eyconst = 0.0 
      coll = 0.0 
      imp = 0 
      inc = 0 
      lpr = 0 
      tlimd = 0. 
      twprtr = 1.E+30 
      tau = 1. 
      scal = 1. 
      tiny = 1.E-20 
      om = 1.0 
      peps = 1.E-04 
      eps = 1.E-04 
      cntr = 0.5 
      ntimes = 1 
      lambda = 0. 
      mu = 0. 
      xi = 1.0 
      gx = 0. 
      gy = 0. 
      a0 = 1.0 
      b0 = 0.0 
      asq = 0. 
      ron = 0. 
      gm1 = 0. 
      siei = 0. 
      uin = 0. 
      vin = 0. 
      roin = 0. 
      siein = 0. 
      pap = 0. 
      roi = 0. 
      in1 = 0 
      in2 = 0 
      in3 = 0 
      in4 = 0 
      in5 = 0 
      in6 = 0 
      dtf = 0.2 
      r36 = 1./36. 
      scale = 0.75 
      orthog = 0. 
      eqvol = 1.0 
      donfac = 1000. 
      ncon = 20 
      epsuv = 1.E-12 
      t = 0. 
      grind = 0. 
      dtmin = 0. 
      dtpdv = 1.E20 
      ncyc = 0 
      numit = 0 
      ipres = 0
!(Leo) Default values for the angular slicing of the probe,
!  and the maximum subcycling
      thetadim=16
      nsubmax=10
 

      read (5, nml=picin) 
      write (6, nml=picin) 
      write (12, nml=picin) 
 
      omcyl = 1. - cyl 
 
      third = 1./3. 
      twlfth = 0.25*third 
      xicof = 0.5*(1. + xi) 
      dpcof = peps*roi/(2.*(1./(dx*dx) + 1./(dy*dy))) 
      grfac = 1000./float(nx*ny) 
      tfilm = twfilm 
      thist = twhist 
      tmovie = twmovie 
      tprtr = twprtr 
      nyp = ny + 1 
      nxp = nx + 1 
      nxm = nx - 1 
      nym = ny - 1 
 
      iprd = 0 
      inj(1) = 0 
      inj(2) = 0 
      isqin(1) = 2 
      isqin(2) = 2 
 
      write (*, *) 'reading reg ' 
      read (5, nml=reg) 
      write (*, *) 'reading emission ' 
      read (5, nml=emission) 
      write (*, *) ' reading obstacl ' 
      read (5, nml=obstacl) 
      write (*, *) ' reading thistry ' 
      read (5, nml=thistry) 
!      viscosty=sqrt((dx**2+dy**2)
!     &     *(uinf**2+vinf**2))
      write (*, *) ' reading controllo' 
      read (5, nml=controllo) 
      write (6, nml=reg)       
      write (6, nml=emission)
      write (6, nml=obstacl) 
      write (6, nml=thistry) 
      write (6, nml=controllo) 
      write (12, nml=reg) 
      nnn = 2*(nx + ny - 2) + 1 
      if (nbdy < nnn) then 
         write (59, 260) nnn, nbdy 
  260    format(" too little storage for boundary pts.",i5," required",i5,&
            " allocated") 
         stop  
      endif 
 
      if (a0/=1.0 .or. b0/=0.0) then 
         if (wl==4 .or. wl==5 .and. uin<0.) write (59, 240) 
         if (wb==4 .or. wb==5 .and. vin<0.) write (59, 240) 
         if (wr==4 .or. wr==5 .and. uin>0.) write (59, 240) 
         if (wt==4 .or. wt==5 .and. vin>0.) write (59, 240) 
      endif 
      
      nsp_dust2=max(nsp_dust2,nsp_dust)
      
      return  
  210 format(a8,i5) 
  220 format(a8,f10.5) 
  230 format(a8,1p,e10.3) 
  240 format(' warning - outflow boundary but not full donor cell.'/,&
         ' are outside density and energy specified?') 
  250 format(12i5) 
      return  
      end subroutine rinput 
