      subroutine global(lun) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: lun 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nh, ntotl, is, j, i, ij, ipj, ipjp, ijp, np, lv, l, imj, imjm&
         , ijm 
      real(double), dimension(nsp) :: xcentre, ycentre, voldust, massdust, &
         ndust 
      real(double) :: totq, totjx, totjy, varvx, varvy, dnorm, vexact, edotb, &
         circe, totdive, totchg, totdivel, totdiver, rld3, source
!-----------------------------------------------
!
!     local variables
!
!
      nh = mod(ncyc,nhst) 
      pi = acos(-1.D0) 
!     +++
!     +++ compute total mass, momentum, and energy of the system
!     +++
      totq = 0. 
      totmass = 0.0 
      totjx = 0. 
      totjy = 0. 
      xmom(nh) = 0.0 
      ymom(nh) = 0.0 
      qtotal(nh) = 0.0 
      ntotal(nh) = 0.0 
      ntotl = 0 
      varvx = 0.0 
      varvy = 0. 
 
      do is = 1, nsp 
         totk(is) = 0.0 
         qdust(nh,is) = 0. 
         if (conductor(is)) then
         phidust(nh,is) = 0d0 !1d10
         else         
         phidust(nh,is) = 0. 
         end if
         emomdustx(nh,is) = 0. 
         emomdusty(nh,is) = 0. 
		 forcespx(nh,is) = 0. 
         forcespy(nh,is) = 0. 
      end do 
 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
            d(ij) = 0.25*(x(ipj)+x(ipjp)+x(ijp)+x(ij)) 
            e(ij) = 0.25*(y(ipj)+y(ipjp)+y(ijp)+y(ij)) 
         end do 
      end do 
!
!     get the centre!
!
      do is = nsp_dust, nsp 
         xcentre(is) = 0. 
         ycentre(is) = 0. 
         massdust(is) = 0. 
         ndust(is) = 0. 
         voldust(is) = 0. 
         do j = 2, ny 
            do i = 2, nx 
               ij = (j - 1)*nxp + i 
               if (number(ij,is) <= 0.5) cycle  
               voldust(is) = voldust(is) + vol(ij) 
               massdust(is) = massdust(is) + vol(ij)*number(ij,is) 
               ndust(is) = ndust(is) + number(ij,is) 
               xcentre(is) = xcentre(is) + d(ij)*vol(ij) 
               ycentre(is) = ycentre(is) + e(ij)*vol(ij) 
            end do 
         end do 
         xcentre(is) = xcentre(is)/(voldust(is)+1.E-10) 
         ycentre(is) = ycentre(is)/(voldust(is)+1.E-10) 
      end do 
!
      do j = 2, ny+1 
         do i = 2, nx+1 
            ij = (j - 1)*nxp + i 

 			   forcespx(nh,1:nsp) = forcespx(nh,1:nsp) + ex(ij)*qvs(ij,1:nsp) *vvol(ij)
               forcespy(nh,1:nsp) = forcespy(nh,1:nsp) + ey(ij)*qvs(ij,1:nsp) *vvol(ij)
			   qdust(nh,1:nsp) = qdust(nh,1:nsp) + qvs(ij,1:nsp) *vvol(ij) 
			   
         end do 
      end do 

 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
 
            do is = nsp_dust, nsp 
               if (number(ij,is) <= 0.5) cycle  
               !qdust(nh,is) = qdust(nh,is) + qnet0(ij)*vol(ij) 
               emomdustx(nh,is) = emomdustx(nh,is) + qnet0(ij)*vol(ij)*(d(ij)-&
                  xcentre(is)) 
               emomdusty(nh,is) = emomdusty(nh,is) + qnet0(ij)*vol(ij)*(e(ij)-&
                  ycentre(is)) 
               !if(conductor(is)) then
               !phidust(nh,is) = min(phidust(nh,is), abs(phiavg(ij)))
!               write(*,*)'conduct',phiavg(ij), phidust(nh,is)
               !else  
               phidust(nh,is) = phidust(nh,is) + phiavg(ij)*vol(ij)  
               !end if
            end do 
 
            np = iphead(ij) 
            if (np <= 0) cycle  
   10       continue 
            call listmkr (np, nploc, link, lvmax, ntmp) 
            do lv = 1, lvmax 
               l = nploc(lv) 
               totq = totq + qpar(l) 
               totmass = totmass + qpar(l)/qom(ico(l)) 
               qtotal(nh) = qtotal(nh) + qpar(l) 
               ntotal(nh) = ntotal(nh) + 1 
               totjx = totjx + qpar(l)*up(l) 
			   if(ico(l)<nsp_dust) then
               xmom(nh) = xmom(nh) + qpar(l)*up(l)/qom(ico(l)) 
               ymom(nh) = ymom(nh) + qpar(l)*vp(l)/qom(ico(l)) 
			   end if
               totk(ico(l)) = totk(ico(l)) + 0.5*qpar(l)*(up(l)**2+vp(l)**2+wp(&
                  l)**2)/qom(ico(l)) 
!
               varvx = varvx + up(l)**2 
               varvy = varvy + vp(l)**2 
               ntotl = ntotl + 1 
!
            end do 
!
            np = link(nploc(lvmax)) 
            if (np > 0) go to 10 
         end do 
      end do 
!
 do is=nsp_dust,nsp
 if (conductor(is))  then
   source=qdust(nh,is)
   phidust(nh,is) = phidust(nh,is)/voldust(is) 
   
   !phidust(nh,is)=.5d0*(phidust(nh,is)-maxval(phiavg)+minval(phiavg))
    
 !  phidust(nh,is)=source/rex(is)/(2d0*two_pi*epsilon_0)
 !  write(*,*)qdust(nh,is),phidust(nh,is)
 end if  
 enddo
 !   
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            do is = nsp_dust, nsp 
!               if (number(ij,is) >0.5.and.conductor(is)) then
               if (number(ij,is) >0.5) then
               phiavg(ij)=phidust(nh,is)
!               write(*,*)'shaving', phidust(nh,is)
               end if
            enddo
         enddo
      enddo      
 
      dnorm = siep(2)**2*(1. + cyl*(siep(2)-1.)) 
	           rld3 = (siep(2)/siep(1)*sqrt(qom(1)))**3 
      do is = nsp_dust, nsp 
         vexact = rex(is)**3/3. 
!        write(*,*)'dust volume correction',(voldust(is)+1.e-10)/vexact
         qtotal(nh) = qtotal(nh)/dnorm*2*pi 
!      *vexact/(voldust(is)+1.e-10)
	     forcespx(nh,is)=forcespx(nh,is)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdust(nh,is)+1d-10)
	     forcespy(nh,is)=forcespy(nh,is)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdust(nh,is)+1d-10)

		 xmom(nh)=xmom(nh)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdust(nh,is)+1d-10)
         ymom(nh)=ymom(nh)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdust(nh,is)+1d-10)


         qdust(nh,is) = qdust(nh,is)/dnorm*2*pi 
!       *vexact/(voldust(is)+1.e-10)
         if (conductor(is)) then
         phidust(nh,is) = phidust(nh,is)/(siep(2)**2+1.E-10) 
         else
         phidust(nh,is) = phidust(nh,is)/(siep(2)**2*voldust(is)+1.E-10) 
         end if 
         emomdustx(nh,is) = emomdustx(nh,is)/(dnorm*siep(2)) 
         emomdusty(nh,is) = emomdusty(nh,is)/(dnorm*siep(2)) 
!		 forcespx(nh,is)=forcespx(nh,is)*qom(1)**2/siep(1)**4/(qdust(nh,is)*rld3&
!                 +1.E-10)**2 
!		 forcespy(nh,is)=forcespy(nh,is)*qom(1)**2/siep(1)**4/(qdust(nh,is)*rld3&
!                 +1.E-10)**2 
		 
      end do 
 
      if (mod(ncyc,10) == 0) then 
         write (*, *) 'TIME=', t,'   Number of particles',ntotl 
         write (*, *) 'qtotal=', qtotal(nh) 
         do is = nsp_dust, nsp 
            write (*, *) 'SPECIES=', is 
            write (*, *) 'xcentre', xcentre(is), xcenter(is) 
            write (*, *) 'ycentre', ycentre(is), ycenter(is) 
            write (*, *) 'CHARGE,POT,DIP,voldust', qdust(nh,is), phidust(nh,is), &
               emomdustx(nh,is), emomdusty(nh,is),voldust(is)
            write (50 + is, 133) t, qdust(nh,is), phidust(nh,is), emomdustx(nh,&
               is), emomdusty(nh,is) 
  133       format(1x,5(e12.5,2x)) 
 
         end do 
      endif 
 
      if (ntotl == 0) stop  
!
      do j = 1, nyp 
         do i = 1, nxp 
!
            ij = (j - 1)*nxp + i 
!
            edotb = ex(ij)*bx(ij) + ey(ij)*by(ij) 
!
            soln(ij) = (1. + dielperp(ij))*ex(ij) + dieltrns(ij)*ey(ij)*bz(ij)&
                + dielpar(ij)*edotb*bx(ij) 
!
            srce(ij) = (1. + dielperp(ij))*ey(ij) - dieltrns(ij)*ex(ij)*bz(ij)&
                + dielpar(ij)*edotb*by(ij) 
!
         end do 
      end do 
!
      do j = 2, nyp 
         do i = 2, nxp 
!
            ij = (j - 1)*nxp + i 
            imj = ij - 1 
            imjm = ij - nxp - 1 
            ijm = ij - nxp 
!
            a(ij) = cx4(ij) + cr4(ij) + cx1(imj) + cr1(imj) + cx2(imjm) + cr2(&
               imjm) + cx3(ijm) + cr3(ijm) 
!
            b(ij) = cy4(ij) + cy1(imj) + cy2(imjm) + cy3(ijm) 
!
         end do 
      end do 
!
      variance(nh) = sqrt((varvx/dx**2 + varvy/dy**2)*dt**2)/ntotl 
!     compare edotj w/ change in kinetic energy
      call deriv (1, nxp, 1, nxp, 1, nyp, cx1, cx2, cx3, cx4, cr1, cr2, cr3, &
         cr4, cy1, cy2, cy3, cy4, soln, srce, dive, curle) 
!
      edotj(nh) = 0. 
      delefld(nh) = 0.0 
      circe = 0.0 
      totdive = 0.0 
      totchg = 0.0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
!
!     calculate current at half-time level, as it is calculated
!     in POISSON
!
            circe = circe + curle(ij) 
            totdive = totdive + dive(ij) 
            totchg = totchg + qtilde(ij) 
!
!
!                                                        calculate chang
!
         end do 
      end do 
      totdivel = 0.0 
      do j = 2, ny 
         ij = (j - 1)*nxp + 1 
         totdivel = totdivel + dive(ij) 
      end do 
!
      totdiver = 0.0 
      do j = 2, ny 
         ij = j*nxp 
         totdiver = totdiver + dive(ij) 
      end do 
!
!
      efld = 0.0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
!
            efld = efld + 0.25*(ex(ipj)**2+ey(ipj)**2+ex(ipjp)**2+ey(ipjp)**2+&
               ex(ijp)**2+ey(ijp)**2+ex(ij)**2+ey(ij)**2)*vol(ij) 
!
            jx1 = jxtil(ipj) + 0.5*opsq(ipj)*ex(ipj)*dt 
            jy1 = jytil(ipj) + 0.5*opsq(ipj)*ey(ipj)*dt 
!
            jx2 = jxtil(ipjp) + 0.5*opsq(ipjp)*ex(ipjp)*dt 
            jy2 = jytil(ipjp) + 0.5*opsq(ipjp)*ey(ipjp)*dt 
!
            jx3 = jxtil(ijp) + 0.5*opsq(ijp)*ex(ijp)*dt 
            jy3 = jytil(ijp) + 0.5*opsq(ijp)*ey(ijp)*dt 
!
            jx4 = jxtil(ij) + 0.5*opsq(ij)*ex(ij)*dt 
            jy4 = jytil(ij) + 0.5*opsq(ij)*ey(ij)*dt 
!
            edotj(nh) = edotj(nh) + 0.25*(jx1*ex(ipj)+jy1*ey(ipj)+jx2*ex(ipjp)+&
               jy2*ey(ipjp)+jx3*ex(ijp)+jy3*ey(ijp)+jx4*ex(ij)+jy4*ey(ij))*vol(&
               ij)*dt 
!
         end do 
      end do 
!
      efld = 0.5*efld 
!
!     store history data
!
      ta(nh) = t 
      fldnrg(nh) = efld 
      if (nh > 1) delefld(nh) = fldnrg(nh) - fldnrg(nh-1) 
!
      do is = 1, nsp 
         parnrg((is-1)*nhst+nh) = totk(is) 
      end do 
!
      return  
   50 format(' t=',1p,e12.5,' cycle',i5,' efield e ,tot kin e ',2(1p,e14.7),3x,&
         'totq = ',1p,e14.7,' tote= ',1p,e10.3,'number ',i6,'variance ',1p,e&
         10.3) 
      return  
      end subroutine global 
