      subroutine gridmkr(imax, jmax, i1, i2, j1, j2, irez1, irez2, jrez1, jrez2&
         , firstime, xn, yn, w, alf, bta, gma, jacob, dqx, dqy, tau, dt, scal, &
         eqvol, nummax, a, b, c, d, e, af, bf, cf, df, ef, eg, dg, cg, bg, ag, &
         ah, bh, ch, dh, eh, soln, srce, residu, sc1, sc2) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: imax 
      integer  :: jmax 
      integer  :: i1 
      integer  :: i2 
      integer  :: j1 
      integer  :: j2 
      integer  :: irez1 
      integer  :: irez2 
      integer  :: jrez1 
      integer  :: jrez2 
      integer  :: nummax 
      real(double) , intent(in) :: tau 
      real(double) , intent(in) :: dt 
      real(double)  :: scal 
      real(double)  :: eqvol 
      real(double)  :: a 
      real(double)  :: b 
      real(double)  :: c 
      real(double)  :: d 
      real(double)  :: e 
      real(double)  :: af 
      real(double)  :: bf 
      real(double)  :: cf 
      real(double)  :: df 
      real(double)  :: ef 
      real(double)  :: eg 
      real(double)  :: dg 
      real(double)  :: cg 
      real(double)  :: bg 
      real(double)  :: ag 
      real(double)  :: ah 
      real(double)  :: bh 
      real(double)  :: ch 
      real(double)  :: dh 
      real(double)  :: eh 
      real(double)  :: soln 
      real(double)  :: srce 
      real(double)  :: residu 
      real(double)  :: sc1 
      real(double)  :: sc2 
      logical , intent(in) :: firstime 
      real(double)  :: xn(imax,*) 
      real(double)  :: yn(imax,*) 
      real(double)  :: w(imax,*) 
      real(double)  :: alf(imax,*) 
      real(double)  :: bta(imax,*) 
      real(double)  :: gma(imax,*) 
      real(double)  :: jacob(imax,*) 
      real(double)  :: dqx(imax,*) 
      real(double)  :: dqy(imax,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: taugrid 
!-----------------------------------------------
!
      call gridgeom (imax, jmax, i1, i2, j1, j2, xn, yn, alf, bta, gma, jacob) 
!
      taugrid = tau/dt 
!
      if (firstime) call gridscal (imax, jmax, i1, i2, j1, j2, alf, bta, gma, &
         jacob, w, scal, 0.D0, 0.D0, 0.D0, 0.D0, eqvol, xn, yn, dqx, dqy, &
         taugrid, a, b, c, d, e, af, bf, cf, df, ef, eg, dg, cg, bg, ag, ah, bh&
         , ch, dh, eh, soln, srce, residu, sc1, sc2) 
!
      call gridbdy (1, imax, irez1, irez2, jrez1, jrez2, xn, yn, w, taugrid, &
         alf, gma, jacob, a, b, c, d, e, af, sc1, sc2, srce, soln) 
      call gridgen (imax, jmax, irez1, irez2, jrez1, jrez2, xn, yn, w, taugrid&
         , eqvol, nummax, alf, bta, gma, jacob, dqx, dqy, a, b, c, d, e, af, bf&
         , cf, df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, srce, &
         residu, sc1, sc2) 
!
      return  
      end subroutine gridmkr 


      subroutine gridcor(imax, i1, i2, j1, j2, xn, ef, cf, c, e, srce) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: imax 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: xn(imax,*) 
      real(double) , intent(inout) :: ef(*) 
      real(double) , intent(inout) :: cf(*) 
      real(double) , intent(inout) :: c(*) 
      real(double) , intent(inout) :: e(*) 
      real(double) , intent(inout) :: srce(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nx, ny, nm 
!-----------------------------------------------
!
      nx = i2 - i1 - 1 
      ny = j2 - j1 - 1 
!
!
!     bottom left corner (dirichlet)
!
      srce(1) = srce(1) - ef(1)*xn(i1,j1) 
      ef(1) = 0.0 
!
!     bottom right corner
!
      srce(nx) = srce(nx) - cf(nx)*xn(i2,j1) 
      cf(nx) = 0.0 
!
!     top left corner
!
      nm = (ny - 1)*nx + 1 
      srce(nm) = srce(nm) - c(nm)*xn(i1,j2) 
      c(nm) = 0.0 
!
!     top right corner
!
      nm = nx*ny 
      srce(nm) = srce(nm) - e(nm)*xn(i2,j2) 
      e(nm) = 0.0 
      return  
      end subroutine gridcor 


      subroutine gridgen(imax, jmax, i1, i2, j1, j2, xn, yn, w, taugrid, eqvol&
         , nummax, alf, bta, gma, jacob, dqx, dqy, a, b, c, d, e, af, bf, cf, &
         df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, srce, residu, &
         sc1, sc2) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: imax 
      integer  :: jmax 
      integer  :: i1 
      integer  :: i2 
      integer  :: j1 
      integer  :: j2 
      integer  :: nummax 
      real(double) , intent(in) :: taugrid 
      real(double)  :: eqvol 
      real(double)  :: xn(imax,*) 
      real(double)  :: yn(imax,*) 
      real(double) , intent(in) :: w(imax,*) 
      real(double) , intent(in) :: alf(imax,*) 
      real(double) , intent(in) :: bta(imax,*) 
      real(double) , intent(in) :: gma(imax,*) 
      real(double) , intent(in) :: jacob(imax,*) 
      real(double) , intent(inout) :: dqx(imax,*) 
      real(double) , intent(in) :: dqy(imax,*) 
      real(double)  :: a(1) 
      real(double)  :: b(1) 
      real(double)  :: c(1) 
      real(double)  :: d(1) 
      real(double)  :: e(1) 
      real(double)  :: af(1) 
      real(double)  :: bf(1) 
      real(double)  :: cf(1) 
      real(double)  :: df(1) 
      real(double)  :: ef(1) 
      real(double)  :: eg(1) 
      real(double)  :: dg(1) 
      real(double)  :: cg(1) 
      real(double)  :: bg(1) 
      real(double)  :: ag(1) 
      real(double)  :: ah(1) 
      real(double)  :: bh(1) 
      real(double)  :: ch(1) 
      real(double)  :: dh(1) 
      real(double)  :: eh(1) 
      real(double)  :: soln(1) 
      real(double)  :: srce(1) 
      real(double)  :: residu(1) 
      real(double)  :: sc1(1) 
      real(double)  :: sc2(1) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nm, j, i 
      real(double) :: wbar, y1, y2, x1, x2 
      logical :: sym 
!-----------------------------------------------
!
!     algorithm to control volume of cells with vertex positions
!     on the boundary specified
!
      nm = 1 
      do j = j1 + 1, j2 - 1 
         do i = i1 + 1, i2 - 1 
!
            wbar = 0.25*(w(i,j)+w(i,j-1)+w(i-1,j)+w(i-1,j-1))*jacob(i,j) 
!
            y1 = 0.5*(yn(i+1,j)-yn(i-1,j)) 
            y2 = 0.5*(yn(i,j+1)-yn(i,j-1)) 
!
            a(nm) = (-2.*wbar*(alf(i,j)+gma(i,j))) - taugrid 
            b(nm) = wbar*alf(i,j) + y2*dqx(i,j) 
            c(nm) = wbar*bta(i,j)/2. 
            d(nm) = wbar*gma(i,j) - y1*dqx(i,j) 
            e(nm) = -wbar*bta(i,j)/2. 
            bf(nm) = wbar*alf(i,j) - y2*dqx(i,j) 
            cf(nm) = c(nm) 
            df(nm) = wbar*gma(i,j) + y1*dqx(i,j) 
            ef(nm) = e(nm) 
            soln(nm) = xn(i,j) 
            srce(nm) = -taugrid*xn(i,j) 
            nm = nm + 1 
         end do 
      end do 
!
      do j = j1, j2 
         do i = i1, i2 
            dqx(i,j) = xn(i,j) 
         end do 
      end do 
!
      call gridcor (imax, i1, i2, j1, j2, xn, ef, cf, c, e, srce) 
!
      sym = .FALSE. 
      call gridsolv (imax, jmax, i1 + 1, i2 - 1, j1 + 1, j2 - 1, a, b, c, d, e&
         , af, bf, cf, df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, &
         srce, residu, sc1, sc2, xn, 1.D0, 1.D0, 1.D0, 1.D0, sym) 
!
      nm = 1 
      do j = j1 + 1, j2 - 1 
         do i = i1 + 1, i2 - 1 
!
            wbar = 0.25*(w(i,j)+w(i,j-1)+w(i-1,j)+w(i-1,j-1))*jacob(i,j) 
!
            x1 = 0.5*(dqx(i+1,j)-dqx(i-1,j)) 
            x2 = 0.5*(dqx(i,j+1)-dqx(i,j-1)) 
!
            a(nm) = (-2.*wbar*(alf(i,j)+gma(i,j))) - taugrid 
            b(nm) = wbar*alf(i,j) - x2*dqy(i,j) 
            c(nm) = wbar*bta(i,j)/2. 
            d(nm) = wbar*gma(i,j) + x1*dqy(i,j) 
            e(nm) = -wbar*bta(i,j)/2. 
            bf(nm) = wbar*alf(i,j) + x2*dqy(i,j) 
            cf(nm) = c(nm) 
            df(nm) = wbar*gma(i,j) - x1*dqy(i,j) 
            ef(nm) = e(nm) 
            soln(nm) = yn(i,j) 
            srce(nm) = -taugrid*yn(i,j) 
            nm = nm + 1 
         end do 
      end do 
      call gridcor (imax, i1, i2, j1, j2, yn, ef, cf, c, e, srce) 
      sym = .FALSE. 
      call gridsolv (imax, jmax, i1 + 1, i2 - 1, j1 + 1, j2 - 1, a, b, c, d, e&
         , af, bf, cf, df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, &
         srce, residu, sc1, sc2, yn, 1.D0, 1.D0, 1.D0, 1.D0, sym) 
!
      return  
      end subroutine gridgen 


      subroutine gridgeom(imax, jmax, i1, i2, j1, j2, xn, yn, alf, bta, gma, &
         jacob) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: imax 
      integer  :: jmax 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: xn(imax,*) 
      real(double) , intent(in) :: yn(imax,*) 
      real(double) , intent(inout) :: alf(imax,*) 
      real(double) , intent(inout) :: bta(imax,*) 
      real(double) , intent(inout) :: gma(imax,*) 
      real(double) , intent(inout) :: jacob(imax,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j 
      real(double) :: volmin, x1, x2, y1, y2 
!-----------------------------------------------
!
!
      volmin = 1.E20 
!
      do i = i1 + 1, i2 - 1 
         do j = j1 + 1, j2 - 1 
            x1 = 0.5*(xn(i+1,j)-xn(i-1,j)) 
            x2 = 0.5*(xn(i,j+1)-xn(i,j-1)) 
            y1 = 0.5*(yn(i+1,j)-yn(i-1,j)) 
            y2 = 0.5*(yn(i,j+1)-yn(i,j-1)) 
            jacob(i,j) = abs(x1*y2 - x2*y1) 
            alf(i,j) = (x2**2 + y2**2)/jacob(i,j) 
            bta(i,j) = (x1*x2 + y1*y2)/jacob(i,j) 
            gma(i,j) = (x1**2 + y1**2)/jacob(i,j) 
!
            volmin = min(volmin,jacob(i,j)) 
!
         end do 
      end do 
!
      do i = i1 + 1, i2 - 1 
         jacob(i,j1) = jacob(i,j1+1) 
         jacob(i,j2) = jacob(i,j2-1) 
         alf(i,j1) = alf(i,j1+1) 
         alf(i,j2) = alf(i,j2-1) 
         bta(i,j1) = bta(i,j1+1) 
         bta(i,j2) = bta(i,j2-1) 
         gma(i,j1) = gma(i,j1+1) 
         gma(i,j2) = gma(i,j2-1) 
      end do 
!
      do j = j1, j2 
         jacob(i1,j) = jacob(i1+1,j) 
         jacob(i2,j) = jacob(i2-1,j) 
         alf(i1,j) = alf(i1+1,j) 
         alf(i2,j) = alf(i2-1,j) 
         bta(i1,j) = bta(i1+1,j) 
         bta(i2,j) = bta(i2-1,j) 
         gma(i1,j) = gma(i1+1,j) 
         gma(i2,j) = gma(i2-1,j) 
      end do 
!
      return  
      end subroutine gridgeom 


      subroutine gridscal(imax, jmax, i1, i2, j1, j2, alf, bta, gma, jacob, w, &
         scal, wsl, wsr, wsb, wst, eqvol, xn, yn, dqx, dqy, taugrid, a, b, c, d&
         , e, af, bf, cf, df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln&
         , srce, residu, sc1, sc2) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: imax 
      integer  :: jmax 
      integer  :: i1 
      integer , intent(in) :: i2 
      integer  :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: scal 
      real(double)  :: wsl 
      real(double)  :: wsr 
      real(double)  :: wsb 
      real(double)  :: wst 
      real(double)  :: eqvol 
      real(double)  :: taugrid 
      real(double) , intent(in) :: alf(imax,*) 
      real(double) , intent(in) :: bta(imax,*) 
      real(double) , intent(in) :: gma(imax,*) 
      real(double)  :: jacob(imax,*) 
      real(double)  :: w(imax,*) 
      real(double) , intent(in) :: xn(imax,*) 
      real(double) , intent(in) :: yn(imax,*) 
      real(double) , intent(out) :: dqx(imax,*) 
      real(double) , intent(out) :: dqy(imax,*) 
      real(double)  :: a(1) 
      real(double)  :: b(1) 
      real(double)  :: c(1) 
      real(double)  :: d(1) 
      real(double)  :: e(1) 
      real(double)  :: af(1) 
      real(double)  :: bf(1) 
      real(double)  :: cf(1) 
      real(double)  :: df(1) 
      real(double)  :: ef(1) 
      real(double)  :: eg(1) 
      real(double)  :: dg(1) 
      real(double)  :: cg(1) 
      real(double)  :: bg(1) 
      real(double)  :: ag(1) 
      real(double)  :: ah(1) 
      real(double)  :: bh(1) 
      real(double)  :: ch(1) 
      real(double)  :: dh(1) 
      real(double)  :: eh(1) 
      real(double)  :: soln(1) 
      real(double)  :: srce(1) 
      real(double)  :: residu(1) 
      real(double)  :: sc1(1) 
      real(double)  :: sc2(1) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nm, j, i 
      real(double) :: refrence, wsvol, w1, w2, x1, x2, y1, y2, dwdx, dwdy 
      logical :: sym 
!-----------------------------------------------
!
!     algorithm to control volume of cells with vertex positions
!     on the boundary specified
!
      refrence = sqrt((xn(i2,j1)-xn(i1,j2))*(yn(i2,j2)-yn(i1,j1))-(xn(i2,j2)-xn&
         (i1,j1))*(yn(i2,j1)-yn(i1,j2))) 
!
!
      nm = 1 
      do j = j1, j2 - 1 
         do i = i1, i2 - 1 
!
            a(nm) = (-0.5*(alf(i+1,j+1)+alf(i+1,j)+alf(i,j+1)+alf(i,j))) - 0.5*&
               (gma(i+1,j+1)+gma(i,j+1)+gma(i+1,j)+gma(i,j)) + (bta(i+1,j+1)-&
               bta(i,j+1)+bta(i,j)-bta(i+1,j)) 
            b(nm) = 0.5*(alf(i+1,j+1)+alf(i+1,j)) 
            c(nm) = bta(i,j+1) 
            d(nm) = 0.5*(gma(i+1,j+1)+gma(i,j+1)) 
            e(nm) = -bta(i+1,j+1) 
            ef(nm) = -bta(i,j) 
            df(nm) = 0.5*(gma(i+1,j)+gma(i,j)) 
            cf(nm) = bta(i+1,j) 
            bf(nm) = 0.5*(alf(i,j+1)+alf(i,j)) 
!
            soln(nm) = w(i,j) 
!
            wsvol = (xn(i+1,j)-xn(i,j+1))*(yn(i+1,j+1)-yn(i,j)) - (xn(i+1,j+1)-&
               xn(i,j))*(yn(i+1,j)-yn(i,j+1)) 
            srce(nm) = -wsvol*w(i,j)/scal**2 
!
            a(nm) = a(nm) - wsvol/scal**2 
            nm = nm + 1 
         end do 
      end do 
!
      sym = .FALSE. 
      call gridsolv (imax, jmax, i1, i2 - 1, j1, j2 - 1, a, b, c, d, e, af, bf&
         , cf, df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, srce, &
         residu, sc1, sc2, w, wsr, wst, wsl, wsb, sym) 
!
!     differentiate cell-centered data
!
      do j = j1 + 1, j2 - 1 
         do i = i1 + 1, i2 - 1 
!
            w1 = 0.5*(w(i,j)+w(i,j-1)-w(i-1,j)-w(i-1,j-1)) 
            w2 = 0.5*(w(i,j)+w(i-1,j)-w(i,j-1)-w(i-1,j-1)) 
!
            x1 = 0.5*(xn(i+1,j)-xn(i-1,j)) 
            x2 = 0.5*(xn(i,j+1)-xn(i,j-1)) 
!
            y1 = 0.5*(yn(i+1,j)-yn(i-1,j)) 
            y2 = 0.5*(yn(i,j+1)-yn(i,j-1)) 
!
            dwdx = w1*y2 - w2*y1 
            dwdy = x1*w2 - x2*w1 
!
            dqx(i,j) = 0.5*dwdx 
            dqy(i,j) = 0.5*dwdy 
         end do 
      end do 
!
!
      return  
      end subroutine gridscal 


      subroutine gridsolv(imax, jmax, i1, i2, j1, j2, a, b, c, d, e, af, bf, cf&
         , df, ef, eg, dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, srce, residu, &
         sc1, sc2, xn, bcr, bct, bcl, bcb, sym) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: imax 
      integer  :: jmax 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: bcr 
      real(double) , intent(in) :: bct 
      real(double) , intent(in) :: bcl 
      real(double) , intent(in) :: bcb 
      logical , intent(in) :: sym 
      real(double)  :: a(1) 
      real(double)  :: b(1) 
      real(double)  :: c(1) 
      real(double)  :: d(1) 
      real(double)  :: e(1) 
      real(double)  :: af(1) 
      real(double)  :: bf(1) 
      real(double)  :: cf(1) 
      real(double)  :: df(1) 
      real(double)  :: ef(1) 
      real(double)  :: eg(1) 
      real(double)  :: dg(1) 
      real(double)  :: cg(1) 
      real(double)  :: bg(1) 
      real(double)  :: ag(1) 
      real(double)  :: ah(1) 
      real(double)  :: bh(1) 
      real(double)  :: ch(1) 
      real(double)  :: dh(1) 
      real(double)  :: eh(1) 
      real(double)  :: soln(1) 
      real(double)  :: srce(1) 
      real(double)  :: residu(1) 
      real(double)  :: sc1(1) 
      real(double)  :: sc2(1) 
      real(double) , intent(inout) :: xn(imax,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nx, ny, nm, i, j, itmax 
      real(double) :: errmax, xratio, yratio, xnorm, ynorm 
!-----------------------------------------------
!
!     an interface routine between newfish and iccg routines
!
!
      nx = i2 - i1 + 1 
      ny = j2 - j1 + 1 
!
!     bottom boundary(bcb=1, dirichlet; bcb=0, von N)
!
      nm = 1 
      do i = i1, i2 
         srce(nm) = srce(nm) - bcb*df(nm)*xn(i,j1-1) - bcb*cf(nm)*xn(i+1,j1-1)&
             - bcb*ef(nm)*xn(i-1,j1-1) 
         a(nm) = a(nm) + (1. - bcb)*df(nm) 
         b(nm) = b(nm) + (1. - bcb)*cf(nm) 
         bf(nm) = bf(nm) + (1. - bcb)*ef(nm) 
         df(nm) = 0.0 
         cf(nm) = 0.0 
         ef(nm) = 0.0 
         nm = nm + 1 
      end do 
!
!     right boundary(bcr=1,dirichlet; bcr=0, von N)
!
      nm = nx 
      do j = j1, j2 
         srce(nm) = srce(nm) - bcr*b(nm)*xn(i2+1,j) - bcr*e(nm)*xn(i2+1,j+1) - &
            bcr*cf(nm)*xn(i2+1,j-1) 
         a(nm) = a(nm) + (1. - bcr)*b(nm) 
         d(nm) = d(nm) + (1. - bcr)*e(nm) 
         df(nm) = df(nm) + (1. - bcr)*cf(nm) 
         b(nm) = 0.0 
         e(nm) = 0.0 
         cf(nm) = 0.0 
         nm = nm + nx 
      end do 
!
!     top boundary (bct=1,dirichlet; bct=0, von N)
!
      nm = (ny - 1)*nx + 1 
      do i = i1, i2 
         srce(nm) = srce(nm) - bct*d(nm)*xn(i,j2+1) - bct*c(nm)*xn(i-1,j2+1) - &
            bct*e(nm)*xn(i+1,j2+1) 
         a(nm) = a(nm) + (1. - bct)*d(nm) 
         b(nm) = b(nm) + (1. - bct)*e(nm) 
         bf(nm) = bf(nm) + (1. - bct)*c(nm) 
         c(nm) = 0.0 
         d(nm) = 0.0 
         e(nm) = 0.0 
         nm = nm + 1 
      end do 
!
!     left boundary (bcl=1,dirichlet; bcl=0, von N)
!
      nm = 1 
      do j = j1, j2 
         srce(nm) = srce(nm) - bcl*bf(nm)*xn(i1-1,j) - bcl*c(nm)*xn(i1-1,j+1)&
             - bcl*ef(nm)*xn(i1-1,j-1) 
         a(nm) = a(nm) + (1. - bcl)*bf(nm) 
         d(nm) = d(nm) + (1. - bcl)*c(nm) 
         df(nm) = df(nm) + (1. - bcl)*ef(nm) 
         bf(nm) = 0.0 
         ef(nm) = 0.0 
         c(nm) = 0.0 
         nm = nm + nx 
      end do 
!
      itmax = 250 
      errmax = 1.E-10 
      if (sym) then 
         call cgitj (a, b, c, d, e, af, bf, cf, df, ef, nx, ny, soln, srce, &
            residu, sc1, sc2, itmax, errmax, xratio, yratio, xnorm, ynorm) 
      else 
         call ilucgj (ef, df, cf, bf, a, b, c, d, e, eh, dh, ch, bh, ag, bg, cg&
            , dg, eg, nx, ny, soln, srce, residu, sc1, sc2, itmax, errmax, &
            xratio, yratio, xnorm, ynorm) 
      endif 
!
      nm = 1 
      do j = j1, j2 
         do i = i1, i2 
!
            xn(i,j) = soln(nm) 
!
            nm = nm + 1 
         end do 
      end do 
!
      return  
      end subroutine gridsolv 


      subroutine gridbdy(nqi, nqj, i1, i2, j1, j2, xn, yn, w, tau, alf, gma, &
         jacob, a, b, c, d, e, f, sc1, sc2, srce, soln) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: nqi 
      integer  :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double)  :: tau 
      real(double)  :: xn(nqj,*) 
      real(double)  :: yn(nqj,*) 
      real(double)  :: w(*) 
      real(double)  :: alf(nqj,*) 
      real(double)  :: gma(nqj,*) 
      real(double)  :: jacob(nqj,*) 
      real(double)  :: a(*) 
      real(double)  :: b(*) 
      real(double)  :: c(*) 
      real(double)  :: d(*) 
      real(double)  :: e(*) 
      real(double)  :: f(*) 
      real(double)  :: sc1(*) 
      real(double)  :: sc2(*) 
      real(double)  :: srce(*) 
      real(double)  :: soln(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij1, ij2, j 
!-----------------------------------------------
!
!     bottom boundary
!
      ij1 = (j1 - 1)*nqj + (i1 - 1)*nqi + 1 
      ij2 = (j1 - 1)*nqj + (i2 - 1)*nqi + 1 
!
      do j = ij1, ij2, nqi 
         sc2(j) = w(j) 
      end do 
!
      call gridcurv (nqi, ij1, ij2, alf, jacob, xn, yn, w, sc1, sc2, a, b, c, d&
         , soln, tau) 
!
      call gridtrid (nqi, ij1, ij2, a, b, c, d, e, f, soln) 
!
      call gridpts (nqi, ij1, ij2, xn, yn, sc1, soln, a, b) 
!
!     right boundary
!
      ij1 = nqj*(j1 - 1) + nqi*(i2 - 1) + 1 
      ij2 = nqj*(j2 - 1) + nqi*(i2 - 1) + 1 
!
      do j = ij1, ij2, nqj 
         sc2(j) = w(j-nqi) 
      end do 
!
      call gridcurv (nqj, ij1, ij2, gma, jacob, xn, yn, w, sc1, sc2, a, b, c, d&
         , soln, tau) 
!
      call gridtrid (nqj, ij1, ij2, a, b, c, d, e, f, soln) 
!
      call gridpts (nqj, ij1, ij2, xn, yn, sc1, soln, a, b) 
!
!      top boundary
!
      ij1 = nqj*(j2 - 1) + nqi*(i1 - 1) + 1 
      ij2 = nqj*(j2 - 1) + nqi*(i2 - 1) + 1 
!
      do j = ij1, ij2, nqi 
         sc2(j) = w(j-nqj) 
      end do 
!
      call gridcurv (nqi, ij1, ij2, alf, jacob, xn, yn, w, sc1, sc2, a, b, c, d&
         , soln, tau) 
!
      call gridtrid (nqi, ij1, ij2, a, b, c, d, e, f, soln) 
!
      call gridpts (nqi, ij1, ij2, xn, yn, sc1, soln, a, b) 
!
!     left boundary
!
      ij1 = nqj*(j1 - 1) + nqi*(i1 - 1) + 1 
      ij2 = nqj*(j2 - 1) + nqi*(i1 - 1) + 1 
!
      do j = ij1, ij2, nqj 
         sc2(j) = w(j) 
      end do 
!
      call gridcurv (nqj, ij1, ij2, gma, jacob, xn, yn, w, sc1, sc2, a, b, c, d&
         , soln, tau) 
!
      call gridtrid (nqj, ij1, ij2, a, b, c, d, e, f, soln) 
!
      call gridpts (nqj, ij1, ij2, xn, yn, sc1, soln, a, b) 
!
      return  
      end subroutine gridbdy 


      subroutine gridcurv(nqi, i1, i2, alfa, jacob, xn, yn, w, s, r, a, b, c, &
         srce, soln, tau) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(in) :: tau 
      real(double) , intent(in) :: alfa(*) 
      real(double) , intent(in) :: jacob(*) 
      real(double) , intent(in) :: xn(*) 
      real(double) , intent(in) :: yn(*) 
      real(double)  :: w(*) 
      real(double) , intent(inout) :: s(*) 
      real(double) , intent(in) :: r(*) 
      real(double) , intent(out) :: a(*) 
      real(double) , intent(out) :: b(*) 
      real(double) , intent(out) :: c(*) 
      real(double) , intent(inout) :: srce(*) 
      real(double) , intent(out) :: soln(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
      real(double) :: tiny 
!-----------------------------------------------
      tiny = 1.E-10 
!
!     calculate arc length
!
      s(i1) = 0.0 
      soln(i1) = 0.0 
      do i = i1 + nqi, i2, nqi 
         s(i) = s(i-nqi) + sqrt((xn(i)-xn(i-nqi))**2+(yn(i)-yn(i-nqi))**2) 
         soln(i) = s(i) 
      end do 
!
!     calculate coefficients for the equation
!
 
      do i = i1 + nqi, i2 - nqi, nqi 
         a(i) = -r(i) 
         b(i) = (-(r(i-nqi)+r(i))) - tau/(alfa(i)*jacob(i)) 
         c(i) = -r(i-nqi) 
         srce(i) = -tau*s(i)/(alfa(i)*jacob(i)) 
      end do 
      srce(i1) = srce(i1+nqi) 
      srce(i2) = srce(i2-nqi) 
!
      return  
      end subroutine gridcurv 


      subroutine gridpts(nqi, i1, i2, xn, yn, s, soln, a, b) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(inout) :: xn(*) 
      real(double) , intent(inout) :: yn(*) 
      real(double) , intent(in) :: s(*) 
      real(double) , intent(in) :: soln(*) 
      real(double) , intent(inout) :: a(*) 
      real(double) , intent(inout) :: b(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
      real(double) :: dx, dy, ds 
!-----------------------------------------------
!
      do i = i1 + nqi, i2 - nqi, nqi 
         dx = xn(i+nqi) - xn(i-nqi) 
         dy = yn(i+nqi) - yn(i-nqi) 
         ds = sqrt(dx**2 + dy**2) 
         a(i) = xn(i) + dx*(soln(i)-s(i))/ds 
         b(i) = yn(i) + dy*(soln(i)-s(i))/ds 
      end do 
!
      do i = i1 + nqi, i2 - nqi, nqi 
         xn(i) = a(i) 
         yn(i) = b(i) 
      end do 
!
      return  
      end subroutine gridpts 


      subroutine gridtrid(nqi, i1, i2, a, b, c, d, e, f, s) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(inout) :: e(*) 
      real(double) , intent(inout) :: f(*) 
      real(double) , intent(inout) :: s(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j 
      real(double) :: rws 
!-----------------------------------------------
!
      e(i1) = 0.0 
      f(i1) = s(i1) 
!
      do i = i1 + nqi, i2 - nqi, nqi 
         rws = 1./(b(i)-c(i)*e(i-nqi)) 
         e(i) = a(i)*rws 
         f(i) = (d(i)+c(i)*f(i-nqi))*rws 
      end do 
!
      j = i2 - nqi 
      do i = i1 + nqi, i2 - nqi, nqi 
         s(j) = e(j)*s(j+nqi) + f(j) 
         j = j - nqi 
      end do 
!
      return  
      end subroutine gridtrid 
