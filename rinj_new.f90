 
      subroutine rinj(isp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: isp 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: iwid, jwid, j, i, ij, ipj, ipjp, ijp, ipjm, ijm, npinj, ip, np 
      real(double) :: sbottom, delz, vtherm, flxrnd, flxdir, fluxin, qbottom, &
         sutop, qtop, rebal, qinj, the, zeta, w1, w2, w3, w4, vmag, ws, th 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: derf, ranf, generate 
!-----------------------------------------------
 
!
!     A subroutine to inject particles
!
      pi = acos(-1.) 
      iwid = 1 
      jwid = nxp 
!
!     rebalance
!
      sbottom = 0. 
      j = 2 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-y(ipj)) 
         sbottom = sbottom + vol(ij)/delz 
      end do 
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*vvi(isp)*(1. + derf(vvi(isp)/vtherm)) 
      fluxin = flxrnd + flxdir 
      qbottom = sbottom*fluxin*dt 
 
      sutop = 0. 
      j = ny 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjm = ij + iwid - jwid 
         ijm = ij - jwid 
 
         delz = 0.5*(yn(ij)+yn(ipj)-yn(ijm)-y(ipjm)) 
         sutop = sutop + vol(ij)/delz 
      end do 
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*(-vvi(isp))*(1. + derf((-vvi(isp))/vtherm)) 
      fluxin = flxrnd + flxdir 
      qtop = sutop*fluxin*dt 
      rebal = abs(qout(isp)/(qtop+qbottom)) 
      rebal = 1. 
!
!     BOTTOM
!
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*vvi(isp)*(1. + derf(vvi(isp)/vtherm)) 
      fluxin = (flxrnd + flxdir)*rebal 
 
      j = 2 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-y(ipj)) 
         npinj = int(fluxin*dt*npcel(isp)/rhr(isp)/delz) 
         if (npinj == 0) then 
!        if(ranf().lt.fluxin*dt*npcel(isp)/rhr(isp)/delz) then
!           npinj=1
!           qinj=rhr(isp)*vol(ij)/npcel(isp)
!           endif
            npinj = 1 
            qinj = fluxin*vol(ij)/delz*dt 
         else 
            qinj = fluxin*vol(ij)/delz*dt 
         endif 
 
         qinj = qinj*sign(1.,qom(isp)) 
 
         do ip = 1, npinj 
            np = iphead(1) 
 
            if (np > 0) then 
 
               iphead(1) = link(np) 
               link(np) = iphead(ij) 
               iphead(ij) = np 
 
               the = ranf() 
               zeta = 0D0 
!      ranf()*fluxin*dt/delz
               xp(np) = i + the 
               yp(np) = j + zeta 
               w1 = the*(1 - zeta) 
               w2 = the*zeta 
               w3 = (1 - the)*zeta 
               w4 = (1 - the)*(1 - zeta) 
 
               xpf(np) = w1*xn(ipj) + w2*xn(ipjp) + w3*xn(ijp) + w4*xn(ij) 
               ypf(np) = w1*yn(ipj) + w2*yn(ipjp) + w3*yn(ijp) + w4*yn(ij) 
 
               qpar(np) = qinj/npinj 
               ico(np) = isp 
               diepar(np) = abs(qpar(np))*diem(isp) 
               vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
               ws = vmag*siep(isp) 
               th = 2.*pi*ranf() 
               up(np) = uvi(isp) + ws*cos(th) 
!        vp(np)=abs(vvi(isp)+ws*sin(th))
               wp(np) = ws*sin(th) 
!        vmag = sqrt(-2.*log(1.-.999999*ranf()))
!        ws=vmag*siep(isp)
!        th = 2*pi*ranf()
!        wp(np)=ws*cos(th)
               vp(np) = generate(vvi(isp),siep(isp)*sqrt(2.)) 
               ypf(np) = ypf(np) + vp(np)*dt*ranf() 
 
 
            else 
 
               write (*, *) 'WARNING: Not Enough Particles' 
 
            endif 
 
         end do 
 
      end do 
 
 
 
!
!     TOP
!
      vtherm = siep(isp)*sqrt(2.) 
      flxrnd = exp((-vvi(isp)**2/vtherm**2))*vtherm/2./sqrt(pi) 
      flxdir = 0.5*(-vvi(isp))*(1. + derf((-vvi(isp))/vtherm)) 
      fluxin = (flxrnd + flxdir)*rebal 
 
      j = ny 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
 
         delz = 0.5*(yn(ijp)+yn(ipjp)-yn(ij)-y(ipj)) 
         npinj = int(fluxin*dt*npcel(isp)/rhr(isp)/delz) 
         if (npinj == 0) then 
!        if(ranf().lt.fluxin*dt*npcel(isp)/rhr(isp)/delz) then
!           npinj=1
!           qinj=rhr(isp)*vol(ij)/npcel(isp)
!           endif
            npinj = 1 
            qinj = fluxin*vol(ij)/delz*dt 
         else 
            qinj = fluxin*vol(ij)/delz*dt 
         endif 
 
         qinj = qinj*sign(1.,qom(isp)) 
 
         do ip = 1, npinj 
            np = iphead(1) 
 
            if (np > 0) then 
 
               iphead(1) = link(np) 
               link(np) = iphead(ij) 
               iphead(ij) = np 
 
               the = ranf() 
               zeta = 1D0 
!      ranf()*fluxin*dt/delz
               xp(np) = i + the 
               yp(np) = j + zeta 
               w1 = the*(1 - zeta) 
               w2 = the*zeta 
               w3 = (1 - the)*zeta 
               w4 = (1 - the)*(1 - zeta) 
 
               xpf(np) = w1*xn(ipj) + w2*xn(ipjp) + w3*xn(ijp) + w4*xn(ij) 
               ypf(np) = w1*yn(ipj) + w2*yn(ipjp) + w3*yn(ijp) + w4*yn(ij) 
 
               qpar(np) = qinj/npinj 
               ico(np) = isp 
               diepar(np) = abs(qpar(np))*diem(isp) 
               vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
               ws = vmag*siep(isp) 
               th = 2.*pi*ranf() 
               up(np) = uvi(isp) + ws*cos(th) 
!        vp(np)=-abs(vvi(isp)+ws*sin(th))
               wp(np) = ws*sin(th) 
!        vmag = sqrt(-2.*log(1.-.999999*ranf()))
!        ws=vmag*siep(isp)
!        th = 2*pi*ranf()
!        wp(np)=ws*cos(th)
               vp(np) = -generate((-vvi(isp)),siep(isp)*sqrt(2.)) 
!      write(*,*)'top',xpf(np),ypf(np),vp(np)
               ypf(np) = ypf(np) + vp(np)*dt*ranf() 
 
 
            else 
 
               write (*, *) 'WARNING: Not Enough Particles' 
 
            endif 
 
         end do 
 
      end do 
 
 
 
      return  
      end subroutine rinj 
