 
      subroutine moments(nqi, nqj, nxyp, i1, i2, j1, j2, ns, cntr, dt, cx1, cx2&
         , cx3, cx4, cr1, cr2, cr3, cr4, cy1, cy2, cy3, cy4, vol, vvol, qom, &
         jxs, jys, qvs, qdn, ex, ey, ufl, vfl) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: nxyp 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: ns 
      real(double)  :: cntr 
      real(double) , intent(in) :: dt 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: vol(*) 
      real(double) , intent(inout) :: vvol(*) 
      real(double) , intent(in) :: qom(*) 
      real(double) , intent(inout) :: jxs(nxyp,*) 
      real(double) , intent(inout) :: jys(nxyp,*) 
      real(double) , intent(inout) :: qvs(nxyp,*) 
      real(double) , intent(inout) :: qdn(nxyp,*) 
      real(double) , intent(in) :: ex(*) 
      real(double) , intent(in) :: ey(*) 
      real(double) , intent(out) :: ufl(*) 
      real(double) , intent(out) :: vfl(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ipjp, ijp 
      real(double) :: dto2, ws, rqs 
!-----------------------------------------------
!
!
!
      dto2 = 0.5*dt 
!
      do j = j1, j2 + 1 
         do i = i1, i2 + 1 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
!
!
            ws = 0.5*qom(ns)*qvs(ij,ns)*dt 
            jxs(ij,ns) = jxs(ij,ns) + ws*ex(ij) 
            jys(ij,ns) = jys(ij,ns) + ws*ey(ij) 
!
         end do 
      end do 
!
      do j = j1, j2 
         do i = i1, i2 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            ipj = ij + nqi 
            ipjp = ij + nqj + nqi 
            ijp = ij + nqj 
!
!
            qdn(ij,ns) = qdn(ij,ns)*vol(ij) - ((cx1(ij)+cr1(ij))*jxs(ij,ns)+cy1&
               (ij)*jys(ij,ns)+(cx2(ij)+cr2(ij))*jxs(ipjp,ns)+cy2(ij)*jys(ipjp,&
               ns)+(cx3(ij)+cr3(ij))*jxs(ijp,ns)+cy3(ij)*jys(ijp,ns)+(cx4(ij)+&
               cr4(ij))*jxs(ij,ns)+cy4(ij)*jys(ij,ns))*dto2 
!
         end do 
      end do 
!
      do j = j1, j2 + 1 
         do i = i1, i2 + 1 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
!
            vvol(ij) = 0.0 
            qvs(ij,ns) = 0.0 
         end do 
      end do 
!
      do j = j1, j2 
         do i = i1, i2 
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            ipj = ij + nqi 
            ipjp = ij + nqj + nqi 
            ijp = ij + nqj 
!
 
            qvs(ipj,ns) = qvs(ipj,ns) + 0.25*qdn(ij,ns) 
            qvs(ipjp,ns) = qvs(ipjp,ns) + 0.25*qdn(ij,ns) 
            qvs(ijp,ns) = qvs(ijp,ns) + 0.25*qdn(ij,ns) 
            qvs(ij,ns) = qvs(ij,ns) + 0.25*qdn(ij,ns) 
!
            vvol(ipj) = vvol(ipj) + 0.25*vol(ij) 
            vvol(ipjp) = vvol(ipjp) + 0.25*vol(ij) 
            vvol(ijp) = vvol(ijp) + 0.25*vol(ij) 
            vvol(ij) = vvol(ij) + 0.25*vol(ij) 
!
         end do 
      end do 
!
      do j = j1, j2 + 1 
         do i = i1, i2 + 1 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
!
            qvs(ij,ns) = qvs(ij,ns)/vvol(ij) 
!
!
            rqs = 1./(qvs(ij,ns)+1.E-10)**2 
            ufl(ij) = jxs(ij,ns)*qvs(ij,ns)*rqs 
            vfl(ij) = jys(ij,ns)*qvs(ij,ns)*rqs 
!
!
         end do 
      end do 
      return  
!
      end subroutine moments 
