      subroutine global_iec(lun) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  17:26:36   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: lun 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nh, ntotl, is, j, i, ij, ipj, ipjp, ijp, np, lv, l, imj, imjm&
         , ijm 
      real(double), dimension(nsp) :: xcentre, ycentre, voldust, massdust, &
         ndust 
      real(double) :: totq, totjx, totjy, varvx, varvy, dnorm, vexact, edotb, &
         circe, totdive, totchg, totdivel, totdiver, dnorm_one, dnorm_two
!-----------------------------------------------
!
!     local variables
!
!
      nh = mod(ncyc,nhst) 
!     +++
!     +++ compute total mass, momentum, and energy of the system
!     +++
      totq = 0. 
      totmass = 0.0 
      totjx = 0. 
      totjy = 0. 
      xmom(nh) = 0.0 
      ymom(nh) = 0.0 
      qtotal(nh) = 0.0 
      ntotal(nh) = 0.0 
      ntotl = 0 
      varvx = 0.0 
      varvy = 0. 
 
      do is = 1, nsp 
         totk(is) = 0.0 
         qdust(nh,is) = 0. 
         phidust(nh,is) = 0. 
         emomdustx(nh,is) = 0. 
         emomdusty(nh,is) = 0. 
         qdustp(nh,is) = 0. 
         phidust(nh,is) = 0. 
      end do 
 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
            d(ij) = 0.25*(x(ipj)+x(ipjp)+x(ijp)+x(ij)) 
            e(ij) = 0.25*(y(ipj)+y(ipjp)+y(ijp)+y(ij)) 
         end do 
      end do 
!
!     get the centre of mass!
!
!      do is=nsp_dust,nsp
!     xcentre(is)=0.
!     ycentre(is)=0.
!     massdust(is)=0.
!     ndust(is)=0.
!     voldust(is)=0.
!      do j = 2,ny
!      do i = 2,nx
!      ij = (j-1)*nxp+i
!     voldust(is)=voldust(is)+vol(ij)*qnet0(ij)
!     massdust(is)=massdust(is)+vol(ij)*qnet0(ij,is)
!     ndust(is)=ndust(is)+number(ij,is)
!     xcentre(is)=xcentre(is)+d(ij)*vol(ij)
!     ycentre(is)=ycentre(is)+e(ij)*vol(ij)
!      enddo
!      enddo
!     xcentre(is)=xcentre(is)/(voldust(is)+1.e-10)
!     ycentre(is)=ycentre(is)/(voldust(is)+1.e-10)
!      enddo
!
 
      dnorm_one = 0D0 
      dnorm_two = 0D0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
 
            dnorm_two = dnorm_two + qnet0(ij)*vol(ij) 
            dnorm_one = dnorm_one + qnet0(ij)**2*vol(ij) 
            do is = 1, nsp 
               qdust(nh,is) = qdust(nh,is) + (qnet0(ij)-q_mean(ij))**2*vol(ij) 
               emomdustx(nh,is) = emomdustx(nh,is) + qnet0(ij)*vol(ij)*(d(ij)-&
                  xcenter(is)) 
               emomdusty(nh,is) = emomdusty(nh,is) + qnet0(ij)*vol(ij)*((e(ij)-&
                  ycenter(is))**2+(d(ij)-xcenter(is))**2)**0.5D0 
               phidust(nh,is) = phidust(nh,is) + (qnet0(ij)-q_mean(ij))**2*vol(&
                  ij) 
               qdustp(nh,is) = qdustp(nh,is) + atan((e(ij)-ycenter(is))/(d(ij)-&
                  xcenter(is)))*qnet0(ij)*vol(ij) 
            end do 
 
            np = iphead(ij) 
            if (np <= 0) cycle  
   10       continue 
            call listmkr (np, nploc, link, lvmax, ntmp) 
            do lv = 1, lvmax 
               l = nploc(lv) 
               totq = totq + qpar(l) 
               totmass = totmass + qpar(l)/qom(ico(l)) 
               qtotal(nh) = qtotal(nh) + qpar(l) 
               ntotal(nh) = ntotal(nh) + 1 
               totjx = totjx + qpar(l)*up(l) 
               xmom(nh) = xmom(nh) + qpar(l)*up(l)/qom(ico(l)) 
               ymom(nh) = ymom(nh) + qpar(l)*vp(l)/qom(ico(l)) 
               totk(ico(l)) = totk(ico(l)) + 0.5*qpar(l)*(up(l)**2+vp(l)**2+wp(&
                  l)**2)/qom(ico(l)) 
!
               varvx = varvx + up(l)**2 
               varvy = varvy + vp(l)**2 
               ntotl = ntotl + 1 
!
            end do 
!
            np = link(nploc(lvmax)) 
            if (np > 0) go to 10 
         end do 
      end do 
 
      qdust(nh,1:nsp) = qdust(nh,1:nsp)/dnorm_one 
      emomdustx(nh,1:nsp) = emomdustx(nh,1:nsp)/dnorm_two 
      emomdusty(nh,1:nsp) = emomdusty(nh,1:nsp)/dnorm_two 
      qdustp(nh,1:nsp) = qdustp(nh,1:nsp)/dnorm_two 
      phidust(nh,1:nsp) = phidust(nh,1:nsp)/dnorm_two 
 
      if (mod(ncyc,10) == 0) then 
         write (*, *) 'TIME=', t 
         write (*, *) 'qtotal=', qtotal(nh) 
         do is = nsp_dust, nsp 
            write (*, *) 'SPECIES=', is 
!     write(*,*)'xcentre',xcentre(is),xcenter(is)
!     write(*,*)'ycentre',ycentre(is),ycenter(is)
  133       format(1x,5(e12.5,2x)) 
 
         end do 
      endif 
 
      if (ntotl == 0) stop  
!
      do j = 1, nyp 
         do i = 1, nxp 
!
            ij = (j - 1)*nxp + i 
!
            edotb = ex(ij)*bx(ij) + ey(ij)*by(ij) 
!
            soln(ij) = (1. + dielperp(ij))*ex(ij) + dieltrns(ij)*ey(ij)*bz(ij)&
                + dielpar(ij)*edotb*bx(ij) 
!
            srce(ij) = (1. + dielperp(ij))*ey(ij) - dieltrns(ij)*ex(ij)*bz(ij)&
                + dielpar(ij)*edotb*by(ij) 
!
         end do 
      end do 
!
      do j = 2, nyp 
         do i = 2, nxp 
!
            ij = (j - 1)*nxp + i 
            imj = ij - 1 
            imjm = ij - nxp - 1 
            ijm = ij - nxp 
!
            a(ij) = cx4(ij) + cr4(ij) + cx1(imj) + cr1(imj) + cx2(imjm) + cr2(&
               imjm) + cx3(ijm) + cr3(ijm) 
!
            b(ij) = cy4(ij) + cy1(imj) + cy2(imjm) + cy3(ijm) 
!
         end do 
      end do 
!
      variance(nh) = sqrt((varvx/dx**2 + varvy/dy**2)*dt**2)/ntotl 
!     compare edotj w/ change in kinetic energy
      call deriv (1, nxp, 1, nxp, 1, nyp, cx1, cx2, cx3, cx4, cr1, cr2, cr3, &
         cr4, cy1, cy2, cy3, cy4, soln, srce, dive, curle) 
!
      edotj(nh) = 0. 
      delefld(nh) = 0.0 
      circe = 0.0 
      totdive = 0.0 
      totchg = 0.0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
!
!     calculate current at half-time level, as it is calculated
!     in POISSON
!
            circe = circe + curle(ij) 
            totdive = totdive + dive(ij) 
            totchg = totchg + qtilde(ij) 
!
!
!                                                        calculate chang
!
         end do 
      end do 
      totdivel = 0.0 
      do j = 2, ny 
         ij = (j - 1)*nxp + 1 
         totdivel = totdivel + dive(ij) 
      end do 
!
      totdiver = 0.0 
      do j = 2, ny 
         ij = j*nxp 
         totdiver = totdiver + dive(ij) 
      end do 
!
!
      efld = 0.0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
!
            efld = efld + 0.25*(ex(ipj)**2+ey(ipj)**2+ex(ipjp)**2+ey(ipjp)**2+&
               ex(ijp)**2+ey(ijp)**2+ex(ij)**2+ey(ij)**2)*vol(ij) 
!
            jx1 = jxtil(ipj) + 0.5*opsq(ipj)*ex(ipj)*dt 
            jy1 = jytil(ipj) + 0.5*opsq(ipj)*ey(ipj)*dt 
!
            jx2 = jxtil(ipjp) + 0.5*opsq(ipjp)*ex(ipjp)*dt 
            jy2 = jytil(ipjp) + 0.5*opsq(ipjp)*ey(ipjp)*dt 
!
            jx3 = jxtil(ijp) + 0.5*opsq(ijp)*ex(ijp)*dt 
            jy3 = jytil(ijp) + 0.5*opsq(ijp)*ey(ijp)*dt 
!
            jx4 = jxtil(ij) + 0.5*opsq(ij)*ex(ij)*dt 
            jy4 = jytil(ij) + 0.5*opsq(ij)*ey(ij)*dt 
!
            edotj(nh) = edotj(nh) + 0.25*(jx1*ex(ipj)+jy1*ey(ipj)+jx2*ex(ipjp)+&
               jy2*ey(ipjp)+jx3*ex(ijp)+jy3*ey(ijp)+jx4*ex(ij)+jy4*ey(ij))*vol(&
               ij)*dt 
!
         end do 
      end do 
!
      efld = 0.5*efld 
!
!     store history data
!
      ta(nh) = t 
      fldnrg(nh) = efld 
      if (nh > 1) delefld(nh) = fldnrg(nh) - fldnrg(nh-1) 
!
      do is = 1, nsp 
         parnrg((is-1)*nhst+nh) = totk(is) 
      end do 
!
      return  
   50 format(' t=',1p,e12.5,' cycle',i5,' efield e ,tot kin e ',2(1p,e14.7),3x,&
         'totq = ',1p,e14.7,' tote= ',1p,e10.3,'number ',i6,'variance ',1p,e&
         10.3) 
      return  
      end subroutine global_iec
