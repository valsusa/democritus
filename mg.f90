 
      subroutine mgpre(ef,df,cf,bf,a,b,c,d,e,                           &
     &                 eh,dh,ch,bh,ag,bg,cg,dg,eg,                      &
     &                  x,y,nxm,nym,igridn)
!
!   This subroutine solves, A x = y for the vector x
!   using a multigrid correction scheme.  A simple
!   additive correction method is used to construct
!   the coares grid operators.
!
!   restriction (mgrest) and prolongation (mgprol) are
!   both piece-wise constant
!
!   the smoother (relax) is a multipass damped Jacobi
!
!   the fine grid operator is a 9-point stencil and stored
!   in a, b, c, d, e, ef, df, cf, bf
!
!   the coarse grid operators are stored in
!   in ah, bh, ch, dh, eh, eg, dg, cg, bg.
!   These coefficients have been constructed in acoarse.f
!   before entering this routine.
!
!
!     nxm = grid dimension in x
!     nym = grid dimension in x
!     igridn = number of grid levels
!
!    common blocks
!
       include 'impl.inc'
       include 'mgdat.h'
!
!
!
!
!    input variables
!
      real*8 ef(*),df(*),cf(*),bf(*),a(*),b(*),c(*),d(*),e(*),          &
     &     eh(*),dh(*),ch(*),bh(*),ag(*),bg(*),cg(*),dg(*),eg(*),       &
     &     x(*), y(*)
!
      integer nxm, nym,  igridn
!
!   local
!
!   set up storage for local linear residual (yy) and correction (xx)
!   these need enough storage for a copy on each grid.
!   This number is ntot
!
      dimension xx(ntotd),                                              &
     &          yy(ntotd),                                              &
     &          wrk(nxm*nym), rn(nxm*nym)
!
!
      integer igrid, izero, itest , ntot
!
! ***************************************************
!
      imgsol = 0
      ntot = ntotd
!
!
      if(igridn .eq. 1)then
       itest = 1
      else
       itest = 0
      endif
!
!
!   local solution vector is xx, local r.h.s. is yy
!
!   initialize
!
      do 3 i = 1,ntot
       xx(i) = 0.00
       yy(i) = 0.00
 3    continue
!
!   put fine grid r.h.s. into local linear residual vector
!
      do 5 i = 1,nxm*nym
       ii = istart(1) + i - 1
       yy(ii) = y(i)
 5    continue
!
!
!  Start V-cycle
!
      do 200 ivcyc = 1,1
!
!  Cycle down to coasest grid, igrid = igridn
!
      do 10 igrid = 1,igridn
!
!
      if(igrid .ne. igridn .or. itest .eq. 1) then
!
!   Relax error on grid number igrid
!
      izero = 1
      if(ivcyc .gt. 1 .and. igrid .eq. 1) izero = 0
!
      if(igrid .eq. 1) then
!
        call relax(ef,df,cf,bf,a,b,c,d,e,                               &
     &  xx(istart(igrid)),yy(istart(igrid)),                            &
     &  nxv(igrid),nyv(igrid),izero,igrid)
!
       if(itest .eq. 1) goto 201
!
      else
!
        call relax(eh(istartm(igrid)),dh(istartm(igrid))                &
     &  ,ch(istartm(igrid)),bh(istartm(igrid)),ag(istartm(igrid)),      &
     &   bg(istartm(igrid)),cg(istartm(igrid)),dg(istartm(igrid)),      &
     &   eg(istartm(igrid)),                                            &
     &  xx(istart(igrid)),yy(istart(igrid)),                            &
     &  nxv(igrid),nyv(igrid),izero,igrid)
!
      endif
!
!
!
!   Evaluate updated R.H.S. (ie yy)
!
!   wrk = A xx
!
        if (igrid .eq. 1) then
!
          call matmul9u(ef,df,cf,bf,a,b,c,d,e,                          &
     &              nxm,nym,xx(istart(igrid)),wrk)
!
        else
!
          call matmul9u(eh(istartm(igrid)),dh(istartm(igrid)),          &
     &     ch(istartm(igrid)),bh(istartm(igrid)),                       &
     &     ag(istartm(igrid)),bg(istartm(igrid)),cg(istartm(igrid)),    &
     &     dg(istartm(igrid)),eg(istartm(igrid)),                       &
     &     nxv(igrid),nyv(igrid),xx(istart(igrid)),wrk)
!
        endif
!
!   yy = yy - A xx = yy - wrk
!
        call vecad2(nxv(igrid)*nyv(igrid),                              &
     &            -1.0,wrk,1.0,yy(istart(igrid)))
!
!   restrict R.H.S ( i.e. yy ) down a grid
!
!    yy_c = R * yy_f
!
        call mgrest(yy(istart(igrid+1)),                                &
     &              nxv(igrid+1),nyv(igrid+1),                          &
     &           wrk,nxv(igrid),nyv(igrid))
!
!
      else
!
!   solve for error on igrid = igridn (i.e. xx_coarse)
!
!
!
        call relax(eh(istartm(igrid)),dh(istartm(igrid))                &
     &  ,ch(istartm(igrid)),bh(istartm(igrid)),ag(istartm(igrid)),      &
     &   bg(istartm(igrid)),cg(istartm(igrid)),dg(istartm(igrid)),      &
     &   eg(istartm(igrid)),                                            &
     &  xx(istart(igrid)),yy(istart(igrid)),                            &
     &  nxv(igrid),nyv(igrid),1,igrid)
!
!
!
!
!    prolongate error (i.e. xx) on grid igridn to grid igridn-1
!
!    wrk = P * xx_coarse
!
        call mgprol(wrk,nxv(igrid-1),nyv(igrid-1),                      &
     &           xx(istart(igrid)),nxv(igrid),nyv(igrid))
!
!    update existing error on grid igridn-1 (i.e. xx_(igridn-1))
!
!    xx_(igridn-1) = xx_(igridn-1) + wrk
!
        call vecad2(nxv(igrid-1)*nyv(igrid-1),                          &
     &            1.0,xx(istart(igrid-1)),1.0,wrk)
!
 
      endif
!
 10   continue
!
!
!    Cycle back up to grid number igridn updating errors (xx)
!    with fixed R.H.S. (yy)
!
      do 20 igrid = igridn-1,1,-1
!
!
!   relax updated error on igrid (i.e. xx_igrid)
!
      if(igrid .eq. 1) then
!
        call relax(ef,df,cf,bf,a,b,c,d,e,                               &
     &  xx(istart(igrid)),yy(istart(igrid)),                            &
     &  nxv(igrid),nyv(igrid),0,igrid)
!
      else
!
        call relax(eh(istartm(igrid)),dh(istartm(igrid))                &
     &  ,ch(istartm(igrid)),bh(istartm(igrid)),ag(istartm(igrid)),      &
     &   bg(istartm(igrid)),cg(istartm(igrid)),dg(istartm(igrid)),      &
     &   eg(istartm(igrid)),                                            &
     &  xx(istart(igrid)),yy(istart(igrid)),                            &
     &  nxv(igrid),nyv(igrid),0,igrid)
!
      endif
!
!
       if( igrid .ne. 1 )then
!
!    prolongate updated error  (i.e. xx) to igrid - 1
!
!    wrk = P * xx_igrid
!
        call mgprol(wrk,nxv(igrid-1),nyv(igrid-1),                      &
     &           xx(istart(igrid)),nxv(igrid),nyv(igrid))
!
!
!    update existing error on grid igrid+1 (i.e. xx_igrid-1)
!
!    xx_igrid-1 = xx_igrid-1 + wrk
!
        call vecad2(nxv(igrid-1)*nyv(igrid-1),                          &
     &            1.0,xx(istart(igrid-1)),1.0,wrk)
!
       endif
!
 20   continue
!
 200  continue
 201  continue
!
!
!   put updated solution into fine grid solution vector
!
      do 50 i = 1,nxm*nym
       ii = istart(1) + i - 1
       x(i) = xx(ii)
 50   continue
!
      return
      end


!
!deck relax
!
!
!
      subroutine relax(ef, df, cf, bf, a, b, c, d, e, x, y, nxm, nym, izero, &
         igrid) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: nxm 
      integer  :: nym 
      integer , intent(in) :: izero 
      integer , intent(in) :: igrid 
      real(double)  :: ef(*) 
      real(double)  :: df(*) 
      real(double)  :: cf(*) 
      real(double)  :: bf(*) 
      real(double)  :: a(*) 
      real(double)  :: b(*) 
      real(double)  :: c(*) 
      real(double)  :: d(*) 
      real(double)  :: e(*) 
      real(double)  :: x(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nn, nsweep, j, i 
      real(double), dimension(nxm*nym) :: res 
      real(double) :: diag, omega 
!-----------------------------------------------
!
!
!
!    local variables
!
!
!
! ***************************************************
!
!
      nn = nxm*nym 
!
!
      if (izero == 1) then 
!
         do j = 1, nn 
            x(j) = 0.00 
         end do 
!
      endif 
!
!
      if (igrid == 1) then 
         nsweep = 3 
      else 
         nsweep = 3 
      endif 
!
      do j = 1, nsweep 
!
!     compute  res = A*x_current
!
         call matmul9u (ef, df, cf, bf, a, b, c, d, e, nxm, nym, x, res) 
!
         do i = 1, nn 
!
            diag = abs(ef(i)) + abs(df(i)) + abs(cf(i)) + abs(bf(i)) + abs(b(i)&
               ) + abs(c(i)) + abs(d(i)) + abs(e(i)) 
!
            diag = max(a(i),diag) 
!
            omega = 0.33 
            x(i) = omega*x(i) + (1. - omega)*(y(i)-(res(i)-a(i)*x(i)))/diag 
!
         end do 
!
!
      end do 
!
!
      return  
      end subroutine relax 


!
! deck mgrest
!
      subroutine mgrest(xc, nxc, nyc, xf, nxf, nyf) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nxc 
      integer , intent(in) :: nyc 
      integer , intent(in) :: nxf 
      integer , intent(in) :: nyf 
      real(double) , intent(out) :: xc(nxc*nyc) 
      real(double) , intent(in) :: xf(nxf*nyf) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ic, if, imatc, imatf, nrow, j 
!-----------------------------------------------
!
!
!     local variables
!
!
! ******************************************************************
!
!
      do imatc = 1, nxc*nyc 
!
         do j = 1, nyc 
            if (float(j) <= float(imatc)/float(nxc)) cycle  
            nrow = j 
            exit  
         end do 
!
         imatf = 2*imatc + (nrow - 1)*nxf 
!
         xc(imatc) = xf(imatf) + xf(imatf-1) + xf(imatf+nxf) + xf(imatf+nxf-1) 
!
      end do 
!
      return  
      end subroutine mgrest 


!
! deck mgprol
!
      subroutine mgprol(xf, nxf, nyf, xc, nxc, nyc) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nxf 
      integer , intent(in) :: nyf 
      integer , intent(in) :: nxc 
      integer , intent(in) :: nyc 
      real(double) , intent(out) :: xf(nxf*nyf) 
      real(double) , intent(in) :: xc(nxc*nyc) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ic, if, imatc, imatf, nrow, j 
!-----------------------------------------------
!
!
!     local variables
!
!
!
!
!
! ******************************************************************
!
!
!
!    piece-wise constant prolongation
!
      do imatc = 1, nxc*nyc 
!
         do j = 1, nyc 
            if (float(j) <= float(imatc)/float(nxc)) cycle  
            nrow = j 
            exit  
         end do 
!
         imatf = 2*imatc + (nrow - 1)*nxf 
!
         xf(imatf) = xc(imatc) 
         xf(imatf-1) = xc(imatc) 
         xf(imatf+nxf) = xc(imatc) 
         xf(imatf+nxf-1) = xc(imatc) 
!
!
      end do 
!
      return  
      end subroutine mgprol 


!
!
!deck vecad2
!
      subroutine vecad2(nx, cc1, x, cc2, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      real(double) , intent(in) :: cc1 
      real(double) , intent(in) :: cc2 
      real(double) , intent(inout) :: x(nx) 
      real(double) , intent(in) :: y(nx) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
!-----------------------------------------------
!
!
! ***************************************************
!
      do i = 1, nx 
!
         x(i) = cc1*x(i) + cc2*y(i) 
!
      end do 
!
!
      return  
      end subroutine vecad2 
!
!
      subroutine acoarse(a,b,c,d,e,af,bf,cf,df,ef,                      &
     &                   ag,bg,cg,dg,eg,ah,bh,ch,dh,eh,                 &
     &                   nxf,nyf,igridn)
!
!  this is a routine to build coarse grid matracies
!  from fine grid matracies, for a 9 point 2-D
!  poission solver.
!
!  fine grid is in a,b,c,d,e,af,bf,cf,df,ef
!
!  coarse grids are in ag,bg,cg,dg,eg,ah,bh,ch,dh,eh
!
!    common blocks
!
       include 'impl.inc'
       include 'mgdat.h'
!
!     input variables
!
      dimension a(*),b(*),c(*),d(*),e(*),af(*),bf(*),cf(*),             &
     &          df(*),ef(*),                                            &
     &          ag(*),bg(*),cg(*),dg(*),eg(*),ah(*),bh(*),ch(*),        &
     &          dh(*),eh(*)
!
!
      integer nxf, nyf
!
!     local variables
!
!
       integer ic,if,igridl,jc,jf,                                      &
     &         nxc, nyc,                                                &
     &         imatc, imatf, nrow, i, j,                                &
     &         nxfl, nyfl
!
!
!
! ******************************************************************
!
      rdiv=1.0
      if(igridn .eq. 1) goto 1000
!
      nxc = nxv(2)
      nyc = nyv(2)
!
!
! first coarse grid operator is built from a-e and bf-ef
!
      do 10 i = 1,nxc*nyc
!
       imatc = i + istartm(2) - 1
!
       do 5 j = 1,nyc
        if(float(j) .gt. (float(i)/float(nxc)))then
         nrow= j
         goto 6
        endif
 5     continue
 6     continue
!
       imatf = 2*i + (nrow-1)*nxf -1
!
       ag(imatc) = (a(imatf) + b(imatf) + d(imatf) + e(imatf)           &
     &   + a(imatf+1) + bf(imatf+1) + c(imatf+1) + d(imatf+1)           &
     &   + a(imatf+nxf) + b(imatf+nxf) + df(imatf+nxf) + cf(imatf+nxf)  &
     &   + a(imatf+nxf+1) + bf(imatf+nxf+1) +                           &
     &     df(imatf+nxf+1) + ef(imatf+nxf+1) )*rdiv
!
       bg(imatc) =                                                      &
     &    ( b(imatf+1) + e(imatf+1)                                     &
     &   + b(imatf+nxf+1) + cf(imatf+nxf+1) )*rdiv
!
       eg(imatc) =                                                      &
     &      e(imatf+nxf+1)*rdiv
!
       ch(imatc) =                                                      &
     &   cf(imatf+1)*rdiv
!
       bh(imatc) = (bf(imatf) + c(imatf)                                &
     &   + bf(imatf+nxf) + ef(imatf+nxf))*rdiv
!
       eh(imatc) =  ef(imatf)*rdiv
!
       cg(imatc) =                                                      &
     &    c(imatf+nxf) *rdiv
!
       dg(imatc) =                                                      &
     &   ( d(imatf+nxf) + e(imatf+nxf)                                  &
     &   + d(imatf+nxf+1) + c(imatf+nxf+1) )*rdiv
!
       dh(imatc) =  (df(imatf) + cf(imatf)                              &
     &   + df(imatf+1) + ef(imatf+1) )*rdiv
!
 10   continue
!
! generate igrid = 3 down to igrid = igridn coarse grids
! from ag-eg and bh - eh.
!
      do 100 igrid = 3,igridn
!
       nxfl = nxc
       nyfl = nyc
       nxc = nxv(igrid)
       nyc = nyv(igrid)
!
!
      do 110 i = 1,nxc*nyc
!
       imatc = i + istartm(igrid) - 1
!
       do 50 j = 1,nyc
        if(float(j) .gt. (float(i)/float(nxc)))then
         nrow= j
         goto 60
        endif
 50    continue
 60    continue
!
       imatf = 2*i + (nrow-1)*nxfl -1 + istartm(igrid-1) - 1
!
       ag(imatc) = (ag(imatf) + bg(imatf) + dg(imatf) + eg(imatf)       &
     &   + ag(imatf+1) + bh(imatf+1) + cg(imatf+1) + dg(imatf+1)        &
     &   + ag(imatf+nxfl) + bg(imatf+nxfl) +                            &
     &     dh(imatf+nxfl) + ch(imatf+nxfl)                              &
     &   + ag(imatf+nxfl+1) + bh(imatf+nxfl+1) +                        &
     &     dh(imatf+nxfl+1) + eh(imatf+nxfl+1))*rdiv
!
       bg(imatc) =                                                      &
     &    ( bg(imatf+1) + eg(imatf+1)                                   &
     &   + bg(imatf+nxfl+1) + ch(imatf+nxfl+1) )*rdiv
!
       eg(imatc) =                                                      &
     &      eg(imatf+nxfl+1)*rdiv
!
       ch(imatc) =                                                      &
     &   ch(imatf+1)*rdiv
!
       bh(imatc) = ( bh(imatf) + cg(imatf)                              &
     &   + bh(imatf+nxfl) + eh(imatf+nxfl) )*rdiv
!
       eh(imatc) =  eh(imatf)*rdiv
!
       cg(imatc) =                                                      &
     &    cg(imatf+nxfl)*rdiv
!
       dg(imatc) =                                                      &
     &    (dg(imatf+nxfl) + eg(imatf+nxfl)                              &
     &   + dg(imatf+nxfl+1) + cg(imatf+nxfl+1))*rdiv
!
       dh(imatc) =  (dh(imatf) + ch(imatf)                              &
     &   + dh(imatf+1) + eh(imatf+1))*rdiv
!
 110  continue
 100  continue
 1000 continue
!
      return
      end
!
      subroutine mgload(nxm,nym,igridn)
       include 'impl.inc'
      include 'mgdat.h'
!      write(*,*)'nxm,nym=',nxm,nym
!      igridn=ngrid
!      igridn=6
      igridn=int(log(min(nxm,nym)-1.d0)/log(2.d0))
       igridn=5
      write(*,*)'igridn=',igridn
      nxv(1) = nxm
      nyv(1) = nym
      istart(1) = 1
      istartm(1) = 1
      istartm(2) = 1
 
      do i = 2,igridn
       nxv(i) = nxv(i-1)/2
       nyv(i) = nyv(i-1)/2
       istart(i) = istart(i-1) + nxv(i-1)*nyv(i-1)
       if(i .gt. 2)then
        istartm(i) = istartm(i-1) + nxv(i-1)*nyv(i-1)
       endif
      enddo
      return
      end
