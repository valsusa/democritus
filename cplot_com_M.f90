      module cplot_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
      integer :: iplot, cdfid, timdim, idtime, idcyc, idnx, idny, idu, idv, idw&
         , iddx, iddy, iddz, idrho, idpot, idex, idey, idqdn, idjxs, idjys, &
         idnumber, iddiegrid, iduhst, idvhst, cdfidh, timdimh, idtimeh, idxmom&
         , idymom, idqtotal, idntotal, idqdust, idpdust, idxmdust, idymdust, &
         idqdustp, idxmdustp, idymdustp, cdfidp, idtimep, idcycp, timdimp, &
         idnxp, idnyp, idxp, idyp, idnumtot, idup, idvp, idwp, idcp, idqp,&
         idtd,idforcespx,idforcespy,idfdragx,idfdragy,idfeletx,idfelety, &
         idinjtop,idinjbottom,idinjleft,idinjright,idemitted, idefpx, idefpy
      end module cplot_com_M 
     
      module work_variables
      use vast_kind_param, only:  double 
      use celest2d_com_M 
      real(double), dimension(npart) :: pxtmp 
      real(double), dimension(nsp) :: pnsp 
      real(double), dimension(npart) :: pxtmp2 
      real(double), dimension(nxyp) :: plotx, ploty 
      end module work_variables