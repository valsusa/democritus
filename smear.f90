      subroutine smear(nx, ny, qtilde, number, nsp, nsp_dust, nxyp, mask, vol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer , intent(in) :: nsp 
      integer , intent(in) :: nsp_dust 
      integer , intent(in) :: nxyp 
      real(double) , intent(inout) :: qtilde(*) 
      real(double) , intent(in) :: number(nxyp,*) 
      real(double) , intent(inout) :: mask(*) 
      real(double) , intent(in) :: vol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, nyp, j, i, ij, is 
      real(double) :: qtot, vtot, qavg 
!-----------------------------------------------
!
      nxp = nx + 1 
      nyp = ny + 1 
!
!     qtot=0.
!     do j=2,ny
!     do i=2,nx
!     ij=(j-1)*nxp+i
!     qtot=qtot+mask(ij)*vol(ij)*qtilde(ij)
!     enddo
!     enddo
!     write(*,*)'pre-smear',qtot
!
      do j = 1, ny + 1 
         do i = 1, nx + 1 
            ij = (j - 1)*nxp + i 
            mask(ij) = 0. 
            do is = nsp_dust, nsp 
               if (number(ij,is) <= 0.5) cycle  
               mask(ij) = 1. 
            end do 
         end do 
      end do 
!
      qtot = 0. 
      vtot = 0. 
      do j = 1, ny + 1 
         do i = 1, nx + 1 
            ij = (j - 1)*nxp + i 
            qtot = qtot + mask(ij)*vol(ij)*qtilde(ij) 
            vtot = vtot + mask(ij)*vol(ij) 
         end do 
      end do 
      qavg = qtot/(vtot + 1.E-10) 
      do j = 1, ny + 1 
         do i = 1, nx + 1 
            ij = (j - 1)*nxp + i 
            qtilde(ij) = qavg*mask(ij) + (1. - mask(ij))*qtilde(ij) 
         end do 
      end do 
!
!     diagnostics
!
!     qtot=0.
!     do j=2,ny
!     do i=2,nx
!     ij=(j-1)*nxp+i
!     qtot=qtot+mask(ij)*vol(ij)*qtilde(ij)
!     enddo
!     enddo
!     write(*,*)'post-smear',qtot
 
      return  
      end subroutine smear 
