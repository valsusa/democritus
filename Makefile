ARCH =gfortran
                                                                                
ifeq ($(ARCH),gfortran)
                                                                                
#FRANKLIN PGI
F90= gfortran
LD= gfortran
LDFLAGS = -L/usr/local/lib -L/usr/lib -L/usr/local/Cellar/netcdf-fortran/4.6.1/lib -L /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib
FFLAGS =       -O -fdefault-real-8 -fdefault-double-8 -fallow-argument-mismatch
#-fno-underscoring
endif

ifeq ($(ARCH),linux)
                                                                                
#LAHEY - linux
F90= lf95
LD= lf95
FFLAGS =       -O --dbl
endif                                                                                

ifeq ($(ARCH),intel)

#INTEL 
F90= ifort
LD= ifort
LDFLAGS = -pg
#FFLAGS =  -nowarn  -g -r8  -assume 2underscores -vec-report1
#FFLAGS = -nowarn  -O3 -xW -assume 2underscores -vec-report3
FFLAGS = -nowarn  -O3  #-assume 2underscores 
#-check all
                 
endif
    
ifeq ($(ARCH),g95)

#GNU G95
F90= g95
LD= g95
LDFLAGS =    -L/usr/lib 
FFLAGS =   -O3 -r8  -fno-second-underscore
#FFLAGS =   -r8 -O3

endif 

ifeq ($(ARCH),mac)                                                                     
#ABSOFT - MAC
F90= f95
LD= f95
LDFLAGS = -P
FFLAGS =  -w  -O3 -N113  -YEXT_NAMES=LCS -YEXT_SFX=_ 
#FFLAGS =  -w  -Rs -N113  -YEXT_NAMES=LCS -YEXT_SFX=_ 
                                                               
endif

# ol lahey computers
#LIBS = libnetcdf.a
# G4 laptop
# LIBS =  /Users/lapenta/Desktop/stuff/netcdf-3.6.0-p1/lib/libnetcdf.a  /Developer/SDKs/MacOSX10.4.0.sdk/usr/lib/libSystemStubs.a
# For IGPP MACS use below
# LIBS=/sw/lib/libnetcdf.a
#for laptop 
#LIBS = /Users/gianni/Documents/fortran/netcdf-3.6.1/lib/libnetcdf.a  /Developer/SDKs/MacOSX10.4u.sdk/usr/lib/libSystemStubs.a
#for office pc in leuven
#LIBS = /usr/local/netcdf/lib/libnetcdf.a
#LIBS = /Users/gianni/Documents/fortran/netcdf-3.6.1/lib/libnetcdf.a
# For IGPP MACS use below
#LIBS=/sw/lib/libnetcdf.a
# For ulisse
#LIBS=/usr/lib/libnetcdf.a
#Johanan @ KU Leuven
#LIBS=-lnetcdf

LIBS = -lnetcdff

CMD =	democritus
 
#sostituito parmove2 con parmove_dust
# usare parmove_dust per fare simulazioni dust
# usare parmove_rick per fare simulazioni rick
MAIN_SOURCE	=   \
        vast_kind_param_M.f90 \
        celest2d_com_M.f90  impl_M.f90\
	cplot_com_M.f90 netcdf_M.f90 emission_com_M.f90\
	cel2d.f90 \
         begin.f90 \
        cgper1.f90 \
      etilde.f90 \
         fuvector.f90\
      magnetic.f90 \
      moments.f90 \
      parcel.f90 \
     parmovki.f90 \
      parmovfl.f90  \
      mover_e.f90 immersed_object.f90\
	parmove_iec.f90 \
      poisson.f90 smear.f90\
		sgrid8x.f90 \
		sgeo.f90 \
	toolbin.f90 \
	global.f90 global_iec.f90 \
	grad.f90 locator.f90 loclib2.f90 \
	rinj_ToBoRi.f90 \
	rinj_iec.f90 \
	adapt_prescribed.f90 \
	setup.f90 \
	dumpone.f90 \
	 tronco.f90 \
        rinput.f90 \
	control.f90 \
	emission.f90 partialpush.f90\
	bincrea.f90 ncdout.f90 nersc.f90 generate.f90 
# Removed erf and derf, not working properly
#      parmove_nim_boris_ben.f \
#	 rinj_new.f 

OBJECTS =  $(MAIN_SOURCE:%.f90=%.o) 

.SUFFIXES:
.SUFFIXES: .o .f .f90

.PHONY: clean tar

.f90.o:
	$(F90) -c $(FFLAGS) $<

.f.o:    
	$(F90) -c $(FFLAGS) $<

democritus:	$(OBJECTS)  
	$(LD) -o democritus $(LDFLAGS) $(OBJECTS) $(LIBS)
 
clean:       
	rm -f xcel *.o *.mod core *.dat fort.* *.out democritus

