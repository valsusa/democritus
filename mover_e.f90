subroutine parmove(niteration)
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
	  use mover_data
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
	  integer :: niteration
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------

!(Leo)
! niteration : subcycling iteration in the current time step
! dtprec : precedent time step of the current particle
! dtX, dtV : timestep for the Drift and Kick
! Omega : cyclotron angular frequency
! nctheta : angular slice strike by the current particle, if it crosses the
! probe
! qhere : charge of the particle that crosses the probe
!-----------------------------------------------
! $Header: /n/cvs/democritus/mover_e.f90,v 1.2 2006/11/21 20:34:51 lapenta Exp $
!
! $Log: mover_e.f90,v $
! Revision 1.2  2006/11/21 20:34:51  lapenta
! Gianni: modifications to have 2 dust particles
!
! Revision 1.1  2006/07/27 22:50:23  lpatacchini
!
! New version with major changes :
! Modified the reinjection at the right boundaries in order to support high magnetic fields without creating a spurious surdensity
! Added a subcycling feature (Controlled by nsubmax in itape) allowing to subcycle the ions and electrons according to their distance to the probe.
! Modified the mover, so that we don't need anymore to resolve the Larmor frequency. Now the time-step only needs to resolve the plasma oscillations.
! Added angular distribution of collected particle diagnostic (thetadim in itape).
!
! Revision 1.2  2006/06/14 16:49:35  lapenta
! updated lib_beniamino
!
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!     ------------------------------------------------------
!     ------------------------------------------------------
!
!     PARMOVE: a routine to advance the particle eqs. of motion
!              using leapfrog differencing
!
!     ------------------------------------------------------
!     ------------------------------------------------------
!
 
      iphd2(1) = iphead(1) 
      ipsrch = 0 
 
!       Define various parameters
      pi = acos(-1.D0) 
      twopi = 2.D0*pi 

      mfact=dabs(qom(2)/qom(1)) 
!      if(ncyc==1) print*, 'mfact',mfact

      call paramcoll2 (evfact, ctype, ntarld, mratio, vthn, siep(2),&
                        mfact) 

!
!     initialize diagnostics variables
!
      nh = mod(ncyc,nhst)  
	  call initialize_particle_diagnostics(niteration)
!
!
      if (nh > 1) enrglost(nh) = enrglost(nh-1) 
 
      kmax = 2*(nx + ny - 2) + 1 
      nlostr = 0 
      nlostl = 0 
      nlost = 0 

      velmax = 0.
      quale = 0.
	
      do j = 2, ny
 
         do i = 2, nx
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
			!
            !
            np = iphead(ij) 
            if (np == 0) cycle  
  100       continue

! Construct the list of links (nploc) for the present cell
            call listmkr (np, nploc, link, lvmax, ntmp) 
!
            do lv = 1, lvmax

               iold(lv) = int(xp(nploc(lv))) 
               jold(lv) = int(yp(nploc(lv))) 
               inew(lv) = iold(lv)
               jnew(lv) = jold(lv)
               the(lv) = xp(nploc(lv)) - iold(lv) 
               zeta(lv) = yp(nploc(lv)) - jold(lv) 
               xpv(lv) = xp(nploc(lv)) 
               ypv(lv) = yp(nploc(lv)) 
               upv(lv) = up(nploc(lv)) 
               vpv(lv) = vp(nploc(lv)) 
               wpv(lv) = wp(nploc(lv)) 
               qptmp(lv) = qpar(nploc(lv))

               xpc(lv)=xpf(nploc(lv))
               ypc(lv)=ypf(nploc(lv))

!     calculate the properties of the particles
!
               isp(lv) = ico(nploc(lv)) 
               xtp0(lv) = xpf(nploc(lv)) 
               ytp0(lv) = ypf(nploc(lv)) 
               massp(lv) = qpar(nploc(lv))/qom(isp(lv))
             


! Decides which particle's turn it is        
               move(lv) = (niteration.le.nsub(nploc(lv)))

!     interpolate the electric field to the particle position
!     Only if it's the particle's turn
               if (.not.move(lv)) cycle

               w1(lv) = the(lv)*(1. - zeta(lv)) 
               w2(lv) = the(lv)*zeta(lv) 
               w3(lv) = (1. - the(lv))*zeta(lv) 
               w4(lv) = (1. - the(lv))*(1. - zeta(lv)) 
!
               expar(lv) = w1(lv)*ex(ipj) + (w2(lv)*ex(ipjp)+(w3(lv)*ex(ijp)+w4&
                  (lv)*ex(ij))) 
!c
               eypar(lv) = w1(lv)*ey(ipj) + (w2(lv)*ey(ipjp)+(w3(lv)*ey(ijp)+w4&
                  (lv)*ey(ij))) 
!
!
               bxpar(lv) = w1(lv)*bx(ipj) + (w2(lv)*bx(ipjp)+(w3(lv)*bx(ijp)+w4&
                  (lv)*bx(ij))) 
!c
               bypar(lv) = w1(lv)*by(ipj) + (w2(lv)*by(ipjp)+(w3(lv)*by(ijp)+w4&
                  (lv)*by(ij))) 
!c
               bzpar(lv) = w1(lv)*bz(ipj) + (w2(lv)*bz(ipjp)+(w3(lv)*bz(ijp)+w4&
                  (lv)*bz(ij))) 

            enddo

! Calculate the dt for the particle advance


            do lv=1,lvmax


               upv1(lv) = upv(lv) 
               vpv1(lv) = vpv(lv) 
               wpv1(lv) = wpv(lv)

               if (.not.move(lv)) cycle

               dtprec=dtsub(nploc(lv))
               dtX=dt/nsub(nploc(lv))
               dtV=0.5*(dtprec+dtX)
               dtsub(nploc(lv))=dtX

               qdt = qpar(nploc(lv))*dtV 
               qomdt = qom(isp(lv))*dtV
! The minus is to get the same rotation direction as the old routine
               Omega=-qom(isp(lv))*bypar(lv)
               cosomdt=cos(Omega*dtX)
               sinomdt=sin(Omega*dtX)
!
!     calculate the new particle velocity
!
!
!     add correction term for motion in cylindrical coordinates
!
!      bzpar(lv)=bzpar(lv)
!     &     -cyl*wp(nploc(lv))/(xpf(nploc(lv))*qom(isp(lv))+1e-10)
!
!      qomdt=qom(isp(lv))*dt
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
               
 
               if (collision) then 
 
!     TO ADD MCC COLLISIONS
 
                  if (qdt<0.D0 .and. isp(lv)/=nsp_dust) then 
 
                     call mccen (mratio, massp(lv), upv1(lv), vpv1(lv), wpv1(lv&
                        ), ntarld, dt, twopi, evfact) 
		  elseif(qdt>0.d0 .and. isp(lv)/=nsp_dust) then 
                     Mneut=massp(lv)
                      call  MCCin(massp(lv),Mneut,upv1(lv),vpv1(lv),wpv1(lv),&
                      vthn,ntarLd,dt,twopi,ctype)
                  endif 
                  if (.not.boris) then 
                     write (6, *) 'modify mover with boris' 
                     stop  
                  endif 
 
                  call borismover (upv1(lv), vpv1(lv), wpv1(lv), expar(lv), eypar(&
                       lv), 0.D0, bxpar(lv), bypar(lv), bzpar(lv), qomdt) 

               endif
               
               qo2mdt = qomdt*0.5D0 
!

! If not electrostatic, use the standard leapfrog


               if ((.not.estatic).or.(abs(Omega).le.0.01/dtX).or.(.not.leonardo)) then



!     The electric field part I
                  upv(lv) = upv(lv) + qo2mdt*expar(lv) 
                  vpv(lv) = vpv(lv) + qo2mdt*eypar(lv) 
                  wpv(lv) = wpv(lv) 
                  
                  
                  
                  bpar2 = bxpar(lv)**2 + bypar(lv)**2 + bzpar(lv)**2 
                  denom = 1D0/(1. + bpar2*qo2mdt**2) 
                  coef1 = 1.D0 - bpar2*qo2mdt**2 
                  udotb = upv(lv)*bxpar(lv) + vpv(lv)*bypar(lv) + wpv(lv)*bzpar(lv&
                  ) 
                  
                  upv1(lv) = (upv(lv)*coef1+qomdt*(vpv(lv)*bzpar(lv)-wpv(lv)*bypar&
                       (lv)+qo2mdt*(udotb*bxpar(lv))))*denom 
                  
                  vpv1(lv) = (vpv(lv)*coef1+qomdt*(wpv(lv)*bxpar(lv)-upv(lv)*bzpar&
                       (lv)+qo2mdt*(udotb*bypar(lv))))*denom 
                  
                  wpv1(lv) = (wpv(lv)*coef1+qomdt*(upv(lv)*bypar(lv)-vpv(lv)*bxpar&
                       (lv)+qo2mdt*(udotb*bzpar(lv))))*denom 

          !     The electric field strikes back
          !
          !
                  upv1(lv) = upv1(lv) + qo2mdt*expar(lv) 
        
                  vpv1(lv) = vpv1(lv) + qo2mdt*eypar(lv) 


          !     calculate the new particle positions
          !
                  if (cyl==0. .or. .not.boris) then 
                     xpc(lv) = xpf(nploc(lv)) + upv1(lv)*dtX 
                  else 
                     x2p = xpf(nploc(lv)) + upv1(lv)*dtX 
                     z2p = wpv1(lv)*dtX 
                     xpc(lv) = sqrt(x2p**2 + z2p**2 + 1E-10) 
                     upvold = upv1(lv) 
                     upv1(lv) = upv1(lv)*x2p/xpc(lv) + wpv1(lv)*z2p/xpc(lv) 
                     wpv1(lv) = (-upvold*z2p/xpc(lv)) + wpv1(lv)*x2p/xpc(lv)
                  endif
               
                  ypc(lv) = ypf(nploc(lv)) + vpv1(lv)*dtX 
               
               else
! If the code is electrostatic and we have enough B, use the new mover 
                  
                  ! Kick

                  upv(lv) = upv(lv) + qomdt*expar(lv) 
                  vpv(lv) = vpv(lv) + qomdt*eypar(lv) 
 
                  ! Drift

                  upv1(lv)=upv(lv)*cosomdt+wpv(lv)*sinomdt
                  vpv1(lv)=vpv(lv)
                  wpv1(lv)=wpv(lv)*cosomdt-upv(lv)*sinomdt


                  if (cyl==0. .or. .not.boris) then 
                     xpc(lv)=xpc(lv)+(wpv(lv)-wpv(lv)*cosomdt+upv(lv)&
                          *sinomdt)/Omega
                  else 
                     x2p=xpc(lv)+(wpv(lv)-wpv(lv)*cosomdt+upv(lv)*&
                          sinomdt)/Omega
                     z2p=0+(-upv(lv)+upv(lv)*cosomdt+wpv(lv)*sinomdt)&
                         /Omega

                     xpc(lv) = sqrt(x2p**2 + z2p**2 + 1E-10) 
                     upvold = upv1(lv) 
                     upv1(lv) = upv1(lv)*x2p/xpc(lv) + wpv1(lv)*z2p/xpc(lv) 
                     wpv1(lv) = (-upvold*z2p/xpc(lv)) + wpv1(lv)*x2p/xpc(lv)
                  endif

                  ypc(lv) = ypc(lv) + vpv1(lv)*dtX 
                  
               endif

 
               if(collision) then 
                  if(dabs(upv1(lv))>velmax) then 
                     velmax=dabs(upv1(lv))
                     quale=isp(lv) 
                  endif
                  if(dabs(vpv1(lv))>velmax) then 
                     velmax=dabs(vpv1(lv))
                     quale=isp(lv) 
                  endif
                  if(dabs(wpv1(lv))>velmax) then
                     velmax=dabs(wpv1(lv))
                     quale=isp(lv) 
                  endif
               endif
          enddo
!
			call inter_p_immersed_object(niteration)
!
!     calculate the new natural coordinates of the particle. only the "move"
!     ones participate
            call mapmove (xn, yn, ij, nxp, xpc, ypc, the, zeta, 1, lvmax, move)


!dir$ ivdep

            do l = 1, lvmax 
! (Leo) 
               if (.not.move(l)) cycle

               up(nploc(l)) = upv1(l) 
               vp(nploc(l)) = vpv1(l) 
               wp(nploc(l)) = wpv1(l) 
               xp(nploc(l)) = iold(l) + the(l) 
               yp(nploc(l)) = jold(l) + zeta(l) 
               xtp0(l) = xpf(nploc(l)) 
               ytp0(l) = ypf(nploc(l)) 
               xpf(nploc(l)) = xpc(l) 
               ypf(nploc(l)) = ypc(l) 
               inew(l) = iold(l) + the(l) 
               jnew(l) = jold(l) + zeta(l) 
            end do 
!
            do lv = 1, lvmax 
!
!     check first to see whether particle has changed cells
!
               if (inew(lv) - iold(lv)==0 .and. jnew(lv)-jold(lv)==0) then 
!    for those particles that have not changed cells,
!     no further tests are required
!     return particle to original cell list
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = iphd2(ij) 
                  iphd2(ij) = nploc(lv) 
!
               else 
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = ipsrch 
                  ipsrch = nploc(lv) 
                  nlost = nlost + 1 
!
!     save old position in xp,yp arrays
!
                  xp(nploc(lv)) = xtp0(lv) 
                  yp(nploc(lv)) = ytp0(lv) 
!
!
!
               endif 
            end do 
            np = iphead(ij) 
            if (np > 0) go to 100 
!
         end do 
      end do  
!
!
		call diagnostics_particles(niteration)
!
!
!     *********************************************************
!
!     reconstruct linked lists
!
!     ***********************************************

      call locator_kin (ipsrch)

            
 
      return  
      end subroutine parmove 


 
      subroutine mccen(mratio, me, velx, vely, velz, ntarld, dt, twopi, evfact) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!
!
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: mratio 
      real(double) , intent(in) :: me 
      real(double) , intent(inout) :: velx 
      real(double) , intent(inout) :: vely 
      real(double) , intent(inout) :: velz 
      real(double) , intent(in) :: ntarld 
      real(double) , intent(in) :: dt 
      real(double) , intent(in) :: twopi 
      real(double) , intent(in) :: evfact 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vscatt, vinc 
      real(double), dimension(3) :: uinc, uscatt 
      real(double) :: kf1, kf2, kf3, coschi, sinchi, cosphi, sinphi, costhe, &
         sinthe, rand, cfqmax, ene, pcoll, crx, enev 
!-----------------------------------------------
 
      crx = 2.13D-18 !2.36D-18 
 
      ene = velx*velx + vely*vely + velz*velz 
      vinc = dsqrt(ene) + 1.D-10 
 
      ene = 0.5D0*ene*me 
 
      cfqmax = crx*ntarld*vinc 
 
      pcoll = 1.D0 - dexp((-dt*cfqmax)) 
 
!     Rand=ranf()
      call random_number (rand) 
 
      if (rand < pcoll) then 
 
 
         enev = ene*evfact + 1.D-10              ! energy in eV 
 
         uinc(1) = velx/vinc 
         uinc(2) = vely/vinc 
         uinc(3) = velz/vinc 
 
         call random_number (rand) 
!       Rand=ranf()
 
         if (rand == 0.D0) rand = rand + 1.D-8 
 
         coschi = (2.D0 + enev - 2.D0*(1.D0 + enev)**rand)/enev 
 
 
         sinchi = dsqrt(1.D0 - coschi*coschi) 
 
         call random_number (rand) 
         rand = twopi*rand                       !ranf() 
         cosphi = dcos(rand) 
         sinphi = dsin(rand) 
 
         costhe = uinc(1) 
         sinthe = dsqrt(1.D0 - costhe*costhe) + 1.D-10 
 
         kf1 = coschi 
         kf3 = sinchi/sinthe 
         kf2 = kf3*sinphi 
         kf3 = kf3*cosphi 
 
         uscatt(1) = kf1*uinc(1) + kf3*(uinc(2)*uinc(2)+uinc(3)*uinc(3)) 
         uscatt(2) = kf1*uinc(2) + kf2*uinc(3) - kf3*uinc(1)*uinc(2) 
         uscatt(3) = kf1*uinc(3) - kf2*uinc(2) - kf3*uinc(1)*uinc(3) 
 
         ene = ene*(1.D0 - 2.D0*mratio*(1 - coschi)) 
 
         vscatt = dsqrt(2.D0/(me + 1.D-10)*ene) 
 
         velx = vscatt*uscatt(1) 
         vely = vscatt*uscatt(2) 
         velz = vscatt*uscatt(3) 
 
 
      endif 
      return  
 
      end subroutine mccen 


 
 
 
      subroutine mccin(massp, masst, velx, vely, velz, vthn, ntarld, dt, twopi&
         , ctype) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ctype 
      real(double)  :: massp 
      real(double)  :: masst 
      real(double)  :: velx 
      real(double)  :: vely 
      real(double)  :: velz 
      real(double) , intent(in) :: vthn 
      real(double) , intent(in) :: ntarld 
      real(double) , intent(in) :: dt 
      real(double)  :: twopi 
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
      integer, parameter :: chex = 1 
      integer, parameter :: scatt = 2 
      integer, parameter :: all = 3 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vxn, vyn, vzn, vrel, vrx, vry, vrz, r1, r2 
      real(double), dimension(2) :: fq 
      real(double) :: cfqmax, fqtot, xtot, pcoll 
      real(double), dimension(2) :: crx 
!-----------------------------------------------
 
      crx(1) = 2.13D-19 !8.D-19                            !charge exchange 
      crx(2) = 1.33D-19 !5.D-19                            ! scattering 
 
!     ene=velx*velx+vely*vely+velz*velz
!     ene=0.5d0*ene*Massp
 
      !generate neutral velocity:
      call random_number (r1) 
      call random_number (r2) 
!     R1=ranf()
!     R2=ranf()
      vxn = vthn*dsqrt((-2.D0*dlog(r1)))*dcos(twopi*r2) 
      vyn = vthn*dsqrt((-2.D0*dlog(r1)))*dsin(twopi*r2) 
 
      call random_number (r1) 
      call random_number (r2) 
!     R1=ranf()
!     R2=ranf()
      vzn = vthn*dsqrt((-2.D0*dlog(r1)))*dcos(twopi*r2) 
 
      !calculate i-n relative velocity
      vrx = velx - vxn 
      vry = vely - vyn 
      vrz = velz - vzn 
      vrel = dsqrt(vrx**2 + vry**2 + vrz**2) 
 
 
      if (ctype == chex) xtot = crx(1) 
      if (ctype == scatt) xtot = crx(2) 
      if (ctype == all) xtot = crx(1) + crx(2) 
 
      cfqmax = xtot*vrel*ntarld 
      pcoll = 1.D0 - dexp((-dt*cfqmax)) 
 
      call random_number (r1) 
!     R1=ranf()
 
 
      if (r1 < pcoll) then 
 
 
         fq(1) = crx(1)*ntarld*vrel 
         fq(2) = crx(2)*ntarld*vrel 
 
         if (ctype == chex) fqtot = fq(1) 
         if (ctype == scatt) fqtot = fq(2) 
         if (ctype == all) then 
            fqtot = fq(1) + fq(2) 
 
            call random_number (r2) 
 
            r2 = cfqmax*r2                       !ranf() 
 
            if (r2 <= fq(1)) call chargex (velx, vely, velz, vxn, vyn, vzn) 
 
            if (r2<=fqtot .and. r2>fq(1)) call scatter (massp, masst, vrx, vry&
               , vrz, vrel, velx, vely, velz, vxn, vyn, vzn, twopi) 
         endif 
 
         if (ctype == chex) call chargex (velx, vely, velz, vxn, vyn, vzn) 
         if (ctype == scatt) call scatter (massp, masst, vrx, vry, vrz, vrel, &
            velx, vely, velz, vxn, vyn, vzn, twopi) 
      endif 
      return  
 
!     ene=velx*velx+vely*vely+velz*velz
!     ene=0.5d0*ene*Massp
 
      end subroutine mccin 


 
!----------------------------------------------------------------------------------
      subroutine chargex(velx, vely, velz, vxn, vyn, vzn) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(out) :: velx 
      real(double) , intent(out) :: vely 
      real(double) , intent(out) :: velz 
      real(double) , intent(in) :: vxn 
      real(double) , intent(in) :: vyn 
      real(double) , intent(in) :: vzn 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
 
      velx = vxn 
      vely = vyn 
      velz = vzn 
      return  
 
      end subroutine chargex 


!----------------------------------------------------------------------------------
      subroutine scatter(massp, masst, vrx, vry, vrz, vrel, velx, vely, velz, &
         vxn, vyn, vzn, twopi) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: massp 
      real(double) , intent(in) :: masst 
      real(double) , intent(in) :: vrx 
      real(double) , intent(in) :: vry 
      real(double) , intent(in) :: vrz 
      real(double) , intent(in) :: vrel 
      real(double) , intent(out) :: velx 
      real(double) , intent(out) :: vely 
      real(double) , intent(out) :: velz 
      real(double) , intent(in) :: vxn 
      real(double) , intent(in) :: vyn 
      real(double) , intent(in) :: vzn 
      real(double) , intent(in) :: twopi 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vscatt 
      real(double), dimension(3) :: uinc, uscatt 
      real(double) :: kf1, kf2, kf3, coschi, sinchi, cosphi, sinphi, costhe, &
         sinthe, cosrel, sinrel, rand, mratio, lossfact 
!-----------------------------------------------
 
 
!     masst=massp
 
 
 
      if (massp == masst) then 
 
!     Rand=ranf()
         call random_number (rand) 
 
         if (rand == 0.D0) rand = rand + 1.D-8 
         if (rand == 1.D0) rand = rand - 1.D-8 
 
         coschi = dsqrt(1 - rand) 
         sinchi = dsqrt(rand) 
 
      else                                       !see mcdaniel 
 
         mratio = massp/masst 
 
         call random_number (rand) 
!     Rand=ranf()
         if (rand == 0.D0) rand = rand + 1.D-8 
         if (rand == 1.D0) rand = rand - 1.D-8 
 
         cosrel = 1.D0 - 2.D0*rand 
         sinrel = dsqrt(1 - cosrel*cosrel) 
!     chi=datan(sinrel/(mratio+cosrel))
 
         lossfact = 2.D0*(massp*masst)/(massp + masst)**2*(1 - cosrel) 
 
      endif 
 
      uinc(1) = vrx/(vrel + 1.D-10) 
      uinc(2) = vry/(vrel + 1.D-10) 
      uinc(3) = vrz/(vrel + 1.D-10) 
 
      call random_number (rand) 
      rand = twopi*rand                          !ranf() 
      cosphi = dcos(rand) 
      sinphi = dsin(rand) 
 
      costhe = uinc(1) 
      sinthe = dsqrt(1.D0 - costhe*costhe) + 1.D-10 
 
      kf1 = coschi 
      kf3 = sinchi/sinthe 
      kf2 = kf3*sinphi 
      kf3 = kf3*cosphi 
 
      uscatt(1) = kf1*uinc(1) + kf3*(uinc(2)*uinc(2)+uinc(3)*uinc(3)) 
      uscatt(2) = kf1*uinc(2) + kf2*uinc(3) - kf3*uinc(1)*uinc(2) 
      uscatt(3) = kf1*uinc(3) - kf2*uinc(2) - kf3*uinc(1)*uinc(3) 
 
      vscatt = vrel*coschi 
 
      velx = vscatt*uscatt(1) + vxn 
      vely = vscatt*uscatt(2) + vyn 
      velz = vscatt*uscatt(3) + vzn 
      return  
 
      end subroutine scatter 


 
 
      subroutine borismover(ux, uy, uz, ex, ey, ez, bxpar, bypar, bzpar, qomdt) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(inout) :: ux 
      real(double) , intent(inout) :: uy 
      real(double) , intent(inout) :: uz 
      real(double) , intent(in) :: ex 
      real(double) , intent(in) :: ey 
      real(double) , intent(in) :: ez 
      real(double) , intent(in) :: bxpar 
      real(double) , intent(in) :: bypar 
      real(double) , intent(in) :: bzpar 
      real(double) , intent(in) :: qomdt 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: dt, qo2mdt, uxm, uym, uzm, uxp, uyp, uzp, uxpp, uypp, &
         uzpp, ttx, tty, ttz, t2, sx, sy, sz 
!-----------------------------------------------
 
 
      qo2mdt = qomdt*0.5D0 
 
!     BORIS
 
!     Half electric field
 
      uxm = ux + qo2mdt*ex 
      uym = uy + qo2mdt*ey 
      uzm = uz + qo2mdt*ez 
 
!     Magnetic field
 
      ttx = qo2mdt*bxpar 
      tty = qo2mdt*bypar 
      ttz = qo2mdt*bzpar 
 
      t2 = ttx*ttx + tty*tty + ttz*ttz 
      t2 = 2.D0/(1.D0 + t2) 
 
      sx = ttx*t2 
      sy = tty*t2 
      sz = ttz*t2 
 
      uxpp = uxm + uym*ttz - uzm*tty 
      uypp = uym - uxm*ttz + uzm*ttx 
      uzpp = uzm + uxm*tty - uym*ttx 
 
      uxp = uxm + uypp*sz - uzpp*sy 
      uyp = uym - uxpp*sz + uzpp*sx 
      uzp = uzm + uxpp*sy - uypp*sx 
 
!     Second half electric field
 
 
      ux = uxp + qo2mdt*ex 
      uy = uyp + qo2mdt*ey 
      uz = uzp + qo2mdt*ez 
      return  
 
 
!     xp=xp+ux*dt
!     yp=yp+uy*dt
!     zp=zp+uz*dt
 
 
      end subroutine borismover 


 
!                                                ! To be used with Cll
      subroutine paramcoll1(ak, dt, nscatt, cvel, vtheitape) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(out) :: ak 
      real(double) , intent(in) :: dt 
      real(double) , intent(out) :: nscatt 
      real(double) , intent(out) :: cvel 
      real(double) , intent(in) :: vtheitape 
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
      real(double), parameter :: c = 2.9979D8 
      real(double), parameter :: me = 9.1094D-31 
      real(double), parameter :: mprot = 1.6726D-27 
      real(double), parameter :: e = 1.6022D-19 
      real(double), parameter :: kb = 1.3807D-23 
      real(double), parameter :: eps0 = 8.8542D-12 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vthe, crx, mratio, te, tev, lde, ne, ntar, mneut, wpe 
!-----------------------------------------------
 
 
 
      ne = 1.D15                                 !  electron density 
      ntar = 2.D21                               ! gas density 
 
      mneut = mprot                              !40.d0* Argon 
      tev = 1.D0                                 ! [eV] 
      te = 1.D0*e/kb                             ! [K] 

      vthe = dsqrt(2.D0*kb*te/me) 
      wpe = dsqrt(e*e*ne/me/eps0) 
      lde = vthe/wpe 
 
      crx = 2.13D-18 !2.36D-18 
 
      mratio = me/mneut 
 
      nscatt = 2.D0/dt 
      cvel = c/vthe*vtheitape 
 
      ak = mratio*dt*crx*ntar*lde/vtheitape 
 
 
      return  
      end subroutine paramcoll1 


!-----------------------------------------------------------------------
 
!                                                ! To b with MCC
      subroutine paramcoll2(evfact, ctype, ntarld, mratio, vthn, vtheitape&
                            ,mfact) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(out) :: ctype 
      real(double) , intent(out) :: evfact 
      real(double) , intent(out) :: ntarld 
      real(double) , intent(out) :: mratio 
      real(double) , intent(out) :: vthn 
      real(double) , intent(in) :: vtheitape 
      real(double) , intent(in) :: mfact
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
      real(double), parameter :: me = 9.1094D-31 
      real(double), parameter :: mprot = 1.6726D-27 
      real(double), parameter :: e = 1.6022D-19 
      real(double), parameter :: kb = 1.3807D-23 
      real(double), parameter :: eps0 = 8.8542D-12 
      integer, parameter :: chex = 1 
      integer, parameter :: scatt = 2 
      integer, parameter :: all = 3 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vthe, ntar, wpe, te, tev, lde, ne, mneut, tgas 
!-----------------------------------------------
 
      ne = 1.D15                                 !  electron density 
      ntar = 2.D21                               ! density target species (neutral) 
      mneut = mprot                              !40.d0* Argon 
 
      tev = 1.D0                                 ! [eV] 
      te = 1.D0*e/kb                             ! [K] 
      tgas = 0.05D0*e/kb                         ! [K] 

      mneut=mfact*me 
 
      mratio = me/mneut 

      vthe = dsqrt(2.D0*kb*te/me) 
      vthn = dsqrt(2.D0*kb*tgas/mneut)/vthe*vtheitape 
      wpe = dsqrt(e*e*ne/me/eps0) 
      lde = vthe/wpe 
 
      ntarld = ntar*lde/vtheitape 
      evfact = me*vthe*vthe/e/vtheitape/vtheitape 
 
      ctype = all 
 
      return  
      end subroutine paramcoll2 
