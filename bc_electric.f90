      subroutine bc_electric 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
      use cplot_com_M 
      implicit none
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer i,j,ij
!-----------------------------------------------

      if(wl==0) then
      i=2
      do j=1,nyp
         ij = (j - 1)*nxp + i 
         ex(ij)=ex(ij+1)
         ey(ij)=ey(ij+1)
      enddo
      end if
      
      end subroutine bc_electric
