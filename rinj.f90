      subroutine rinj(isp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: isp 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: iwid, jwid, j, i, ij, ipj, ipjp, ijp, ip, np 
      real(double) :: charginj, the, zeta, w1, w2, w3, w4, vmag, th, fi, &
         vtherml, ws, sinfi, cosfi, sinth, costh 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: ranf 
!-----------------------------------------------
 
!
!     A subroutine to inject particles
!     It does the bottom only
!
      nxp = nx + 1 
      nyp = ny + 1 
      iwid = 1 
      jwid = nxp 
!
!     BOTTOM
!
      isq = sqrt(float(npcel(isp))) 
 
      j = 2 
      do i = 2, nx 
         ij = (j - 1)*jwid + i 
         ipj = ij + iwid 
         ipjp = ij + iwid + jwid 
         ijp = ij + jwid 
         charginj = vvi(isp)*dt*rhr(isp)*(xn(ipj)-xn(ij))*sign(1.,qom(isp)) 
 
         charginj = charginj/isq 
 
         do ip = 1, isq 
            np = iphead(1) 
            if (np > 0) then 
               iphead(1) = link(np) 
               link(np) = iphead(ij) 
               iphead(ij) = np 
 
               the = (ip - 0.5)/float(isq) 
               zeta = ranf() 
               xp(np) = i + the 
               yp(np) = j + zeta 
               w1 = the*(1 - zeta) 
               w2 = the*zeta 
               w3 = (1 - the)*zeta 
               w4 = (1 - the)*(1 - zeta) 
 
               xpf(np) = w1*xn(ipj) + w2*xn(ipjp) + w3*xn(ijp) + w4*xn(ij) 
               ypf(np) = w1*yn(ipj) + w2*yn(ipjp) + w3*yn(ijp) + w4*yn(ij) 
 
               charginj = charginj*(cyl*xpf(np)+1.-cyl) 
 
               qpar(np) = charginj 
               ico(np) = isp 
               diepar(np) = abs(qpar(np))*diem(isp) 
               vmag = sqrt(1.E-10 - 2.*log(1. - 0.999999*ranf())) 
               th = 2*pi*ranf() 
               fi = pi*ranf() 
               vtherml = siep(isp) 
               ws = vtherml*vmag 
               sinfi = sin(fi) 
               cosfi = cos(fi) 
               sinth = sin(th) 
               costh = cos(th) 
               up(np) = uvi(isp) + ws*sinfi*costh 
               vp(np) = vvi(isp) 
!      vp(np)=vvi(isp)+ws*sinfi*sinth
               wp(np) = ws*cosfi 
               do while(vp(np) <= 0.) 
                  th = 2*pi*ranf() 
                  fi = pi*ranf() 
                  vtherml = siep(isp) 
                  ws = vtherml*vmag 
                  sinfi = sin(fi) 
                  cosfi = cos(fi) 
                  sinth = sin(th) 
                  costh = cos(th) 
                  up(np) = uvi(isp) + ws*sinfi*costh 
                  vp(np) = vvi(isp) 
!      vp(np)=vvi(isp)+ws*sinfi*sinth
                  wp(np) = ws*cosfi 
               end do 
 
            else 
               write (*, *) 'WARNING: Not Enough Particles' 
            endif 
 
         end do 
 
      end do 
 
      return  
      end subroutine rinj 
