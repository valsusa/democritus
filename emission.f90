subroutine thermionic
  !---------------------------------------------------------!
  !---------------------------------------------------------!
  ! A subroutine to inject thermionically emitted particles !
  ! from the surface of a dust grain.     		    !
  !---------------------------------------------------------!
  !---------------------------------------------------------!

  use vast_kind_param, only: double
  use celest2d_com_M
  use emission_com_M

  implicit none

  !  Local Variables
  integer      :: ip, whichdust,npinj, ipsrch, nnew, nh
  real(double) :: qinj,smdr,smdr_in,su_dust,vtherm,flux, &
                  qpinj,muu,sintheta,vr,vt1,vt2,vmag,th,randt

  !  External Functions
  real(double) , external :: ranf, generate

  nh = mod(ncyc,nhst) 
      
  ipsrch=0

  do whichdust = nsp_dust,nsp_dust2

     smdr = rex(whichdust)*0.03d0  ! ERA 0.1
     smdr_in = rex(whichdust)/9d0  ! ERA 0.1
     ! Assuming spherical dust and remembering the normalization in democritus that removes 4pi from the geometry
     !su_dust=voldust_exact/rex(whichdust)*3
     su_dust=rex(whichdust)**2
     !write(*,*)'superfice dust',su_dust,voldust_exact/rex(whichdust)*3
     
     ! now assign properties of second dust particle if it is time to do so
 
      vtherm = siep_emission(whichdust) *sqrt(2.) 
      flux = n0_emission(whichdust)*vtherm/2./sqrt(pi)
      qinj = su_dust*flux*dt 					

     if (qinj.eq.0)then
        print*, "Maxwellian emission current = 0, no charge injected"
     end if

     npinj = int(qinj*npcel(emitted_species)/(rhr(emitted_species)*dx**2*dy))
     npinj=min(npinj,5000)
     write(*,*)'npinj',npinj
     npinj=max(1,npinj)
     qpinj=qinj/npinj
     
!     write(*,*)'Maxwellian emission: Np=',npinj,'   Qinj=',qinj
      
     ! Now find which grid cells these particles are to be emitted from 
     ! and assign their properties to the grid
     do ip = 1,npinj

        muu = 2d0*ranf()-1d0  ! generate a random cos(theta)
        sintheta=sqrt(1d0-muu**2)

        ! Assign a random position just outside the surface of dust particle
 
        !write(*,*) (xp1(ip)-xcenter(whichdust))**2/rex(whichdust)**2 + (yp1(ip)-ycenter(whichdust))**2/rey(whichdust)**2,muu,sintheta
        vr = generate(0.d0,vtherm)	

        vmag = vtherm*sqrt(-log(1-0.99999*ranf()))	
        th = 2*pi*ranf() 
        vt1 = vmag*cos(th)
        vt2 = vmag*sin(th)
                  
        up1(ip) = vr*sintheta - vt1*muu    
        vp1(ip) = vr*muu + vt1*sintheta    
        wp1(ip) = vt2            !---Assume particle has vt2 along z=0 axis
        randt=dt*ranf()
        xp1(ip) = xcenter(whichdust)+(rex(whichdust)+smdr)*sintheta + up1(ip)*randt	
        yp1(ip) = ycenter(whichdust)+(rey(whichdust)+smdr)*muu + vp1(ip)*randt
        !write(*,*)xp1(ip),yp1(ip),up1(ip),vp1(ip)
        ! for every electron we emit, add a positive charge with zero 
        ! velocity just inside the surface of the dust particle
        xp1(ip+npinj) = xcenter(whichdust) + ((rex(whichdust)-smdr_in)*sintheta)
        yp1(ip+npinj) = ycenter(whichdust) + ((rey(whichdust)-smdr_in)*muu)	
 !write(*,*) (xp1(ip+npinj)-xcenter(whichdust))**2/rex(whichdust)**2 + (yp1(ip+npinj)-ycenter(whichdust))**2/rey(whichdust)**2,muu,sintheta
       
        vr = 0.0 
        vt1 = 0.0
        vt2 = 0.0
        up1(ip+npinj) = 0
        vp1(ip+npinj) = 0
        wp1(ip+npinj) = 0    

     end do

     !----------------------------------------------------!
     ! Partial advance of e's emitted during the timestep !
     !----------------------------------------------------!
     !call partial_push(xp1(1:npint),yp1(1:npint),up1(1:npint),vp1(1:npint),wp1(1:npint),-qpinj,npint)

     ! add injected particles to the list of parts in system
     do ip=1,2*npinj ! se si mette 1+npinj come starting point non emette elettroni

        if (iphead(1) > 0)then	
           nnew = iphead(1)
           iphead(1) = link(iphead(1))
           link(nnew)=ipsrch
           ipsrch=nnew          

           xpf(nnew) = xp1(ip)				
           ypf(nnew) = yp1(ip)				
           up(nnew) = up1(ip)    
           vp(nnew) = vp1(ip)    
           wp(nnew) = wp1(ip)            ! assume vt2 along z=0 axis
           !xp(nnew) = 2 
           !yp(nnew) = ny/2+2
           istatus(nnew) = 0
           
           if (ip <= npinj) then
              dtsub(nnew) = dt/nsubmax
              nsub(nnew) = nsubmax
              ico(nnew) = 2 
              qpar(nnew) = -qpinj 
           else               !inject ions into dust to account for ejected e's
 			  dtsub(nnew) = dt
 			  nsub(nnew) = 1
              ico(nnew) =  1
              qpar(nnew) = qpinj 
           end if
           diepar(nnew) = abs(qpar(nnew))*diem(ico(nnew))
           flux_emitted(nh,ico(nnew))=flux_emitted(nh,ico(nnew))+qpar(nnew)
        else
           print*, "WARNING: Not Enough Particles : dust emission" 
           stop  
        end if

     end do

  end do

  iphd2 = iphead
  call locator_kin (ipsrch)

end subroutine thermionic

subroutine photoemission

end subroutine photoemission
