 
 
      program celeste 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
      use cplot_com_M 
      use emission_com_M
      use work_variables
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!      (itape,tape5=itape,tape6,tape7,tape8,tty,tape59=tty)
! *++
! +++ flip - an adaptive pic code in two dimensions
! +++ j.u.brackbill and hans ruppel  lanl
! +++
!    * * * * list of variables- * * * *
!
! (1) input quantities-
!
!    name     problem identification line
!    problem_type    =1 for dust charging
!	 	     =2 for iec devices   
!    nx       no. of cells in x-direction  (or 0 if tape restart)
!    ny       no. of cells in y-direction (or dump no. if tape restart)
!     nsp     the number of partilce species
!    imp      =1 for implicit pressure calculation,
!             =0 for purely explicit calculation
!    inc      =1 for incompressible limit variant of imp=1 calculation
!             for some specified continuous rezone
!    lpr      long print control- 0=omit, 1=film, 2=film and printer,
!             3=printer
!    wb,wl,wr,wt    indicators for boundary condition to be used
!                   along the bottom,left,right, and top edges
!                   of the mesh (0=lagrangian surface, 1=simple freeslip
!                   2=general freeslip for curvilinear boundaries, 3=
!                   noslip, 4=continuative outflow, 5=specified inflow
!                   or outflow, 6=applied pressure)
!    dx       cell size in the x-direction if uniformly zoned
!    dy       cell size in the y-direction if uniformly zoned
!    cyl      =1.0 for cylindrical geometry, =0.0 for plane geometry
!    dt       time step, subject to automatic recalculation during run
!    dtmax    maximum dt allowed
!    tlimd    =1.0 forces a tape dump exit before job time limit
!    twfilm   problem time interval between film plots
!    twprtr   problem time interval between long prints
!    twfin    problem time when to terminate the calculation
!    om       relaxation coefficient used in pressure iteration
!    peps     pressure fraction scaling the relaxation factor rdsdp
!    eps      allowed relative error in the pressure iteration
!    lambda   bulk viscosity coefficient
!    mu       shear viscosity coefficient
!    xi       =1. for 4th-order node coupling,
!             =0. for 2nd-order node-coupling
!    gx       body acceleration in x-direction, + or -
!    gy       body acceleration in y-direction, + or -
!    a0, b0   factors controlling convective fluxing. limiting cases-
!     0   0      centered; unstable
!     1   0      full donor cell; stable; diffusive
!      0   1      interpolated donor cell; linearly stable; non-diffusiv
!     1   1      stable; diffusive
!    asq, ron, gm1    parameters for stiffened polytropic gas equation
!                     of state- asq is the square of the zero-tempera-
!                     ture sound speed, ron is the fluid normal
!                     density, and gm1 is gamma-1, in which gamma is the
!                     ratio of specific heats
!    roi, siei   initial fluid density and specific internal energy
!    uin, vin, roin, siein   descriptors for a specified flow boundary-
!                          x-direction velocity, y-direction velocity,
!                          density, and specific internal energy
!    pap      applied pressure for pressure boundary condition
!
! (2) derived quantities-
!
!    omcyl    1-cyl
!    xicof    (1+xi)/2
!    dpcof    peps*roi/(2(1/(dx*dx) + 1/(dy*dy)))
!    grfac    1/(nx*ny)
!    tfilm    problem time interval between film plots
!    tprtr    problem time interval between long prints
!    nyp      ny+1
!    nxp      nx+1
!  the following 12 indices define do-loop limits, to
!  provide fictitious cells for certain boundary types-
!    ifirst   =1  if wl=1,2,3 or 4; =2   if wl=5 or 6
!    jfirst   =1  if wb=1,2,3 or 4; =2   if wb=5 or 6
!    ilast    =nx if wr=1,2,3 or 4; =nx-1 if wr=5 or 6
!    jlast    =ny if wt=1,2,3 or 4; =ny-1 if wt=5 or 6
!    ifph1    =1  if wl=1,2,3,4 or 6; =2   if wl=5
!    jfph1    =1  if wb=1,2,3,4 or 6; =2   if wb=5
!    ilph1    =nx if wr=1,2,3,4 or 6; =nx-1 if wr=5
!    jlph1    =ny if wt=1,2,3,4 or 6; =ny-1 if wt=5
!    ilastv   ilast+1
!    jlastv   jlast+1
!    ilastm   ilast-1
!    jlastm   jlast-1
!
! (3) scalar quantities-
!
!    t        problem time
!    ncyc     cycle counter
!    ipres    =1 during iteration to let continuative bdry. ul,vl float
!    maxit    maximum no. of iterations allowed before dt cut in half
!    numit    no. of iterations reqd. for pressure iteration convergence
!    loops    no. of dt cuts for non-convergence performed
!             in a given cycle
!             cut due to non-convergence in a given cycle
!    third    1/3
!    twlfth   1/12
!    pmax     maximum pressure in the system
!    grind    central processor time per cell per cycle, in msec
!    ndump    tape dump counter
!    drou     scaling factor for the velocity vector plot
!    vmax     maximum velocity in the system
!    dtf      time-step factor for convective stability and accuracy
!    c1       wall clock hrs/min/sec when the job began
!    d1       month/day/year when the job began
!    xl       x-coordinate of the leftmost vertex
!    yb       y-coordinate of the bottommost vertex
!    fixl     film-frame coordinate analog of xl
!    fiyb     film-frame coordinate analog of yb
!    xconv    film-frame conversion coefficient in x-direction
!    yconv    film-frame conversion coefficient in y-direction
!    timlmt   job time limit, in seconds
!    dtmin    minimum dt allowed, = (dt of cycle 1)*e-10
!    iddt     symbol identifying the restriction controlling the
!             current time step, where- g=105% of previous dt,
!             c=convection (dtf), v=shear viscosity, or
!             m=maximum dt (dtmax)
!    jnm      optional, identifier for code version, user, etc.
!    aa       dummy word identifying beginning of tape-dump data
!    zz       dummy word identifying end of tape-dump data
!
! (4) vector quantities-
!
!    x        cartesian coordinate in radial direction
!    r        radial coordinate (r=1 for cartesian geometry,
!                              r=x for cylindrical geometry)
!    y        cartesian coordinate in axial direction
!    u        x-direction vertex velocity component
!    v        y-direction vertex velocity component
!     qdn     specie charge density
!     qnet0     net charge density calculated from particle data
!     jxs,jys     components of specie current density
!     jx0,jy0   net current density calculated from particle data
!    ro       cell density
!    vol      cell volume
!
!
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij, j, i, nm, ijp, ipj, ipjp, ijl, ijr, n, ns, isp, nh 
      real(double) :: cel2plt, qsum, avgjx, avgjy, volsum, qavg, plasma, &
         wateg, onoff, totjxl, totjxr, spiu, smeno, cpiu, cmeno,sortime,&
         sortime2
      real(4) :: trl
!-----------------------------------------------
!
      open(unit=5, file='itape', position='asis') 

!      open(unit=15, file='Output.dat', status='replace')
!      close(15)
!
!     set counters
      spiu=0d0
      smeno=0d0
      cpiu=0d0
      cmeno=0d0
      
!     set potential
      phiavg=0d0
      
!     to run on Rho (lanl) unc the following 3 lines
      call gplot ('u', cel2plt, 7) 
      call libdisp 
!     to run on NERSC unc the following line
!     call cgmbo('nome',4,1)
!
!     to run on SUN
!     call opngks
      call begin (name, jnm, d1, c1, timlmt) 
      call rinput 
      call bincrea 
      if (nx == 0) call taperd 
      if (ndump /= ndumpsav) go to 22 
 
 
      call second (trl) 
      if (iprd == 1) then 
         write (6, 110) trl 
         write (59, 110) trl 
      endif 
      write(*,*)'setup'
      call setup 
      write(*,*)'geometry_object'
      call geometry_object
      call second (trl) 
      if (iprd == 1) then 
         write (6, 110) trl 
         write (59, 110) trl 
      endif 
       write(*,*)'plotinit'
      call plotinit (nxp, nyp, npart, nrg, nsp, thetadim) 
 
   10 continue 
      call second (trl) 
!      if (iprd.eq.1) write (6,110) trl, ncyc,iphead(1),np
!      if (iprd.eq.1) write (59,110) trl,ncyc,iphead(1),np
      call vinit 
      call timstp 
      idone = 0 
   22 continue 
      call newcyc 
!

      do ij = 1, nxyp 
         qtilde(ij) = 0.0 
      end do 
 
      qsum = 0.0 
      avgjx = 0.0 
      avgjy = 0.0 
      volsum = 0.0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            qsum = qsum + qnet0(ij)*vol(ij) 
            avgjx = avgjx + jxtil(ij)*vvol(ij) 
            avgjy = avgjy + jytil(ij)*vvol(ij) 
            volsum = volsum + vol(ij) 
         end do 
      end do 
      qavg = qsum/volsum 
      qavg = 0. 
      avgjx = avgjx/volsum 
      avgjy = avgjy/volsum 
!     write(*,*)'qavg=',qavg
      if (estatic) then 
         do j = 2, nyp 
            do i = 2, nxp 
               ij = nxp*(j - 1) + i 
			   if(diegrid(ij).ge.0d0) then
			   if(geometric_object) then 
			   coef(ij) = (1. + ycut*inside_object(ij)*diegrid(ij))/vvol(ij) 
			   else 
               coef(ij) = (1. + ycut*diegrid(ij))/vvol(ij) 
               end if 
			   else
			   coef(ij) = 1./vvol(ij)
			   end if
               coeftrns(ij) = 0.0 
               coefpar(ij) = 0.0 
            end do 
         end do 
!
         nm = 1 
!     qtildetot=0.
         do j = 2, ny 
            ij = (j - 1)*nxp + 2 
            do i = 2, nx 
!
               ij = (j - 1)*nxp + i 
!      qtilde(ij)=(1.-elle)*qtilde(ij)+elle*qnet0(ij)*vol(ij)
               qtilde(ij) = (qnet0(ij)-0d0*qavg)*vol(ij) 
!      qtilde(ij)=qnet0(ij)*vol(ij)
!      qtilde(ij)=0.
!     qtilde(ij)=1.*vol(ij)
!      qtildetot=qtildetot+qtilde(ij)
!
!
               nm = nm + 1 
!
            end do 
         end do 
!      write(*,*)'qtildetot=',qtildetot
         call smooth (nx, ny, 2, nx, 2, ny, smth, nsmth, periodic, sc1, qtilde) 
!
         if (conductive) then 
!      write(*,*)'CALLING SMEAR'
            call smear (nx, ny, qtilde, number, nsp, nsp_dust, nxyp, a, vol) 
         endif 
!

         call poisson
         call potential_object

!
!         call electric (1, nxp, 2, nxp, 2, nyp, cx1, cx2, cx3, cx4, cr1, cr2, &
 !           cr3, cr4, cy1, cy2, cy3, cy4, phi0, vvol, ex, ey, xn, yn) 
         phiavg=phiavg*(1d0-sor)+phi0*sor
         call electric (1, nxp, 2, nxp, 2, nyp, cx1, cx2, cx3, cx4, cr1, cr2, &
            cr3, cr4, cy1, cy2, cy3, cy4, phiavg, vvol, ex, ey, xn, yn) 

!      call global(0)
 
!
!     calculate the electrostatic potential
!
!
      else 
!
!
!     calculate the appropriate dielectric for a quasi-neutral
!     plasma if necessary
!
         plasma = 1. 
         if (neutral) plasma = 0.0 
!
         do j = 2, nyp 
            do i = 2, nxp 
               ij = nxp*(j - 1) + i 
               if(geometric_object) then 
               coef(ij) = (plasma + ycut*inside_object(ij)*diegrid(ij))/vvol(ij) + dielperp(ij) 
			   else 
               coef(ij) = (plasma + ycut*diegrid(ij))/vvol(ij) + dielperp(ij) 
               end if 
!
!     add a term to the transverse terms that should be zero if
!     the potential is single-valued
!
               coeftrns(ij) = bz(ij)*dieltrns(ij) 
               coefpar(ij) = dielpar(ij) 
!      jxtil(ij)=jxtil(ij)-avgjx
               jytil(ij) = jytil(ij) - avgjy 
            end do 
         end do 
!
         do j = 2, ny 
!
            do i = 2, nx 
!
               ij = (j - 1)*nxp + i 
               ijp = ij + nxp 
               ipj = ij + 1 
               ipjp = ijp + 1 
!
!      qtilde(ij)=(qnet0(ij)-qavg)*vol(ij)
               qtilde(ij) = qnet0(ij)*vol(ij) - ((cx1(ij)+cr1(ij))*jxtil(ipj)+&
                  cy1(ij)*jytil(ipj)+(cx2(ij)+cr2(ij))*jxtil(ipjp)+cy2(ij)*&
                  jytil(ipjp)+(cx3(ij)+cr3(ij))*jxtil(ijp)+cy3(ij)*jytil(ijp)+(&
                  cx4(ij)+cr4(ij))*jxtil(ij)+cy4(ij)*jytil(ij))*cntr*dt 
!
!
            end do 
         end do 
!
         call smooth (nx, ny, 2, nx, 2, ny, smth, nsmth, periodic, sc1, qtilde) 
!
         call poisson 
!
!     calculate the electric field from the potential
!
!         call electric (1, nxp, 2, nxp, 2, nyp, cx1, cx2, cx3, cx4, cr1, cr2, &
!            cr3, cr4, cy1, cy2, cy3, cy4, phi0, vvol, ex, ey, xn, yn) 
         phiavg=phiavg*(1d0-sor)+phi0*sor           
         call electric (1, nxp, 2, nxp, 2, nyp, cx1, cx2, cx3, cx4, cr1, cr2, &
            cr3, cr4, cy1, cy2, cy3, cy4, phiavg, vvol, ex, ey, xn, yn) 
      endif 
!
!
      call magnetic (1, nxp, 1, nyp, 1, nxp, x, cyl, rmaj, bx0, by0, bz0, bx, &
         by, bz) 
!
!     calculate the electric displacement
!
      call etilde (1, nxp, nxyp, nsp, 2, nxp, 2, nyp, cyl, dt, qom, ex, ey, bx&
         , by, bz, extil, eytil, eztil) 
!

      if(problem_type.eq.1) then
      call global (0) 
      else
      call global_iec(0)
      end if     

!     construct the grid for the next cycle
!
!      grid=0.0 **** eulerian mesh
!     0.0<grid<1.0 **** adaptive mesh
!     grid=1.0 **** lagrangean mesh
!     1.0<grid<2.0 **** almost-lagrangean mesh
!
      wateg = mod(grid,1.0) 
      onoff = grid - wateg 
!
!     call rezone routines if grid is not zero
!
!      if(grid.ne.0..and.ncyc.eq.1) then
!      call adapt
!      firstime=.true.
!      call gridmkr(nxp,nyp,2,nxp,2,nyp,2,nxp,2,nyp,
!     &     firstime,
!     &     xn,yn,w,cx1,cx2,cx3,cx4,
!     &            cy1,cy2,tau,dt,scal,eqvol,nummax,
!     &        a,b,c,d,e,af,bf,cf,df,ef,eg,dg,cg,bg,ag,
!     &        ah,bh,ch,dh,eh,soln,srce,residu,sc1,sc2)
!      endif
!      if(grid.eq.1..and.ncyc.gt.1) then
      if (grid==1. .and. mod(ncyc,nadaptfr)==0) then 
         write (*, *) 'CALLING ADAPTIVE GRID' 
         call adapt 
         firstime = .TRUE. 
         call gridmkr (nxp, nyp, 2, nxp, 2, nyp, 2, nxp, 2, nyp, firstime, xn, &
            yn, w, cx1, cx2, cx3, cx4, cy1, cy2, tau, dt, scal, eqvol, nummax, &
            a, b, c, d, e, af, bf, cf, df, ef, eg, dg, cg, bg, ag, ah, bh, ch, &
            dh, eh, soln, srce, residu, sc1, sc2) 
      endif 
      rdt = 1./dt 
 
      call nbdhood (1, nxp, 1, nyp, 1, nxp, dx, dy, xn, yn, ichead, jchead) 
!
!     ************************************************************
!
!     solve the equation of motion for the particles
!      and relocate on the grid
!
!     for hybrid simulations
!     species with fluid=true are treated as fluid particles
!
!
!     set normal (xc-current to zero on right and left boundaries)
!
      do j = 2, ny 
!
         ijl = (j - 1)*nxp + 2 
         ijr = j*nxp 
!
         totjxl = 0.0 
         totjxr = 0.0 
!
         do n = 1, nsp 
            if (fluid(n)) cycle  
            totjxl = totjxl + jxs(ijl,n) 
            totjxr = totjxr + jxs(ijr,n) 
         end do 
!
         do n = 1, nsp 
            if (.not.fluid(n)) cycle  
            jxs(ijl,n) = -totjxl 
            jxs(ijr,n) = -totjxr 
         end do 
!
      end do 
!
      do ns = 1, nsp 
!
         if (fluid(ns)) then 
!
            call moments (1, nxp, nxyp, 2, nx, 2, ny, ns, cntr, dt, cx1, cx2, &
               cx3, cx4, cr1, cr2, cr3, cr4, cy1, cy2, cy3, cy4, vol, ah, qom, &
               jxs, jys, qvs, qdn, ex, ey, ufl, vfl) 
!
            call parmovfl (ns) 
!
         else 
!
            if (imp > 0) call parmovki (ns) 
         endif 
!
      end do 

      if (imp == 0) then
         if(problem_type.eq.1) then
      !(Leo) Here we can time the mover
      ! sortime2=sortime
      ! call cpu_time(sortime)


! (Leo) New mover. We subcycle up to nsubmax times
            
            do i=1,nsubmax
      !         open(unit=15, file='Output.dat', access='append')
               call parmove(i)
         !      write(15,*) xpf(840),ypf(840)
       !        close(15)
            enddo
       ! write(*,*) "Time: ",sortime-sortime2
         else
            call parmove_iec
         endif
      endif

!
!     ************************************************************
!
!     create new particles at an injection boundary
!
      nh = mod(ncyc,nhst) 
      flux_inject_left(nh,1:nsp)=0d0
      flux_inject_right(nh,1:nsp)=0d0
      flux_inject_top(nh,1:nsp)=0d0
      flux_inject_bottom(nh,1:nsp)=0d0
      flux_emitted(nh,1:nsp)=0d0
      
      do isp = 1, nsp 
         if (inj(isp) == 0) cycle  
         if(problem_type.eq.1) then
         call rinj (isp) 
         else
         call rinj_iec(isp)
         end if
!      if (inj(isp).ne.0) call rinj_circle(isp)
      end do 
!
!	Emission from the dust
!
      if (maxwellian)then
         call thermionic
      else
         call photoemission
      end if
 
!
!     interpolate particle mass and energy on to grid
!
!     distribute the particle properties to the grid
!
!      call parcel
!     ncount=0
!     np=iphead(1)
!132  if(np.ne.0) then
!     ncount=ncount+1
!     np=link(np)
!     goto 132
!     end if
!     write(*,*)'call control, cycle:',ncyc
      call control (spiu, smeno, cpiu, cmeno) 
      if (mod(ncyc,10) == 0) then 
         write (*, *) 'SPLITTER: succ,fall', spiu, smeno 
         write (*, *) 'COALESCE: succ,fall', cpiu, cmeno 
      endif 
      call parcel
      go to 10 
  110 format(' main',e12.5,10i5) 
      stop  
 
      end program celeste 
