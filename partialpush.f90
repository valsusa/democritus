subroutine partial_push(xpi,ypi,up1,vp1,wp1,qpart,npush)

  ! A subroutine to advance the particle eqns of motion using standard boris 
  ! push for the partial timestep after emission before the next PARMOVE cycle

  use vast_kind_param, ONLY:  double 
  use impl_M 
  use celest2d_com_M


  implicit none

  !  Local Variables
  integer, dimension(npush) :: npgrande
  real(double) :: dtfrac,qpart
  integer :: isp, ij, ipj, ipjp, ijp,npush, iold,jold,n
  real(double), dimension(npush) :: xpi, ypi, up1, vp1, wp1,xpl,ypl
  real(double) :: the, zeta, w1, w2, w3, w4, upv1, vpv1, wpv1, bxpar, &
       bypar, bzpar, expar, eypar
  real(double) :: qdt, qomdt, x2p, z2p, upvold,  udotb, denom, bpar2, &
       qo2mdt, Omega

  !  External Functions
  real(double) , external :: ranf

   
  

  ! Find where my particle is located

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Need to figure out how which subroutine to call to 
  ! find the natural coords of my specific particle
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   
  ! call (whatever subroutine I need)
  !call mapv (x, y, ij, nxp, xp, yp, the, zeta, 1, lvmax)
   do n=1,npush
      npgrande(n)=n
   enddo
   xpl=2
   ypl=nyp/2+1
   call whereami(npush,npgrande,npgrande,1,nxp,xn,yn,nx,ny, &
                 xpi,ypi,xpl,ypl)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Need to figure out how to do this for my specific particle
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  do n = 1,npush
     iold=int(xpl(n))
     jold=int(ypl(n))
     ij = (jold - 1)*nxp + iold 
     ipj = ij + 1 
     ipjp = ij + nxp + 1 
     ijp = ij + nxp 
    ! print*, ij, ipj, ipjp, ijp


     !  iold = int(xp) 
     !  jold = int(yp) 
     !  inew = iold
     !  jnew = jold
     the = xpl(n) - iold 
     zeta = ypl(n) - jold 
     !  xpv = xp
     !  ypv = yp call mapv (xp, yp, ij, nxp, xpi, ypi, the, zeta, 1, lvmax)

     ! interpolate the fields to the particle position

     w1 = the*(1. - zeta) 
     w2 = the*zeta
     w3 = (1. - the)*zeta 
     w4 = (1. - the)*(1. - zeta) 

     expar = w1*ex(ipj) + (w2*ex(ipjp)+(w3*ex(ijp)+w4*ex(ij))) 
     eypar = w1*ey(ipj) + (w2*ey(ipjp)+(w3*ey(ijp)+w4*ey(ij))) 
     bxpar = w1*bx(ipj) + (w2*bx(ipjp)+(w3*bx(ijp)+w4*bx(ij))) 
     bypar = w1*by(ipj) + (w2*by(ipjp)+(w3*by(ijp)+w4*by(ij))) 
     bzpar = w1*bz(ipj) + (w2*bz(ipjp)+(w3*bz(ijp)+w4*bz(ij))) 
    ! print*,"POSITION BEFORE PUSH:", xpi(n)
     isp = 2

     ! Assign a partial timestep to push each particle
     dtfrac = dt*ranf()
     !  qomdt = qom*dtfrac
     qomdt = abs(qpart)*qom(isp)*dtfrac
     qo2mdt = qomdt*0.5D0 
     Omega = 0.0



     ! Look out, here comes Boris...

     if ((.not.estatic).or.(abs(Omega).le.0.01/dtfrac).or.(.not.leonardo)) then

        ! The electric field part I
        up1(n) = up1(n) + qo2mdt*expar 
        vp1(n) = vp1(n) + qo2mdt*eypar 
        wp1(n) = wp1(n) 
        ! Now the magnetic field
        bpar2 = bxpar**2 + bypar**2 + bzpar**2 
        denom = 1D0/(1. + bpar2*qo2mdt**2) 
        coef1 = 1.D0 - bpar2*qo2mdt**2 
        udotb = up1(n)*bxpar + vp1(n)*bypar + wp1(n)*bzpar 
        upv1 = (up1(n)*coef1+qomdt*(vp1(n)*bzpar-wp1(n)*bypar+qo2mdt*(udotb*bxpar)))*denom 
        vpv1 = (vp1(n)*coef1+qomdt*(wp1(n)*bxpar-up1(n)*bzpar+qo2mdt*(udotb*bypar)))*denom 
        wpv1 = (wp1(n)*coef1+qomdt*(up1(n)*bypar-vp1(n)*bxpar+qo2mdt*(udotb*bzpar)))*denom 

        ! The electric field strikes back

        upv1 = upv1 + qo2mdt*expar 
        vpv1 = vpv1 + qo2mdt*eypar 

        ! Calculate the new particle positions

        if (cyl==0. .or. .not.boris) then 
           xpi(n) = xpi(n) + upv1*dtfrac
        else 
           x2p = xpi(n) + upv1*dtfrac 
           z2p = wpv1*dtfrac 
           xpi(n) = sqrt(x2p**2 + z2p**2 + 1E-10) 
           upvold = upv1 
           upv1 = upv1*x2p/xpi(n) + wpv1*z2p/xpi(n) 
           wpv1 = (-upvold*z2p/xpi(n)) + wpv1*x2p/xpi(n)
        endif
        ypi(n) = ypi(n) + vpv1*dtfrac   
     endif
  end do

  return  
end subroutine partial_push
