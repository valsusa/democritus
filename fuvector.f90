      real(kind(0.0d0)) function sdot (n, x, incx, y, incy) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: incx 
      integer , intent(in) :: incy 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
!-----------------------------------------------
!     eorks only if incx,incy=1
      if (incx/=1 .or. incy/=1) write (*, *) 'sdot fasilure' 
      sdot = 0. 
      do i = 1, n 
         sdot = sdot + x(i)*y(i) 
      end do 
      return  
      end function sdot 


 
      real(kind(0.0d0)) function snrm2 (n, x, inc) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: inc 
      real(double) , intent(in) :: x(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'snrm2 failure' 
      snrm2 = 0. 
      do i = 1, n 
         snrm2 = snrm2 + x(i)**2 
      end do 
      snrm2 = sqrt(snrm2) 
      return  
      end function snrm2 


 
      real(kind(0.0d0)) function sasum (n, x, inc) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: inc 
      real(double) , intent(in) :: x(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'sasum failure' 
      sasum = 0. 
      do i = 1, n 
         sasum = sasum + abs(x(i)) 
      end do 
      return  
      end function sasum 


 
      integer function ismax (n, x, inc) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: inc 
      real(double) , intent(in) :: x(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'ismax failure' 
      ismax = 1 
      do i = 2, n 
         if (x(i) <= x(ismax)) cycle  
         ismax = i 
      end do 
      return  
      end function ismax 


 
      integer function ismin (n, x, inc) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: inc 
      real(double) , intent(in) :: x(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
!-----------------------------------------------
!     works only if inc=1
      if (inc /= 1) write (*, *) 'ismin failure' 
      ismin = 1 
      do i = 2, n 
         if (x(i) >= x(ismin)) cycle  
         ismin = i 
      end do 
      return  
      end function ismin 


 
      subroutine second(t) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(4)  :: t 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      t=0d0
      return  
      end subroutine second 
      function ranf()
      include 'impl.inc'
!       HP
!     ranf=drand48()
!       LAHEY fortran - LINUX
      call random_number(ranf)
      return
      end


 
      integer function cvmgp (i, j, x) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i 
      integer , intent(in) :: j 
      real(double) , intent(in) :: x 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      cvmgp = j 
      if (x >= 0.D0) cvmgp = i 
      return  
      end function cvmgp 
