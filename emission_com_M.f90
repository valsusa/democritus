module emission_com_M 
  use vast_kind_param, only:  double  
  use celest2d_com_M

  logical :: maxwellian  
  integer :: emitted_species
  real(double), dimension(nreg) :: siep_emission, n0_emission
  real(double), dimension(10000) :: xp1, yp1, up1, vp1, wp1
  
end module emission_com_M
