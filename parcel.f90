      subroutine parcel 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(ntmp) :: isp, inew, jnew 
      integer , dimension(4) :: iv 
      integer , dimension(9) :: ic 
      integer :: is, ij, j, i, ipj, ipjp, ijp, imjp, imj, imjm, ijm, ipjm, np, &
         lv, lq, ijl, ijrm, ijlp, ijr, ijb, ijbp, ijt, ijtm 
      real(double), dimension(ntmp) :: uptil, vptil, wptil, upsq, uuqp, uvqp, &
         vvqp, uwqp, vwqp, uqp, vqp, wqp 
      real(double), dimension(4,ntmp) :: wc 
      real(double) :: rcol, qomdt, the, zeta, w1, w2, w3, w4, bxpar, bypar, &
         bzpar, up0, vp0, wp0, udotb, denom, thev, zetav, rvol, om2cyl, opcyl, &
         dto2, rq, wsjx, wsjy, wsjz, omsqs, denoms, wsopsq, opsqs, ws, rmv 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/parcel.f90,v 1.1.1.1 2006/06/13 23:37:45 cfichtl Exp $
!
! $Log: parcel.f90,v $
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!      dimension onur(ntmp),onul(ntmp),onll(ntmp),onlr(ntmp),
!     &     w1(ntmp),w2(ntmp),w3(ntmp),w4(ntmp), ijcel(ntmp)
 
!
!     initialize the accumulators
!
      do is = 1, nsp 
         do ij = 1, nxyp 
            qdn(ij,is) = 0.0 
            qnet0(ij) = 0.0 
            emxc(ij) = 0.0 
            emyc(ij) = 0.0 
            emxs(ij,is) = 0. 
            emys(ij,is) = 0. 
            jxtil(ij) = 0.0 
            jytil(ij) = 0.0 
            jztil(ij) = 0.0 
            dielperp(ij) = 0.0 
            dieltrns(ij) = 0.0 
            dielpar(ij) = 0.0 
            jxs(ij,is) = 0.0 
            jx0(ij) = 0.0 
            jys(ij,is) = 0.0 
            jy0(ij) = 0.0 
            jzs(ij,is) = 0.0 
            jz0(ij) = 0.0 
            qvs(ij,is) = 0.0 
            opsq(ij) = 0.0 
            pixx(ij,is) = 0.0 
            pixy(ij,is) = 0.0 
            piyx(ij,is) = 0.0 
            piyy(ij,is) = 0.0 
            pixz(ij,is) = 0.0 
            piyz(ij,is) = 0.0 
            number(ij,is) = 0.0 
            diegrid(ij) = 0.0 
            tempture(ij,is) = 0.0 
            u(ij) = 0.0 
            v(ij) = 0.0 
            qv0(ij) = 0.0 
         end do 
      end do 
!
      do j = 2, ny 
         ij = (j - 1)*nxp + 2 
         do i = 2, nx 
            ipj = ij + 1 
            ipjp = ipj + nxp 
            ijp = ipjp - 1 
            imjp = ijp - 1 
            imj = ij - 1 
            imjm = imj - nxp 
            ijm = imjm + 1 
            ipjm = ijm + 1 
!
            iv(1) = ipj 
            iv(2) = ipjp 
            iv(3) = ijp 
            iv(4) = ij 
!
            ic(1) = ij 
            ic(2) = ipj 
            ic(3) = ipjp 
            ic(4) = ijp 
            ic(5) = imjp 
            ic(6) = imj 
            ic(7) = imjm 
            ic(8) = ijm 
            ic(9) = ipjm 
!
            np = iphead(ij) 
            if (np /= 0) then 
   10          continue 
               call listmkr (np, nploc, link, lvmax, ntmp) 
               if (debug) then 
                  do lv = 1, lvmax 
!      inew(lv)=ifix(xp(nploc(lv)))
!      jnew(lv)=ifix(yp(nploc(lv)))
                     inew(lv) = int(xp(nploc(lv))) 
                     jnew(lv) = int(yp(nploc(lv))) 
                     if (i==inew(lv) .and. j==jnew(lv)) cycle  
                     write (59, 1011) i, j, nploc(lv), xp(nploc(lv)), yp(nploc(&
                        lv)) 
 1011                format(' i,j,np,xp,yp=',3i5,2(1p,e10.3)) 
                  end do 
               endif 
!
!                interpolate particle internal energy onto the grid
!
!
!
!     calculate the particle displacement velocity
!
               rcol = 1./(1. + 0.5*coll*dt) 
               do lv = 1, lvmax 
                  qomdt = 0.5*qom(ico(nploc(lv)))*dt*rcol 
!
                  the = xp(nploc(lv)) - i 
                  zeta = yp(nploc(lv)) - j 
                  w1 = the*(1. - zeta) 
                  w2 = the*zeta 
                  w3 = (1. - the)*zeta 
                  w4 = (1. - the)*(1. - zeta) 
!
                  bxpar = w1*bx(ipj) + w2*bx(ipjp) + w3*bx(ijp) + w4*bx(ij) 
                  bypar = w1*by(ipj) + w2*by(ipjp) + w3*by(ijp) + w4*by(ij) 
                  bzpar = w1*bz(ipj) + w2*bz(ipjp) + w3*bz(ijp) + w4*bz(ij) 
!
                  up0 = up(nploc(lv))*rcol 
                  vp0 = vp(nploc(lv))*rcol 
                  wp0 = wp(nploc(lv))*rcol 
!
                  udotb = up0*bxpar + vp0*bypar + wp0*bzpar 
!
                  denom = 1./(1. + (bxpar**2 + bypar**2 + bzpar**2)*qomdt**2) 
!
                  uptil(lv) = (up0 + qomdt*(vp0*bzpar - wp0*bypar + qomdt*(&
                     udotb*bxpar)))*denom 
!
                  vptil(lv) = (vp0 + qomdt*(wp0*bxpar - up0*bzpar + qomdt*(&
                     udotb*bypar)))*denom 
                  upsq(lv) = up(nploc(lv))**2 + vp(nploc(lv))**2 + wp(nploc(lv)&
                     )**2 
!
!
                  wptil(lv) = (wp0 + qomdt*(up0*bypar - vp0*bxpar + qomdt*(&
                     udotb*bzpar)))*denom 
!
               end do 
!
!dir$  ivdep
               do lv = 1, lvmax 
                  thev = xp(nploc(lv)) - i 
                  zetav = yp(nploc(lv)) - j 
                  wc(1,lv) = thev*(1. - zetav) 
                  wc(2,lv) = thev*zetav 
                  wc(3,lv) = (1. - thev)*zetav 
                  wc(4,lv) = (1. - thev)*(1. - zetav) 
                  isp(lv) = ico(nploc(lv)) 
                  uqp(lv) = uptil(lv)*qpar(nploc(lv)) 
                  vqp(lv) = vptil(lv)*qpar(nploc(lv)) 
                  wqp(lv) = wptil(lv)*qpar(nploc(lv)) 
!
                  uuqp(lv) = uqp(lv)*uptil(lv) 
                  uvqp(lv) = uqp(lv)*vptil(lv) 
                  vvqp(lv) = vqp(lv)*vptil(lv) 
                  uwqp(lv) = uqp(lv)*wptil(lv) 
                  vwqp(lv) = vqp(lv)*wptil(lv) 
!
               end do 
               do lv = 1, lvmax 
!dir$  ivdep
                  do lq = 1, 4 
!
                     jxs(iv(lq),isp(lv)) = jxs(iv(lq),isp(lv)) + wc(lq,lv)*uqp(&
                        lv) 
                     jys(iv(lq),isp(lv)) = jys(iv(lq),isp(lv)) + wc(lq,lv)*vqp(&
                        lv) 
                     jzs(iv(lq),isp(lv)) = jzs(iv(lq),isp(lv)) + wc(lq,lv)*wqp(&
                        lv) 
                     qvs(iv(lq),isp(lv)) = qvs(iv(lq),isp(lv)) + wc(lq,lv)*qpar&
                        (nploc(lv)) 
                     diegrid(iv(lq)) = diegrid(iv(lq)) + wc(lq,lv)*diepar(nploc&
                        (lv)) 
!
                  end do 
               end do 
!
               call wates (xp, yp, nploc, lvmax, wght) 
!
               do lv = 1, lvmax 
!dir$  ivdep
                  do lq = 1, 9 
!
                     qdn(ic(lq),isp(lv)) = qdn(ic(lq),isp(lv)) + wght(lq,lv)*&
                        qpar(nploc(lv)) 
!
                     emxs(ic(lq),isp(lv)) = emxs(ic(lq),isp(lv)) + wght(lq,lv)*&
                        qpar(nploc(lv))*(xpf(nploc(lv))-xcenter(3)) 
                     emys(ic(lq),isp(lv)) = emys(ic(lq),isp(lv)) + wght(lq,lv)*&
                        qpar(nploc(lv))*(ypf(nploc(lv))-ycenter(3)) 
!
!
!      number(ic(lq),isp(lv))=number(ic(lq),isp(lv))+wght(lq,lv)
!      diegrid(ic(lq))=diegrid(ic(lq))+wght(lq,lv)*
!     &   diepar(nploc(lv))
!
!
                     tempture(ic(lq),isp(lv)) = tempture(ic(lq),isp(lv)) + wght&
                        (lq,lv)*qpar(nploc(lv))*upsq(lv) 
!
!
                  end do 
               end do 
!
!     calculate the pressure using nearest-grid-point interpolation
!
               do lv = 1, lvmax 
                  pixx(ij,isp(lv)) = pixx(ij,isp(lv)) + uuqp(lv) 
!
                  pixy(ij,isp(lv)) = pixy(ij,isp(lv)) + uvqp(lv) 
!
                  piyy(ij,isp(lv)) = piyy(ij,isp(lv)) + vvqp(lv) 
!
                  pixz(ij,isp(lv)) = pixz(ij,isp(lv)) + uwqp(lv) 
!
                  piyz(ij,isp(lv)) = piyz(ij,isp(lv)) + vwqp(lv) 
!
                  number(ij,isp(lv)) = number(ij,isp(lv)) + 1. 
!
               end do 
!
!
!          get another batch of particles
!
               np = link(nploc(lvmax)) 
               if (np > 0) go to 10 
            endif 
!
            ij = ij + 1 
         end do 
      end do 
!
      do is = 1, nsp 
         do j = 1, nyp 
            do i = 1, nxp 
!
               ij = (j - 1)*nxp + i 
!
               rvol = 1./vol(ij) 
!
               pixx(ij,is) = pixx(ij,is)*rvol 
               pixy(ij,is) = pixy(ij,is)*rvol 
               piyy(ij,is) = piyy(ij,is)*rvol 
               pixz(ij,is) = pixz(ij,is)*rvol 
               piyz(ij,is) = piyz(ij,is)*rvol 
!
            end do 
         end do 
      end do 
!
!
!
!     add contribution from the ghost cells at symmetry boundary
!
!
      om2cyl = 1. - 2.*cyl 
      opcyl = 1. + cyl*(float(nx + 1)/float(nx) - 1.) 
      do is = 1, nsp 
         if (wl==(-1.) .or. wr==(-1.)) then 
            ijl = 1 
            ijrm = 2 
            ijlp = nx 
            ijr = nxp 
         else 
            ijl = 1 
            ijlp = ijl + 1 
            ijr = nxp 
            ijrm = ijr - 1 
         endif 
         do j = 1, nyp 
            qdn(ijlp,is) = qdn(ijlp,is) + qdn(ijl,is)*om2cyl 
            qdn(ijrm,is) = qdn(ijrm,is) + qdn(ijr,is)*opcyl 
            qdn(ijl,is) = qdn(ijlp,is) 
            qdn(ijr,is) = qdn(ijrm,is) 
            emxs(ijlp,is) = emxs(ijlp,is) + emxs(ijl,is)*om2cyl 
            emxs(ijrm,is) = emxs(ijrm,is) + emxs(ijr,is)*opcyl 
            emxs(ijl,is) = emxs(ijlp,is) 
            emxs(ijr,is) = emxs(ijrm,is) 
            emys(ijlp,is) = emys(ijlp,is) + emys(ijl,is)*om2cyl 
            emys(ijrm,is) = emys(ijrm,is) + emys(ijr,is)*opcyl 
            emys(ijl,is) = emys(ijlp,is) 
            emys(ijr,is) = emys(ijrm,is) 
            tempture(ijlp,is) = tempture(ijlp,is) + tempture(ijl,is)*om2cyl 
            tempture(ijrm,is) = tempture(ijrm,is) + tempture(ijr,is)*opcyl 
            tempture(ijl,is) = tempture(ijlp,is) 
            tempture(ijr,is) = tempture(ijrm,is) 
!           number(ijlp,is) = number(ijlp,is)+number(ijl,is)*om2cyl
!            number(ijrm,is) = number(ijrm,is)+number(ijr,is)*opcyl
!            number(ijl,is) = number(ijlp,is)
!            number(ijr,is) = number(ijrm,is)
            pixx(ijlp,is) = pixx(ijlp,is) + pixx(ijl,is)*om2cyl 
            pixx(ijrm,is) = pixx(ijrm,is) + pixx(ijr,is)*opcyl 
            pixx(ijl,is) = pixx(ijlp,is) 
            pixx(ijr,is) = pixx(ijrm,is) 
!
            pixy(ijlp,is) = pixy(ijlp,is) + pixy(ijl,is)*om2cyl 
            pixy(ijrm,is) = pixy(ijrm,is) + pixy(ijr,is)*opcyl 
            pixy(ijl,is) = pixy(ijlp,is) 
            pixy(ijr,is) = pixy(ijrm,is) 
!
!
            piyy(ijlp,is) = piyy(ijlp,is) + piyy(ijl,is)*om2cyl 
            piyy(ijrm,is) = piyy(ijrm,is) + piyy(ijr,is)*opcyl 
            piyy(ijl,is) = piyy(ijlp,is) 
            piyy(ijr,is) = piyy(ijrm,is) 
!
            pixz(ijlp,is) = pixz(ijlp,is) + pixz(ijl,is)*om2cyl 
            pixz(ijrm,is) = pixz(ijrm,is) + pixz(ijr,is)*opcyl 
            pixz(ijl,is) = pixz(ijlp,is) 
            pixz(ijr,is) = pixz(ijrm,is) 
!
            piyz(ijlp,is) = piyz(ijlp,is) + piyz(ijl,is)*om2cyl 
            piyz(ijrm,is) = piyz(ijrm,is) + piyz(ijr,is)*opcyl 
            piyz(ijl,is) = piyz(ijlp,is) 
            piyz(ijr,is) = piyz(ijrm,is) 
!
            ijl = ijl + nxp 
            ijlp = ijlp + nxp 
            ijr = ijr + nxp 
            ijrm = ijrm + nxp 
         end do 
         if (periodic) then 
            ijb = 1 
            ijbp = (ny - 1)*nxp + 1 
            ijt = ny*nxp + 1 
            ijtm = nxp + 1 
         else 
            ijb = 1 
            ijbp = ijb + nxp 
            ijt = ny*nxp + 1 
            ijtm = ijt - nxp 
         endif 
         do i = 1, nxp 
            qdn(ijbp,is) = qdn(ijbp,is) + qdn(ijb,is) 
            qdn(ijtm,is) = qdn(ijtm,is) + qdn(ijt,is) 
            qdn(ijb,is) = qdn(ijbp,is) 
            qdn(ijt,is) = qdn(ijtm,is) 
            emxs(ijbp,is) = emxs(ijbp,is) + emxs(ijb,is) 
            emxs(ijtm,is) = emxs(ijtm,is) + emxs(ijt,is) 
            emxs(ijb,is) = emxs(ijbp,is) 
            emxs(ijt,is) = emxs(ijtm,is) 
            emys(ijbp,is) = emys(ijbp,is) + emys(ijb,is) 
            emys(ijtm,is) = emys(ijtm,is) + emys(ijt,is) 
            emys(ijb,is) = emys(ijbp,is) 
            emys(ijt,is) = emys(ijtm,is) 
            tempture(ijbp,is) = tempture(ijbp,is) + tempture(ijb,is) 
            tempture(ijtm,is) = tempture(ijtm,is) + tempture(ijt,is) 
            tempture(ijb,is) = tempture(ijbp,is) 
            tempture(ijt,is) = tempture(ijtm,is) 
!            number(ijbp,is) = number(ijbp,is)+number(ijb,is)
!            number(ijtm,is) = number(ijtm,is)+number(ijt,is)
!            number(ijb,is) = number(ijbp,is)
!            number(ijt,is) = number(ijtm,is)
            pixx(ijbp,is) = pixx(ijbp,is) + pixx(ijb,is) 
            pixx(ijtm,is) = pixx(ijtm,is) + pixx(ijt,is) 
            pixx(ijb,is) = pixx(ijbp,is) 
            pixx(ijt,is) = pixx(ijtm,is) 
!
            pixy(ijbp,is) = pixy(ijbp,is) + pixy(ijb,is) 
            pixy(ijtm,is) = pixy(ijtm,is) + pixy(ijt,is) 
            pixy(ijb,is) = pixy(ijbp,is) 
            pixy(ijt,is) = pixy(ijtm,is) 
!
!
            piyy(ijbp,is) = piyy(ijbp,is) + piyy(ijb,is) 
            piyy(ijtm,is) = piyy(ijtm,is) + piyy(ijt,is) 
            piyy(ijb,is) = piyy(ijbp,is) 
            piyy(ijt,is) = piyy(ijtm,is) 
!
            pixz(ijbp,is) = pixz(ijbp,is) + pixz(ijb,is) 
            pixz(ijtm,is) = pixz(ijtm,is) + pixz(ijt,is) 
            pixz(ijb,is) = pixz(ijbp,is) 
            pixz(ijt,is) = pixz(ijtm,is) 
!
            piyz(ijbp,is) = piyz(ijbp,is) + piyz(ijb,is) 
            piyz(ijtm,is) = piyz(ijtm,is) + piyz(ijt,is) 
            piyz(ijb,is) = piyz(ijbp,is) 
            piyz(ijt,is) = piyz(ijtm,is) 
!
            ijb = ijb + 1 
            ijbp = ijbp + 1 
            ijt = ijt + 1 
            ijtm = ijtm + 1 
         end do 
!
      end do 
!
      do is = 1, nsp 
         call bcj (1, nxp, 2, nxp, 2, nyp, periodic, wl, wr, wb, wt, jxs(1,is)&
            , jys(1,is), jzs(1,is)) 
         call bcv (1, nxp, 1, nxp, 2, nyp, qvs(1,is)) 
      end do 
      call bcv (1, nxp, 1, nxp, 2, nyp, diegrid) 
      if (hot) then 
!
!     correct the specie current for finite pressure effects
!
         do is = 1, nsp 
!
            call gradp (1, nxp, 2, nxp, 2, nyp, cx1, cx2, cx3, cx4, cy1, cy2, &
               cy3, cy4, cr1, cr2, cr3, cr4, pixx(1,is), pixy(1,is), pixy(1,is)&
               , piyy(1,is), pixz(1,is), piyz(1,is), gradpx, gradpy, gradpz) 
!
            dto2 = 0.5*dt 
!
            do j = 2, nyp 
               do i = 2, nxp 
                  ij = (j - 1)*nxp + i 
                  imj = ij - 1 
                  imjm = ij - nxp - 1 
                  ijm = ij - nxp 
!     ***************************************
!
!     calculate the current at t+.5*dt
!
!     *******************************************
                  jxs(ij,is) = jxs(ij,is) - gradpx(ij)*dto2 
                  jys(ij,is) = jys(ij,is) - gradpy(ij)*dto2 
                  jzs(ij,is) = jzs(ij,is) - gradpz(ij)*dto2 
!
               end do 
            end do 
!
            call bcj (1, nxp, 2, nxp, 2, nyp, periodic, wl, wr, wb, wt, jxs(1,&
               is), jys(1,is), jzs(1,is)) 
!
         end do 
      endif 
!
!     accumulate the net charge and current
!
      rcol = 1./(1. + coll*dto2) 
      dto2 = 0.5*dt*rcol 
      do is = 1, nsp 
         qomdt = qom(is)*dto2 
!
         rq = 1./qom(is) 
!
         do j = 2, nyp 
            do i = 2, nxp 
!
               ij = (j - 1)*nxp + i 
!
!
!     calculate the vertex charge density from the vertex charge
!
               qvs(ij,is) = qvs(ij,is)/vvol(ij) 
!
               denom = 1./(1. + (bx(ij)**2+by(ij)**2+bz(ij)**2)*qomdt**2) 
               wsjx = jxs(ij,is)*rcol 
               wsjy = jys(ij,is)*rcol 
               wsjz = jzs(ij,is)*rcol 
!
!
               jxtil(ij) = jxtil(ij) + wsjx 
!
               jytil(ij) = jytil(ij) + wsjy 
!
               jztil(ij) = jztil(ij) + wsjz 
!
!
               if(conductor(is).and. is.ge.nsp_dust) then
               ! do nothing
               else
               qnet0(ij) = qnet0(ij) + qdn(ij,is) 
               end if
               emxc(ij) = emxc(ij) + emxs(ij,is) 
               emyc(ij) = emyc(ij) + emys(ij,is) 
               pixx0(ij) = pixx0(ij) + pixx(ij,is) 
               pixy0(ij) = pixy0(ij) + pixy(ij,is) 
               piyy0(ij) = piyy0(ij) + piyy(ij,is) 
!
!     calculate the elements of the dielectric tensor
!
               omsqs = (bx(ij)**2+by(ij)**2+bz(ij)**2)*qomdt**2 
               denoms = 1./(1. + omsqs) 
               wsopsq = qom(is)*qvs(ij,is) 
               opsqs = wsopsq*0.5*cntr*dt**2*rcol 
!
               dielperp(ij) = dielperp(ij) + opsqs*denoms 
               dieltrns(ij) = dieltrns(ij) + opsqs*denoms*qomdt 
               dielpar(ij) = dielpar(ij) + opsqs*denoms*qomdt**2 
!
               opsq(ij) = opsq(ij) + wsopsq 
!
               jx0(ij) = jx0(ij) + jxs(ij,is) 
               jy0(ij) = jy0(ij) + jys(ij,is) 
               jz0(ij) = jz0(ij) + jzs(ij,is) 
!
!
!     calculate the temperature
!
               ws = number(ij,is)/(number(ij,is)+1.E-10)**2 
               tempture(ij,is) = 0.5*tempture(ij,is)*ws/qom(is) 
!     calculate the center of mass velocity
!
               u(ij) = u(ij) + jxs(ij,is)*rq 
               v(ij) = v(ij) + jys(ij,is)*rq 
               qv0(ij) = qv0(ij) + qvs(ij,is) 
               mv(ij) = mv(ij) + qvs(ij,is)*rq 
            end do 
         end do 
      end do 
!
!
!     transfor the current to the center of mass frame
!
      do ij = 1, nxyp 
         rmv = 1./(mv(ij)+tiny) 
         u(ij) = u(ij)*rmv 
         v(ij) = v(ij)*rmv 
!jub         jx0(ij)=jx0(ij)-qv0(ij)*u(ij)
!jub         jy0(ij)=jy0(ij)-qv0(ij)*v(ij)
      end do 
!
!     do is=1,nsp
!     do i=2,nx
!     do j=2,ny
!         ij = (j-1)*nxp+2
!         write(*,*)'parcel: ',ij,is,number(ij,is)
!     enddo
!     enddo
!     enddo
!
!       normalize dielectric
!
      do ij = 1, nxyp 
!        diegrid(ij)=diegrid(ij)/(vol(ij)+1.e-10)
         diegrid(ij) = diegrid(ij)/(vvol(ij)+1.E-10) 
         if(diegrid(ij).gt.chimin) diegrid(ij)=diem(nsp_dust)
      end do 
      return  
 
      end subroutine parcel 
