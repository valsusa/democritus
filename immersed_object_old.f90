subroutine inter_p_immersed_object(niteration)
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
	  use mover_data
      use celest2d_com_M 
!	  	  
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
      integer :: niteration
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------

	  !
! Handling the particle crossing with spherical object
!
!
			   
            do lv=1,lvmax
			
			   discard(lv)=.false.
			   
			   do is=nsp_dust,nsp

                  radp = (xpc(lv)-xcenter(is))**2/rex(is)**2 + (ypc(lv)-ycenter(is))**2/rey(is)**2
                  if (radp <= 1d0) then 
				  
                     discard(lv) = .TRUE.

			  if(isp(lv).le.nsp_dust) then
                  nctheta=floor(thetadim/2*(1+(ypc(lv)-ycenter(is))/rey(is)))+1
                  qhere=abs(qpar(nploc(lv)))
                  
                  thetadust(1,isp(lv),nctheta)=thetadust(1,isp(lv),nctheta)+&
                       qhere
                  thetadust(2,isp(lv),nctheta)=thetadust(2,isp(lv),nctheta)+&
                       qhere*upv1(lv)
                  thetadust(3,isp(lv),nctheta)=thetadust(3,isp(lv),nctheta)+&
                       qhere*vpv1(lv)
                  thetadust(4,isp(lv),nctheta)=thetadust(4,isp(lv),nctheta)+&
                       qhere*wpv1(lv)

				  fdragx(nh,is) = fdragx(nh,is) - massp(lv)*up(nploc(lv))/&
                     dt
                  fdragy(nh,is) = fdragy(nh,is) - massp(lv)*vp(nploc(lv))/&
                     dt 

               endif
			   
			       isp(lv) = is
				   ico(nploc(lv)) = is 
					 
					 upv1(lv) = 0.D0 
                     vpv1(lv) = 0.D0 
					 wpv1(lv) = 0.D0 

				  endif 
			   enddo   
			     
			   feletx(nh,isp(lv))=feletx(nh,isp(lv))+ massp(lv)*(upv1(lv)-up(nploc(lv)))/dt
			   felety(nh,isp(lv))=felety(nh,isp(lv))+ massp(lv)*(vpv1(lv)-vp(nploc(lv)))/dt
            end do
 
 
 ! Handling the particle counts and kin energy diff with last subcycling. Even
 ! the particles not advancesd this iteration should participate
       if (niteration.eq.nsubmax) then
            do lv = 1, lvmax
               qdustp(nh,isp(lv)) = qdustp(nh,isp(lv)) + qptmp(lv) 

               dkdt(ij,isp(lv)) = dkdt(ij,isp(lv)) + 0.5*massp(lv)*(upv1(lv)**2&
                  +vpv1(lv)**2+wpv1(lv)**2-upv(lv)**2-vpv(lv)**2-wpv(lv)**2) 
            end do 
       end if


end subroutine inter_p_immersed_object



subroutine diagnostics_particles(niteration)
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
	  use mover_data
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
integer :: niteration
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!


      if(niteration.eq.nsubmax) then
	  
 ! Handling the particle counts and kin energy diff with last subcycling. Even
 ! the particles not advancesd this iteration should participate
            do lv = 1, lvmax
               qdustp(nh,isp(lv)) = qdustp(nh,isp(lv)) + qptmp(lv) 

               dkdt(ij,isp(lv)) = dkdt(ij,isp(lv)) + 0.5*massp(lv)*(upv1(lv)**2&
                  +vpv1(lv)**2+wpv1(lv)**2-upv(lv)**2-vpv(lv)**2-wpv(lv)**2) 
				  
				  emomdustxp(nh,isp(lv)) = emomdustxp(nh,isp(lv)) + qptmp(lv)*(xpc(lv)-&
                     xcenter(is)) 
                  emomdustyp(nh,isp(lv)) = emomdustyp(nh,isp(lv)) + qptmp(lv)*(ypc(lv)-&
                     ycenter(is)) 
				   
            end do 
!
!     output dust diagnostics
!


         dnorm = siep(2)**2*(1. + cyl*(siep(2)-1.)) 
         rld3 = (siep(2)/siep(1)*sqrt(qom(1)))**3 
         do is = nsp_dust, nsp 
            vexact = rex(is)**3/3. 
            emomdustxp(nh,is) = emomdustxp(nh,is)/(qdustp(nh,is)*rex(is)+1.E-10) 
            emomdustyp(nh,is) = emomdustyp(nh,is)/(qdustp(nh,is)*rex(is)+1.E-10) 
			
			fdragx(nh,is) = fdragx(nh,is)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdustp(nh,is)+1d-10)
			fdragy(nh,is) = fdragy(nh,is)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdustp(nh,is)+1d-10)
			feletx(nh,is) = feletx(nh,is)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdustp(nh,is)+1d-10)
			felety(nh,is) = felety(nh,is)/siep(2)**2*(siep(1)/sqrt(qom(1)))/(qdustp(nh,is)+1d-10)
            qdustp(nh,is) = qdustp(nh,is)/dnorm*2*pi 
            
            if (mod(ncyc,10) == 0) then
               write (*, *) 'PARMOVE - SPECIES ', is 
               write (*, *) 'time,charge,dipx,dipy  ', t, qdustp(nh,is), &
                    emomdustxp(nh,is), emomdustyp(nh,is) 
               write (*, *) 'forces: drag (x,y)  ', fdragx(nh,is), &
                    fdragy(nh,is) !, feletx(nh,is), felety(nh,is) 
               write (30 + is, 176) t, qdustp(nh,is), emomdustxp(nh,is), &
                    emomdustyp(nh,is), fdragx(nh,is), fdragy(nh,is) !, feletx(nh,is), &
                    ! felety(nh,is) 
               IF(collision) THEN 
                  write (*, *) 'CHECK on velocity, velmax=',velmax,quale
                  write(*,*) 'nptotal=', ntotal(nh)
               ENDIF
            endif
  176    format(1x,8(e12.5,2x))
         end do

!      write(30+is,176)t,
!     & qdustp(nh,is),emomdustxp(nh,is),emomdustyp(nh,is),
!     & fdragx(nh,is),fdragy(nh,is),feletx(nh,is),felety(nh,is)
end if

end subroutine diagnostics_particles

subroutine initialize_particle_diagnostics(niteration)
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
	  use mover_data
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
integer :: niteration
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!

!     get the centreof the dust
!

      if (niteration.eq.1) then
       !  do n = 1, nrg 
       !     do ij = 1, nxyp 
       !        dkdt(ij,n) = 0.0 
       !     end do
        ! end do
         do is = nsp_dust, nsp
            qdustp(nh,is) = 0. 
            emomdustxp(nh,is) = 0. 
            emomdustyp(nh,is) = 0. 
            fdragx(nh,is) = 0. 
            fdragy(nh,is) = 0. 
            feletx(nh,is) = 0. 
            felety(nh,is) = 0. 
         end do
      end if
	  
end subroutine initialize_particle_diagnostics	  
