      subroutine etilde(nqi, nqj, nxyp, nsp, i1, i2, j1, j2, cyl, dt, qom, ex, &
         ey, bx, by, bz, extil, eytil, eztil) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: nxyp 
      integer , intent(in) :: nsp 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double)  :: cyl 
      real(double) , intent(in) :: dt 
      real(double) , intent(in) :: qom(*) 
      real(double) , intent(in) :: ex(*) 
      real(double) , intent(in) :: ey(*) 
      real(double) , intent(in) :: bx(*) 
      real(double) , intent(in) :: by(*) 
      real(double) , intent(in) :: bz(*) 
      real(double) , intent(out) :: extil(nxyp,*) 
      real(double) , intent(out) :: eytil(nxyp,*) 
      real(double) , intent(out) :: eztil(nxyp,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: is, j, i, ij 
      real(double) :: dto2, qomdt, denom, edotb 
!-----------------------------------------------
!
      dto2 = 0.5*dt 
!
      do is = 1, nsp 
!
         qomdt = qom(is)*dto2 
!
!
         do j = j1, j2 
            do i = i1, i2 
!
               ij = (j - 1)*nqj + (i - 1)*nqi + 1 
!
               denom = 1./(1. + (bx(ij)**2+by(ij)**2+bz(ij)**2)*qomdt**2) 
!
               edotb = ex(ij)*bx(ij) + ey(ij)*by(ij) 
!
               extil(ij,is) = (ex(ij)+qomdt*(ey(ij)*bz(ij)+qomdt*edotb*bx(ij)))&
                  *denom 
!
               eytil(ij,is) = (ey(ij)+qomdt*((-ex(ij)*bz(ij))+qomdt*edotb*by(ij&
                  )))*denom 
!
               eztil(ij,is) = qomdt*((ex(ij)*by(ij)-ey(ij)*bx(ij))+qomdt*edotb*&
                  bz(ij))*denom 
!
            end do 
         end do 
!
      end do 
!
      return  
      end subroutine etilde 
