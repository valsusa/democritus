      subroutine fulout(icall) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use netcdf_M 
      use celest2d_com_M 
      use cplot_com_M 
      use work_variables
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: icall 
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(nrg) :: icount 
      integer :: status, nh, is, ij, i, j, k
      real(double) :: scalar  
      real(double), dimension(nxp,nyp,nsp) :: plotxys
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer :: nf_inq_base_pe, nf_set_base_pe, nf_create, nf__create, &
         nf__create_mp, nf_open, nf__open, nf__open_mp, nf_set_fill, nf_redef, &
         nf_enddef, nf__enddef, nf_sync, nf_abort, nf_close, nf_delete, nf_inq&
         , nf_inq_ndims, nf_inq_nvars, nf_inq_natts, nf_inq_unlimdim, &
         nf_def_dim, nf_inq_dimid, nf_inq_dim, nf_inq_dimname, nf_inq_dimlen, &
         nf_rename_dim, nf_inq_att, nf_inq_attid, nf_inq_atttype, nf_inq_attlen&
         , nf_inq_attname, nf_copy_att, nf_rename_att, nf_del_att, &
         nf_put_att_text, nf_get_att_text, nf_put_att_int1, nf_get_att_int1, &
         nf_put_att_int2, nf_get_att_int2, nf_put_att_int, nf_get_att_int, &
         nf_put_att_real, nf_get_att_real, nf_put_att_double, nf_get_att_double&
         , nf_def_var, nf_inq_var, nf_inq_varid, nf_inq_varname, nf_inq_vartype&
         , nf_inq_varndims, nf_inq_vardimid, nf_inq_varnatts, nf_rename_var, &
         nf_copy_var, nf_put_var_text, nf_get_var_text, nf_put_var_int1, &
         nf_get_var_int1, nf_put_var_int2, nf_get_var_int2, nf_put_var_int, &
         nf_get_var_int, nf_put_var_real, nf_get_var_real, nf_put_var_double, &
         nf_get_var_double, nf_put_var1_text, nf_get_var1_text, &
         nf_put_var1_int1, nf_get_var1_int1, nf_put_var1_int2, nf_get_var1_int2&
         , nf_put_var1_int, nf_get_var1_int, nf_put_var1_real, nf_get_var1_real&
         , nf_put_var1_double, nf_get_var1_double, nf_put_vara_text, &
         nf_get_vara_text, nf_put_vara_int1, nf_get_vara_int1, nf_put_vara_int2&
         , nf_get_vara_int2, nf_put_vara_int, nf_get_vara_int, nf_put_vara_real&
         , nf_get_vara_real, nf_put_vara_double, nf_get_vara_double, &
         nf_put_vars_text, nf_get_vars_text, nf_put_vars_int1, nf_get_vars_int1&
         , nf_put_vars_int2, nf_get_vars_int2, nf_put_vars_int, nf_get_vars_int&
         , nf_put_vars_real, nf_get_vars_real, nf_put_vars_double, &
         nf_get_vars_double, nf_put_varm_text, nf_get_varm_text, &
         nf_put_varm_int1, nf_get_varm_int1, nf_put_varm_int2, nf_get_varm_int2&
         , nf_put_varm_int, nf_get_varm_int, nf_put_varm_real, nf_get_varm_real&
         , nf_put_varm_double, nf_get_varm_double, nccre, ncopn, ncddef, ncdid&
         , ncvdef, ncvid, nctlen, ncsfil 
      logical :: nf_issyserr 
      character :: nf_inq_libvers*80, nf_strerror*80 
!-----------------------------------------------
!
!
      nh = mod(ncyc,nhst) 
!
!     write history variables every time step:
!     write current time to file
!
      scalar = t 
      status = nf_put_vara_double(cdfidh,idtimeh,ncyc,1,scalar) 
!     write history variables
      scalar = xmom(nh) 
      status = nf_put_vara_double(cdfidh,idxmom,ncyc,1,scalar) 
      scalar = ymom(nh) 
      status = nf_put_vara_double(cdfidh,idymom,ncyc,1,scalar) 
      scalar = qtotal(nh) 
      status = nf_put_vara_double(cdfidh,idqtotal,ncyc,1,scalar) 
      scalar = ntotal(nh) 
      status = nf_put_vara_double(cdfidh,idntotal,ncyc,1,scalar) 
      do is = 1, nsp 
         pnsp(is) = flux_inject_left(nh,is) 
      end do 
      call write2dbl (cdfidh, idinjleft, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = flux_inject_right(nh,is) 
      end do 
      call write2dbl (cdfidh, idinjright, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = flux_inject_top(nh,is) 
      end do 
      call write2dbl (cdfidh, idinjtop, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = flux_inject_bottom(nh,is) 
      end do 
      call write2dbl (cdfidh, idinjbottom, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = flux_emitted(nh,is) 
      end do 
      call write2dbl (cdfidh, idemitted, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = qdust(nh,is) 
      end do 
      call write2dbl (cdfidh, idqdust, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = qdustp(nh,is) 
      end do 
      call write2dbl (cdfidh, idqdustp, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = phidust(nh,is) 
      end do 
      call write2dbl (cdfidh, idpdust, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = emomdustx(nh,is) 
      end do 
      call write2dbl (cdfidh, idxmdust, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = emomdusty(nh,is) 
      end do 
      call write2dbl (cdfidh, idymdust, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = emomdustxp(nh,is) 
      end do 
      call write2dbl (cdfidh, idxmdustp, pnsp, ncyc, nsp) 
      do is = 1, nsp 
         pnsp(is) = emomdustyp(nh,is) 
      end do 
      call write2dbl (cdfidh, idymdustp, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = forcespx(nh,is) 
      end do 
      call write2dbl (cdfidh, idforcespx, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = forcespy(nh,is) 
      end do 
      call write2dbl (cdfidh, idforcespy, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = fdragx(nh,is) 
      end do 
      call write2dbl (cdfidh, idfdragx, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = fdragy(nh,is) 
      end do 
      call write2dbl (cdfidh, idfdragy, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = feletx(nh,is) 
      end do 
      call write2dbl (cdfidh, idfeletx, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = felety(nh,is) 
      end do 
      call write2dbl (cdfidh, idfelety, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = eforce_partx(nh,is) 
      end do 
      call write2dbl (cdfidh, idefpx, pnsp, ncyc, nsp) 
	  do is = 1, nsp 
         pnsp(is) = eforce_party(nh,is) 
      end do 
      call write2dbl (cdfidh, idefpy, pnsp, ncyc, nsp) 
!
!     check if it is time to plot other data:
!

      if (icall < 0) return  
!
!     increment the plot counter:
      write (*, *) 'saving fields and particles, time=', t 
      iplot = iplot + 1 
!
!     write MATLAB ASCII FILE
!
!      call outavs
!
!     write current time and cycle number to file
!
      scalar = t 
      status = nf_put_vara_double(cdfid,idtime,iplot,1,scalar) 
      status = nf_put_vara_int(cdfid,idcyc,iplot,1,ncyc) 
      status = nf_put_vara_double(cdfidp,idtimep,iplot,1,scalar) 
      status = nf_put_vara_int(cdfidp,idcycp,iplot,1,ncyc) 
 
!
!     write other grid variables as required:
!     grid
      do ij = 1, nxp*nyp 
         plotx(ij) = x(ij) 
         ploty(ij) = y(ij) 
      end do 
!
!     write grid
!
      call write3dbl (cdfid, idnx, plotx, iplot, nxp, nyp) 
      call write3dbl (cdfid, idny, ploty, iplot, nxp, nyp) 
!        needed for particles also
      call write3dbl (cdfidp, idnxp, plotx, iplot, nxp, nyp) 
      call write3dbl (cdfidp, idnyp, ploty, iplot, nxp, nyp) 
!
!     write fields
!
      call write3dbl (cdfid, idpot, phiavg, iplot, nxp, nyp) 
      call write3dbl (cdfid, idex, ex, iplot, nxp, nyp) 
      call write3dbl (cdfid, idey, ey, iplot, nxp, nyp) 
      call write3dbl (cdfid, idrho, qnet0, iplot, nxp, nyp) 
      call write3dbl (cdfid, iddiegrid, diegrid, iplot, nxp, nyp) 
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            do is = 1, nsp 
               plotxys(i,j,is) = qdn(ij,is) 
            end do 
         end do 
      end do 
      call write4dbl (cdfid, idqdn, plotxys, iplot, nxp, nyp, nsp) 
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            do is = 1, nsp 
               plotxys(i,j,is) = jxs(ij,is) 
            end do 
         end do 
      end do 
      call write4dbl (cdfid, idjxs, plotxys, iplot, nxp, nyp, nsp) 
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            do is = 1, nsp 
               plotxys(i,j,is) = jys(ij,is) 
            end do 
         end do 
      end do 
      call write4dbl (cdfid, idjys, plotxys, iplot, nxp, nyp, nsp) 
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            do is = 1, nsp 
               plotxys(i,j,is) = number(ij,is) 
            end do 
         end do 
      end do 
      call write4dbl (cdfid, idnumber, plotxys, iplot, nxp, nyp, nsp) 
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            do is = 1, nsp 
               plotxys(i,j,is) = uhst(ij,is) 
            end do 
         end do 
      end do 
      call write4dbl (cdfid, iduhst, plotxys, iplot, nxp, nyp, nsp) 
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            do is = 1, nsp 
               plotxys(i,j,is) = vhst(ij,is) 
            end do 
         end do 
      end do 
      call write4dbl (cdfid, idvhst, plotxys, iplot, nxp, nyp, nsp)

!(Leo)

      call write4dbl(cdfid, idtd, thetadust, iplot,4,nsp,thetadim)
      do i=1,4
         do j=1,nsp
            do k=1,thetadim
               thetadust(i,j,k)=0
            enddo
         enddo
      enddo



 
!
!     particles:
!
      if(iout(20)==1) then
      if (nrg /= 0) then 
!
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, xpf, pxtmp) 
         call write2dbl (cdfidp, idxp, pxtmp, iplot, numtotl) 
 
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, ypf, pxtmp) 
         call write2dbl (cdfidp, idyp, pxtmp, iplot, numtotl) 
 
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, up, pxtmp) 
         call write2dbl (cdfidp, idup, pxtmp, iplot, numtotl) 
 
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, vp, pxtmp) 
         call write2dbl (cdfidp, idvp, pxtmp, iplot, numtotl) 
 
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, wp, pxtmp) 
         call write2dbl (cdfidp, idwp, pxtmp, iplot, numtotl) 
 
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, qpar, pxtmp) 
         call write2dbl (cdfidp, idqp, pxtmp, iplot, numtotl) 
 
         pxtmp2 = real(ico) 
         call repack (nsp, icount, numtot, link, nxp, nyp, iphead, jchead, &
            ichead, ico, pxtmp2, pxtmp) 
         call write2dbl (cdfidp, idcp, pxtmp, iplot, numtotl) 
 
         call write2int (cdfidp, idnumtot, numtot, iplot, nsp) 
      endif 
      endif
!
!     sync files:
      status = nf_sync(cdfid) 
      status = nf_sync(cdfidh) 
      status = nf_sync(cdfidp) 
!
      return  
      end subroutine fulout 


!
!---------------------------------------------------------------
      subroutine write2int(cdfid, idvar, ivar, iplot, ndim) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: cdfid 
      integer  :: idvar 
      integer , intent(in) :: iplot 
      integer , intent(in) :: ndim 
      integer  :: ivar(ndim) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: status 
      integer, dimension(2) :: istart, icount 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: nf_put_vara_int 
!-----------------------------------------------
!
! generic netcdf write of particle data assuming start at 1
      istart(1) = 1 
      istart(2) = iplot 
!
      icount(1) = ndim 
      icount(2) = 1 
!
      status = nf_put_vara_int(cdfid,idvar,istart,icount,ivar) 
!
      return  
      end subroutine write2int 


!---------------------------------------------------------------
      subroutine write2dbl(cdfid, idvar, var, iplot, npart) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: cdfid 
      integer  :: idvar 
      integer , intent(in) :: iplot 
      integer , intent(in) :: npart 
      real(double)  :: var(npart) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: status 
      integer, dimension(2) :: istart, icount 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: nf_put_vara_double 
!-----------------------------------------------
!
! generic netcdf write of particle data assuming start at 1
      istart(1) = 1 
      istart(2) = iplot 
!
      icount(1) = npart 
      icount(2) = 1 
!
      status = nf_put_vara_double(cdfid,idvar,istart,icount,var) 
!
      return  
      end subroutine write2dbl 


!---------------------------------------------------------------
 
      subroutine write3dbl(cdfid, idvar, var, iplot, nxp, nyp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: cdfid 
      integer  :: idvar 
      integer , intent(in) :: iplot 
      integer , intent(in) :: nxp 
      integer , intent(in) :: nyp 
      real(double)  :: var(nxp,nyp) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: status 
      integer, dimension(3) :: istart, icount 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: nf_put_vara_double 
!-----------------------------------------------
 
! generic netcdf write of grid quantities assuming start at 1
      istart(1) = 1 
      istart(2) = 1 
      istart(3) = iplot 
!
      icount(1) = nxp 
      icount(2) = nyp 
      icount(3) = 1 
!
      status = nf_put_vara_double(cdfid,idvar,istart,icount,var) 
!
      return  
      end subroutine write3dbl 


!---------------------------------------------------------------
 
      subroutine write4dbl(cdfid, idvar, var, iplot, nxp, nyp, nsp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: cdfid 
      integer  :: idvar 
      integer , intent(in) :: iplot 
      integer , intent(in) :: nxp 
      integer , intent(in) :: nyp 
      integer , intent(in) :: nsp 
      real(double)  :: var(nxp,nyp,nsp) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: status 
      integer, dimension(4) :: istart, icount 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: nf_put_vara_double 
!-----------------------------------------------
 
! generic netcdf write of grid quantities assuming start at 1
      istart(1) = 1 
      istart(2) = 1 
      istart(3) = 1 
      istart(4) = iplot 
!
      icount(1) = nxp 
      icount(2) = nyp 
      icount(3) = nsp 
      icount(4) = 1 
!
      status = nf_put_vara_double(cdfid,idvar,istart,icount,var) 
!
      return  
      end subroutine write4dbl 


!---------------------------------------------------------------
!
      subroutine repack(nrg, icount, numtot, link, nxp, nyp, iphead, iphd2, &
         ijctmp, ico, px, pxtmp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nrg 
      integer , intent(in) :: nxp 
      integer , intent(in) :: nyp 
      integer , intent(inout) :: icount(*) 
      integer , intent(in) :: numtot(*) 
      integer , intent(in) :: link(*) 
      integer , intent(in) :: iphead(*) 
      integer , intent(inout) :: iphd2(*) 
      integer , intent(inout) :: ijctmp(*) 
      integer , intent(in) :: ico(*) 
      real(double) , intent(in) :: px(*) 
      real(double) , intent(out) :: pxtmp(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: kr, newcell, i, j, ij, np, n, iploc, newcell_nxt 
!-----------------------------------------------
!
!
!      repack particles:
 
      icount(1) = 1 
      do kr = 2, nrg 
         icount(kr) = icount(kr-1) + numtot(kr-1) 
      end do 
!
      newcell = 0 
      do i = 2, nxp - 1 
         do j = 2, nyp - 1 
            ij = i + nxp*(j - 1) 
            np = iphead(ij) 
            if (np <= 0) cycle  
            newcell = newcell + 1 
            ijctmp(newcell) = ij 
            iphd2(ij) = np 
         end do 
      end do 
!
!
   15 continue 
      if (newcell == 0) go to 500 
!
      do n = 1, newcell 
         ij = ijctmp(n) 
         np = iphd2(ij) 
         kr = ico(np) 
         iploc = icount(kr) 
         pxtmp(iploc) = px(np) 
         icount(kr) = icount(kr) + 1 
      end do 
!
      newcell_nxt = 0 
      do n = 1, newcell 
         ij = ijctmp(n) 
         np = link(iphd2(ij)) 
         if (np <= 0) cycle  
         newcell_nxt = newcell_nxt + 1 
         iphd2(ij) = np 
         ijctmp(newcell_nxt) = ij 
      end do 
      newcell = newcell_nxt 
!
      go to 15 
!
  500 continue 
      return  
      end subroutine repack 


!
!---------------------------------------------------------------
      subroutine plotclose 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
      use cplot_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: status 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: nf_close 
!-----------------------------------------------
      status = nf_close(cdfid) 
      status = nf_close(cdfidh) 
      status = nf_close(cdfidp) 
      return  
      end subroutine plotclose 


!
!---------------------------------------------------------------
      subroutine plotinit(ibp2, jbp2, npart, nrg, nsp, thetadim) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use netcdf_M 
      use cplot_com_M
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
! Routine to create netcdf file and define variable, dimensions, etc.
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: ibp2 
      integer  :: jbp2 
      integer  :: npart 
      integer  :: nrg 
      integer  :: nsp
!(Leo) 
      integer :: thetadim
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(4) :: dims 
      integer :: status, nspdimh, nspdim, nxdim, nydim, nxdimp, nydimp, npdim, &
         nspdimp,four,tdim !(Leo) 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer :: nf_inq_base_pe, nf_set_base_pe, nf_create, nf__create, &
         nf__create_mp, nf_open, nf__open, nf__open_mp, nf_set_fill, nf_redef, &
         nf_enddef, nf__enddef, nf_sync, nf_abort, nf_close, nf_delete, nf_inq&
         , nf_inq_ndims, nf_inq_nvars, nf_inq_natts, nf_inq_unlimdim, &
         nf_def_dim, nf_inq_dimid, nf_inq_dim, nf_inq_dimname, nf_inq_dimlen, &
         nf_rename_dim, nf_inq_att, nf_inq_attid, nf_inq_atttype, nf_inq_attlen&
         , nf_inq_attname, nf_copy_att, nf_rename_att, nf_del_att, &
         nf_put_att_text, nf_get_att_text, nf_put_att_int1, nf_get_att_int1, &
         nf_put_att_int2, nf_get_att_int2, nf_put_att_int, nf_get_att_int, &
         nf_put_att_real, nf_get_att_real, nf_put_att_double, nf_get_att_double&
         , nf_def_var, nf_inq_var, nf_inq_varid, nf_inq_varname, nf_inq_vartype&
         , nf_inq_varndims, nf_inq_vardimid, nf_inq_varnatts, nf_rename_var, &
         nf_copy_var, nf_put_var_text, nf_get_var_text, nf_put_var_int1, &
         nf_get_var_int1, nf_put_var_int2, nf_get_var_int2, nf_put_var_int, &
         nf_get_var_int, nf_put_var_real, nf_get_var_real, nf_put_var_double, &
         nf_get_var_double, nf_put_var1_text, nf_get_var1_text, &
         nf_put_var1_int1, nf_get_var1_int1, nf_put_var1_int2, nf_get_var1_int2&
         , nf_put_var1_int, nf_get_var1_int, nf_put_var1_real, nf_get_var1_real&
         , nf_put_var1_double, nf_get_var1_double, nf_put_vara_text, &
         nf_get_vara_text, nf_put_vara_int1, nf_get_vara_int1, nf_put_vara_int2&
         , nf_get_vara_int2, nf_put_vara_int, nf_get_vara_int, nf_put_vara_real&
         , nf_get_vara_real, nf_put_vara_double, nf_get_vara_double, &
         nf_put_vars_text, nf_get_vars_text, nf_put_vars_int1, nf_get_vars_int1&
         , nf_put_vars_int2, nf_get_vars_int2, nf_put_vars_int, nf_get_vars_int&
         , nf_put_vars_real, nf_get_vars_real, nf_put_vars_double, &
         nf_get_vars_double, nf_put_varm_text, nf_get_varm_text, &
         nf_put_varm_int1, nf_get_varm_int1, nf_put_varm_int2, nf_get_varm_int2&
         , nf_put_varm_int, nf_get_varm_int, nf_put_varm_real, nf_get_varm_real&
         , nf_put_varm_double, nf_get_varm_double, nccre, ncopn, ncddef, ncdid&
         , ncvdef, ncvid, nctlen, ncsfil 
      logical :: nf_issyserr 
      character :: nf_inq_libvers*80, nf_strerror*80 
!-----------------------------------------------
!
!     initialize counter for plots
      iplot = 0 
!
!     netcdf for history plots
!
!     create ntecdf file
      status = nf_create('Celh.dat',nf_clobber,cdfidh) 
!     define dimensions
      status = nf_def_dim(cdfidh,'time',nf_unlimited,timdimh) 
      status = nf_def_dim(cdfidh,'nsp',nsp,nspdimh) 
!       define varibales
      dims(1) = timdimh 
      status = nf_def_var(cdfidh,'t',nf_double,1,dims,idtimeh) 
      status = nf_def_var(cdfidh,'qtotal',nf_double,1,dims,idqtotal) 
      status = nf_def_var(cdfidh,'ntotal',nf_double,1,dims,idntotal) 
      status = nf_def_var(cdfidh,'xmom',nf_double,1,dims,idxmom) 
      status = nf_def_var(cdfidh,'ymom',nf_double,1,dims,idymom) 
!     history by species
      dims(1) = nspdimh 
      dims(2) = timdimh 
      status = nf_def_var(cdfidh,'inj_left',nf_double,2,dims,idinjleft) 
      status = nf_def_var(cdfidh,'inj_right',nf_double,2,dims,idinjright)       
      status = nf_def_var(cdfidh,'inj_top',nf_double,2,dims,idinjtop) 
      status = nf_def_var(cdfidh,'inj_bottom',nf_double,2,dims,idinjbottom) 
      status = nf_def_var(cdfidh,'emitted',nf_double,2,dims,idemitted) 
      status = nf_def_var(cdfidh,'qdust',nf_double,2,dims,idqdust) 
      status = nf_def_var(cdfidh,'phidust',nf_double,2,dims,idpdust) 
      status = nf_def_var(cdfidh,'xmomdust',nf_double,2,dims,idxmdust) 
      status = nf_def_var(cdfidh,'ymomdust',nf_double,2,dims,idymdust) 
      status = nf_def_var(cdfidh,'qdustp',nf_double,2,dims,idqdustp) 
      status = nf_def_var(cdfidh,'xmomdustp',nf_double,2,dims,idxmdustp) 
      status = nf_def_var(cdfidh,'ymomdustp',nf_double,2,dims,idymdustp) 
	  status = nf_def_var(cdfidh,'forcespx',nf_double,2,dims,idforcespx) 
      status = nf_def_var(cdfidh,'forcespy',nf_double,2,dims,idforcespy) 
	  status = nf_def_var(cdfidh,'fdragx',nf_double,2,dims,idfdragx) 
      status = nf_def_var(cdfidh,'fdragy',nf_double,2,dims,idfdragy) 
	  status = nf_def_var(cdfidh,'feletx',nf_double,2,dims,idfeletx) 
      status = nf_def_var(cdfidh,'felety',nf_double,2,dims,idfelety) 
      status = nf_def_var(cdfidh,'feletpx',nf_double,2,dims,idefpx) 
      status = nf_def_var(cdfidh,'feletpy',nf_double,2,dims,idefpy) 


!       end definition section
      status = nf_enddef(cdfidh) 
!
!
!     netcdf for fields
!
!     create ntecdf file
      status = nf_create('Cel.dat',nf_clobber,cdfid) 
!     define dimensions
      status = nf_def_dim(cdfid,'nsp',nsp,nspdim) 
      status = nf_def_dim(cdfid,'nx',ibp2,nxdim) 
      status = nf_def_dim(cdfid,'ny',jbp2,nydim) 
      status = nf_def_dim(cdfid,'time',nf_unlimited,timdim)

!(Leo)
      status=nf_def_dim(cdfid,'four',4,four)
      status=nf_def_dim(cdfid,'thetadim',thetadim,tdim)
      dims(1) = four
      dims(2) = nspdim
      dims(3) = tdim
      dims(4) = timdim
      status = nf_def_var(cdfid,'thetadust',nf_double,4,dims,idtd)
 

!       define varibales
      dims(1) = nxdim 
      dims(2) = nydim 
      dims(3) = timdim 
      status = nf_def_var(cdfid,'x',nf_double,3,dims,idnx) 
      status = nf_def_var(cdfid,'y',nf_double,3,dims,idny) 
      status = nf_def_var(cdfid,'qnet',nf_double,3,dims,idrho) 
      status = nf_def_var(cdfid,'pot',nf_double,3,dims,idpot) 
      status = nf_def_var(cdfid,'ex',nf_double,3,dims,idex) 
      status = nf_def_var(cdfid,'ey',nf_double,3,dims,idey) 
      status = nf_def_var(cdfid,'diegrid',nf_double,3,dims,iddiegrid)

 
      dims(1) = nxdim 
      dims(2) = nydim 
      dims(3) = nspdim 
      dims(4) = timdim 
      status = nf_def_var(cdfid,'qdn',nf_double,4,dims,idqdn) 
      status = nf_def_var(cdfid,'jxs',nf_double,4,dims,idjxs) 
      status = nf_def_var(cdfid,'jys',nf_double,4,dims,idjys) 
      status = nf_def_var(cdfid,'number',nf_double,4,dims,idnumber) 
      status = nf_def_var(cdfid,'uhst',nf_double,4,dims,iduhst) 
      status = nf_def_var(cdfid,'vhst',nf_double,4,dims,idvhst) 
      dims(1) = timdim 
      status = nf_def_var(cdfid,'t',nf_double,1,dims,idtime) 
      status = nf_def_var(cdfid,'ncyc',nf_int,1,dims,idcyc) 
!       end definition section
      status = nf_enddef(cdfid) 
 
!
!
!     netcdf for particles
!
      status = nf_create('Celp.dat',nf_clobber,cdfidp) 
! define dimensions
      status = nf_def_dim(cdfidp,'nx',ibp2,nxdimp) 
      status = nf_def_dim(cdfidp,'ny',jbp2,nydimp) 
      status = nf_def_dim(cdfidp,'np',npart,npdim) 
      status = nf_def_dim(cdfidp,'nsp',nsp,nspdimp) 
      status = nf_def_dim(cdfidp,'time',nf_unlimited,timdimp) 
!       define varibales
      dims(1) = timdimp 
      status = nf_def_var(cdfidp,'t',nf_double,1,dims,idtimep) 
      status = nf_def_var(cdfidp,'ncyc',nf_int,1,dims,idcycp) 
      dims(1) = nxdimp 
      dims(2) = nydimp 
      dims(3) = timdimp 
      status = nf_def_var(cdfidp,'x',nf_double,3,dims,idnxp) 
      status = nf_def_var(cdfidp,'y',nf_double,3,dims,idnyp) 
      dims(1) = nspdimp 
      dims(2) = timdimp 
      status = nf_def_var(cdfidp,'numtot',nf_int,2,dims,idnumtot) 
      dims(1) = npdim 
      dims(2) = timdimp 
      status = nf_def_var(cdfidp,'xp',nf_double,2,dims,idxp) 
      status = nf_def_var(cdfidp,'yp',nf_double,2,dims,idyp) 
      status = nf_def_var(cdfidp,'up',nf_double,2,dims,idup) 
      status = nf_def_var(cdfidp,'vp',nf_double,2,dims,idvp) 
      status = nf_def_var(cdfidp,'wp',nf_double,2,dims,idwp) 
      status = nf_def_var(cdfidp,'qp',nf_double,2,dims,idqp) 
      status = nf_def_var(cdfidp,'cp',nf_double,2,dims,idcp) 
!       end definition section
      status = nf_enddef(cdfidp) 
!
      return  
      end subroutine plotinit 
