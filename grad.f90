      subroutine gradient(ro, gradrox, gradroy, gradro, nx, ny, periodic, cx1, &
         cx2, cx3, cx4, cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      logical , intent(in) :: periodic 
      real(double) , intent(in) :: ro(*) 
      real(double) , intent(inout) :: gradrox(*) 
      real(double) , intent(inout) :: gradroy(*) 
      real(double) , intent(inout) :: gradro(*) 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: vol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, nyp, iwid, jwid, j, i, ij, imj, imjm, ijm, ijb, ijt, ijr&
         , ijl 
      real(double) :: jacob, vvol 
!-----------------------------------------------
!
!
!     a routine to calculate the gradient of the density
!
!
      nxp = nx + 1 
      nyp = ny + 1 
      iwid = 1 
      jwid = nxp 
      do j = 2, nyp 
!
         do i = 2, nx 
            ij = (j - 1)*jwid + (i - 1)*iwid + 1 
            imj = ij - 1 
            imjm = ij - nxp - 1 
            ijm = ij - nxp 
!
!
            vvol = 0.25*(vol(ij)+vol(imj)+vol(imjm)+vol(ijm)) 
            gradrox(ij) = ((-(cx4(ij)+cr4(ij))*ro(ij))-(cx1(imj)+cr1(imj))*ro(&
               imj)-(cx2(imjm)+cr2(imjm))*ro(imjm)-(cx3(ijm)+cr3(ijm))*ro(ijm))&
               /vvol 
!
            gradroy(ij) = ((-cy4(ij)*ro(ij))-cy1(imj)*ro(imj)-cy2(imjm)*ro(imjm&
               )-cy3(ijm)*ro(ijm))/vvol 
!
            gradro(ij) = sqrt(gradrox(ij)**2+gradroy(ij)**2+1.E-20) 
!
         end do 
      end do 
!
      ijb = 2 + nxp 
      ijt = ny*nxp + 2 
      if (periodic) then 
         do i = 2, nx 
            gradrox(ijb) = gradrox(ijb) + gradrox(ijt) 
            gradrox(ijt) = gradrox(ijb) 
            gradroy(ijb) = gradroy(ijb) + gradroy(ijt) 
            gradroy(ijt) = gradroy(ijb) 
            gradro(ijb) = gradro(ijb) + gradro(ijt) 
            gradro(ijt) = gradro(ijb) 
            ijb = ijb + 1 
            ijt = ijt + 1 
         end do 
!
      else 
         do i = 2, nx 
            gradrox(ijb) = gradrox(ijb+nxp) 
            gradrox(ijt) = gradrox(ijt-nxp) 
            gradroy(ijb) = gradroy(ijb+nxp) 
            gradroy(ijt) = gradroy(ijt-nxp) 
            gradro(ijb) = gradro(ijb+nxp) 
            gradro(ijt) = gradro(ijt-nxp) 
            ijb = ijb + 1 
            ijt = ijt + 1 
         end do 
 
      endif 
      ijr = nxp 
      ijl = 2 
      do j = 1, nyp 
         gradrox(ijl) = gradrox(ijl+1) 
         gradrox(ijr) = gradrox(ijr-1) 
         gradroy(ijl) = gradroy(ijl+1) 
         gradroy(ijr) = gradroy(ijr-1) 
         gradro(ijl) = gradro(ijl+1) 
         gradro(ijr) = gradro(ijr-1) 
         ijr = ijr + nxp 
         ijl = ijl + nxp 
      end do 
!
      return  
      end subroutine gradient 
