      subroutine adapt 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ipjp, ijp, ijmin, ijmax  
      real(double) :: alfa, tinytmp
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: ismin, ismax 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/adapt.f90,v 1.2 2006/11/21 20:34:51 lapenta Exp $
!
! $Log: adapt.f90,v $
! Revision 1.2  2006/11/21 20:34:51  lapenta
! Gianni: modifications to have 2 dust particles
!
! Revision 1.1.1.1  2006/06/13 23:37:44  cfichtl
! Initial version of democritus 6/13/06
!
!
 
!!
!         tinytmp=tinyi
!         tinyi=1d0/10d0
         wadapt=diegrid/diem(nsp_dust)
         do j = 2, nyp 
            do i = 2, nxp 
               ij = nxp*(j - 1) + i 
			   ijp = ij + nxp 
               ipj = ij + 1 
               ipjp = ijp + 1 
               coef(ij) = (1d0+wadapt(ij))/vvol(ij)
               coeftrns(ij) = 0d0 
               coefpar(ij) = 0d0  
			   qtilde(ij)=.25d0*(wadapt(ij)+wadapt(ipj)+wadapt(ipjp)+wadapt(ijp))
            end do 
         end do 
      phi0=0d0
      call poisson
!      tinyi=tinytmp
 !     call gradient (phi0, sc1, sc2, wadapt, nx, ny, periodic, cx1, cx2, cx3, &
!         cx4, cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol) 

      wadapt=abs(phi0/(.1+wadapt))
      w=wadapt
      call smooth (nx, ny, 2, nx, 2, ny, elle, iesp, periodic, w, wadapt) 
      ijmax = ismax(nxp*nyp,wadapt,1) 
      wmax = wadapt(ijmax)
      wadapt=wadapt/wmax 
      ijmin = ismin(nxp*nyp,wadapt,1) 
      wmin = wadapt(ijmin)
      wadapt=wadapt-wmin+1d0/offset
      w=wadapt 
      write(*,*)wmin,wmax
!            write (*, *) 'wmin,wmax', wmin, wmax 
!      do i = 1, nx + 1 
!         do j = 1, ny + 1 
!            ij = (j - 1)*nxp + i 
!            w(ij)=(wadapt(ij)-wmin)/(wmax-wmin)+1d0/offset
!         end do 
!      end do 
!      wadapt=w
      ijmin = ismin(nxp*nyp,wadapt,1) 
      wmin = wadapt(ijmin) 
      ijmax = ismax(nxp*nyp,wadapt,1) 
      wmax = wadapt(ijmax) 
      write (*, *) 'wmin,wmax', wmin, wmax 
      return  
      end subroutine adapt 

