 
      subroutine mccin(massp, masst, velx, vely, velz, vthn, ntarld, dt, twopi&
         , ctype) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ctype 
      real(double)  :: massp 
      real(double)  :: masst 
      real(double)  :: velx 
      real(double)  :: vely 
      real(double)  :: velz 
      real(double) , intent(in) :: vthn 
      real(double) , intent(in) :: ntarld 
      real(double) , intent(in) :: dt 
      real(double)  :: twopi 
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
      integer, parameter :: chex = 1 
      integer, parameter :: scatt = 2 
      integer, parameter :: all = 3 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vxn, vyn, vzn, vrel, vrx, vry, vrz, r1, r2 
      real(double), dimension(2) :: fq 
      real(double) :: cfqmax, fqtot, xtot, pcoll 
      real(double), dimension(2) :: crx 
!-----------------------------------------------
 
      crx(1) = 8.D-19                            !charge exchange 
      crx(2) = 5.D-19                            ! scattering 
 
!     ene=velx*velx+vely*vely+velz*velz
!     ene=0.5d0*ene*Massp
 
      !generate neutral velocity:
      call random_number (r1) 
      call random_number (r2) 
!     R1=ranf()
!     R2=ranf()
      vxn = vthn*dsqrt((-2.D0*dlog(r1)))*dcos(twopi*r2) 
      vyn = vthn*dsqrt((-2.D0*dlog(r1)))*dsin(twopi*r2) 
 
      call random_number (r1) 
      call random_number (r2) 
!     R1=ranf()
!     R2=ranf()
      vzn = vthn*dsqrt((-2.D0*dlog(r1)))*dcos(twopi*r2) 
 
      !calculate i-n relative velocity
      vrx = velx - vxn 
      vry = vely - vyn 
      vrz = velz - vzn 
      vrel = dsqrt(vrx**2 + vry**2 + vrz**2) 
 
 
      if (ctype == chex) xtot = crx(1) 
      if (ctype == scatt) xtot = crx(2) 
      if (ctype == all) xtot = crx(1) + crx(2) 
 
      cfqmax = xtot*vrel*ntarld 
      pcoll = 1.D0 - dexp((-dt*cfqmax)) 
 
      call random_number (r1) 
!     R1=ranf()
 
 
      if (r1 < pcoll) then 
 
 
         fq(1) = crx(1)*ntarld*vrel 
         fq(2) = crx(2)*ntarld*vrel 
 
         if (ctype == chex) fqtot = fq(1) 
         if (ctype == scatt) fqtot = fq(2) 
         if (ctype == all) then 
            fqtot = fq(1) + fq(2) 
 
            call random_number (r2) 
 
            r2 = cfqmax*r2                       !ranf() 
 
            if (r2 <= fq(1)) call chargex (velx, vely, velz, vxn, vyn, vzn) 
 
            if (r2<=fqtot .and. r2>fq(1)) call scatter (massp, masst, vrx, vry&
               , vrz, vrel, velx, vely, velz, vxn, vyn, vzn, twopi) 
         endif 
 
         if (ctype == chex) call chargex (velx, vely, velz, vxn, vyn, vzn) 
         if (ctype == scatt) call scatter (massp, masst, vrx, vry, vrz, vrel, &
            velx, vely, velz, vxn, vyn, vzn, twopi) 
      endif 
      return  
 
!     ene=velx*velx+vely*vely+velz*velz
!     ene=0.5d0*ene*Massp
 
      end subroutine mccin 


 
!----------------------------------------------------------------------------------
      subroutine chargex(velx, vely, velz, vxn, vyn, vzn) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(out) :: velx 
      real(double) , intent(out) :: vely 
      real(double) , intent(out) :: velz 
      real(double) , intent(in) :: vxn 
      real(double) , intent(in) :: vyn 
      real(double) , intent(in) :: vzn 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
 
      velx = vxn 
      vely = vyn 
      velz = vzn 
      return  
 
      end subroutine chargex 


!----------------------------------------------------------------------------------
      subroutine scatter(massp, masst, vrx, vry, vrz, vrel, velx, vely, velz, &
         vxn, vyn, vzn, twopi) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: massp 
      real(double) , intent(in) :: masst 
      real(double) , intent(in) :: vrx 
      real(double) , intent(in) :: vry 
      real(double) , intent(in) :: vrz 
      real(double) , intent(in) :: vrel 
      real(double) , intent(out) :: velx 
      real(double) , intent(out) :: vely 
      real(double) , intent(out) :: velz 
      real(double) , intent(in) :: vxn 
      real(double) , intent(in) :: vyn 
      real(double) , intent(in) :: vzn 
      real(double) , intent(in) :: twopi 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vscatt 
      real(double), dimension(3) :: uinc, uscatt 
      real(double) :: kf1, kf2, kf3, coschi, sinchi, cosphi, sinphi, costhe, &
         sinthe, cosrel, sinrel, rand, mratio, lossfact 
!-----------------------------------------------
 
 
!     masst=massp
 
 
 
      if (massp == masst) then 
 
!     Rand=ranf()
         call random_number (rand) 
 
         if (rand == 0.D0) rand = rand + 1.D-8 
         if (rand == 1.D0) rand = rand - 1.D-8 
 
         coschi = dsqrt(1 - rand) 
         sinchi = dsqrt(rand) 
 
      else                                       !see mcdaniel 
 
         mratio = massp/masst 
 
         call random_number (rand) 
!     Rand=ranf()
         if (rand == 0.D0) rand = rand + 1.D-8 
         if (rand == 1.D0) rand = rand - 1.D-8 
 
         cosrel = 1.D0 - 2.D0*rand 
         sinrel = dsqrt(1 - cosrel*cosrel) 
!     chi=datan(sinrel/(mratio+cosrel))
 
         lossfact = 2.D0*(massp*masst)/(massp + masst)**2*(1 - cosrel) 
 
      endif 
 
      uinc(1) = vrx/(vrel + 1.D-10) 
      uinc(2) = vry/(vrel + 1.D-10) 
      uinc(3) = vrz/(vrel + 1.D-10) 
 
      call random_number (rand) 
      rand = twopi*rand                          !ranf() 
      cosphi = dcos(rand) 
      sinphi = dsin(rand) 
 
      costhe = uinc(1) 
      sinthe = dsqrt(1.D0 - costhe*costhe) + 1.D-10 
 
      kf1 = coschi 
      kf3 = sinchi/sinthe 
      kf2 = kf3*sinphi 
      kf3 = kf3*cosphi 
 
      uscatt(1) = kf1*uinc(1) + kf3*(uinc(2)*uinc(2)+uinc(3)*uinc(3)) 
      uscatt(2) = kf1*uinc(2) + kf2*uinc(3) - kf3*uinc(1)*uinc(2) 
      uscatt(3) = kf1*uinc(3) - kf2*uinc(2) - kf3*uinc(1)*uinc(3) 
 
      vscatt = vrel*coschi 
 
      velx = vscatt*uscatt(1) + vxn 
      vely = vscatt*uscatt(2) + vyn 
      velz = vscatt*uscatt(3) + vzn 
      return  
 
      end subroutine scatter 
