      subroutine adapt 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ipjp, ijp, ijmin, ijmax 
      real(double), dimension(nxyp) :: sc3, sc4 
      real(double) :: alfa, xc, yc, r
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: ismin, ismax 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/adapt_gianni.f90,v 1.1.1.1 2006/06/13 23:37:44 cfichtl Exp $
!
! $Log: adapt_gianni.f90,v $
! Revision 1.1.1.1  2006/06/13 23:37:44  cfichtl
! Initial version of democritus 6/13/06
!
!
 
!
      wadapt=1d0
      do j = 1, ny 
         do i = 1, nx 
!
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
!
            wadapt(ij) = 0.25*(diegrid(ij)+diegrid(ijp)+diegrid(ipj)+diegrid(ipjp)) 
            if (wadapt(ij) <= 1.D0) cycle  
            wadapt(ij) = 1D0 
         end do 
      end do 
      sc4=wadapt 
      call smooth(nx,ny,2,nx,2,ny,elle,iesp,periodic,sc1,sc4)
      wadapt=sc1
      call gradient (wadapt, sc1, sc2, sc3, nx, ny, periodic, cx1, cx2, cx3, &
         cx4, cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol) 
!      call smooth (nx, ny, 2, nx, 2, ny, elle, iesp, periodic, sc1, sc3) 
!      sc3=abs(wadapt) 
      wadapt=1d0
      do j = 1, ny 
         do i = 1, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
            wadapt(ij) = 0.25*(sc3(ij)+sc3(ijp)+sc3(ipj)+sc3(ipjp))
            xc=(xn(ij)+xn(ijp)+xn(ipj)+xn(ipjp))*.25d0
            yc=(yn(ij)+yn(ijp)+yn(ipj)+yn(ipjp))*.25d0
            r=sqrt((xc-xcenter(nsp))**2+(yc-ycenter(nsp))**2)
            wadapt(ij)=1d0+exp(-(r-rex(nsp))**2/maxval(siep))
            if(r<=rex(nsp)) wadapt(ij)=1d0
         end do 
      end do 
      ijmin = ismin(nxp*nyp,wadapt,1) 
      wmin = wadapt(ijmin) 
      ijmax = ismax(nxp*nyp,wadapt,1) 
      wmax = wadapt(ijmax) 
            write (*, *) 'wmin,wmax', wmin, wmax 
      alfa = max(1./(offset - 1.),1.E-5) 
      do i = 1, nx + 1 
         do j = 1, ny + 1 
            ij = (j - 1)*nxp + i 
            wadapt(ij) = (wadapt(ij)-wmin)/(wmax-wmin) + 1d0/offset 
            w(ij) = wadapt(ij) 
         end do 
      end do 
      ijmin = ismin(nxp*nyp,wadapt,1) 
      wmin = wadapt(ijmin) 
      ijmax = ismax(nxp*nyp,wadapt,1) 
      wmax = wadapt(ijmax) 
      write (*, *) 'wmin,wmax', wmin, wmax 
      return  
      end subroutine adapt 
