     subroutine geometry_object 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!        
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
	integer :: i, j, ij, is
	real(double) :: radp
!-----------------------------------------------
!
!
	inside_object=0d0
        do j = 1, nyp 
            do i = 1, nxp 
               ij = nxp*(j - 1) + i 
 			   do is=nsp_dust,nsp

                  radp = (xn(ij)-xcenter(is))**2/rex(is)**2 + (yn(ij)-ycenter(is))**2/rey(is)**2
             
                  if(radp.lt.1d0) then
                     inside_object(ij)=1d0
                  end if    
			   end do
            end do 
         end do 
!
end subroutine geometry_object 