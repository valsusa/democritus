      subroutine rinj_iec(isp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  17:26:36   5/ 6/06  
!...Switches: -yfv -x1            
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: isp 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: iwid, jwid, ipsrch, npinj, ip, np 
      real(double) ::  etot, etan, er, raginj, &
         qinj, dnpinj, sint, cost, fi, vrad, vtan, fdt  
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: ranf 
!-----------------------------------------------
!
!
!     A subroutine to inject particles
!
 
 
      pi = acos(-1.D0) 
      iwid = 1 
      jwid = nxp 
 
      ipsrch = 0 
!
!     CIRCLE
!
      raginj = reinj(isp) 
      qinj = fluxinj(isp)*2*raginj**2*dt 
      dnpinj = qinj*npcel(isp)/(rhr(isp)*dx*dy) 
      npinj = int(dnpinj) 
      if (ranf() < dnpinj - npinj) then 
         npinj = npinj + 1 
         qinj = npinj*rhr(isp)*dx*dy/npcel(isp) 
      endif 
      qinj = qinj*qom(isp)/abs(qom(isp)+1.D-10) 
!     write(*,*)'injector',isp,npinj
!     npinj=0
 
      do ip = 1, npinj 
         np = iphead(1) 
 
         if (np > 0) then 
 
!     theta=pi*(ranf()-.5)
            sint = 2*ranf() - 1. 
            cost = sqrt(1 - sint**2) 
!     costhv=ranf()**1.2
 
!     PARTE CLASSICA
!     vrad=velinj(isp)*costhv
!     vtan=velinj(isp)*sqrt(1.-costhv**2*.99999)
            fi = 2.*pi*ranf() 
 
!     QUI PARTE CON COMPONENTE RADIALE
!
            etot = 0.5*velinj(isp)**2 
            etan = ranf()**p_inject*etot 
            er = etot - etan 
            vrad = sqrt(2.0D0*er) 
            vtan = sqrt(2.0D0*etan) 
 
            up(np) = (-vrad*cost) - vtan*sint*sin(fi) 
            vp(np) = (-vrad*sint) + vtan*cost*sin(fi) 
            wp(np) = vtan*cos(fi) 
 
            fdt = ranf()*dt 
 
            xpf(np) = xcenter(isp) + raginj*cost + up(np)*fdt 
            ypf(np) = ycenter(isp) + raginj*sint + vp(np)*fdt 
!     write(*,*)ip,theta,xpf(np),ypf(np)
 
            qpar(np) = qinj/npinj 
            ico(np) = isp 
            diepar(np) = abs(qpar(np))*diem(isp) 
 
            iphead(1) = link(np) 
            link(np) = ipsrch 
            ipsrch = np 
 
         else 
 
            write (*, *) 'WARNING: Not Enough Particles' 
 
         endif 
 
      end do 
 
      iphd2 = iphead 
 
      call locator_kin (ipsrch) 
 
      return  
      end subroutine rinj_iec
 
