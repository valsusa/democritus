      subroutine parmovki(ns) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ns 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(ntmp) :: lvs, isp, inew, jnew, iold, jold, numx, ls 
      integer :: ipsrch, n, ij, nh, nlostr, nlostl, nlost, j, i, ipj, ipjp, ijp&
         , np, lvsmax, lv, ll, numy, ijnew, ipjnew, ipjpnew, ijpnew, l 
      real(double), dimension(ntmp) :: the, zeta, errx, erry, omt, omz, off, &
         xpt, ypt, xtp0, ytp0, xpc, ypc, w1, w2, w3, w4, uptil, vptil, wptil, &
         upv1, vpv1, wpv1, bxpar, bypar, bzpar, expar, eypar, ezpar, dexdx, &
         dexdy, deydx, deydy, testx, testy, fail 
      real(double) :: reflr, reflt, refll, reflb, dto2, rfnym, rcol, qomdt, &
         denom, udotb 
      logical, dimension(ntmp) :: discard 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/parmovki.f90,v 1.1.1.1 2006/06/13 23:37:45 cfichtl Exp $
!
! $Log: parmovki.f90,v $
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!     calculate particle motion in natural coordinates
!
!
!
!
      data reflr, reflt, refll, reflb/ 1., 1., 1., 1./  
      iphd2(1) = iphead(1) 
      ipsrch = 0 
      do n = 1, nrg 
         do ij = 1, nxyp 
            dkdt(ij,n) = 0.0 
         end do 
      end do 
!
!
      nh = mod(ncyc,nhst) 
      edotjp(nh) = 0.0 
      if (nh > 1) enrglost(nh) = enrglost(nh-1) 
      dto2 = 0.5*dt 
      kmax = 2*(nx + ny - 2) + 1 
      nlostr = 0 
      nlostl = 0 
      nlost = 0 
      rfnym = 1./(ny - 1) 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
!
!
            np = iphead(ij) 
            if (np == 0) cycle  
  100       continue 
            call listmkr (np, nploc, link, lvmax, ntmp) 
!
!     make a list of particles of species ns
!
            lvsmax = 0 
            do lv = 1, lvmax 
               if (ico(nploc(lv)) /= ns) cycle  
               lvsmax = lvsmax + 1 
               lvs(lvsmax) = lv 
            end do 
!
            rcol = 1./(1. + coll*dto2) 
            do lv = 1, lvmax 
               iold(lv) = int(xp(nploc(lv))) 
               jold(lv) = int(yp(nploc(lv))) 
               inew(lv) = iold(lv) 
               jnew(lv) = jold(lv) 
               the(lv) = xp(nploc(lv)) - iold(lv) 
               zeta(lv) = yp(nploc(lv)) - jold(lv) 
               xpv(lv) = xp(nploc(lv)) 
               ypv(lv) = yp(nploc(lv)) 
!
               isp(lv) = ico(nploc(lv)) 
               xtp0(lv) = xpf(nploc(lv)) 
               ytp0(lv) = ypf(nploc(lv)) 
               massp(lv) = qpar(nploc(lv))/qom(isp(lv)) 
!
            end do 
!
!
            do ll = 1, lvsmax 
               lv = lvs(ll) 
               w1(lv) = the(lv)*(1. - zeta(lv)) 
               w2(lv) = the(lv)*zeta(lv) 
               w3(lv) = (1. - the(lv))*zeta(lv) 
               w4(lv) = (1. - the(lv))*(1. - zeta(lv)) 
!
               bxpar(lv) = w1(lv)*bx(ipj) + (w2(lv)*bx(ipjp)+(w3(lv)*bx(ijp)+w4&
                  (lv)*bx(ij))) 
!c
               bypar(lv) = w1(lv)*by(ipj) + (w2(lv)*by(ipjp)+(w3(lv)*by(ijp)+w4&
                  (lv)*by(ij))) 
!c
               bzpar(lv) = w1(lv)*bz(ipj) + (w2(lv)*bz(ipjp)+(w3(lv)*bz(ijp)+w4&
                  (lv)*bz(ij))) 
!c
!
!     add correction term for motion in cylindrical coordinates
!
!      bzpar(lv)=bzpar(lv)
!     &     -cyl*wp(nploc(lv))/(xpf(nploc(lv))*qom(isp(lv))+1e-10)
!
               qomdt = qom(isp(lv))*dto2*rcol 
!
               denom = rcol/(1. + (bxpar(lv)**2+bypar(lv)**2+bzpar(lv)**2)*&
                  qomdt**2) 
               udotb = up(nploc(lv))*bxpar(lv) + vp(nploc(lv))*bypar(lv) + wp(&
                  nploc(lv))*bzpar(lv) 
               uptil(lv) = (up(nploc(lv))+qomdt*(vp(nploc(lv))*bzpar(lv)-wp(&
                  nploc(lv))*bypar(lv)+qomdt*(udotb*bxpar(lv))))*denom 
!
               vptil(lv) = (vp(nploc(lv))+qomdt*(wp(nploc(lv))*bxpar(lv)-up(&
                  nploc(lv))*bzpar(lv)+qomdt*(udotb*bypar(lv))))*denom 
!
               wptil(lv) = (wp(nploc(lv))+qomdt*(up(nploc(lv))*bypar(lv)-vp(&
                  nploc(lv))*bxpar(lv)+qomdt*(udotb*bzpar(lv))))*denom 
!
               xpc(lv) = xpf(nploc(lv)) + dto2*uptil(lv) 
               ypc(lv) = ypf(nploc(lv)) + dto2*vptil(lv) 
!
            end do 
!
            do ll = 1, lvsmax 
               lv = lvs(ll) 
               xpv(lv) = xpc(lv)/dx + 2. 
               if (xpv(lv)<2. .or. xpv(lv)>float(nx+1)) then 
                  off(lv) = 0. 
                  xpc(lv) = xpf(nploc(lv)) + dto2*uptil(lv) 
               else 
                  off(lv) = 1. 
               endif 
            end do 
!     *************************************************************
!
!     corrector step
!
!     ******************************************************************
!
            if (periodic) then 
               do ll = 1, lvsmax 
                  lv = lvs(ll) 
                  xpv(lv) = xpc(lv)/dx + 2. 
                  ypv(lv) = ypc(lv)/dy + 2. 
                  numy = (ypv(lv)-2+100*(ny-1))*rfnym 
                  ypv(lv) = ypv(lv) + (100. - numy)*(ny - 1) 
                  jnew(lv) = int(ypv(lv)) 
                  zeta(lv) = ypv(lv) - jnew(lv) 
!
                  xpv(lv) = min(float(nx + 1) - 1.E-15,xpv(lv)) 
                  xpv(lv) = max(2.,xpv(lv)) 
                  inew(lv) = int(xpv(lv)) 
                  the(lv) = xpv(lv) - inew(lv) 
!
!
               end do 
!
            else 
               do ll = 1, lvsmax 
                  lv = lvs(ll) 
!
                  inew(lv) = max(2,inew(lv)) 
                  inew(lv) = min(nx,inew(lv)) 
                  jnew(lv) = max(2,jnew(lv)) 
                  jnew(lv) = min(ny,jnew(lv)) 
!
                  the(lv) = xpv(lv) - inew(lv) 
                  the(lv) = max(0.,the(lv)) 
                  the(lv) = min(1.,the(lv)) 
!
                  zeta(lv) = ypv(lv) - jnew(lv) 
                  zeta(lv) = max(0.,zeta(lv)) 
                  zeta(lv) = min(1.,zeta(lv)) 
!
               end do 
            endif 
            do ll = 1, lvsmax 
               lv = lvs(ll) 
               ijnew = (jnew(lv)-1)*nxp + (inew(lv)-1)*1 + 1 
               ipjnew = ijnew + 1 
               ipjpnew = ijnew + nxp + 1 
               ijpnew = ijnew + nxp 
!
               w1(lv) = the(lv)*(1. - zeta(lv)) 
               w2(lv) = the(lv)*zeta(lv) 
               w3(lv) = (1. - the(lv))*zeta(lv) 
               w4(lv) = (1. - the(lv))*(1. - zeta(lv)) 
!
               expar(lv) = w1(lv)*extil(ipjnew,isp(lv)) + (w2(lv)*extil(ipjpnew&
                  ,isp(lv))+(w3(lv)*extil(ijpnew,isp(lv))+w4(lv)*extil(ijnew,&
                  isp(lv)))) 
!c
               eypar(lv) = w1(lv)*eytil(ipjnew,isp(lv)) + (w2(lv)*eytil(ipjpnew&
                  ,isp(lv))+(w3(lv)*eytil(ijpnew,isp(lv))+w4(lv)*eytil(ijnew,&
                  isp(lv)))) 
!
               ezpar(lv) = w1(lv)*eztil(ipjnew,isp(lv)) + (w2(lv)*eztil(ipjpnew&
                  ,isp(lv))+(w3(lv)*eztil(ijpnew,isp(lv))+w4(lv)*eztil(ijnew,&
                  isp(lv)))) 
!
!
!
            end do 
!
!
            do ll = 1, lvsmax 
               lv = lvs(ll) 
               qomdt = qom(isp(lv))*dto2*rcol 
!
               upv1(lv) = uptil(lv) + qomdt*expar(lv) 
!
               vpv1(lv) = vptil(lv) + qomdt*eypar(lv) 
!
               wpv1(lv) = wptil(lv) + qomdt*ezpar(lv) 
!
               xpc(lv) = xpf(nploc(lv)) + upv1(lv)*dt 
               ypc(lv) = ypf(nploc(lv)) + vpv1(lv)*dt 
               up(nploc(lv)) = 2.*upv1(lv) - up(nploc(lv)) 
               vp(nploc(lv)) = 2.*vpv1(lv) - vp(nploc(lv)) 
               wp(nploc(lv)) = 2.*wpv1(lv) - wp(nploc(lv)) 
            end do 
!
!
!
!     calculate the new natural coordinates of the particle
!
            call mapv (xn, yn, ij, nxp, xpc, ypc, the, zeta, 1, lvmax) 
!
!     restore updated particle positions
!
!dir$ ivdep
            do ll = 1, lvsmax 
               l = lvs(ll) 
               xp(nploc(l)) = iold(l) + the(l) 
               yp(nploc(l)) = jold(l) + zeta(l) 
               xpf(nploc(l)) = xpc(l) 
               ypf(nploc(l)) = ypc(l) 
               inew(l) = iold(l) + the(l) 
               jnew(l) = jold(l) + zeta(l) 
            end do 
!
            do lv = 1, lvmax 
!
!     check first to see whether particle has changed cells
!
               if (inew(lv)==iold(lv) .and. jnew(lv)==jold(lv)) then 
!    for those particles that have not changed cells,
!     no further tests are required
!     return particle to original cell list
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = iphd2(ij) 
                  iphd2(ij) = nploc(lv) 
!
               else 
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = ipsrch 
                  ipsrch = nploc(lv) 
                  nlost = nlost + 1 
!
!     save old position in xp,yp arrays
!
                  xp(nploc(lv)) = xtp0(lv) 
                  yp(nploc(lv)) = ytp0(lv) 
!
!
!
               endif 
            end do 
            np = iphead(ij) 
            if (np > 0) go to 100 
!
         end do 
      end do 
!
!     *********************************************************
!
!     reconstruct linked lists
!
!     ***********************************************
      call locator_kin (ipsrch) 
      return  
      end subroutine parmovki 
