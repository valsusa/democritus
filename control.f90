!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
 
 
      subroutine control(spiu, smeno, cpiu, cmeno) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
!     Library of routines to control the number
!     of particles in 2d kinetic plasma
!     simulations
!
!     by Gianni Lapenta
!     LANL
!     Feb, 1994
!
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double)  :: spiu 
      real(double)  :: smeno 
      real(double)  :: cpiu 
      real(double)  :: cmeno, cmenold 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: is, j, i, ij, icoal, nvolte
      logical :: verbose
!-----------------------------------------------
 
      verbose=.false.
      
!     ncount=0
!     np=iphead(1)
!1    if(np.ne.0) then
!     ncount=ncount+1
!     np=link(np)
!     goto 1
!     end if
!     write(6,*)'avail. particles',ncount
      do is = 1, nsp
         do j = 2, ny 
            do i = 2, nx 
               ij = (j - 1)*nxp + i 
!        write(*,*)'control',ij,is,number(ij,is)
               if (number(ij,is)<npcel(is)/2 .and. number(ij,is)>1 .and. lsplit(is)) then 
!     do isplit=1,int(-number(ij,is)+npcel(is))/4+1
                  call splitter (ij, i, j, is, spiu, smeno, iphead, link, up, &
                     vp, wp, xp, yp, xpf, ypf, qpar, diepar, ico, istatus, nx, ny, x, y&
                     , npcel, ntmp,dtsub,verbose) 
!        enddo
               endif 
               ij = (j - 1)*nxp + i 
               if (.not.(number(ij,is)>npcel(is)*1.5 .and. lcoal(is))) cycle  
			   nvolte=int(number(ij,is)-npcel(is))+1
			   if(is<nsp_dust) nvolte=1
			   do icoal=1,nvolte
!     cmenold=cmeno
!     write(6,*)'coalesce ',cpiu,cmeno,number(ij,is),ij,is
               call coalesce (ij, i, j, is, cpiu, cmeno, ntresh, vtresh, iphead&
                  , link, up, vp, wp, xp, yp, xpf, ypf, qpar, diepar, ico, istatus, nx&
                  , ny, x, y, ntmp,verbose) 
!     write(6,*)'coalesce called ',cpiu,cmeno,number(ij,is),ij,is
!     if(cmeno.gt.cmenold) cycle
               enddo
            end do 
         end do 
      end do 
      return  
      end subroutine control 


 
      subroutine coalesce(ij, i, j, is, suc, fail, ntresh, treshco, iphead, &
         link, up, vp, wp, xp, yp, xpf, ypf, qpar, diepar, ico, istatus, nx, ny, x, y, &
         ntmp,verbose) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ij 
      integer  :: i 
      integer  :: j 
      integer  :: is 
      integer , intent(in) :: ntresh 
      integer , intent(in) :: nx 
      integer  :: ny 
      integer  :: ntmp 
      logical :: verbose
      real(double) , intent(inout) :: suc 
      real(double) , intent(inout) :: fail 
      real(double) , intent(in) :: treshco 
      integer , intent(inout) :: iphead(*) 
      integer  :: link(*) 
      integer  :: ico(*) 
      integer , intent(out) :: istatus(*) 
      real(double) , intent(inout) :: up(*) 
      real(double) , intent(inout) :: vp(*) 
      real(double) , intent(inout) :: wp(*) 
      real(double) , intent(inout) :: xp(*) 
      real(double) , intent(inout) :: yp(*) 
      real(double) , intent(out) :: xpf(*) 
      real(double) , intent(out) :: ypf(*) 
      real(double) , intent(inout) :: qpar(*) 
      real(double) , intent(inout) :: diepar(*)
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(4) :: nvlist 
      integer , dimension(4,ntmp) :: lvlist 
      integer , dimension(ntmp) :: itmp, nploc 
      integer :: nxp, np1, ipdone, np2, np, lvmax, lvmaxini, ntimes, ii, lv, ix&
         , iy, lismax, lv1, n1, lv2, n2, npn, ipj, ipjp, ijp 
      real(double), dimension(ntmp) :: wdif 
      real(double) :: e1, e2, xl, xr, xm, yb, yt, ym, xpp, ypp, dmmin, qtotl, &
         ume, u2me, dist, vme, v2me, wme, w2me, frac1, frac2, x1, x2, x3, x4, &
         y1, y2, y3, y4, the, zeta, qp1, qp2
      logical :: flag 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: cvmgp 
!-----------------------------------------------
!
!
      nxp = nx + 1 
      fail = fail + 1 
      e1 = 1.E10 
      e2 = 1.E10 
      np1 = 0 
      ipdone = 0 
      np2 = 0 
!
      np = iphead(ij) 
      if (iphead(ij) /= 0) then 
 
!
!     dicotomic search
!
 
 
         call listmkr_control (np, nploc, link, ico, is, lvmax, ntmp) 
         lvmaxini = lvmax 
         if (lvmax < 2) then 
            if (verbose) write (66, *) 'coal fails, no particles found with right color' 
            return  
         else 
            lvmax = min(lvmax,32) 
         endif 
 
 
         flag = .TRUE. 
         xl = 0. 
         xr = 1. 
         xm = 0.5 
         yb = 0. 
         yt = 1. 
         ym = 0.5 
         ntimes = 0 
 
   11    continue 
         if (lvmax > ntresh) then 
!
            ntimes = ntimes + 1 
            do ii = 1, 4 
               nvlist(ii) = 0 
            end do 
!DIR$ IVDEP
            do lv = 1, lvmax 
               np = nploc(lv) 
               if (ico(np) /= is) cycle  
               xpp = xp(np) - int(xp(np)) 
               ypp = yp(np) - int(yp(np)) 
               ix = cvmgp(0,1,xm - xpp) 
               iy = cvmgp(0,1,ym - ypp) 
               itmp(lv) = 1 + ix + iy*2 
            end do 
!
            do lv = 1, lvmax 
!
               np = nploc(lv) 
               if (ico(np) /= is) cycle  
               nvlist(itmp(lv)) = nvlist(itmp(lv)) + 1 
               lvlist(itmp(lv),nvlist(itmp(lv))) = np 
            end do 
!
            lvmax = 0 
            lismax = 0 
            do ii = 1, 4 
               if (nvlist(ii) < lvmax) cycle  
               lvmax = nvlist(ii) 
               lismax = ii 
            end do 
!DIR$ IVDEP
            do lv = 1, lvmax 
               nploc(lv) = lvlist(lismax,lv) 
            end do 
            if (lismax==1 .or. lismax==3) then 
               xr = xm 
            else 
               xl = xm 
            endif 
            if (lismax==1 .or. lismax==2) then 
               yt = ym 
            else 
               yb = ym 
            endif 
            xm = 0.5*(xl + xr) 
            ym = 0.5*(yb + yt) 
            if (ntimes < 10) go to 11 
!
         endif 
 
         if (lvmax == 2) then 
            np1 = nploc(1) 
            np2 = nploc(2) 
         else 
            dmmin = 1.E10 
            do lv1 = 1, lvmax - 1 
               n1 = nploc(lv1) 
               do lv2 = lv1 + 1, lvmax 
                  n2 = nploc(lv2) 
                  wdif(lv2) = sqrt(1.D-10 + (up(n1)-up(n2))**2+(vp(n1)-vp(n2))&
                     **2+(wp(n1)-wp(n2))**2) 
               end do 
!
               do lv2 = lv1 + 1, lvmax 
                  n2 = nploc(lv2) 
                  if (dmmin < wdif(lv2)) cycle  
                  dmmin = wdif(lv2) 
                  np1 = n1 
                  np2 = n2 
               end do 
            end do 
         endif 
 
 
         if (np1<=0 .or. np2<=0) then 
            if(verbose) write (66, *) 'coalesce failure in dicotomic search', lvmax 
            if(verbose) write (*, *) 'coalesce failure in dicotomic search', lvmax, &
               lvmaxini, ntresh 
            return  
         endif 
 
 
 
         if (qpar(np1)*qpar(np2) <= 0.D0) then 
            if(verbose) write (66, *) 'coalesce error: Particles of diff. charge' 
            return  
         endif 
         if (ico(np1)/=is .or. ico(np2)/=is) then 
            if(verbose) write (66, *) 'coalesce error: Particles of wrong species' 
            return  
         endif 
         
		 qp1=abs(qpar(np1))
		 qp2=abs(qpar(np2))
         qtotl = qp1 + qp2 
 
         ume = (qp1*up(np1)+qp2*up(np2))/qtotl 
         u2me = sqrt((qp1*up(np1)**2+qp2*up(np2)**2)/qtotl) 
         dist = abs(abs(ume) - u2me)/(abs(ume) + u2me + 1E-10)*2 
         if (dist>treshco .and. treshco<1.E5) then 
            if(verbose) write (66, *) 'coalesce fails the tests' 
            return  
         endif 
 
         vme = (qp1*vp(np1)+qp2*vp(np2))/qtotl 
         v2me = sqrt((qp1*vp(np1)**2+qp2*vp(np2)**2)/qtotl) 
         dist = abs(abs(vme) - v2me)/(abs(vme) + v2me + 1E-10)*2 
         if (dist>treshco .and. treshco<1.E5) then 
            if(verbose) write (66, *) 'coalesce fails the tests' 
            return  
         endif 
 
         wme = (qp1*wp(np1)+qp2*wp(np2))/qtotl 
         w2me = sqrt((qp1*wp(np1)**2+qp2*wp(np2)**2)/qtotl) 
         dist = abs(abs(wme) - w2me)/(abs(wme) + w2me + 1E-10)*2 
         if (dist>treshco .and. treshco<1.E5) then 
            if(verbose) write (66, *) 'coalesce fails the tests' 
            return  
         endif 
 
 
 
!
!
         np = iphead(ij) 
   12    continue 
         if (np /= 0) then 
!
            if (np==np1 .or. np==np2) then 
!
               iphead(ij) = link(np) 
               link(np) = iphead(1) 
               iphead(1) = np 
!
            else 
!
               iphead(ij) = link(np) 
               link(np) = ipdone 
               ipdone = np 
!
            endif 
!
            np = iphead(ij) 
!
            go to 12 
!
         endif 
!
!
         npn = iphead(1) 
         if (npn==0 .or. np1==0 .or. np2==0) then 
            write (*, *) 'coalesce major failure' 
            iphead(ij) = ipdone 
            ipdone = 0 
            return  
         endif 
!
!     combine particles using simple linear combination
!

         frac1 = qp1/qtotl 
         frac2 = 1.D0 - frac1 
         xp(npn) = xp(np1)*frac1 + xp(np2)*frac2 
         yp(npn) = yp(np1)*frac1 + yp(np2)*frac2 
         ipj = ij + 1 
         ipjp = ij + nxp + 1 
         ijp = ij + nxp 
         x1 = x(ipj) 
         x2 = x(ipjp) 
         x3 = x(ijp) 
         x4 = x(ij) 
         y1 = y(ipj) 
         y2 = y(ipjp) 
         y3 = y(ijp) 
         y4 = y(ij) 
         the = xp(npn) - int(xp(npn)) 
         zeta = yp(npn) - int(yp(npn)) 
         xpf(npn) = the*((1. - zeta)*x1 + zeta*x2) + (1. - the)*((1. - zeta)*x4&
             + zeta*x3) 
         ypf(npn) = the*((1. - zeta)*y1 + zeta*y2) + (1. - the)*((1. - zeta)*y4&
             + zeta*y3) 
 
 
!      up(npn)=sqrt(v2me)*vme/(1e-10+abs(vme))
         up(npn) = ume 
!      vp(npn)=sqrt(v2me)*vme/(1e-10+abs(vme))
         vp(npn) = vme 
!      wp(npn)=sqrt(v2me)*vme/(1e-10+abs(vme))
         wp(npn) = wme 
         qpar(npn) = qpar(np1)+qpar(np2) 
         diepar(npn) = diepar(np1)+diepar(np2)
         ico(npn) = is 
         istatus(npn) = 2 
!
         iphead(1) = link(npn) 
         link(npn) = ipdone 
         ipdone = npn 
         suc = suc + 1 
         fail = fail - 1 
!
!
!
         iphead(ij) = ipdone 
         ipdone = 0 
!
      endif 
!
      return  
      end subroutine coalesce 


 
      integer function isegno (x) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: x 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      if (x <= 0) then 
         isegno = 0 
      else 
         isegno = 1 
      endif 
      return  
      end function isegno 


 
 
 
 
      subroutine splitter(ij, i, j, is, succ, fall, iphead, link, up, vp, wp, &
         xp, yp, xpf, ypf, qpar, diepar, ico, istatus, nx, ny, x, y, npcel,&
         ntmp,dtsub,verbose) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
!(Leo)
      real(double) :: dtsub(*)
      logical :: verbose
      integer , intent(in) :: ij 
      integer , intent(in) :: i 
      integer , intent(in) :: j 
      integer  :: is 
      integer , intent(in) :: nx 
      integer  :: ny 
      integer  :: ntmp 
      real(double) , intent(inout) :: succ 
      real(double) , intent(inout) :: fall 
      integer , intent(inout) :: iphead(*) 
      integer  :: link(*) 
      integer  :: ico(*) 
      integer , intent(out) :: istatus(*) 
      integer , intent(in) :: npcel(*) 
      real(double) , intent(inout) :: up(*) 
      real(double) , intent(inout) :: vp(*) 
      real(double) , intent(inout) :: wp(*) 
      real(double) , intent(inout) :: xp(*) 
      real(double) , intent(inout) :: yp(*) 
      real(double) , intent(out) :: xpf(*) 
      real(double) , intent(out) :: ypf(*) 
      real(double) , intent(inout) :: qpar(*) 
      real(double) , intent(inout) :: diepar(*)
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(4) :: npnew 
      integer , dimension(ntmp) :: nploc 
      integer :: nxp, nsplit, np, lvmax, lv, lp, ip, ipj, ipjp, ijp 
      real(double), dimension(4) :: xpnew, ypnew 
      real(double), dimension(ntmp) :: enkin 
      real(double) :: the, zeta, esptar, epsilon, x1, x2, x3, x4, y1, y2, y3, &
         y4 
      logical :: sux 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: ismax 
!-----------------------------------------------
 
      nxp = nx + 1 
      fall = fall + 1 
      nsplit = 0 
 
!
!
!     Choose the most energetic particle
!
      np = iphead(ij) 
      if (np == 0) then 
         if(verbose) write (66, *) 'split fails, no particles feed to the routine' 
         return  
      endif 
 
 
      call listmkr_control (np, nploc, link, ico, is, lvmax, ntmp) 
!
      if (lvmax == 0) then 
         if(verbose) write (66, *) 'split fails, no particles found with right color' 
         return  
      else 
         lvmax = min(lvmax,32) 
      endif 
 
      do lv = 1, lvmax 
         np = nploc(lv) 
         enkin(lv) = abs(qpar(np))*(up(np)**2+vp(np)**2+wp(np)**2) 
      end do 
!
      lp = ismax(lvmax,enkin,1) 
      nsplit = nploc(lp) 
!
      if (nsplit == 0) then 
         if(verbose) write (66, *) 'splitter failure #1 nsplit=0' 
         return  
      endif 
 
      np = nsplit 
 
      the = xp(np) - int(xp(np)) 
      zeta = yp(np) - int(yp(np)) 
      if (int(xp(np))/=i .or. int(yp(np))/=j) then 
         if(verbose) write (66, *) 'splitter fa' 
         return  
      endif 
      esptar = 1./sqrt(float(npcel(is))) 
 
      epsilon = min(min(min(min(the,zeta),esptar),abs(1. - the)),abs(1. - zeta)&
         ) 
      epsilon = epsilon*0.9 
 
      if (epsilon <= esptar/50.) then 
         if(verbose) write (66, *) 'splitter failure: epsilon<0', epsilon 
         return  
      endif 
 
      sux = .TRUE. 
      do ip = 1, 3 
         if (iphead(1) > 0) then 
            npnew(ip) = iphead(1) 
            iphead(1) = link(iphead(1)) 
         else 
            sux = .FALSE. 
         endif 
      end do 
      if (sux) then 
         npnew(4) = np 
!     write(6,*)'splittate',npnew
 
         xpnew(1) = xp(np) + epsilon 
         xpnew(2) = xp(np) 
         xpnew(3) = xp(np) - epsilon 
         xpnew(4) = xp(np) 
 
         ypnew(1) = yp(np) 
         ypnew(2) = yp(np) + epsilon 
         ypnew(3) = yp(np) 
         ypnew(4) = yp(np) - epsilon 
 
         do ip = 1, 4 
 
!(Leo)
            dtsub(npnew(ip))=dtsub(nsplit)

            xp(npnew(ip)) = xpnew(ip) 
            yp(npnew(ip)) = ypnew(ip) 
            up(npnew(ip)) = up(nsplit) 
            vp(npnew(ip)) = vp(nsplit) 
            wp(npnew(ip)) = wp(nsplit) 
            ico(npnew(ip)) = is 
            istatus(npnew(ip)) = 1 
            qpar(npnew(ip)) = qpar(nsplit)/4. 
            diepar(npnew(ip)) = diepar(nsplit)/4.
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
            x1 = x(ipj) 
            x2 = x(ipjp) 
            x3 = x(ijp) 
            x4 = x(ij) 
            y1 = y(ipj) 
            y2 = y(ipjp) 
            y3 = y(ijp) 
            y4 = y(ij) 
            the = xp(npnew(ip)) - int(xp(npnew(ip))) 
            zeta = yp(npnew(ip)) - int(yp(npnew(ip))) 
!     if(the.gt.1..or.the.lt.0.) write(*,*)'splitter tradisce'
!     if(zeta.gt.1..or.zeta.lt.0.) write(*,*)'splitter tradisce'
            xpf(npnew(ip)) = the*((1. - zeta)*x1 + zeta*x2) + (1. - the)*((1.&
                - zeta)*x4 + zeta*x3) 
            ypf(npnew(ip)) = the*((1. - zeta)*y1 + zeta*y2) + (1. - the)*((1.&
                - zeta)*y4 + zeta*y3) 
 
!     write(*,*)npnew(ip),xpf(npnew(ip)),x(ij),x(ipj),xp(npnew(ip))
!     write(*,*)npnew(ip),ypf(npnew(ip)),y(ij),y(ipj),yp(npnew(ip))
 
         end do 
 
 
         do ip = 1, 3 
 
            link(npnew(ip)) = iphead(ij) 
            iphead(ij) = npnew(ip) 
 
         end do 
 
         fall = fall - 1 
         succ = succ + 1 
 
         return  
 
      else 
 
         if(verbose) write (66, *) 'splitter failure: not enough new particles', iphead(1) 
         return  
 
      endif 
      return  
 
 
      end subroutine splitter 


!
      subroutine listmkr_control(np, nploc, link, ico, ivul, lvmax, ntmp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: np 
      integer , intent(in) :: ivul 
      integer , intent(out) :: lvmax 
      integer , intent(in) :: ntmp 
      integer , intent(out) :: nploc(ntmp) 
      integer , intent(in) :: link(*) 
      integer , intent(in) :: ico(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nps 
!-----------------------------------------------
!
!      get particles from linked list &  place indices in table nploc
!
!      INPUTS:
!
!        np      :      first particle index
!        link  :      array of links to next partcile
!        ntmp  :      dimension of nploc array
!
!      OUTPUTS:
!
!        nploc      :      array of particle indices
!        lvmax      :      no of particles in nploc
!
      lvmax = 0 
      nps = np 
    1 continue 
      if (nps > 0) then 
         if (ico(nps) == ivul) then 
            lvmax = lvmax + 1 
            nploc(lvmax) = nps 
         endif 
         if (lvmax >= ntmp) return  
         nps = link(nps) 
         go to 1 
      endif 
      return  
      end subroutine listmkr_control 
