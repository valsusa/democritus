      subroutine reflctp(xpf, ypf, xr, xl, yt, yb, fail) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(inout) :: xpf 
      real(double) , intent(in) :: ypf 
      real(double) , intent(in) :: xr 
      real(double) , intent(in) :: xl 
      real(double) , intent(in) :: yt 
      real(double) , intent(in) :: yb 
      real(double) , intent(out) :: fail 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
!-----------------------------------------------
      if (xpf <= xl) then 
         fail = 7 
!     xpf=xr+(xpf-xl)
         xpf = xpf + 2.*(xl - xpf) 
      endif 
      if (xpf > xr) then 
         fail = 5 
!     xpf=xl+(xpf-xr)
         xpf = xpf + 2.*(xr - xpf) 
      endif 
      if (ypf >= yt) fail = 6 
      if (ypf <= yb) fail = 4 
      return  
      end subroutine reflctp 


      subroutine gauge(nx, ny, phi0) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      real(double) , intent(inout) :: phi0(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, ij0, i, j, ij 
      real(double) :: bates 
!-----------------------------------------------
      nxp = nx + 1 
      ij0 = (ny/2)*nxp + 1 + nx/2 
      bates = phi0(ij0) 
      do i = 2, nx 
         do j = 2, ny 
            ij = (j - 1)*nxp + i 
            phi0(ij) = phi0(ij) - bates 
         end do 
      end do 
      return  
      end subroutine gauge 
