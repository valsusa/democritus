!-------------------------------------------------------------------------
!
!  cdf2fortran.c - This file contains code to generate Fortran code to
!                  read in the data of a specified netcdf file.
!
!  History:
!  Date       Name          Action
!  ---------  ------------  -------------------------------------------------
!  ?? Oct 93  B. Schwartz   Created.
!  24 Aug 94  E. Boer       Modified dimension output.
!  29 Jul 97  S. Rupert     Standardized and reinstated required include of
!                           netcdf.inc.
!  30 Jul 87  S. Rupert     Added usage message and command line inputs.
!
!-------------------------------------------------------------------------
 
!# func_description
!  This program will ask the user for the name of a netcdf file
!  to read. it will open that file and using the proper netCDF
!  calls, get info on the variables and their dimensions. it then
!  generates a Fortran program that can be used to actually read the
!  netcDF file and fills the variables with data. This program
!  can be used to read any netCDF file. The user only has to
!  write fortran statements to print the data or pass to another
!  program. Once you have generated a program, you may use it
!  to read any file of that data type; i.e., the program is general
!  until the powers to be change variable attributes.
 
! Required includes.
      include 'netcdf.inc'
 
! Define variables.
      parameter    (mvrlen=3)      ! max number of valid-range values
      parameter    (mtlen=80)      ! max length of title attributes
      integer      ncid            ! netCDF file handle
      integer      rcode           ! read code
      integer      ndims           ! # of dimensions
      integer      nvars           ! # of variables
      integer      natts           ! # of attributes
      integer      recdim          ! record dimension
      integer      dimid           ! dimension handle
      integer      varid           ! variable handle
      integer      vartyp(100)     ! netCDF variable type
      integer      vdims(100)      ! variable dimensions
      integer      vvdims(100,10)  ! variable
      integer      nvatts(100)     ! # of attributes assoc to given var
      character*31 dimnam(100)     ! array holding dimension names
      character*31 varnam(100)     ! array holding variable names
      character*31 attnam(100,10)  ! array holding attribute names
      integer      dimsiz(100)     ! array holding dimension sizes
      integer      nvdims(100)     ! # of variable dimensions
      integer      attype(100,10)  ! netCDF attribute type
      integer      attlen(100,10)  ! attribute length
!                                  ! Note:  max # of variables is 100
!                                  !        max # of attributes per var=10
      character*11 avartyp(6)      ! attribute variable type
      character*11 avt(100)        !
      character*1024 input_file    ! full path to input file
      character*1024 output_file   ! full path to output file, defaults
!                                  ! readnet.f
      data avartyp /'logical*1','character*1','integer*2',              &
     &'integer*4','real*4','real*8'/
 
!     Verify command line.
      if ((iargc() .lt. 1) .or. (iargc() .gt. 2)) then
         write(6,*) 'Usage:  cdf2fortran input_file <output_file>'
         write(6,*) '        input_file:   full path to input file'
         write(6,*) '        output_file:  optional output filename'
         call exit()
      endif
 
!     Retrieve input file path.
      call getarg(1,input_file)
 
!     Open output file.
      if (iargc() .eq. 2) then
         call getarg(2,output_file)
      else
         output_file = 'readnet.f'
      endif
      open(unit=10,file=output_file,status='unknown')
 
!     Open netcdf file.
      ncid=ncopn(input_file,ncnowrit,rcode)
 
!     Inquire about the number of dimensions, varaibles, and attributes.
!     Dimension ids run sequentially from 1 to ndims.
      call ncinq(ncid,ndims,nvars,natts,recdim,rcode)
 
!     Store the dimension names and sizes in arrays.
      do i=1,ndims !number of dimensions returned from ncinq
         dimid=i
         call ncdinq(ncid,dimid,dimnam(i),dimsiz(i),rcode)
!        Dimension ids are i, i.e, 1,ndims
!        dimnam are dimension names (character ids)
!        dimsiz is the size of each dimension
!        recdim is the id of the record dimension
         if (recdim.ne.-1) numrecs=dimsiz(recdim)
!        write(6,70) i,dimnam(i),dimsiz(i)
   70    format(' dimension id= ',i3,' name= ',a31,' size= ',i3)
      end do
 
!                           Variables
!     Variables like dimensions..run sequentially from 1 to nvars.
      do i=1,nvars
         varid=i
!        ncvinq gets variable names, their types and their shapes.
         call ncvinq(ncid,varid,varnam(i),vartyp(i),nvdims(i),          &
     &vdims,nvatts(i),rcode)
!        be careful...vdims is an array (size nvdims(i) thus the
!        use of 2nd array
         if (nvdims(i).ne.0) then
            do k=1,nvdims(i)
               vvdims(i,k)=vdims(k) !vvdims contains the dimension id's
            end do
         endif
!        varnam=variable names
!        vartyp=variable types
!        nvdims=number of dimensions for variable
!        vvdims=nvdims dimension ids for this variable
!        nvatts=number of attributes for variable
  150    format(/,' var id= ',i2,' varnam= ',a10,' vartyp= ',i1,        &
     &' nvdims= ',i1,'num atts=',i2,' vdims= ',<nvdims(i)>i3)
      end do
 
!     Get info on the variable attributes.
      do i=1,nvars !get attributes for all variables
         varid=i
         do j=1,nvatts(i)
!           Get attribute names.
            call ncanam(ncid,varid,j,attnam(i,j),rcode)
!           Get attribute types and length.
            call ncainq(ncid,varid,attnam(i,j),attype(i,j),attlen(i,j),
     &rcode)
!           Get attribute values; be careful first must know if char or
!           if (attype(i,j).eq.2) then
!              if (attlen(i,j).gt.mtlen) then
!                 write(6,245)
!  245            format(' problem with attribute name: too long')
!                 stop
!              endif
!              call ncagtc(ncid,varid,attnam(i,j),attit(i,j),mtlen,rcode)
!           endif
!           if (attype(i,j).ne.2) then
!              call ncagt(ncid,varid,attnam(i,j),vrval(i,j),rcode)
         end do
  275    format(/,' variable= ',a31,                                    &
     &<nvatts(i)>(/,' attribute #',i2,' is: ',a31,'type= ',i3,          &
     &' len= ',i3))
      end do
 
!     Now that the data have been retrieved, generate the fortran program
!     interface to read the data into memory.
 
      do i=1,nvars
         varid=i
         avt(i)=avartyp(vartyp(varid)) !avt is the character name of vartyp
      end do
 
!     Generate the fortran template with variable types, names, and dims.
!     Allow variables to have 4 dimensions.
      write(10,501)
  501 format('c-----------------------------------------',              &
     &'------------------------------')
      write(10,502)
  502 format('c')
      write(10,503) output_file
  503 format('c  ',a31)
      write(10,504)
  504 format('c  This file is a fortran template file designed ',       &
     &'to read the given',/,'c  netCDF file into memory.')
      write(10,502)
      write(10,506)
  506 format('c  History:')
      write(10,507)
  507 format('c  Date       Name          Action')
      write(10,508)
  508 format('c  ---------  ------------  ---------------',             &
     &'-----------------------------')
      write(10,509)
  509 format('c  ?? ??? ??  cids          Created.')
      write(10,502)
      write(10,501)
      write(10,*)
      write(10,510)
  510 format('c',5x,'Do not forget to include the -I path_to_netcdf_'   &
     &'includes in your',/,'c',5x,'compile statement')
      write(10,513)
  513 format('c',5x,'Required includes.')
      write(10,515)
  515 format(6x,'include',1x,1h','netcdf.inc',1h')
      write(10,*)
      write(10,520)
  520 format('c',5x,'Define Variables.')
      write(10,530) nvars
  530 format('c     Variable ids run sequentially from 1 to nvars=',i3)
      write(10,550) nvars
  550 format(6x,'parameter',4x,'(nvars =',i2,')',10x,'! number of '     &
     &'variables')
      if (recdim.ne.-1) write(10,554) dimsiz(recdim)
  554 format(6x,'parameter',4x,'(nrec=',i6,')',9x,'! change this '      &
     &'to generalize')
      write(10,576)
  576 format(6x,'integer*4',4x,'rcode',16x,'! read code')
      if (recdim.ne.-1) write(10,577)
  577 format(6x,'integer*4',3x,'recdim',15x,'! record dimension')
      do i=1,nvars
         varid=i
         vsize=index(varnam(varid),' ')-1
         if (nvdims(varid).eq.0) write(10,582) avt(varid),              &
     &varnam(varid)(1:vsize)
  582    format(6x,a,2x,a)
         if (nvdims(varid).eq.1) then ! single dimension variable
            if (recdim.ne.-1) then
               if (vvdims(varid,1).eq.recdim) then
                  write(10,580) avt(varid),varnam(varid)(1:vsize)
               endif
  580          format(6x,a,2x,a,'(nrec)')
               if (vvdims(varid,1).ne.recdim) then
                  write(10,583) avt(varid),varnam(varid)(1:vsize),      &
     &dimsiz(vvdims(varid,1))
               endif
  583          format(6x,a,2x,a20,'(',i4,')')
            endif
            if (recdim.eq.-1) write(10,581) avt(varid),                 &
     &varnam(varid)(1:vsize),(dimsiz(vvdims(varid,j)),j=1,nvdims(varid))
  581       format(6x,a,2x,a,'(',i4,')')
         endif
         if (nvdims(varid).eq.2) then ! double dimension variable
            if (recdim.ne.-1) write(10,585) avt(varid),                 &
     &varnam(varid)(1:vsize),                                           &
     &(dimsiz(vvdims(varid,j)),j=1,nvdims(varid)-1)
  585          format(6x,a,2x,a,'(',i4,',nrec)')
            if (recdim.eq.-1) write(10,586) avt(varid),                 &
     &varnam(varid)(1:vsize),                                           &
     &(dimsiz(vvdims(varid,j)),j=1,nvdims(varid))
  586          format(6x,a,2x,a,'(',i4,',',i4,')')
         endif
         if (nvdims(varid).eq.3) then ! triple dimension variable
            if (recdim.ne.-1) write(10,590) avt(varid),                 &
     &varnam(varid)(1:vsize),                                           &
     &(dimsiz(vvdims(varid,j)),j=nvdims(varid)-1,1,-1)
  590          format(6x,a,2x,a,'(',i4,',',i4,',nrec)')
            if (recdim.eq.-1) write(10,591) avt(varid),                 &
     &varnam(varid)(1:vsize),                                           &
     &(dimsiz(vvdims(varid,j)),j=1,nvdims(varid))
  591          format(6x,a,2x,a,'(',i4,',',i4,',',i4,')')
         endif
         if (nvdims(varid).eq.4) then !variable with 4 dimensions (rare)
            if (recdim.ne.-1) write(10,595) avt(varid),                 &
     &varnam(varid)(1:vsize),                                           &
     &(dimsiz(vvdims(varid,j)),j=nvdims(varid)-1,1,-1)
  595          format(6x,a,2x,a,'(',i4,',',i4,',',i4,',nrec)')
            if (recdim.eq.-1) write(10,596) avt(varid),                 &
     &varnam(varid)(1:vsize),                                           &
     &(dimsiz(vvdims(varid,j)),j=1,nvdims(varid))
  596          format(6x,a,2x,a,'(',i4,',',i4,',',i4,',',i4,')')
         endif
      end do
      write(10,605)
  605 format(6x,'integer*4',4x,'start(10)',/,6x,'integer*4',4x,         &
     &'count(10)')
      write(10,620)
  620 format(6x,'integer',6x,'vdims(10)',12x,'! allow up to 10 '        &
     &'dimensions')
      write(10,621)
  621 format(6x,'character*31 dummy')
!
!     Write out the statements to declare start and count for each variable.
!      do i=1,nvars
!         write(10,622) i,nvdims(i)
!  622    format(6x,'integer*4 start',i2.2,'(',i1,')')
!         write(10,623) i,nvdims(i)
!  623    format(6x,'integer*4 count',i2.2,'(',i1,')')
!      end do
!
!     Generate data statements with variable ids and types and names.
!
!     write(10,750)
! 750 format(6x,'data vars/')
!      write(10,775) (varnam(i),i=1,nvars) !check this
!  775 format(<nvars-1>(5x,'+',1h',a31,2h',/),5x,'+',1h',a31,2h'/)
!      write(10,778)
!  778 formatm(6x,'data vartyp/')
!      write(10,779) (vartyp(i),i=1,nvars)
!  779 format(5x,'+',<nvars-1>(i1,','),i1,'/')
!      write(10,780)
!  780 format(6x,'data nvdims/')
!      write(10,790) (nvdims(i),i=1,nvars)
!  790 format(5x,'+',<nvars-1>(i1,','),i1,'/')
!      write(10,805)
!  805 format(6x,'data dimsiz/')
!      write(10,810) (dimsiz(i),i=1,ndims)
!  810 format(5x,'+',<ndims-1>(i5,','),i5,'/')
!      generate start and count data arrays
!
!     generate statements to create data statements for start and count
!
!      do i=1,nvars
!         if (nvdims(i).eq.1) then
!            write(10,829) i,start(i,1)
!  829       format(6x,'data start',i2.2,'/',i1,'/')
!            write(10,830) i,count(i,1)
!  830       format(6x,'data count',i2.2,'/',i4,'/')
!         else
!            write(10,831) i,(start(i,j),j=1,nvdims(i))
!  831       format(6x,'data start',i2.2,'/',<nvdims(i)-1>(i1,','),
!     +i1,'/')
!            write(10,835) i,(count(i,j),j=1,nvdims(i))
!  835       format(6x,'data count',i2.2,'/',<nvdims(i)-1>(i4,','),
!     +i4,'/')
!         endif
!      end do
 
!     Write the statement to open the netCDF file.
      write(10,*)
      write(10,840)
  840 format('c',5x,'Open netCDF file.')
      fsize=index(input_file,' ')-1
      write(10,1000) input_file(1:fsize)
 1000 format(6x,'ncid=ncopn(',1h',a,1h',/,5x,'+',                       &
     &',ncnowrit,rcode)')
!     Get info on the record dimension for this file.
      if (recdim.ne.-1) then
         write(10,*)
         write(10,1002)
 1002    format('c',5x,'Get info on the record dimension for this '     &
     &'file.')
         write(10,1005)
 1005    format(6x,'call ncinq(ncid,ndims,nvars,ngatts,recdim,rcode)')
         write(10,1006)
 1006    format(6x,'call ncdinq(ncid,recdim,dummy,nrecs,rcode)')
         write(10,1007)
 1007    format('c     !nrecs! now contains the # of records for '      &
     &'this file')
      endif
 
!     Get info on the dimensions; recdim will contain the id of the
!     record dimension.
!     Generate call statements to fill variables with values.
!     In order to make the generated program usable, we need info
!     on the dimensions of the variables. if we do this in the pgm,
!     the only variable not with a constant dimension is the record
!     variable.
      do i=1,nvars
         write(10,1010) varnam(i)
 1010    format(/,'c',5x,'Retrieve data for ',a31)
         lenstr=1
         k=0
         write(10,1015) i
 1015    format(6x,'call ncvinq(ncid,',i2,',dummy,ntp,nvdim,',          &
     &'vdims,nvs,rcode)')
!        Get number of sdims and their ids nvdim and vdims.
         write(10,1018)
 1018    format(6x,'lenstr=1')
         ii=i*10
         write(10,1020)
 1020    format(6x,'do j=1,nvdim')
         write(10,1025)
!        Get the size of each nvdim dimension in ndsize.
 1025    format(9x,'call ncdinq(ncid,vdims(j),dummy,ndsize,rcode)')
         write(10,1030)
 1030    format(9x,'lenstr=lenstr*ndsize')
         write(10,1035)
 1035    format(9x,'start(j)=1',/,9x,'count(j)=ndsize')
         write(10,1040)
 1040    format(6x,'end do')
!         do j=1,nvdims(i)
!            lenstr= lenstr*count(i,j) !needed for character variables
!            start(i,j)=1
!            index=vvdims(i,j)
!            count(i,j)=dimsiz(index)
!            write(10,1025) j,start(i,j),j,count(i,j)
! 1025       format(6x,'start(',i1,')=',i5,/,6x,'count(',i1,')=',i5)
!         end do
         if (vartyp(i).eq.2) then !character variables
            write(10,1250) i,varnam(i)
 1250       format(6x,'call ncvgtc(ncid,',i2,',start',',count',         &
     &',',/,5x,'+',a,',lenstr,rcode)')
         else
            write(10,1350) i,varnam(i)
 1350       format(6x,'call ncvgt(ncid,',i2,',start',',count',          &
     &',',/,5x,'+',a,',rcode)')
         endif
      end do
      write(10,1600)
 1600 format(/,'c',5x,'Begin writing statements to use the data.',/)
      write(10,1800)
 1800 format('c',5x,'End Program.',/6x,'stop')
      write(10,1900)
 1900 format(6x,'end')
!
      write(6,2000), output_file
 2000 format('Generated fortran program called ', a31)
!
      stop
      end
