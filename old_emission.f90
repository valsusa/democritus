
subroutine old_thermionic
  !---------------------------------------------------------!
  !---------------------------------------------------------!
  ! A subroutine to inject thermionically emitted particles !
  ! from the surface of a dust grain.     		    !
  !---------------------------------------------------------!
  !---------------------------------------------------------!

  use vast_kind_param, only: double
  use celest2d_com_M
  use emission_com_M

  implicit none

  !  Local Variables
  integer      :: ip, npint, ipsrch, nnew, whichdust
  real(double) :: cur, muu, vtde, smdr, sintheta
  real(double) :: h, me, q, tep, tdp, mep, qp, vr, vt1, vt2
  real(double) :: qpinj, qtinj, wrkfn, r, tdust
  real(double) :: realecur, realthermcur, thoe, cur_e, npinj

  real(kind=8), dimension(4000) :: xp1, yp1, up1, vp1, wp1

  !  External Functions
  real(double) , external :: ranf, generate

  !Constants	
  me = 9.11d-31; q = 1.6d-19; h = 6.6261d-34
  ipsrch = 0

  tep = siep(2)**2		! normalized electron temp
  tdp = Td*tep/Te		! normalized dust temp
  mep = abs(qom(2))		! normalized electron mass
  qp = dx*dy/npcel(2)	        ! ave charge of injected particles
  wrkfn = Wfn; r = rd; tdust = Td  

  do whichdust = nsp_dust,nsp_dust2

     smdr = rex(whichdust)*0.05
     ! now assign properties of second dust particle if it is time to do so
     if (whichdust == nsp_dust2) then
        tdp = Td2*tep/Te		
        wrkfn = Wfn2; r = rd2; tdust = Td2
     end if

     realecur = 4._8*pi*r**2*q*n0*sqrt(Te/me)*sqrt(q/2._8/pi)           
     realthermcur = (4._8*pi*r*tdust)**2*q**3*me/h**3*exp(-wrkfn/tdust)
     thoe = realthermcur/realecur                              

     cur_e = (rex(3)/siep(2))**2*sqrt(2._8/pi)*siep(2)**3  
     cur = thoe*cur_e 					

     if (cur.eq.0)then
        print*, "Thermionic emission current = 0, no charge injected"
     end if

     npinj = cur*dt/qp 

     if (npint .lt. 1000) then
        qtinj = qp*npinj      			
        npint = int(npinj)	  		
        qpinj = qtinj/real(npint,8)	
     else
        qtinj = npinj*qp
        qpinj = qtinj/1000._8
        npint = 1000
     end if

     vtde = sqrt(2._8*tdp/mep)

     ! Now find which grid cells these particles are to be emitted from 
     ! and assign their properties to the grid
     do ip = 1,npint

        muu = 2d0*ranf()-1d0  ! generate a random cos(theta)
        sintheta=sqrt(1d0-muu**2)
        ! Assign a random position just outside the surface of dust particle
        xp1(ip) = xcenter(whichdust)+((rex(whichdust)+smdr)*sintheta)	
        yp1(ip) = ycenter(whichdust)+((rex(whichdust)+smdr)*muu)	
        vr = generate(0.,vtde)		
        vt1 = vtde*sqrt(-log(1-0.99999*ranf()))	
        vt2 = vtde*sqrt(-log(1-0.99999*ranf()))

        up1(ip) = vr*muu + vt1*sintheta    
        vp1(ip) = vr*sintheta - vt1*muu    
        wp1(ip) = vt2            !---Assume particle has vt2 along z=0 axis

        ! for every electron we emit, add a positive charge with zero 
        ! velocity just inside the surface of the dust particle
        xp1(ip+npint) = xcenter(whichdust)+((rex(whichdust)-smdr)*sintheta)
        yp1(ip+npint) = ycenter(whichdust)+((rex(whichdust)-smdr)*muu)	
        vr = 0.0; vt1 = 0.0; vt2 = 0.0
        up1(ip+npint) = 0; vp1(ip+npint) = 0; wp1(ip+npint) = 0    

     end do

     !----------------------------------------------------!
     ! Partial advance of e's emitted during the timestep !
     !----------------------------------------------------!
     !call partial_push(xp1(1:npint),yp1(1:npint),up1(1:npint),vp1(1:npint),wp1(1:npint),-qpinj,npint)

     ! add injected particles to the list of parts in system
     do ip=1,2*npint

        if (iphead(1) > 0)then		
           nnew = iphead(1)
           iphead(1) = link(iphead(1))
           link(nnew)=ipsrch
           ipsrch=nnew          

           xpf(nnew) = xp1(ip)				
           ypf(nnew) = yp1(ip)				
           up(nnew) = up1(ip)    
           vp(nnew) = vp1(ip)    
           wp(nnew) = wp1(ip)            ! assume vt2 along z=0 axis

           if (ip <= npint) then
              ico(nnew) = 2 
              qpar(nnew) = -qpinj 
           else               !inject ions into dust to account for ejected e's
              ico(nnew) = whichdust 
              qpar(nnew) = qpinj 
           end if
           diepar(nnew) =0.d0* abs(qpar(nnew))*diem(ico(nnew))

        else
           print*, "WARNING: Not Enough Particles : dust emission" 
           stop  
        end if

     end do

  end do

  iphd2 = iphead
  call locator_kin (ipsrch)

end subroutine old_thermionic
!--------------------------------------------------------------------------------------------------------!
!--------------------------------------------------------------------------------------------------------!
subroutine photoemission
  !---------------------------------------------------------!
  !---------------------------------------------------------!
  ! A subroutine to inject particles from the surface of a  !
  ! dust grain due to the photoelectric effect.     		   !
  !---------------------------------------------------------!
  !---------------------------------------------------------!
  use vast_kind_param, only: double
  use celest2d_com_M
  use emission_com_M

  implicit none

  !  Local Variables
  integer      :: ip, npint, ipsrch, nnew, whichdust
  real(double) :: cur, muu, vtde, smdr, sintheta, costheta, phi
  real(double) :: h, me, q, tep, mep, qp, vr, vt1, vt2
  real(double) :: qpinj, qtinj, r, Emax, Ep
  real(double) :: realecur, realpecur, peoe, cur_e, npinj

  real(kind=8), dimension(4000) :: xp1, yp1, up1, vp1, wp1

  !  External Functions
  real(double) , external :: ranf, generate

  !Constants	
  me = 9.11d-31; q = 1.6d-19; h = 6.6261d-34
  ipsrch = 0

  tep = siep(2)**2					! normalized electron temp
  mep = abs(qom(2))					! normalized electron mass
  qp = dx*dy/npcel(2)	        	! ave charge of injected particles
  r = rd; Emax = E0  

	whichdust = nsp_dust
  !do whichdust = nsp_dust,nsp_dust2
     smdr = rex(whichdust)*0.05
     ! now assign properties of second dust particle if it is time to do so
     if (whichdust == nsp_dust2) then
        jp = jp2; qabs = qabs2; yield = yield2		
        r = rd2; Emax = E02
     end if

     realecur = 4._8*pi*r**2*q*n0*sqrt(Te/me)*sqrt(q/2._8/pi)           
     realpecur = pi*r**2*q*jp*qabs*yield          
     peoe = realpecur/realecur                              

     cur_e = (rex(3)/siep(2))**2*sqrt(2._8/pi)*siep(2)**3  
     cur = peoe*cur_e 						

     if (cur.eq.0)then
        print*, "Photo emission current = 0, no charge injected"
     end if

     !print*, "Real electron current: ", realecur
     !print*, "Real photoemission current: ", realpecur
     !print*, "Ratio (PE/e-):", peoe
     !print*, "Normalized i_e:", cur_e
     !print*, "Normalized i_PE:", cur


     npinj = cur*dt/qp 

     if (npint .lt. 1000) then
        qtinj = qp*npinj      			
        npint = int(npinj)	  		
        qpinj = qtinj/real(npint,8)	
     else
        qtinj = npinj*qp
        qpinj = qtinj/1000._8
        npint = 1000
     end if

    print*, "PHOTOEMISSION: # injected = ", npint

     ! Now find which grid cells these particles are to be emitted from 
     ! and assign their properties to the grid
     do ip = 1,npint
        ! generate energy uniformly
        Ep = Emax*ranf()*tep/Te !--> normalize the energy to code's units

        ! generate two random angles
        muu = ranf()
        phi = 2.d0*pi*ranf()
        costheta = 2d0*ranf()-1d0  ! generate a random cos(theta)
        sintheta=sqrt(1d0-costheta**2)

		  ! Assign a random position just outside the surface of dust particle
        xp1(ip) = xcenter(whichdust)+((rex(whichdust)+smdr)*sintheta)	
        yp1(ip) = ycenter(whichdust)+((rex(whichdust)+smdr)*costheta)	
        
        ! generate 3 components of velocity in relative coords with vr being normal (out) of the surface
        vr = sqrt(2.d0*Ep/mep)*muu
        vt1 = sqrt(2.d0*Ep/mep)*sqrt(1-muu**2)*sin(phi)
        vt2 = sqrt(2.d0*Ep/mep)*sqrt(1-muu**2)*cos(phi)

        up1(ip) = vr*costheta + vt1*sintheta    
        vp1(ip) = vr*sintheta - vt1*costheta    
        wp1(ip) = vt2            !---Assume particle has vt2 along z=0 axis

        ! for every electron we emit, add a positive charge with zero 
        ! velocity just inside the surface of the dust particle
        xp1(ip+npint) = xcenter(whichdust)+((rex(whichdust)-smdr)*sintheta)
        yp1(ip+npint) = ycenter(whichdust)+((rex(whichdust)-smdr)*costheta)	
        up1(ip+npint) = 0.; vp1(ip+npint) = 0.; wp1(ip+npint) = 0.    

     end do

     !----------------------------------------------------!
     ! Partial advance of e's emitted during the timestep !
     !----------------------------------------------------!
     !call partial_push(xp1(1:npint),yp1(1:npint),up1(1:npint),vp1(1:npint),wp1(1:npint),-qpinj,npint)

     ! add injected particles to the list of parts in system
     do ip=1,2*npint

        if (iphead(1) > 0)then		
           nnew = iphead(1)
           iphead(1) = link(iphead(1))
           link(nnew)=ipsrch
           ipsrch=nnew          

           xpf(nnew) = xp1(ip)				
           ypf(nnew) = yp1(ip)				
           up(nnew) = up1(ip)    
           vp(nnew) = vp1(ip)    
           wp(nnew) = wp1(ip)            ! assume vt2 along z=0 axis

           if (ip <= npint) then
              ico(nnew) = 2 
              qpar(nnew) = -qpinj 
           else               !inject ions into dust to account for ejected e's
              ico(nnew) = whichdust 
              qpar(nnew) = qpinj 
           end if
           diepar(nnew) =0.d0* abs(qpar(nnew))*diem(ico(nnew))

        else
           print*, "WARNING: Not Enough Particles : dust emission" 
           stop  
        end if

     end do

  !end do

  iphd2 = iphead
  call locator_kin (ipsrch)

end subroutine photoemission

