      subroutine mccen(mratio, me, velx, vely, velz, ntarld, dt, twopi, evfact) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
!
!
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(in) :: mratio 
      real(double) , intent(in) :: me 
      real(double) , intent(inout) :: velx 
      real(double) , intent(inout) :: vely 
      real(double) , intent(inout) :: velz 
      real(double) , intent(in) :: ntarld 
      real(double) , intent(in) :: dt 
      real(double) , intent(in) :: twopi 
      real(double) , intent(in) :: evfact 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: vscatt, vinc 
      real(double), dimension(3) :: uinc, uscatt 
      real(double) :: kf1, kf2, kf3, coschi, sinchi, cosphi, sinphi, costhe, &
         sinthe, rand, cfqmax, ene, pcoll, crx, enev 
!-----------------------------------------------
 
      crx = 2.36D-18 
 
      ene = velx*velx + vely*vely + velz*velz 
      vinc = dsqrt(ene) + 1.D-10 
 
      ene = 0.5D0*ene*me 
 
      cfqmax = crx*ntarld*vinc 
 
      pcoll = 1.D0 - dexp((-dt*cfqmax)) 
 
!     Rand=ranf()
      call random_number (rand) 
 
      if (rand < pcoll) then 
 
 
         enev = ene*evfact + 1.D-10              ! energy in eV 
 
         uinc(1) = velx/vinc 
         uinc(2) = vely/vinc 
         uinc(3) = velz/vinc 
 
         call random_number (rand) 
!       Rand=ranf()
 
         if (rand == 0.D0) rand = rand + 1.D-8 
 
         coschi = (2.D0 + enev - 2.D0*(1.D0 + enev)**rand)/enev 
 
 
         sinchi = dsqrt(1.D0 - coschi*coschi) 
 
         call random_number (rand) 
         rand = twopi*rand                       !ranf() 
         cosphi = dcos(rand) 
         sinphi = dsin(rand) 
 
         costhe = uinc(1) 
         sinthe = dsqrt(1.D0 - costhe*costhe) + 1.D-10 
 
         kf1 = coschi 
         kf3 = sinchi/sinthe 
         kf2 = kf3*sinphi 
         kf3 = kf3*cosphi 
 
         uscatt(1) = kf1*uinc(1) + kf3*(uinc(2)*uinc(2)+uinc(3)*uinc(3)) 
         uscatt(2) = kf1*uinc(2) + kf2*uinc(3) - kf3*uinc(1)*uinc(2) 
         uscatt(3) = kf1*uinc(3) - kf2*uinc(2) - kf3*uinc(1)*uinc(3) 
 
         ene = ene*(1.D0 - 2.D0*mratio*(1 - coschi)) 
 
         vscatt = dsqrt(2.D0/(me + 1.D-10)*ene) 
 
         velx = vscatt*uscatt(1) 
         vely = vscatt*uscatt(2) 
         velz = vscatt*uscatt(3) 
 
 
      endif 
      return  
 
      end subroutine mccen 
