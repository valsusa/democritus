      subroutine locator_kin(ipsrch) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(inout) :: ipsrch 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(ntmp) :: inew, jnew, killer 
      integer :: iplost, np, nchng, lwt, lwb, lwl, lwr, lv, iphold, ijnew, ij, &
         iwid, jwid, i, j, ipj, ipjp, ijp 
      real(double) :: dr, dz, the, zeta, w1, w2, w3, w4, wsx, wsy 
      logical :: discard, left 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/locator_box.f90,v 1.2 2006/07/05 15:42:23 lapenta Exp $
!
! $Log: locator_box.f90,v $
! Revision 1.2  2006/07/05 15:42:23  lapenta
! Correction on BC for the particles and electric fields. Major revision
! Gianni
!
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!      a routine to locate a point in a computation mesh of quadrilat
!         and calculate its natural coordinates
!
!
!     *********************************************************
!
!     reconstruct linked lists
!
!     ***********************************************
!
!
!     ***************************************************************
!
!     Stage 1: Detect intersections of the particle orbits with the boun
!              Discard particles that strike an absorbing boundary.
!
!     ***************************************************************
!
      iplost = 0 
      np = ipsrch 
      nchng = -1 
      if (np /= 0) then 
 1100    continue 
         call listmkr (np, nploc, link, lvmax, ntmp) 
 
         if (periodic) then 
            lwt = 2 
            lwb = 2 
         else 
            lwt = 3 
            lwl = 3 
         endif 
         if (reflctr) then 
            lwr = 1 
         else 
            lwr = 3 
         endif 
         if (reflctl) then 
            lwl = 1 
         else 
            lwl = 3 
         endif 
         call fence_box (lvmax, nploc, xpf, ypf, cyl, up, vp, wp, xl, xr, yt, yb, &
            lwl, lwr, lwb, lwt, killer) 
!
         do lv = 1, lvmax 
            if (killer(lv) /= 0) then 
               ipsrch = link(nploc(lv)) 
               link(nploc(lv)) = iphd2(1) 
               iphd2(1) = nploc(lv) 
            else 
               ipsrch = link(nploc(lv)) 
               link(nploc(lv)) = iplost 
               iplost = nploc(lv) 
            endif 
            xp(nploc(lv)) = ifix((xpf(nploc(lv))-xorigin)/dr+2.) 
            yp(nploc(lv)) = ifix((ypf(nploc(lv))-yorigin)/dz+2.) 
         end do 
!
         np = ipsrch 
         if (np > 0) go to 1100 
!
!     *****************************************************************
!
!     Stage 2: Particles lying in the domain are assigned to cell lists.
!
!     *****************************************************************
!
         iphold = 0 
         np = iplost 
 1901    continue 
         if (np > 0) then 
!
            call listmkr (np, nploc, link, lvmax, ntmp) 
!
!
            call whereami (lvmax, nploc, nploc, 1, nxp, xn, yn, nx, ny, xpf, &
               ypf, xp, yp) 
!
            do lv = 1, lvmax 
               inew(lv) = ifix(xp(nploc(lv))) 
               jnew(lv) = ifix(yp(nploc(lv))) 
               ijnew = (jnew(lv)-1)*nxp + inew(lv) 
               iplost = link(nploc(lv)) 
               link(nploc(lv)) = iphd2(ijnew) 
               iphd2(ijnew) = nploc(lv) 
            end do 
!
            np = iplost 
            if (np > 0) go to 1901 
!
         endif 
      endif 
!
      do ij = 1, nxyp 
         iphead(ij) = iphd2(ij) 
         iphd2(ij) = 0 
      end do 
!
      return  
!
      end subroutine locator_kin 
