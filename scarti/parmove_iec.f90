      subroutine parmove_iec
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  17:26:36   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(ntmp) :: isp, inew, jnew, iold, jold, numx, numy, ls 
      integer :: ipsrch, nlostr, nlostl, nlost, j, i, ij, ipj, ipjp, ijp, np, &
         lv, l, is
      real(double), dimension(ntmp) :: the, zeta, xtp0, ytp0, xpc, ypc, w1, w2&
         , w3, w4, upv1, vpv1, wpv1, qptmp, expar, eypar 
      real(double), dimension(nsp) :: xcentre, ycentre, voldust 
      real(double) :: reflr, reflt, refll, reflb, qomdt, x2p, z2p, upvold 
      logical, dimension(ntmp) :: discard 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/parmove_iec.f90,v 1.2 2006/11/21 20:34:51 lapenta Exp $
!
! $Log: parmove_iec.f90,v $
! Revision 1.2  2006/11/21 20:34:51  lapenta
! Gianni: modifications to have 2 dust particles
!
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!     ------------------------------------------------------
!     ------------------------------------------------------
!
!     PARMOVE: a routine to advance the particle eqs. of motion
!              using leapfrog differencing
!
!     ------------------------------------------------------
!     ------------------------------------------------------
!
!
!
!
!
      data reflr, reflt, refll, reflb/ 1., 1., 1., 1./  
      iphd2(1) = iphead(1) 
      ipsrch = 0 
!
 
!
!
      dkdt = 0. 
 
      nlostr = 0 
      nlostl = 0 
      nlost = 0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
!
!
!
            np = iphead(ij) 
            if (np == 0) cycle  
  100       continue 
            call listmkr (np, nploc, link, lvmax, ntmp) 
!
            do lv = 1, lvmax 
               iold(lv) = int(xp(nploc(lv))) 
               jold(lv) = int(yp(nploc(lv))) 
               inew(lv) = iold(lv) 
               jnew(lv) = jold(lv) 
               the(lv) = xp(nploc(lv)) - iold(lv) 
               zeta(lv) = yp(nploc(lv)) - jold(lv) 
               xpv(lv) = xp(nploc(lv)) 
               ypv(lv) = yp(nploc(lv)) 
               upv(lv) = up(nploc(lv)) 
               vpv(lv) = vp(nploc(lv)) 
               wpv(lv) = wp(nploc(lv)) 
               qptmp(lv) = qpar(nploc(lv)) 
!
            end do 
!
!     interpolate the electric field to the particle position
!
            do lv = 1, lvmax 
!
               w1(lv) = the(lv)*(1. - zeta(lv)) 
               w2(lv) = the(lv)*zeta(lv) 
               w3(lv) = (1. - the(lv))*zeta(lv) 
               w4(lv) = (1. - the(lv))*(1. - zeta(lv)) 
!
               expar(lv) = w1(lv)*ex(ipj) + (w2(lv)*ex(ipjp)+(w3(lv)*ex(ijp)+w4&
                  (lv)*ex(ij))) 
!c
               eypar(lv) = w1(lv)*ey(ipj) + (w2(lv)*ey(ipjp)+(w3(lv)*ey(ijp)+w4&
                  (lv)*ey(ij))) 
!
            end do 
!
!dir$ ivdep
            do l = 1, lvmax 
!
!     calculate the grid indices of the particle
!
               isp(l) = ico(nploc(l)) 
               xtp0(l) = xpf(nploc(l)) 
               ytp0(l) = ypf(nploc(l)) 
               massp(l) = qpar(nploc(l))/qom(isp(l)) 
            end do 
!
!
            do l = 1, lvmax 
!
               qomdt = qom(isp(l))*dt 
!
!
!
 
!
!     calculate the new particle velocity
!
!
 
               upv1(l) = upv(l) + qomdt*expar(l) 
               vpv1(l) = vpv(l) + qomdt*eypar(l) 
               wpv1(l) = wpv(l) 
 
!
!     calculate the new particle positions
!
               if (cyl==0. .or. .not.boris) then 
                  xpc(l) = xpf(nploc(l)) + upv1(l)*dt 
               else 
                  x2p = xpf(nploc(l)) + upv1(l)*dt 
                  z2p = wpv1(l)*dt 
                  xpc(l) = sqrt(x2p**2 + z2p**2 + 1E-10) 
                  upvold = upv1(l) 
                  upv1(l) = upv1(l)*x2p/xpc(l) + wpv1(l)*z2p/xpc(l) 
                  wpv1(l) = (-upvold*z2p/xpc(l)) + wpv1(l)*x2p/xpc(l) 
               endif 
 
               ypc(l) = ypf(nploc(l)) + vpv1(l)*dt 
               if (isp(l)<nsp_dust) then
			   if(sqrt((xpc(l)-xcenter(1))**2+(ypc(l)-ycenter(1&
                  ))**2)>rex(nsp_dust)) then				   
!     this is designed to kill the particle
               ypc(l) = -dx 
			   end if
			   do is=nsp_dust+1,nrg
			   if(sqrt((xpc(l)-xcenter(1))**2+(ypc(l)-ycenter(1&
                  ))**2)<rex(is)) then				   
               ypc(l) = -dx 
			   end if
			   enddo
			   end if
!
            end do 
!
!
 
 
 
            do lv = 1, lvmax 
               dkdt(ij,isp(lv)) = dkdt(ij,isp(lv)) + 0.5*massp(lv)*(upv1(lv)**2&
                  +vpv1(lv)**2+wpv1(lv)**2-upv(lv)**2-vpv(lv)**2-wpv(lv)**2) 
            end do 
!
!
!
!     calculate the new natural coordinates of the particle
!
            call mapv (xn, yn, ij, nxp, xpc, ypc, the, zeta, 1, lvmax) 
!
!     restore updated particle positions
!
!dir$ ivdep
            do l = 1, lvmax 
               up(nploc(l)) = upv1(l) 
               vp(nploc(l)) = vpv1(l) 
               wp(nploc(l)) = wpv1(l) 
               xp(nploc(l)) = iold(l) + the(l) 
               yp(nploc(l)) = jold(l) + zeta(l) 
               xtp0(l) = xpf(nploc(l)) 
               ytp0(l) = ypf(nploc(l)) 
               xpf(nploc(l)) = xpc(l) 
               ypf(nploc(l)) = ypc(l) 
               inew(l) = iold(l) + the(l) 
               jnew(l) = jold(l) + zeta(l) 
            end do 
!
            do lv = 1, lvmax 
!
!     check first to see whether particle has changed cells
!
               if (inew(lv) - iold(lv)==0 .and. jnew(lv)-jold(lv)==0) then 
!    for those particles that have not changed cells,
!     no further tests are required
!     return particle to original cell list
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = iphd2(ij) 
                  iphd2(ij) = nploc(lv) 
!
               else 
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = ipsrch 
                  ipsrch = nploc(lv) 
                  nlost = nlost + 1 
!
!     save old position in xp,yp arrays
!
                  xp(nploc(lv)) = xtp0(lv) 
                  yp(nploc(lv)) = ytp0(lv) 
!
!
!
               endif 
            end do 
            np = iphead(ij) 
            if (np > 0) go to 100 
!
         end do 
      end do 
!
!
!     *********************************************************
!
!     reconstruct linked lists
!
!     ***********************************************
      call locator_kin (ipsrch) 
      return  
      end subroutine parmove_iec 
