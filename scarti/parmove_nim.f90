      subroutine parmove 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(ntmp) :: isp, inew, jnew, iold, jold, numx, numy, ls 
      integer :: ipsrch, n, ij, nh, is, j, i, ipj, ipjp, ijp, nlostr, nlostl, &
         nlost, np, lv, l 
      real(double), dimension(ntmp) :: the, zeta, xtp0, ytp0, xpc, ypc, w1, w2&
         , w3, w4, upv1, vpv1, wpv1, qptmp, bxpar, bypar, bzpar, expar, eypar 
      real(double), dimension(nsp) :: xcentre, ycentre, voldust 
      real(double) :: reflr, reflt, refll, reflb, xcdust, ycdust, rdust, qomdt&
         , denom, udotb, x2p, z2p, upvold, adust, bdust, cdust, testdust, &
         sdust1, sdust2, sdust, radp, dnorm, rld3, vexact 
      logical , dimension(ntmp) :: discard 
      logical :: nim 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/parmove_nim.f90,v 1.1.1.1 2006/06/13 23:37:45 cfichtl Exp $
!
! $Log: parmove_nim.f90,v $
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!     ------------------------------------------------------
!     ------------------------------------------------------
!
!     PARMOVE: a routine to advance the particle eqs. of motion
!              using leapfrog differencing
!
!     ------------------------------------------------------
!     ------------------------------------------------------
!
!
!
!
!
      data reflr, reflt, refll, reflb/ 1., 1., 1., 1./  
      iphd2(1) = iphead(1) 
      ipsrch = 0 
!
      pi = acos(-1D0) 
!
      nim = .FALSE. 
!
!
      do n = 1, nrg 
         do ij = 1, nxyp 
            dkdt(ij,n) = 0.0 
         end do 
      end do 
!
!     dust diagnostics
!
      nh = mod(ncyc,nhst) 
!
      do is = nsp_dust, nsp 
         qdustp(nh,is) = 0. 
         emomdustxp(nh,is) = 0. 
         emomdustyp(nh,is) = 0. 
         fdragx(nh,is) = 0. 
         fdragy(nh,is) = 0. 
         feletx(nh,is) = 0. 
         felety(nh,is) = 0. 
      end do 
!
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
            d(ij) = 0.25*(x(ipj)+x(ipjp)+x(ijp)+x(ij)) 
            e(ij) = 0.25*(y(ipj)+y(ipjp)+y(ijp)+y(ij)) 
         end do 
      end do 
!
!     get the centre!
!
 
      xcdust = xcenter(nsp_dust) 
      ycdust = ycenter(nsp_dust) 
      rdust = rex(nsp_dust) 
 
!
 
!
!
      nh = mod(ncyc,nhst) 
      if (nh > 1) enrglost(nh) = enrglost(nh-1) 
 
      kmax = 2*(nx + ny - 2) + 1 
      nlostr = 0 
      nlostl = 0 
      nlost = 0 
      do j = 2, ny 
         do i = 2, nx 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ipjp = ij + nxp + 1 
            ijp = ij + nxp 
!
!
!
            np = iphead(ij) 
            if (np == 0) cycle  
  100       continue 
            call listmkr (np, nploc, link, lvmax, ntmp) 
!
            do lv = 1, lvmax 
               iold(lv) = int(xp(nploc(lv))) 
               jold(lv) = int(yp(nploc(lv))) 
               inew(lv) = iold(lv) 
               jnew(lv) = jold(lv) 
               the(lv) = xp(nploc(lv)) - iold(lv) 
               zeta(lv) = yp(nploc(lv)) - jold(lv) 
               xpv(lv) = xp(nploc(lv)) 
               ypv(lv) = yp(nploc(lv)) 
               upv(lv) = up(nploc(lv)) 
               vpv(lv) = vp(nploc(lv)) 
               wpv(lv) = wp(nploc(lv)) 
               qptmp(lv) = qpar(nploc(lv)) 
!
            end do 
!
!     interpolate the electric field to the particle position
!
            do lv = 1, lvmax 
!
               w1(lv) = the(lv)*(1. - zeta(lv)) 
               w2(lv) = the(lv)*zeta(lv) 
               w3(lv) = (1. - the(lv))*zeta(lv) 
               w4(lv) = (1. - the(lv))*(1. - zeta(lv)) 
!
               expar(lv) = w1(lv)*ex(ipj) + (w2(lv)*ex(ipjp)+(w3(lv)*ex(ijp)+w4&
                  (lv)*ex(ij))) 
!c
               eypar(lv) = w1(lv)*ey(ipj) + (w2(lv)*ey(ipjp)+(w3(lv)*ey(ijp)+w4&
                  (lv)*ey(ij))) 
!
!
               bxpar(lv) = w1(lv)*bx(ipj) + (w2(lv)*bx(ipjp)+(w3(lv)*bx(ijp)+w4&
                  (lv)*bx(ij))) 
!c
               bypar(lv) = w1(lv)*by(ipj) + (w2(lv)*by(ipjp)+(w3(lv)*by(ijp)+w4&
                  (lv)*by(ij))) 
!c
               bzpar(lv) = w1(lv)*bz(ipj) + (w2(lv)*bz(ipjp)+(w3(lv)*bz(ijp)+w4&
                  (lv)*bz(ij))) 
!
            end do 
!
!dir$ ivdep
            do l = 1, lvmax 
!
!     calculate the grid indices of the particle
!
               isp(l) = ico(nploc(l)) 
               xtp0(l) = xpf(nploc(l)) 
               ytp0(l) = ypf(nploc(l)) 
               massp(l) = qpar(nploc(l))/qom(isp(l)) 
            end do 
!
 
!
            do l = 1, lvmax 
               lv = l 
!
               qomdt = qom(isp(l))*dt 
!
!     calculate the new particle velocity
!
!
!     add correction term for motion in cylindrical coordinates
!
!      bzpar(lv)=bzpar(lv)
!     &     -cyl*wp(nploc(lv))/(xpf(nploc(lv))*qom(isp(lv))+1e-10)
!
               qomdt = qom(isp(lv))*dt 
!
!
!     The electric field part I
!
!
               upv(lv) = upv(lv) + qomdt*expar(lv)*.5D0 
               vpv(lv) = vpv(lv) + qomdt*eypar(lv)*.5D0 
               wpv(lv) = wpv(lv) 
!
               denom = 1D0/(1. + (bxpar(lv)**2+bypar(lv)**2+bzpar(lv)**2)*qomdt&
                  **2) 
               udotb = upv(lv)*bxpar(lv) + vpv(lv)*bypar(lv) + wpv(lv)*bzpar(lv&
                  ) 
               upv1(lv) = (upv(lv)+qomdt*(vpv(lv)*bzpar(lv)-wpv(lv)*bypar(lv)+&
                  qomdt*(udotb*bxpar(lv))))*denom 
!
               vpv1(lv) = (vpv(lv)+qomdt*(wpv(lv)*bxpar(lv)-upv(lv)*bzpar(lv)+&
                  qomdt*(udotb*bypar(lv))))*denom 
!
               wpv1(lv) = (wpv(lv)+qomdt*(upv(lv)*bypar(lv)-vpv(lv)*bxpar(lv)+&
                  qomdt*(udotb*bzpar(lv))))*denom 
!
!     The electric field strikes back
!
!
               upv1(lv) = upv1(lv) + qomdt*expar(lv)*.5D0 
!
               vpv1(lv) = vpv1(lv) + qomdt*eypar(lv)*.5D0 
!
!
!     calculate the new particle positions
!
               if (cyl==0. .or. .not.boris) then 
                  xpc(l) = xpf(nploc(l)) + upv1(l)*dt 
               else 
                  x2p = xpf(nploc(l)) + upv1(l)*dt 
                  z2p = wpv1(l)*dt 
                  xpc(l) = sqrt(x2p**2 + z2p**2 + 1E-10) 
                  upvold = upv1(l) 
                  upv1(l) = upv1(l)*x2p/xpc(l) + wpv1(l)*z2p/xpc(l) 
                  wpv1(l) = (-upvold*z2p/xpc(l)) + wpv1(l)*x2p/xpc(l) 
               endif 
 
               ypc(l) = ypf(nploc(l)) + vpv1(l)*dt 
!
            end do 
!
!
!
!
!
            is = nsp_dust 
            do l = 1, lvmax 
 
               if (nim) then 
 
                  adust = (xpc(l)-xpf(nploc(l)))**2 + (ypc(l)-ypf(nploc(l)))**2 
                  bdust = 2D0*((xpf(nploc(l))-xpc(l))*xpf(nploc(l))+(ypf(nploc(&
                     l))-ypc(l))*ypf(nploc(l))) 
                  cdust = xpf(nploc(l))**2 + ypf(nploc(l))**2 
 
                  testdust = bdust**2 - 4D0*adust*(cdust - rdust**2) 
 
 
 
                  if (testdust<0D0 .or. adust==0D0) then 
                     discard(l) = .FALSE. 
                  else 
                     discard(l) = .FALSE. 
                     sdust1 = (sqrt(testdust) - bdust)/(2D0*adust) 
                     sdust2 = ((-sqrt(testdust)) - bdust)/(2D0*adust) 
                     if (sdust1<=1D0 .and. sdust1>=0D0 .or. sdust2<=1D0 .and. &
                        sdust1>=0D0) discard(l) = .TRUE. 
                     if (discard(l)) then 
                        sdust = sdust1 
                        if (sdust2 > 0D0) sdust = sdust2 
                        xpc(l) = xpf(nploc(l)) + sdust*(xpc(l)-xpf(nploc(l))) 
                        ypc(l) = ypf(nploc(l)) + sdust*(ypc(l)-ypf(nploc(l))) 
                     endif 
                  endif 
 
               else 
 
                  radp = (xpc(l)-xcdust)**2 + (ypc(l)-ycdust)**2 
                  if (radp <= rdust**2) then 
                     discard(l) = .TRUE. 
                  else 
                     discard(l) = .FALSE. 
                  endif 
 
               endif 
!
               if (.not.discard(l)) cycle  
 
               upv1(l) = 0.D0 
               vpv1(l) = 0.D0 
               wpv1(l) = 0.D0 
               ico(nploc(l)) = nsp_dust 
 
 
 
            end do 
 
 
 
            do lv = 1, lvmax 
               qdustp(nh,isp(lv)) = qdustp(nh,isp(lv)) + qptmp(lv) 
            end do 
 
 
            do lv = 1, lvmax 
               dkdt(ij,isp(lv)) = dkdt(ij,isp(lv)) + 0.5*massp(lv)*(upv1(lv)**2&
                  +vpv1(lv)**2+wpv1(lv)**2-upv(lv)**2-vpv(lv)**2-wpv(lv)**2) 
            end do 
!
!
!
!     calculate the new natural coordinates of the particle
!
            call mapv (xn, yn, ij, nxp, xpc, ypc, the, zeta, 1, lvmax) 
!
!     restore updated particle positions
!
!dir$ ivdep
            do l = 1, lvmax 
               up(nploc(l)) = upv1(l) 
               vp(nploc(l)) = vpv1(l) 
               wp(nploc(l)) = wpv1(l) 
               xp(nploc(l)) = iold(l) + the(l) 
               yp(nploc(l)) = jold(l) + zeta(l) 
               xtp0(l) = xpf(nploc(l)) 
               ytp0(l) = ypf(nploc(l)) 
               xpf(nploc(l)) = xpc(l) 
               ypf(nploc(l)) = ypc(l) 
               inew(l) = iold(l) + the(l) 
               jnew(l) = jold(l) + zeta(l) 
            end do 
!
            do lv = 1, lvmax 
!
!     check first to see whether particle has changed cells
!
               if (inew(lv) - iold(lv)==0 .and. jnew(lv)-jold(lv)==0) then 
!    for those particles that have not changed cells,
!     no further tests are required
!     return particle to original cell list
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = iphd2(ij) 
                  iphd2(ij) = nploc(lv) 
!
               else 
!
                  iphead(ij) = link(nploc(lv)) 
                  link(nploc(lv)) = ipsrch 
                  ipsrch = nploc(lv) 
                  nlost = nlost + 1 
!
!     save old position in xp,yp arrays
!
                  xp(nploc(lv)) = xtp0(lv) 
                  yp(nploc(lv)) = ytp0(lv) 
!
!
!
               endif 
            end do 
            np = iphead(ij) 
            if (np > 0) go to 100 
!
         end do 
      end do 
!
!     output dust diagnostics
!
      dnorm = siep(2)**2*(1. + cyl*(siep(2)-1.)) 
      rld3 = (siep(2)/siep(1)*sqrt(qom(1)))**3 
      do is = nsp_dust, nsp 
         vexact = rex(is)**3/3. 
         emomdustxp(nh,is) = emomdustxp(nh,is)/(qdustp(nh,is)*rex(is)+1.E-10) 
         emomdustyp(nh,is) = emomdustyp(nh,is)/(qdustp(nh,is)*rex(is)+1.E-10) 
         qdustp(nh,is) = qdustp(nh,is)/dnorm*2*pi 
!     print*, "!!!!!!!!!!!!!!!!!!!!!!!qdustp=", qdustp
         fdragx(nh,is) = fdragx(nh,is)*qom(1)**2/siep(1)**4/(qdustp(nh,is)*rld3&
            +1.E-10)**2 
!     &   /siep(1)*sqrt(qom(1))*16*pi**2
         fdragy(nh,is) = fdragy(nh,is)*qom(1)**2/siep(1)**4/(qdustp(nh,is)*rld3&
            +1.E-10)**2 
!     &   /siep(1)*sqrt(qom(1))*16*pi**2
         feletx(nh,is) = feletx(nh,is)*qom(1)**2/siep(1)**4/(qdustp(nh,is)*rld3&
            +1.E-10)**2 
!     &   /siep(1)*sqrt(qom(1))*16*pi**2
         felety(nh,is) = felety(nh,is)*qom(1)**2/siep(1)**4/(qdustp(nh,is)*rld3&
            +1.E-10)**2 
!     &   /siep(1)*sqrt(qom(1))*16*pi**2
         if (mod(ncyc,10) == 0) then 
            write (*, *) 'PARMOVE- SPECIES ', is 
            write (*, *) 'time,charge,dipx,dipy  ', t, qdustp(nh,is), &
               emomdustxp(nh,is), emomdustyp(nh,is) 
            write (*, *) 'forces: drag (x,y); elect (x,y)  ', fdragx(nh,is), &
               fdragy(nh,is), feletx(nh,is), felety(nh,is) 
         endif 
         write (30 + is, 176) t, qdustp(nh,is), emomdustxp(nh,is), emomdustyp(&
            nh,is), fdragx(nh,is), fdragy(nh,is), feletx(nh,is), felety(nh,is) 
  176    format(1x,8(e12.5,2x)) 
      end do 
!
!
!     *********************************************************
!
!     reconstruct linked lists
!
!     ***********************************************
      call locator_kin (ipsrch) 
      return  
      end subroutine parmove 
