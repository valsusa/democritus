 
 
      subroutine bdycoef(nskip, k1, k2, ijbdy, x, y, xghost, yghost, cx1, cx2, &
         cx3, cx4, cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol, vvol, cyl) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nskip 
      integer , intent(in) :: k1 
      integer , intent(in) :: k2 
      real(double) , intent(in) :: cyl 
      integer , intent(in) :: ijbdy(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: xghost(*) 
      real(double) , intent(in) :: yghost(*) 
      real(double) , intent(out) :: cx1(*) 
      real(double) , intent(out) :: cx2(*) 
      real(double) , intent(out) :: cx3(*) 
      real(double) , intent(out) :: cx4(*) 
      real(double) , intent(out) :: cy1(*) 
      real(double) , intent(out) :: cy2(*) 
      real(double) , intent(out) :: cy3(*) 
      real(double) , intent(out) :: cy4(*) 
      real(double) , intent(out) :: cr1(*) 
      real(double) , intent(out) :: cr2(*) 
      real(double) , intent(out) :: cr3(*) 
      real(double) , intent(out) :: cr4(*) 
      real(double) , intent(out) :: vol(*) 
      real(double) , intent(inout) :: vvol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: k 
      real(double) :: r36, x1, y1, r1, x2, y2, r2, x3, y3, r3, x4, y4, r4, a1, &
         a2, a3, a4, r1b, r2b, r3b, r4b, vol1, vol2, vol3, vol4 
!-----------------------------------------------
!
      r36 = 1./36. 
      do k = k1, k2 
!
         x1 = xghost(k+1) 
         y1 = yghost(k+1) 
         r1 = abs(x1)*cyl + 1. - cyl 
!
         x2 = x(ijbdy(k+1)) 
         y2 = y(ijbdy(k+1)) 
         r2 = abs(x2)*cyl + 1. - cyl 
!
         x3 = x(ijbdy(k)) 
         y3 = y(ijbdy(k)) 
         r3 = abs(x3)*cyl + 1. - cyl 
!
         x4 = xghost(k) 
         y4 = yghost(k) 
         r4 = abs(x4)*cyl + 1. - cyl 
!
         r1 = abs(x1)*cyl + 1. - cyl 
         r2 = abs(x2)*cyl + 1. - cyl 
         r3 = abs(x3)*cyl + 1. - cyl 
         r4 = abs(x4)*cyl + 1. - cyl 
!
!
         a1 = (x2 - x1)*(y4 - y1) - (y2 - y1)*(x4 - x1) 
         a2 = (x3 - x2)*(y1 - y2) - (y3 - y2)*(x1 - x2) 
         a3 = (x4 - x3)*(y2 - y3) - (y4 - y3)*(x2 - x3) 
         a4 = (x1 - x4)*(y3 - y4) - (y1 - y4)*(x3 - x4) 
!
         r1b = 4.*r1 + 2.*r2 + r3 + 2.*r4 
         r2b = 2.*r1 + 4.*r2 + 2.*r3 + r4 
         r3b = r1 + 2.*r2 + 4.*r3 + 2.*r4 
         r4b = 2.*r1 + r2 + 2.*r3 + 4.*r4 
!
         vol1 = r1b*a1*r36 
         vol2 = r2b*a2*r36 
         vol3 = r3b*a3*r36 
         vol4 = r4b*a4*r36 
!
         cx1(ijbdy(k)+nskip) = (r1b*(y2 - y4) + r2b*(y2 - y3) + r4b*(y3 - y4))*&
            r36 
         cx2(ijbdy(k)+nskip) = (r1b*(y4 - y1) + r2b*(y3 - y1) + r3b*(y3 - y4))*&
            r36 
         cx3(ijbdy(k)+nskip) = (r2b*(y1 - y2) + r3b*(y4 - y2) + r4b*(y4 - y1))*&
            r36 
         cx4(ijbdy(k)+nskip) = (r1b*(y1 - y2) + r3b*(y2 - y3) + r4b*(y1 - y3))*&
            r36 
!
         cr1(ijbdy(k)+nskip) = (4.*a1 + 2.*a2 + a3 + 2.*a4)*r36*cyl 
         cr2(ijbdy(k)+nskip) = (2.*a1 + 4.*a2 + 2.*a3 + a4)*r36*cyl 
         cr3(ijbdy(k)+nskip) = (a1 + 2.*a2 + 4.*a3 + 2.*a4)*r36*cyl 
         cr4(ijbdy(k)+nskip) = (2.*a1 + a2 + 2.*a3 + 4.*a4)*r36*cyl 
!
         cy1(ijbdy(k)+nskip) = (r1b*(x4 - x2) + r2b*(x3 - x2) + r4b*(x4 - x3))*&
            r36 
         cy2(ijbdy(k)+nskip) = (r1b*(x1 - x4) + r2b*(x1 - x3) + r3b*(x4 - x3))*&
            r36 
         cy3(ijbdy(k)+nskip) = (r2b*(x2 - x1) + r3b*(x2 - x4) + r4b*(x1 - x4))*&
            r36 
         cy4(ijbdy(k)+nskip) = (r1b*(x2 - x1) + r3b*(x3 - x2) + r4b*(x3 - x1))*&
            r36 
!
         vol(ijbdy(k)+nskip) = vol1 + vol2 + vol3 + vol4 
!
         vvol(ijbdy(k)) = vvol(ijbdy(k)) + vol2 
         vvol(ijbdy(k+1)) = vvol(ijbdy(k+1)) + vol3 
!
      end do 
!
      return  
      end subroutine bdycoef 


      subroutine geocoef(nqi, nqj, i1, i2, j1, j2, cyl, x, y, cx1, cx2, cx3, &
         cx4, cr1, cr2, cr3, cr4, cy1, cy2, cy3, cy4, voll, vol, vvol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: cyl 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(out) :: cx1(*) 
      real(double) , intent(out) :: cx2(*) 
      real(double) , intent(out) :: cx3(*) 
      real(double) , intent(out) :: cx4(*) 
      real(double) , intent(out) :: cr1(*) 
      real(double) , intent(out) :: cr2(*) 
      real(double) , intent(out) :: cr3(*) 
      real(double) , intent(out) :: cr4(*) 
      real(double) , intent(out) :: cy1(*) 
      real(double) , intent(out) :: cy2(*) 
      real(double) , intent(out) :: cy3(*) 
      real(double) , intent(out) :: cy4(*) 
      real(double) , intent(inout) :: voll(*) 
      real(double) , intent(out) :: vol(*) 
      real(double) , intent(inout) :: vvol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ijp, ipjp 
      real(double) :: r36, x1, y1, x2, y2, x3, y3, x4, y4, r1, r2, r3, r4, a1, &
         a2, a3, a4, r1b, r2b, r3b, r4b 
!-----------------------------------------------
!
      r36 = 1./36. 
!
      do j = j1, j2 + 1 
         do i = i1, i2 + 1 
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            vvol(ij) = 0. 
         end do 
      end do 
!
      do j = j1, j2 
         do i = i1, i2 
 
 
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            ipj = ij + nqi 
            ijp = ij + nqj 
            ipjp = ijp + nqi 
!
            x1 = x(ipj) 
            y1 = y(ipj) 
            x2 = x(ipjp) 
            y2 = y(ipjp) 
            x3 = x(ijp) 
            y3 = y(ijp) 
            x4 = x(ij) 
            y4 = y(ij) 
            r1 = abs(x(ipj))*cyl + 1. - cyl 
            r2 = abs(x(ipjp))*cyl + 1. - cyl 
            r3 = abs(x(ijp))*cyl + 1. - cyl 
            r4 = abs(x(ij))*cyl + 1. - cyl 
!
!
            a1 = (x2 - x1)*(y4 - y1) - (y2 - y1)*(x4 - x1) 
            a2 = (x3 - x2)*(y1 - y2) - (y3 - y2)*(x1 - x2) 
            a3 = (x4 - x3)*(y2 - y3) - (y4 - y3)*(x2 - x3) 
            a4 = (x1 - x4)*(y3 - y4) - (y1 - y4)*(x3 - x4) 
!
            r1b = 4.*r1 + 2.*r2 + r3 + 2.*r4 
            r2b = 2.*r1 + 4.*r2 + 2.*r3 + r4 
            r3b = r1 + 2.*r2 + 4.*r3 + 2.*r4 
            r4b = 2.*r1 + r2 + 2.*r3 + 4.*r4 
!
            voll(1) = r1b*a1*r36 
            voll(2) = r2b*a2*r36 
            voll(3) = r3b*a3*r36 
            voll(4) = r4b*a4*r36 
!
            cx1(ij) = (r1b*(y2 - y4) + r2b*(y2 - y3) + r4b*(y3 - y4))*r36 
            cx2(ij) = (r1b*(y4 - y1) + r2b*(y3 - y1) + r3b*(y3 - y4))*r36 
            cx3(ij) = (r2b*(y1 - y2) + r3b*(y4 - y2) + r4b*(y4 - y1))*r36 
            cx4(ij) = (r1b*(y1 - y2) + r3b*(y2 - y3) + r4b*(y1 - y3))*r36 
!
            cr1(ij) = (4.*a1 + 2.*a2 + a3 + 2.*a4)*r36*cyl 
            cr2(ij) = (2.*a1 + 4.*a2 + 2.*a3 + a4)*r36*cyl 
            cr3(ij) = (a1 + 2.*a2 + 4.*a3 + 2.*a4)*r36*cyl 
            cr4(ij) = (2.*a1 + a2 + 2.*a3 + 4.*a4)*r36*cyl 
!
            cy1(ij) = (r1b*(x4 - x2) + r2b*(x3 - x2) + r4b*(x4 - x3))*r36 
            cy2(ij) = (r1b*(x1 - x4) + r2b*(x1 - x3) + r3b*(x4 - x3))*r36 
            cy3(ij) = (r2b*(x2 - x1) + r3b*(x2 - x4) + r4b*(x1 - x4))*r36 
            cy4(ij) = (r1b*(x2 - x1) + r3b*(x3 - x2) + r4b*(x3 - x1))*r36 
!
            vol(ij) = voll(1) + voll(2) + voll(3) + voll(4) 
!
            vvol(ij) = vvol(ij) + voll(4) 
            vvol(ipj) = vvol(ipj) + voll(1) 
            vvol(ipjp) = vvol(ipjp) + voll(2) 
            vvol(ijp) = vvol(ijp) + voll(3) 
!
         end do 
      end do 
!
      return  
      end subroutine geocoef 


      subroutine ghostpts(nskip, k1, k2, x, y, ijbdy, xnorm, ynorm, xghost, &
         yghost) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nskip 
      integer , intent(in) :: k1 
      integer , intent(in) :: k2 
      integer , intent(in) :: ijbdy(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: xnorm(*) 
      real(double) , intent(in) :: ynorm(*) 
      real(double) , intent(out) :: xghost(*) 
      real(double) , intent(out) :: yghost(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: k 
      real(double) :: stretch, xdotn 
!-----------------------------------------------
!
      xghost(k1) = x(ijbdy(k1)) 
      yghost(k1) = y(ijbdy(k1)) 
!
      xghost(k2) = x(ijbdy(k2)) 
      yghost(k2) = y(ijbdy(k2)) 
!
      stretch = 0.99999 
!
      do k = k1 + 1, k2 - 1 
!
         xdotn = xnorm(k)*(x(ijbdy(k))-x(ijbdy(k)+nskip)) + ynorm(k)*(y(ijbdy(k&
            ))-y(ijbdy(k)+nskip)) 
!
         xghost(k) = xnorm(k)*(1. + stretch)*xdotn + x(ijbdy(k)+nskip) 
         yghost(k) = ynorm(k)*(1. + stretch)*xdotn + y(ijbdy(k)+nskip) 
!
      end do 
!
      return  
      end subroutine ghostpts 
