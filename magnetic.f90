      subroutine magnetic(i1, i2, j1, j2, nqi, nqj, x, cyl, rmaj, bx0, by0, bz0&
         , bx, by, bz) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      real(double)  :: cyl 
      real(double)  :: rmaj 
      real(double) , intent(in) :: bx0 
      real(double) , intent(in) :: by0 
      real(double) , intent(in) :: bz0 
      real(double)  :: x(*) 
      real(double) , intent(out) :: bx(*) 
      real(double) , intent(out) :: by(*) 
      real(double) , intent(out) :: bz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij 
!-----------------------------------------------
!     a routine to calculate the magnetic field
!
      do j = j1, j2 
         do i = i1, i2 
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            bx(ij) = bx0 
            by(ij) = by0 
            bz(ij) = bz0 
!*((1.-cyl)+cyl*rmaj/(x(ij)+1.e-20))
         end do 
      end do 
!
      return  
      end subroutine magnetic 
