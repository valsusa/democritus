      subroutine matmul9(e, d, c, b, a, bu, cu, du, eu, nh, nv, x, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nh 
      integer , intent(in) :: nv 
      real(double) , intent(in) :: e(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: bu(*) 
      real(double) , intent(in) :: cu(*) 
      real(double) , intent(in) :: du(*) 
      real(double) , intent(in) :: eu(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(inout) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, nhm1, nhp1, i, l, k, m 
!-----------------------------------------------
!*** FIRST EXECUTABLE STATEMENT   MATMUL9
      n = nh*nv 
      nhm1 = nh - 1 
      nhp1 = nh + 1 
      i = 1 
      y(i) = a(i)*x(i) + bu(i)*x(i+1) + du(i)*x(i+nh) + eu(i)*x(i+nhp1) 
      do i = 2, nh 
         y(i) = b(i)*x(i-1) + (a(i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(du(i)*&
            x(i+nh)+eu(i)*x(i+nhp1))))) 
      end do 
      i = nhp1 
      y(i) = d(i)*x(i-nh) + c(i)*x(i-nhm1) + b(i)*x(i-1) + a(i)*x(i) + bu(i)*x(&
         i+1) + cu(i)*x(i+nhm1) + du(i)*x(i+nh) + eu(i)*x(i+nhp1) 
      do i = nh + 2, n - nhp1 
         y(i) = e(i)*x(i-nhp1) + (d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(i-1)+(a(&
            i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(du(i)*x(i+nh)+eu(i)*x(i+&
            nhp1)))))))) 
      end do 
      i = n - nh 
      y(i) = e(i)*x(i-nhp1) + d(i)*x(i-nh) + c(i)*x(i-nhm1) + b(i)*x(i-1) + a(i&
         )*x(i) + bu(i)*x(i+1) + cu(i)*x(i+nh-1) + du(i)*x(i+nh) 
      do i = n - nhm1, n - 1 
         y(i) = e(i)*x(i-nhp1) + (d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(i-1)+(a(&
            i)*x(i)+bu(i)*x(i+1))))) 
      end do 
      i = n 
      y(i) = e(i)*x(i-nhp1) + d(i)*x(i-nh) + b(i)*x(i-1) + a(i)*x(i) 
!
!cc periodic equations
!
      l = n - nh 
      k = l - 1 
      m = l + 1 
      i = 1 
      y(i) = y(i) + du(i+l)*x(i+l) + cu(i+m)*x(i+m) 
      y(i+l) = y(i+l) + du(i+l)*x(i) + eu(i+l)*x(i+1) 
      do i = 2, nhm1 
         y(i) = y(i) + eu(i+k)*x(i+k) + du(i+l)*x(i+l) + cu(i+m)*x(i+m) 
         y(i+l) = y(i+l) + cu(i+l)*x(i-1) + du(i+l)*x(i) + eu(i+l)*x(i+1) 
      end do 
      i = nh 
      y(i) = y(i) + eu(i+k)*x(i+k) + du(i+l)*x(i+l) 
      y(i+l) = y(i+l) + cu(i+l)*x(i-1) + du(i+l)*x(i) 
      return  
      end subroutine matmul9 


      subroutine matmul9u(e, d, c, b, a, bu, cu, du, eu, nh, nv, x, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nh 
      integer , intent(in) :: nv 
      real(double) , intent(in) :: e(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: bu(*) 
      real(double) , intent(in) :: cu(*) 
      real(double) , intent(in) :: du(*) 
      real(double) , intent(in) :: eu(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(inout) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, nhm1, nhp1, i, l, k, m 
!-----------------------------------------------
!*** FIRST EXECUTABLE STATEMENT   MATMUL9
!
      n = nh*nv 
      nhm1 = nh - 1 
      nhp1 = nh + 1 
      i = 1 
      y(i) = a(i)*x(i) + bu(i)*x(i+1) + du(i)*x(i+nh) + eu(i)*x(i+nhp1) 
      do i = 2, nh 
         y(i) = b(i)*x(i-1) + (a(i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(du(i)*&
            x(i+nh)+eu(i)*x(i+nhp1))))) 
      end do 
      i = nhp1 
      y(i) = d(i)*x(i-nh) + c(i)*x(i-nhm1) + b(i)*x(i-1) + a(i)*x(i) + bu(i)*x(&
         i+1) + cu(i)*x(i+nhm1) + du(i)*x(i+nh) + eu(i)*x(i+nhp1) 
      do i = nh + 2, n - nhp1 
         y(i) = e(i)*x(i-nhp1) + (d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(i-1)+(a(&
            i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(du(i)*x(i+nh)+eu(i)*x(i+&
            nhp1)))))))) 
      end do 
      i = n - nh 
      y(i) = e(i)*x(i-nhp1) + d(i)*x(i-nh) + c(i)*x(i-nhm1) + b(i)*x(i-1) + a(i&
         )*x(i) + bu(i)*x(i+1) + cu(i)*x(i+nh-1) + du(i)*x(i+nh) 
      do i = n - nhm1, n - 1 
         y(i) = e(i)*x(i-nhp1) + (d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(i-1)+(a(&
            i)*x(i)+bu(i)*x(i+1))))) 
      end do 
      i = n 
      y(i) = e(i)*x(i-nhp1) + d(i)*x(i-nh) + b(i)*x(i-1) + a(i)*x(i) 
!
!cc periodic equations
!
      l = n - nh 
      k = l - 1 
      m = l + 1 
      i = 1 
      y(i) = y(i) + d(i)*x(i+l) + c(i)*x(i+m) 
      y(i+l) = y(i+l) + du(i+l)*x(i) + eu(i+l)*x(i+1) 
      do i = 2, nhm1 
         y(i) = y(i) + e(i)*x(i+k) + d(i)*x(i+l) + c(i)*x(i+m) 
         y(i+l) = y(i+l) + cu(i+l)*x(i-1) + du(i+l)*x(i) + eu(i+l)*x(i+1) 
      end do 
      i = nh 
      y(i) = y(i) + e(i)*x(i+k) + d(i)*x(i+l) 
      y(i+l) = y(i+l) + cu(i+l)*x(i-1) + du(i+l)*x(i) 
!
      return  
      end subroutine matmul9u 


      subroutine cgitj(a, b, c, d, e, af, bf, cf, df, ef, n, m, x, y, r, up, &
         aux, istop, ercg, xratio, ratio, xnorm, ynorm) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: n 
      integer  :: m 
      integer , intent(inout) :: istop 
      real(double) , intent(in) :: ercg 
      real(double) , intent(inout) :: xratio 
      real(double) , intent(inout) :: ratio 
      real(double) , intent(out) :: xnorm 
      real(double) , intent(out) :: ynorm 
      real(double)  :: a(*) 
      real(double)  :: b(*) 
      real(double)  :: c(*) 
      real(double)  :: d(*) 
      real(double)  :: e(*) 
      real(double)  :: af(*) 
      real(double)  :: bf(*) 
      real(double)  :: cf(*) 
      real(double)  :: df(*) 
      real(double)  :: ef(*) 
      real(double)  :: x(*) 
      real(double)  :: y(*) 
      real(double)  :: r(*) 
      real(double)  :: up(*) 
      real(double)  :: aux(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: mn, i, i4, i8, i9, i11 
      real(double) :: e3, rdot, alpha, rnorm, dxnorm, beta 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: sasum, sdot 
!-----------------------------------------------
      mn = m*n 
      e3 = ercg 
!
!cc copy A matrix transposed into factor matrix
!
      do i = 1, n + 1 
         ef(i) = 0. 
         df(i) = 0. 
         cf(i) = 0. 
      end do 
      do i = 1, mn - n - 1 
         ef(i+n+1) = e(i) 
         cf(i+n) = c(i+1) 
      end do 
      do i = 1, mn - n 
         df(i+n) = d(i) 
      end do 
      do i = 1, mn - 1 
         bf(i+1) = b(i) 
      end do 
      do i = 1, mn 
         af(i) = a(i) 
      end do 
!
!cc periodic block copy
!
      do i = 2, n 
         ef(i) = e(mn-n+i-1) 
      end do 
      do i = 1, n 
         df(i) = d(mn-n+i) 
      end do 
      do i = 1, n - 1 
         cf(i) = c(mn-n+i+1) 
      end do 
      call ldlt9f (n, m, ef, df, cf, bf, af) 
!
! initialize the residual r
!
      call matmul9 (e((-n)), d(1-n), c(2-n), b(0), a, b, c, d, e, n, m, x, r) 
      do i = 1, mn 
         r(i) = y(i) - r(i) 
      end do 
      if (sasum(mn,r,1) == 0.) then 
         i = 0 
         go to 200 
      endif 
      ynorm = sasum(mn,y,1) 
!
!cc solve l * d * l(trans)*aux = r0
!
      call ldlt (n, m, ef, df, cf, bf, af, bf(2), cf(n), df(n+1), ef(n+2), up, &
         r) 
!
!cc dot r with aux
!
      rdot = sdot(mn,r,1,up,1) 
!
! put up into aux (p0)
!
      do i4 = 1, mn 
         aux(i4) = up(i4) 
      end do 
! p is stored in aux and Mp or (ldlt)-1(r) are in up.
!
!cc begin main loop
!
      do i = 1, istop 
!
!---- Find m * p and store it in up
!
         call matmul9 (e((-n)), d(1-n), c(2-n), b(0), a, b, c, d, e, n, m, aux&
            , up) 
!
!cc compute alpha(i)
!
         alpha = sdot(mn,up,1,aux,1) 
         alpha = rdot/alpha 
!
!
!cc compute x(i+1)
!
         do i8 = 1, mn 
            x(i8) = x(i8) + alpha*aux(i8) 
         end do 
!
!cc compute r(i+1)
!
         do i9 = 1, mn 
            r(i9) = r(i9) - alpha*up(i9) 
         end do 
!
!cc termination test
!cc
!cc stop whenever norm(aux) / norm (x) .lt. e3
!
         rnorm = sasum(mn,r,1) 
         dxnorm = sasum(mn,aux,1)*abs(alpha) 
         xnorm = sasum(mn,x,1) 
!***
!cc output ratio
!
         ratio = rnorm/ynorm 
         xratio = dxnorm/xnorm 
!
!***
         if (xratio<=e3 .and. ratio<=e3) go to 200 
!
!cc compute beta(i)
!
         call ldlt (n, m, ef, df, cf, bf, af, bf(2), cf(n), df(n+1), ef(n+2), &
            up, r) 
         alpha = sdot(mn,r,1,up,1) 
         beta = alpha/rdot 
         rdot = alpha 
! get new p (stored in aux)
         do i11 = 1, mn 
            aux(i11) = beta*aux(i11) + up(i11) 
         end do 
      end do 
      write (59, 102) xratio, ratio 
      write (6, 102) xratio, ratio 
  102 format('0','cg terminates because of too many iterations. ',/,'xratio = '&
         ,e12.4,'ratio = ',e12.4) 
  200 continue 
      istop = i 
      return  
      end subroutine cgitj 


      subroutine ldlt(n, m, e, d, c, b, a, bu, cu, du, eu, x, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: m 
      real(double) , intent(in) :: e(n,m) 
      real(double) , intent(in) :: d(n,m) 
      real(double) , intent(in) :: c(n,m) 
      real(double) , intent(in) :: b(n,m) 
      real(double) , intent(in) :: a(n,m) 
      real(double) , intent(in) :: bu(n,m) 
      real(double) , intent(in) :: cu(n,m) 
      real(double) , intent(in) :: du(n,m) 
      real(double) , intent(in) :: eu(n,m) 
      real(double) , intent(inout) :: x(n,m) 
      real(double) , intent(in) :: y(n,m) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j 
!-----------------------------------------------
!*** FIRST EXECUTABLE STATEMENT   LDLT
!
!cc   copy y into x
!
      do i = 1, n 
         do j = 1, m 
            x(i,j) = y(i,j) 
         end do 
      end do 
!
!cc   forward solve
!
      do j = 1, m 
         if (j /= 1) then 
            do i = 1, n - 1 
               x(i,j) = x(i,j) - c(i,j)*x(i+1,j-1) 
            end do 
            do i = 1, n 
               x(i,j) = x(i,j) - d(i,j)*x(i,j-1) 
            end do 
            do i = 2, n 
               x(i,j) = x(i,j) - e(i,j)*x(i-1,j-1) 
            end do 
!
!cc vertical periodic equations
!
            if (j == m) then 
               do i = 1, n - 1 
                  x(i,m) = x(i,m) - c(i,1)*x(i+1,1) 
               end do 
               do i = 1, n 
                  x(i,m) = x(i,m) - d(i,1)*x(i,1) 
               end do 
               do i = 2, n 
                  x(i,m) = x(i,m) - e(i,1)*x(i-1,1) 
               end do 
            endif 
         endif 
!
!cc   van der worst approximation
!
!dir$ ivdep
         do i = n, 2, -1 
            x(i,j) = x(i,j) - b(i,j)*x(i-1,j) 
         end do 
!dir$ ivdep
         do i = n, 3, -1 
            x(i,j) = x(i,j) + b(i-1,j)*b(i,j)*x(i-2,j) 
         end do 
      end do 
!
!cc   diagonal solve
!
      do i = 1, n 
         do j = 1, m 
            x(i,j) = a(i,j)*x(i,j) 
         end do 
      end do 
!
!cc  backward solve
!
      do j = m, 1, -1 
         if (j /= m) then 
            do i = 2, n 
               x(i,j) = x(i,j) - cu(i,j)*x(i-1,j+1) 
            end do 
            do i = 1, n 
               x(i,j) = x(i,j) - du(i,j)*x(i,j+1) 
            end do 
            do i = 1, n - 1 
               x(i,j) = x(i,j) - eu(i,j)*x(i+1,j+1) 
            end do 
!
!cc vertical periodic equations
!
            if (j == 1) then 
               do i = 2, n 
                  x(i,1) = x(i,1) - c(i-1,1)*x(i-1,m) 
               end do 
               do i = 1, n 
                  x(i,1) = x(i,1) - d(i,1)*x(i,m) 
               end do 
               do i = 1, n - 1 
                  x(i,1) = x(i,1) - e(i+1,1)*x(i+1,m) 
               end do 
            endif 
         endif 
!
!cc van der worst approximation
!
!dir$ ivdep
         do i = 1, n - 1 
            x(i,j) = x(i,j) - bu(i,j)*x(i+1,j) 
         end do 
!dir$ ivdep
         do i = 1, n - 2 
            x(i,j) = x(i,j) + bu(i+1,j)*bu(i,j)*x(i+2,j) 
         end do 
      end do 
      return  
      end subroutine ldlt 


      subroutine ldlt9f(n, m, e, d, c, b, a) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: m 
      real(double) , intent(inout) :: e(n,m) 
      real(double) , intent(inout) :: d(n,m) 
      real(double) , intent(inout) :: c(n,m) 
      real(double) , intent(inout) :: b(n,m) 
      real(double) , intent(inout) :: a(n,m) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i 
!-----------------------------------------------
!
!cc   form a = l*v
!
      do j = 1, m 
!
!cc factor central block
!
         a(1,j) = 1./a(1,j) 
         do i = 2, n 
            a(i,j) = 1./(a(i,j)-b(i,j)*(a(i-1,j)*b(i,j))) 
         end do 
         if (j /= m) then 
!
!cc   form lower diagonals
!
            do i = 2, n 
               d(i,j+1) = d(i,j+1) - (a(i-1,j)*e(i,j+1))*b(i,j) 
            end do 
            do i = 1, n - 1 
               c(i,j+1) = c(i,j+1) - (a(i,j)*d(i,j+1))*b(i+1,j) 
            end do 
!
!cc factor lower periodic diagonals
!
            if (j == 1) then 
               do i = 2, n 
                  d(i,1) = d(i,1) - (a(i-1,1)*e(i,1))*b(i,1) 
               end do 
               do i = 1, n - 1 
                  c(i,1) = c(i,1) - (a(i,1)*d(i,1))*b(i+1,1) 
               end do 
            endif 
!
!cc   form central diagonals
!
            do i = 2, n 
               b(i,j+1) = b(i,j+1) - (a(i-1,j)*e(i,j+1))*d(i-1,j+1) - (a(i,j)*d&
                  (i,j+1))*c(i-1,j+1) 
            end do 
            a(1,j+1) = a(1,j+1) - (a(1,j)*d(1,j+1))*d(1,j+1) - (a(2,j)*c(1,j+1)&
               )*c(1,j+1) 
            do i = 2, n - 1 
               a(i,j+1) = a(i,j+1) - (a(i-1,j)*e(i,j+1))*e(i,j+1) - (a(i,j)*d(i&
                  ,j+1))*d(i,j+1) - (a(i+1,j)*c(i,j+1))*c(i,j+1) 
            end do 
            a(n,j+1) = a(n,j+1) - (a(n-1,j)*e(n,j+1))*e(n,j+1) - (a(n,j)*d(n,j+&
               1))*d(n,j+1) 
!
!cc factor central periodic block
!
            if (j == 1) then 
               do i = 2, n 
                  b(i,m) = b(i,m) - (a(i-1,1)*e(i,1))*d(i-1,1) - (a(i,1)*d(i,1)&
                     )*c(i-1,1) 
               end do 
               a(1,m) = a(1,m) - (a(1,1)*d(1,1))*d(1,1) - (a(2,1)*c(1,1))*c(1,1&
                  ) 
               do i = 2, n - 1 
                  a(i,m) = a(i,m) - (a(i-1,1)*e(i,1))*e(i,1) - (a(i,1)*d(i,1))*&
                     d(i,1) - (a(i+1,1)*c(i,1))*c(i,1) 
               end do 
               a(n,m) = a(n,m) - (a(n-1,1)*e(n,1))*e(n,1) - (a(n,1)*d(n,1))*d(n&
                  ,1) 
            endif 
!
!cc scale diagonals
!
            do i = 2, n 
               e(i,j+1) = a(i-1,j)*e(i,j+1) 
            end do 
            do i = 1, n 
               d(i,j+1) = a(i,j)*d(i,j+1) 
            end do 
            do i = 1, n - 1 
               c(i,j+1) = a(i+1,j)*c(i,j+1) 
            end do 
!
!cc scale periodic block
!
            if (j == 1) then 
               do i = 2, n 
                  e(i,1) = a(i-1,1)*e(i,1) 
               end do 
               do i = 1, n 
                  d(i,1) = a(i,1)*d(i,1) 
               end do 
               do i = 1, n - 1 
                  c(i,1) = a(i+1,1)*c(i,1) 
               end do 
            endif 
         endif 
         do i = 2, n 
            b(i,j) = a(i-1,j)*b(i,j) 
         end do 
      end do 
      return  
      end subroutine ldlt9f 


      subroutine ilucgj(e, d, c, b, a, bu, cu, du, eu, el, dl, cl, bl, af, bf, &
         cf, df, ef, n, m, x, y, r, up, aux, istop, ercg, xratio, ratio, xnorm&
         , ynorm) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: n 
      integer  :: m 
      integer , intent(inout) :: istop 
      real(double) , intent(in) :: ercg 
      real(double) , intent(inout) :: xratio 
      real(double) , intent(inout) :: ratio 
      real(double) , intent(out) :: xnorm 
      real(double) , intent(out) :: ynorm 
      real(double)  :: e(*) 
      real(double)  :: d(*) 
      real(double)  :: c(*) 
      real(double)  :: b(*) 
      real(double)  :: a(*) 
      real(double)  :: bu(*) 
      real(double)  :: cu(*) 
      real(double)  :: du(*) 
      real(double)  :: eu(*) 
      real(double)  :: el(*) 
      real(double)  :: dl(*) 
      real(double)  :: cl(*) 
      real(double)  :: bl(*) 
      real(double)  :: af(*) 
      real(double)  :: bf(*) 
      real(double)  :: cf(*) 
      real(double)  :: df(*) 
      real(double)  :: ef(*) 
      real(double)  :: x(*) 
      real(double)  :: y(*) 
      real(double)  :: r(*) 
      real(double)  :: up(*) 
      real(double)  :: aux(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: mn, i, i100, i7, i8 
      real(double) :: e3, rdot, alpha, rnorm, dxnorm, beta 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: snrm2, sdot 
!-----------------------------------------------
      mn = m*n 
      e3 = ercg 
!
!cc copy A into factor storage B
!
      do i = 1, mn 
         el(i) = e(i) 
         dl(i) = d(i) 
         cl(i) = c(i) 
         bl(i) = b(i) 
         af(i) = a(i) 
         bf(i) = bu(i) 
         cf(i) = cu(i) 
         df(i) = du(i) 
         ef(i) = eu(i) 
      end do 
!
!cc factor B into incomplete LDU
!
      call lu9p (n, m, el, dl, cl, bl, af, bf, cf, df, ef) 
! initialize the residual r
      ynorm = snrm2(mn,y,1) 
!
!cc compute r0
!
      call matmul9u (e(1), d(1), c(1), b(1), a(1), bu(1), cu(1), du(1), eu(1), &
         n, m, x, r) 
      do i = 1, mn 
         r(i) = y(i) - r(i) 
      end do 
      if (snrm2(mn,r,1) == 0.) return  
!
!cc solve l * l(trans)*aux = r0
!
      call slve9 (n, m, el(1), dl(1), cl(1), bl(1), af(1), bl(2), cl(n), dl(n+1&
         ), el(n+2), aux, r) 
!
!cc dot r with ldlt[-1]r
!
      rdot = sdot(mn,r,1,aux,1) 
!
!cc compute q(0)
!
      call rypaxu (eu((-n)), du(1-n), cu(2-n), bu(0), a(1), b(2), c(n), d(n+1)&
         , e(n+2), n, m, aux, up, 0.) 
!
!cc compute p0
!
      call slve9 (n, m, ef((-n)), df(1-n), cf(2-n), bf(0), af(1), bf(1), cf(1)&
         , df(1), ef(1), aux, up) 
!
!cc begin main loop
!
      do i100 = 1, istop 
!
!cc compute alpha
!
         alpha = sdot(mn,up,1,aux,1) 
         alpha = rdot/alpha 
!
!cc multiply aux by alpha
!
         do i7 = 1, mn 
            aux(i7) = alpha*aux(i7) 
         end do 
!
!cc compute new x
!
         do i8 = 1, mn 
            x(i8) = x(i8) + aux(i8) 
         end do 
!
!cc r = r-Ax
!
         call ymax9pu (e(1), d(1), c(1), b(1), a(1), bu(1), cu(1), du(1), eu(1)&
            , n, m, aux, r) 
!
!cc termination test
!cc
!cc stop whenever norm(aux) / norm (x) .lt. e3
!
         rnorm = snrm2(mn,r,1) 
         dxnorm = snrm2(mn,aux,1) 
         xnorm = snrm2(mn,x,1) 
!***
!cc output ratio
!
         ratio = rnorm/ynorm 
         xratio = dxnorm/xnorm 
!
!***
         if (xratio<=e3 .and. ratio<=e3) go to 200 
!
!cc compute beta
!
         call slve9 (n, m, el(1), dl(1), cl(1), bl(1), af(1), bl(2), cl(n), dl(&
            n+1), el(n+2), aux, r) 
         alpha = sdot(mn,r,1,aux,1) 
         beta = alpha/rdot 
         rdot = alpha 
!
!cc compute q
!
         call rypaxu (eu((-n)), du(1-n), cu(2-n), bu(0), a(1), b(2), c(n), d(n+&
            1), e(n+2), n, m, aux, up, beta) 
!
!cc compute p
!
         call slve9 (n, m, ef((-n)), df(1-n), cf(2-n), bf(0), af(1), bf(1), cf(&
            1), df(1), ef(1), aux, up) 
      end do 
      write (59, 102) xratio, ratio 
      write (6, 102) xratio, ratio 
  102 format('0','cg terminates because of too many iterations. ',/,'xratio = '&
         ,e12.4,'ratio = ',e12.4) 
  200 continue 
      istop = i100 
      return  
      end subroutine ilucgj 


      subroutine slve9(n, m, e, d, c, b, a, bu, cu, du, eu, x, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: m 
      real(double) , intent(in) :: e(n,m) 
      real(double) , intent(in) :: d(n,m) 
      real(double) , intent(in) :: c(n,m) 
      real(double) , intent(in) :: b(n,m) 
      real(double) , intent(in) :: a(n,m) 
      real(double) , intent(in) :: bu(n,m) 
      real(double) , intent(in) :: cu(n,m) 
      real(double) , intent(in) :: du(n,m) 
      real(double) , intent(in) :: eu(n,m) 
      real(double) , intent(inout) :: x(n,m) 
      real(double) , intent(in) :: y(n,m) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j 
!-----------------------------------------------
!
!cc   copy y into x
!
      do i = 1, n 
         do j = 1, m 
            x(i,j) = y(i,j) 
         end do 
      end do 
!
!cc   forward solve
!
      do j = 1, m 
         if (j /= 1) then 
            x(1,j) = x(1,j) - c(1,j)*x(2,j-1) - d(1,j)*x(1,j-1) 
!dir$ ivdep
            do i = 2, n - 1 
               x(i,j) = x(i,j) - c(i,j)*x(i+1,j-1) - d(i,j)*x(i,j-1) - e(i,j)*x&
                  (i-1,j-1) 
            end do 
            x(n,j) = x(n,j) - d(n,j)*x(n,j-1) - e(n,j)*x(n-1,j-1) 
         endif 
!
!cc   van der worst approximation
!
!dir$ ivdep
         do i = n, 2, -1 
            x(i,j) = x(i,j) - b(i,j)*x(i-1,j) 
         end do 
!dir$ ivdep
         do i = n, 3, -1 
            x(i,j) = x(i,j) + b(i-1,j)*b(i,j)*x(i-2,j) 
         end do 
      end do 
!
!cc diagonal solve
!
      do i = 1, n 
         do j = 1, m 
            x(i,j) = a(i,j)*x(i,j) 
         end do 
      end do 
!
!cc  backward solve
!
      do j = m, 1, -1 
         if (j /= m) then 
            x(n,j) = x(n,j) - cu(n,j)*x(n-1,j+1) - du(n,j)*x(n,j+1) 
!dir$ ivdep
            do i = 2, n - 1 
               x(i,j) = x(i,j) - cu(i,j)*x(i-1,j+1) - du(i,j)*x(i,j+1) - eu(i,j&
                  )*x(i+1,j+1) 
            end do 
            x(1,j) = x(1,j) - du(1,j)*x(1,j+1) - eu(1,j)*x(2,j+1) 
         endif 
!
!cc van der worst approximation
!
!dir$ ivdep
         do i = 1, n - 1 
            x(i,j) = x(i,j) - bu(i,j)*x(i+1,j) 
         end do 
!dir$ ivdep
         do i = 1, n - 2 
            x(i,j) = x(i,j) + bu(i+1,j)*bu(i,j)*x(i+2,j) 
         end do 
      end do 
      return  
      end subroutine slve9 


      subroutine lu9p(n, m, e, d, c, b, a, bu, cu, du, eu) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: n 
      integer , intent(in) :: m 
      real(double) , intent(inout) :: e(n,m) 
      real(double) , intent(inout) :: d(n,m) 
      real(double) , intent(inout) :: c(n,m) 
      real(double) , intent(inout) :: b(n,m) 
      real(double) , intent(inout) :: a(n,m) 
      real(double) , intent(inout) :: bu(n,m) 
      real(double) , intent(inout) :: cu(n,m) 
      real(double) , intent(inout) :: du(n,m) 
      real(double) , intent(inout) :: eu(n,m) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i 
!-----------------------------------------------
!
!cc   form a = l*v
!
      do j = 1, m 
!
!cc factor central block
!
         a(1,j) = 1./a(1,j) 
         do i = 2, n 
            b(i,j) = a(i-1,j)*b(i,j) 
            a(i,j) = 1./(a(i,j)-b(i,j)*bu(i-1,j)) 
         end do 
         if (j == m) cycle  
!
!cc   form upper diagonals
!
         do i = 2, n 
            du(i,j) = du(i,j) - b(i,j)*eu(i-1,j) 
         end do 
         do i = 2, n 
            cu(i,j) = cu(i,j) - b(i,j)*du(i-1,j) 
         end do 
!
!cc   form lower diagonals
!
         do i = 2, n 
            e(i,j+1) = a(i-1,j)*e(i,j+1) 
         end do 
         d(1,j+1) = a(1,j)*d(1,j+1) 
         do i = 2, n 
            d(i,j+1) = a(i,j)*(d(i,j+1)-e(i,j+1)*bu(i-1,j)) 
         end do 
         do i = 1, n - 1 
            c(i,j+1) = a(i+1,j)*(c(i,j+1)-d(i,j+1)*bu(i,j)) 
         end do 
!
!cc   form central diagonals
!
         do i = 2, n 
            b(i,j+1) = b(i,j+1) - e(i,j+1)*du(i-1,j) - d(i,j+1)*cu(i,j) 
         end do 
         do i = 1, n - 1 
            bu(i,j+1) = bu(i,j+1) - d(i,j+1)*eu(i,j) - c(i,j+1)*du(i+1,j) 
         end do 
         a(1,j+1) = a(1,j+1) - d(1,j+1)*du(1,j) - c(1,j+1)*cu(2,j) 
         do i = 2, n - 1 
            a(i,j+1) = a(i,j+1) - e(i,j+1)*eu(i-1,j) - d(i,j+1)*du(i,j) - c(i,j&
               +1)*cu(i+1,j) 
         end do 
         a(n,j+1) = a(n,j+1) - e(n,j+1)*eu(n-1,j) - d(n,j+1)*du(n,j) 
      end do 
      do i = 1, n 
         do j = 1, m 
            bu(i,j) = a(i,j)*bu(i,j) 
         end do 
      end do 
!
!cc   form u = d[-1]*v
!
!      do 120 i=1,n*m-n
      do i = 1, n - 1 
         do j = 1, m 
            cu(i,j) = a(i,j)*cu(i,j) 
            du(i,j) = a(i,j)*du(i,j) 
            eu(i,j) = a(i,j)*eu(i,j) 
         end do 
      end do 
      return  
      end subroutine lu9p 


      subroutine rypaxu(e, d, c, b, a, bu, cu, du, eu, nh, nv, x, y, r) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nh 
      integer , intent(in) :: nv 
      real(double) , intent(in) :: r 
      real(double) , intent(in) :: e(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: bu(*) 
      real(double) , intent(in) :: cu(*) 
      real(double) , intent(in) :: du(*) 
      real(double) , intent(in) :: eu(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(inout) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, nhm1, nhp1, i, l, k, m 
!-----------------------------------------------
!*** FIRST EXECUTABLE STATEMENT   RYPAX
!
      n = nh*nv 
      nhm1 = nh - 1 
      nhp1 = nh + 1 
      i = 1 
      y(i) = r*y(i) + (a(i)*x(i)+bu(i)*x(i+1)+du(i)*x(i+nh)+eu(i)*x(i+nhp1)) 
      do i = 2, nh 
         y(i) = r*y(i) + (b(i)*x(i-1)+(a(i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)&
            +(du(i)*x(i+nh)+eu(i)*x(i+nhp1)))))) 
      end do 
      i = nhp1 
      y(i) = r*y(i) + (d(i)*x(i-nh)+c(i)*x(i-nhm1)+b(i)*x(i-1)+a(i)*x(i)+bu(i)*&
         x(i+1)+cu(i)*x(i+nhm1)+du(i)*x(i+nh)+eu(i)*x(i+nhp1)) 
      do i = nh + 2, n - nhp1 
         y(i) = r*y(i) + (e(i)*x(i-nhp1)+(d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(&
            i-1)+(a(i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(du(i)*x(i+nh)+eu(i)&
            *x(i+nhp1))))))))) 
      end do 
      i = n - nh 
      y(i) = r*y(i) + (e(i)*x(i-nhp1)+d(i)*x(i-nh)+c(i)*x(i-nhm1)+b(i)*x(i-1)+a&
         (i)*x(i)+bu(i)*x(i+1)+cu(i)*x(i+nh-1)+du(i)*x(i+nh)) 
      do i = n - nhm1, n - 1 
         y(i) = r*y(i) + (e(i)*x(i-nhp1)+(d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(&
            i-1)+(a(i)*x(i)+bu(i)*x(i+1)))))) 
      end do 
      i = n 
      y(i) = r*y(i) + (e(i)*x(i-nhp1)+d(i)*x(i-nh)+b(i)*x(i-1)+a(i)*x(i)) 
!
!cc periodic equations
!
      l = n - nh 
      k = l - 1 
      m = l + 1 
      i = 1 
      y(i) = y(i) + d(i+n)*x(i+l) + c(i+n)*x(i+m) 
      y(i+l) = y(i+l) + du(i-nh)*x(i) + eu(i-nh)*x(i+1) 
      do i = 2, nhm1 
         y(i) = y(i) + e(i+n)*x(i+k) + d(i+n)*x(i+l) + c(i+n)*x(i+m) 
         y(i+l) = y(i+l) + cu(i-nh)*x(i-1) + du(i-nh)*x(i) + eu(i-nh)*x(i+1) 
      end do 
      i = nh 
      y(i) = y(i) + e(i+n)*x(i+k) + d(i+n)*x(i+l) 
      y(i+l) = y(i+l) + cu(i-nh)*x(i-1) + du(i-nh)*x(i) 
!
      return  
      end subroutine rypaxu 


      subroutine ymax9pu(e, d, c, b, a, bu, cu, du, eu, nh, nv, x, y) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nh 
      integer , intent(in) :: nv 
      real(double) , intent(in) :: e(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: bu(*) 
      real(double) , intent(in) :: cu(*) 
      real(double) , intent(in) :: du(*) 
      real(double) , intent(in) :: eu(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(inout) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, nhm1, nhp1, i, l, k, m 
!-----------------------------------------------
!*** FIRST EXECUTABLE STATEMENT   YMAX9P
      n = nh*nv 
      nhm1 = nh - 1 
      nhp1 = nh + 1 
      i = 1 
      y(i) = y(i) - (a(i)*x(i)+bu(i)*x(i+1)+du(i)*x(i+nh)+eu(i)*x(i+nhp1)) 
      do i = 2, nh 
         y(i) = y(i) - (b(i)*x(i-1)+(a(i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(&
            du(i)*x(i+nh)+eu(i)*x(i+nhp1)))))) 
      end do 
      i = nhp1 
      y(i) = y(i) - (d(i)*x(i-nh)+c(i)*x(i-nhm1)+b(i)*x(i-1)+a(i)*x(i)+bu(i)*x(&
         i+1)+cu(i)*x(i+nhm1)+du(i)*x(i+nh)+eu(i)*x(i+nhp1)) 
      do i = nh + 2, n - nhp1 
         y(i) = y(i) - (e(i)*x(i-nhp1)+(d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(i-&
            1)+(a(i)*x(i)+(bu(i)*x(i+1)+(cu(i)*x(i+nhm1)+(du(i)*x(i+nh)+eu(i)*x&
            (i+nhp1))))))))) 
      end do 
      i = n - nh 
      y(i) = y(i) - (e(i)*x(i-nhp1)+d(i)*x(i-nh)+c(i)*x(i-nhm1)+b(i)*x(i-1)+a(i&
         )*x(i)+bu(i)*x(i+1)+cu(i)*x(i+nh-1)+du(i)*x(i+nh)) 
      do i = n - nhm1, n - 1 
         y(i) = y(i) - (e(i)*x(i-nhp1)+(d(i)*x(i-nh)+(c(i)*x(i-nhm1)+(b(i)*x(i-&
            1)+(a(i)*x(i)+bu(i)*x(i+1)))))) 
      end do 
      i = n 
      y(i) = y(i) - (e(i)*x(i-nhp1)+d(i)*x(i-nh)+b(i)*x(i-1)+a(i)*x(i)) 
!
!cc periodic equations
!
      l = n - nh 
      k = l - 1 
      m = l + 1 
      i = 1 
      y(i) = y(i) - (d(i)*x(i+l)+c(i)*x(i+m)) 
      y(i+l) = y(i+l) - (du(i+l)*x(i)+eu(i+l)*x(i+1)) 
      do i = 2, nhm1 
         y(i) = y(i) - (e(i)*x(i+k)+d(i)*x(i+l)+c(i)*x(i+m)) 
         y(i+l) = y(i+l) - (cu(i+l)*x(i-1)+du(i+l)*x(i)+eu(i+l)*x(i+1)) 
      end do 
      i = nh 
      y(i) = y(i) - (e(i)*x(i+k)+d(i)*x(i+l)) 
      y(i+l) = y(i+l) - (cu(i+l)*x(i-1)+du(i+l)*x(i)) 
!
      return  
      end subroutine ymax9pu 
