      subroutine locator(x_grid, y_grid, nx, ny, iwid, jwid, dx, dy, xl, xr, yb&
         , yt, wl, wr, wb, wt, cyl, list, index_phys, index_logic, x_phys, y_phys, &
         x_logic, y_logic, killer) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: nx 
      integer  :: ny 
      integer  :: iwid 
      integer  :: jwid 
      integer  :: wl 
      integer  :: wr 
      integer  :: wb 
      integer  :: wt 
      integer  :: list 
      real(double)  :: dx 
      real(double)  :: dy 
      real(double)  :: xl 
      real(double)  :: xr 
      real(double)  :: yb 
      real(double)  :: yt 
      real(double)  :: cyl
      integer  :: index_phys(*) 
      integer  :: index_logic(*) 
      integer  :: killer(*) 
      real(double)  :: x_grid(*) 
      real(double)  :: y_grid(*) 
      real(double)  :: x_phys(*) 
      real(double)  :: y_phys(*) 
      real(double)  :: x_logic(*) 
      real(double)  :: y_logic(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(double) :: up, vp, wp 
!-----------------------------------------------
!
!     call a procedure to handle particle outside the
!     domain
!
      call fence_box (list, index_phys, x_phys, y_phys, cyl, up, vp, wp, xl, xr, yt&
         , yb, wl, wr, wb, wt, killer) 
!
!     remove the particles to be absorbed
!
!
!     call a procedure to locate particle given
!     that they are inside the domain
!
      call whereami (list, index_phys, index_logic, iwid, jwid, x_grid, y_grid&
         , nx, ny, x_phys, y_phys, x_logic, y_logic) 
 
      return  
      end subroutine locator 


 
 
      subroutine fence_box(list, index, x_phys, y_phys, cyl, up, vp, wp, xl, xr, yt&
         , yb, wl, wr, wb, wt, killer) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: list 
      integer , intent(in) :: wl 
      integer , intent(in) :: wr 
      integer , intent(in) :: wb 
      integer , intent(in) :: wt 
      real(double) , intent(in) :: xl 
      real(double) , intent(in) :: xr 
      real(double) , intent(in) :: yt 
      real(double) , intent(in) :: yb 
      real(double) , intent(in) :: cyl
      integer , intent(in) :: index(*) 
      integer , intent(out) :: killer(*) 
      real(double) , intent(inout) :: x_phys(*) 
      real(double) , intent(inout) :: y_phys(*) 
      real(double) , intent(inout) :: up(*) 
      real(double) , intent(inout) :: vp(*) 
      real(double)  :: wp(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, np 
!-----------------------------------------------
      do n = 1, list 
         killer(n) = 0 
         np = index(n) 
   11    continue 
         if (x_phys(np) < xl) then 
            if (wl == 1) then 
               x_phys(np) = x_phys(np) + 2.*(xl - x_phys(np)) 
               up(np) = -up(np) 
               !wp(np) = (1d0-2d0*cyl)*wp(np)
            else if (wl == 2) then 
               x_phys(np) = x_phys(np) + (xr - xl) 
            else 
               x_phys(np) = xl 
               killer(n) = 1 
            endif 
            go to 11 
         endif 
         if (x_phys(np) > xr) then 
            if (wr == 1) then 
               x_phys(np) = x_phys(np) + 2.*(xr - x_phys(np)) 
               up(np) = -up(np) 
            else if (wr == 2) then 
               x_phys(np) = x_phys(np) - (xr - xl) 
            else 
               x_phys(np) = xr 
               killer(n) = 2 
            endif 
            go to 11 
         endif 
   13    continue 
         if (y_phys(np) < yb) then 
            if (wb == 1) then 
               y_phys(np) = y_phys(np) + 2.*(yb - y_phys(np)) 
               vp(np) = -vp(np) 
            else if (wb == 2) then 
               y_phys(np) = y_phys(np) + (yt - yb) 
            else 
               y_phys(np) = yb 
               killer(n) = 3 
            endif 
            go to 13 
         endif 
         if (y_phys(np) <= yt) cycle  
         if (wt == 1) then 
            y_phys(np) = y_phys(np) + 2.*(yt - y_phys(np)) 
            vp(np) = -vp(np) 
         else if (wt == 2) then 
            y_phys(np) = y_phys(np) - (yt - yb) 
         else 
            y_phys(np) = yt 
            killer(n) = 4 
         endif 
         go to 13 
      end do 
      return  
      end subroutine fence_box 


 
 
      subroutine locator_putter(ncells, nxyp, nx, ny, ijc, is, iphead, link, xp&
         , yp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ncells 
      integer , intent(in) :: nxyp 
      integer , intent(in) :: nx 
      integer  :: ny 
      integer , intent(in) :: is 
      integer , intent(in) :: ijc(*) 
      integer , intent(inout) :: iphead(nxyp,*) 
      integer , intent(inout) :: link(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, n, ij, iptemp, np, i, j, ijp 
!-----------------------------------------------
!
!     This routine move particles from the wrong list into
!     the new one
!
      nxp = nx + 1 
      do n = 1, ncells 
         ij = ijc(n) 
         iptemp = 0 
         np = iphead(ij,is) 
    1    continue 
         if (np /= 0) then 
            iphead(ij,is) = link(iphead(ij,is)) 
            i = int(xp(np)) 
            j = int(yp(np)) 
            ijp = (j - 1)*nxp + i 
            if (ijp /= ij) then 
               link(np) = iphead(ijp,is) 
               iphead(ijp,is) = np 
            else 
               link(np) = iptemp 
               iptemp = np 
            endif 
            np = iphead(ij,is) 
            go to 1 
         endif 
         iphead(ij,is) = iptemp 
      end do 
 
      return  
      end subroutine locator_putter 


 
 
      subroutine whereami(list, index_phys, index_logic, iwid, jwid, x_grid, &
         y_grid, nx, ny, x_phys, y_phys, x_logic, y_logic) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: list 
      integer  :: iwid 
      integer  :: jwid 
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer  :: index_phys(*) 
      integer , intent(in) :: index_logic(*) 
      real(double)  :: x_grid(*) 
      real(double)  :: y_grid(*) 
      real(double)  :: x_phys(*) 
      real(double)  :: y_phys(*) 
      real(double) , intent(inout) :: x_logic(*) 
      real(double) , intent(inout) :: y_logic(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(list) :: index_locator, iold, jold, inew, jnew 
      integer :: indexmax, indexmin, n, list_locator, npl, npp, itry, list_next 
      real(double), dimension(list) :: the, zeta 
!-----------------------------------------------
!
!     local arrays
!
 
      indexmax = max(iwid,jwid) 
      indexmin = min(iwid,jwid) 
 
      do n = 1, list 
         index_locator(n) = n 
      end do 
      list_locator = list 
      do n = 1, list_locator 
         npl = index_logic(index_locator(n)) 
         npp = index_phys(index_locator(n)) 
         iold(n) = int(x_logic(npl)) 
         jold(n) = int(y_logic(npl)) 
         iold(n) = min(nx,max(2,iold(n))) 
         jold(n) = min(ny,max(2,jold(n))) 
      end do 
      itry = 1 
 
    1 continue 
      call map (x_grid, y_grid, iwid, jwid, iold, jold, index_phys, &
         index_locator, list_locator, x_phys, y_phys, the, zeta, inew, jnew) 
 
      list_next = 0 
      do n = 1, list_locator 
!      ichange=0.5*(int(sign(1.,the(n)-1.-eps)
!     &     +sign(1.,the(n)+eps)))
!      jchange=0.5*(int(sign(1.,zeta(n)-1.-eps)
!     &     +sign(1.,zeta(n)+eps)))
         if (inew(n)==iold(n) .and. jnew(n)==jold(n)) then 
            npl = index_logic(index_locator(n)) 
            x_logic(npl) = inew(n) + the(n) 
            y_logic(npl) = jnew(n) + zeta(n) 
         else 
            list_next = list_next + 1 
            index_locator(list_next) = index_locator(n) 
            iold(list_next) = inew(n) 
            jold(list_next) = jnew(n) 
            iold(list_next) = min(nx,max(2,iold(list_next))) 
            jold(list_next) = min(ny,max(2,jold(list_next))) 
         endif 
      end do 
      list_locator = list_next 
      if (list_locator == 0) return  
      itry = itry + 1 
      if (itry < 100*nx*ny) then 
         go to 1 
      else 
         write (*, *) 'SHAME ON YOU: locator unable to locate' 
         do n = 1, list_locator 
            write (*, *) x_phys(index_phys(n)), y_phys(index_phys(n)) 
         end do 
      endif 
      return  
      end subroutine whereami 


 
 
      subroutine map(x, y, iwid, jwid, i, j, index_phys, index_locator, list, &
         xp, yp, the, zeta, inew, jnew) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(in) :: list 
      integer , intent(in) :: i(*) 
      integer , intent(in) :: j(*) 
      integer , intent(in) :: index_phys(*) 
      integer , intent(in) :: index_locator(*) 
      integer , intent(out) :: inew(*) 
      integer , intent(out) :: jnew(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
      real(double) , intent(inout) :: the(*) 
      real(double) , intent(inout) :: zeta(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, ij, npp, ipj, ipjp, ijp, ichange, jchange 
      real(double) :: eps, x1, x2, x3, x4, y1, y2, y3, y4, dx1, dx2, dx12, dy1&
         , dy2, dy12, xc, yc, a, b, c, discrim, wsthe, alfa, beta, f1, f2, epsi&
         , xi, f3, f4 
!-----------------------------------------------
!
      eps = 1.E-6 
!
      do n = 1, list 
         ij = (j(n)-1)*jwid + (i(n)-1)*iwid + 1 
         npp = index_phys(index_locator(n)) 
!
!     calculate cell geometry
!
         ipj = ij + iwid 
         ipjp = ij + jwid + iwid 
         ijp = ij + jwid 
!
         x1 = x(ipj) 
         x2 = x(ipjp) 
         x3 = x(ijp) 
         x4 = x(ij) 
!
         y1 = y(ipj) 
         y2 = y(ipjp) 
         y3 = y(ijp) 
         y4 = y(ij) 
!
         dx1 = 0.5*(x1 + x2 - x3 - x4) 
         dx2 = 0.5*(x2 + x3 - x4 - x1) 
         dx12 = x2 - x1 + x4 - x3 
         dy1 = 0.5*(y1 + y2 - y3 - y4) 
         dy2 = 0.5*(y2 + y3 - y4 - y1) 
         dy12 = y2 - y1 + y4 - y3 
!
         xc = 0.25*(x1 + x2 + x3 + x4) 
         yc = 0.25*(y1 + y2 + y3 + y4) 
!
!     new particle position in natural coordinates
!
         a = dx1*dy12 - dy1*dx12 
         b = dx1*dy2 - dx2*dy1 + dx12*(yp(npp)-yc) - dy12*(xp(npp)-xc) 
         c = dx2*(yp(npp)-yc) - dy2*(xp(npp)-xc) 
         discrim = max(0.,b**2 - 4.*a*c) 
         wsthe = 2.*c/((-b) - sign(1.,b)*sqrt(discrim)) 
         zeta(n) = (yp(npp)-yc-dy1*wsthe+xp(npp)-xc-dx1*wsthe)/((dx2 + dy2 + (&
            dx12 + dy12)*wsthe) + 1.E-10) + 0.5 
!
         the(n) = wsthe + 0.5 
!
 
         if (.not.(abs(the(n))>2. .or. abs(zeta(n))>2.)) then 
 
            ichange = 0.5*int(sign(1.,the(n)-1.-eps)+sign(1.,the(n)+eps)) 
            jchange = 0.5*int(sign(1.,zeta(n)-1.-eps)+sign(1.,zeta(n)+eps)) 
 
         else 
 
            alfa = (yp(npp)-yc)/(xp(npp)-xc+1.E-10) 
            beta = yc - alfa*xc 
            f1 = y1 - alfa*x1 - beta 
            f2 = y2 - alfa*x2 - beta 
            if (f1*f2 <= 0.) then 
               epsi = -f1/(f2 - f1) 
               xi = x1*(1. - epsi) + x2*epsi 
               if ((xi - xc)*(xp(npp)-xc) >= 0.) then 
                  ichange = 1 
                  jchange = 0 
                  go to 1 
               endif 
            endif 
 
            f3 = y3 - alfa*x3 - beta 
            if (f3*f2 <= 0.) then 
               epsi = -f2/(f3 - f2) 
               xi = x2*(1. - epsi) + x3*epsi 
               if ((xi - xc)*(xp(npp)-xc) >= 0.) then 
                  ichange = 0 
                  jchange = 1 
                  go to 1 
               endif 
            endif 
            f4 = y4 - alfa*x4 - beta 
            if (f3*f4 <= 0.) then 
               epsi = -f3/(f4 - f3) 
               xi = x3*(1. - epsi) + x4*epsi 
               if ((xi - xc)*(xp(npp)-xc) >= 0.) then 
                  ichange = -1 
                  jchange = 0 
                  go to 1 
               endif 
            endif 
            ichange = 0 
            jchange = -1 
 
         endif 
 
    1    continue 
         inew(n) = i(n) + ichange 
         jnew(n) = j(n) + jchange 
 
 
      end do 
      return  
      end subroutine map 


 
      subroutine locator_lister(is, is_electron, list, index, killer, iwid, &
         jwid, iphead_electron, iphead, ipabsorb, xp, yp, qpar, mass, epar, &
         link, npar_left, npar_rite, npar_bottom, npar_top, npar_emitted, wl, &
         wr, wb, wt, gamma) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: is 
      integer , intent(in) :: is_electron 
      integer , intent(in) :: list 
      integer , intent(in) :: iwid 
      integer , intent(in) :: jwid 
      integer , intent(inout) :: ipabsorb 
      integer , intent(inout) :: npar_left 
      integer , intent(inout) :: npar_rite 
      integer , intent(inout) :: npar_bottom 
      integer , intent(inout) :: npar_top 
      integer , intent(inout) :: npar_emitted 
      integer , intent(in) :: wl 
      integer , intent(in) :: wr 
      integer , intent(in) :: wb 
      integer , intent(in) :: wt 
      real(double) , intent(in) :: gamma 
      integer , intent(in) :: index(*) 
      integer , intent(in) :: killer(*) 
      integer , intent(inout) :: iphead_electron(*) 
      integer , intent(inout) :: iphead(*) 
      integer , intent(out) :: link(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
      real(double) , intent(inout) :: qpar(*) 
      real(double) , intent(inout) :: mass(*) 
      real(double)  :: epar(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: n, np, i, j, ij 
!-----------------------------------------------
      do n = 1, list 
         np = index(n) 
         i = int(xp(np)) 
         j = int(yp(np)) 
         ij = (j - 1)*jwid + (i - 1)*iwid + 1 
 
         select case (killer(n))  
         case (1)  
!
!     left
!
            if (wl==3 .or. is==is_electron .or. gamma==0.) then 
!     absorption
               link(np) = ipabsorb 
               ipabsorb = np 
               npar_left = npar_left + 1 
            else 
!     secondary emission if ion
               link(np) = iphead_electron(ij) 
               iphead_electron(ij) = np 
               qpar(np) = -gamma*qpar(np) 
               mass(np) = gamma*mass(np) 
               npar_emitted = npar_emitted + 1 
            endif 
 
         case (2)  
!
!     right
!
            if (wr==3 .or. is==is_electron .or. gamma==0.) then 
!     absorption
               link(np) = ipabsorb 
               ipabsorb = np 
               npar_rite = npar_rite + 1 
            else 
!     secondary emission if ion
               link(np) = iphead_electron(ij) 
               iphead_electron(ij) = np 
               qpar(np) = -gamma*qpar(np) 
               mass(np) = gamma*mass(np) 
               npar_emitted = npar_emitted + 1 
            endif 
 
         case (3)  
!
!     bottom
!
            if (wb==3 .or. is==is_electron .or. gamma==0.) then 
!     absorption
               link(np) = ipabsorb 
               ipabsorb = np 
               npar_bottom = npar_bottom + 1 
            else 
!     secondary emission if ion
               link(np) = iphead_electron(ij) 
               iphead_electron(ij) = np 
               qpar(np) = -gamma*qpar(np) 
               mass(np) = gamma*mass(np) 
               npar_emitted = npar_emitted + 1 
            endif 
 
         case (4)  
!
!     top
!
            if (wt==3 .or. is==is_electron .or. gamma==0.) then 
!     absorption
               link(np) = ipabsorb 
               ipabsorb = np 
               npar_top = npar_top + 1 
            else 
!     secondary emission if ion
               link(np) = iphead_electron(ij) 
               iphead_electron(ij) = np 
               qpar(np) = -gamma*qpar(np) 
               mass(np) = gamma*mass(np) 
               npar_emitted = npar_emitted + 1 
            endif 
 
         case default 
 
 
            link(np) = iphead(ij) 
            iphead(ij) = np 
 
         end select 
      end do 
      return  
      end subroutine locator_lister 
