      module celest2d_com_M 
      use vast_kind_param, only:  double 
!...Created by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
      integer, parameter :: nxyp = 30000 
      integer, parameter :: npart = 1000000 
      integer, parameter :: ntmp = 1024 
      integer, parameter :: nsp = 3
	  integer, parameter :: nreg = 20 
      integer, parameter :: nvert = 20 
      integer, parameter :: nbdy = 750 
      integer, parameter :: ntpltp = 10 
      integer, parameter :: ntplt = 16384 
      integer, parameter :: nhst = 10000 
      integer, parameter :: nbinm = 200 
      real(double), parameter :: two_pi = 1d0 
      real(double), parameter :: epsilon_0 = 1d0 
      integer, dimension(nxyp) :: iphd2, ichead, jchead 
      integer, dimension(nsp) :: numtot 
      integer :: ivisc, numtotl, problem_type 
      real(double), dimension(1) :: aa 
      real(double), dimension(nxyp) :: x, y, u, v, mv, vvol, dive, curle, &
         displx, disply, phi0, phiavg, coef, vol, pkntc, ug, vg, color 
      real(double), dimension(nxyp,nsp) :: qdn, jxs, jys 
      real(double), dimension(nsp) :: qom, friction, frdriftx, frdrifty 
      real(double), dimension(nxyp,nsp) :: jzs 
      real(double), dimension(nxyp) :: jz0, qtilde 
      real(double), dimension(nxyp,nsp) :: pixx, pixy, piyx, piyy, pixz, piyz 
      real(double), dimension(nxyp) :: pixx0, pixy0, piyy0 
      real(double), dimension(nxyp,nsp) :: number, tempture 
      real(double), dimension(nxyp) :: gradpx, gradpy, gradpz, qnet0, q_mean, &
           jx0, jy0, ex, ey 
      real(double), dimension(nxyp,nsp) :: extil, eytil, eztil, dkdt 
      real(double), dimension(nxyp) :: bx, by, bz, jxtil, jytil, jztil 
      real(double), dimension(nxyp,nsp) :: qvs 
      real(double), dimension(nxyp) :: opsq, qv0 
      real(double), dimension(nxyp,nsp) :: emxs, emys 
      real(double), dimension(nxyp) :: emxc, emyc 
      real(double), dimension(nxyp,nsp) :: uhst, vhst 
      real(double) :: zz1, rdt 
      real(double), dimension(nxyp) :: dielperp, dieltrns, dielpar, coeftrns, &
         coefpar, diegrid, inside_object
      real(double), dimension(nsp) :: diem 
	  real(double) :: phiring
      real(double) :: chimin, voldust_exact 
      logical :: neutral, collision
      character :: iddt
      real(double), dimension(nxyp) :: a, b, c, d, e, af, bf, cf, df, ef, eg, &
         dg, cg, bg, ag, ah, bh, ch, dh, eh, soln, srce, residu, sc1, sc2 
      integer :: idone, ilast, imp, ipres, isq, itmax, icoplt, ijmix, lpr&
         , lvmax, maxset, modey, ncon, ncyc, ndump, ndumpsav, niter, null, &
         ntimes, numit, nx, nxm, nxp, ny, nym, nyp, nummax, nactiv, npltpt, &
         nstat, nsmth 
      real(double) :: aaa, angmomv, amptud, bx0, by0, bz0, cntr, coef1, coef2, &
         consrv, cyl, circlate, coll, droux, drouy, dt, dtmax, dtmin, dx, dxmax&
         , dy, dilmax, dtpdv, diss, eps, exconst, eyconst, fixl, fiyb, gm1, &
         grfac, grind, gx, gy, jdotb, epsp 
      logical, dimension(nsp) :: fluid, conductor 
      logical :: debug, estatic, firstime, hybrid, history, gravity, hot, movie&
         , mix 
      integer :: ninner, wb, wl, wr, wt, kbr, ktr, ktl, kbl, kmax 
      real(double), dimension(nsp) :: totk 
      real(double) :: om, omcyl, pi, omfield, phibdy, romin, romax, scale, siei&
         , stabl, scal, smth, tau, tiny, t, tfilm, third, timlmt, tlimd, tprtr&
         , twfilm, twfin, twlfth, tmovie, twmovie, twprtr, twhist, thist, &
         totmass, tinyi, ug0, vg0, uin, vin, vmax, xdif, ydif, xaxis, yaxis, &
         wmin, wmax, xmin, xmax, ymin, ymax, xconv, xl, xr, yb, yconv, yt, zzz 
      logical :: plots, reflctr, reflctl, reflctb, reflctt, timing, symmetry 
      real(double), dimension(nxyp) :: xplt, yplt, zplt 
      real(double), dimension(2) :: xwindo, ywindo 
      character, dimension(4) :: zname*8 
      character :: name*80, jnm*8, d1*8, c1*8 
      real(double) :: rmaj 
      integer, dimension(nsp) :: inj, isqin 
      integer, dimension(nreg) :: nvg, npcel, icoi 
      integer :: nrg, nsp_dust, nsp_dust2, icoinf, icofix, iprd 
      real(double), dimension(nreg) :: anpr, rhr 
      real(double), dimension(nsp) :: dtin, tinf, vinf, rhinf, riinj, reinj, &
         einf, uinf, fluxinj, velinj 
      real(double), dimension(nreg,nvert) :: zvi, rvi 
      real(double), dimension(nreg) :: uvi, vvi, siep, siepr, siepl, siept, &
         siepb, tscal, rex, rey, rix, riy, xcenter, ycenter, angmom, vlr, anpar 
      real(double) :: totam, p_inject
      integer :: nobs 
      real(double), dimension(4,10) :: rvo, zvo 
      integer, dimension(nbdy) :: ijbdy 
      integer :: nadaptfr 
      real(double), dimension(nxyp) :: xn, yn, w, wadapt 
      real(double), dimension(4) :: voll 
      real(double), dimension(nbdy) :: xbdy, ybdy, grnormx, grnormy, xghost, &
         yghost 
      real(double) :: onw1, onw2, onw3, grid, eqvol, r36, xorigin, yorigin 
      logical :: periodic, boris, leonardo, conductive 
      integer, dimension(20) :: iout 
      integer :: in1, in2, in3, in4, in5, in6 
      real(double), dimension(nxyp) :: cx1, cx2, cx3, cx4, cy1, cy2, cy3, cy4, &
         cr1, cr2, cr3, cr4 
      real(double), dimension(nxyp) :: ufl, vfl 
      integer, dimension(nxyp) :: iphead 
      integer, dimension(npart) :: link, ico, istatus 
      real(double), dimension(npart) :: xp, yp, xpf, ypf, up, vp, wp, qpar, &
         diepar 
      integer, dimension(ntmp) :: icop, nploc 
      real(double), dimension(ntmp) :: xpv, ypv, upv, vpv, wpv, massp, sp 
      real(double), dimension(9,ntmp) :: wght 
      integer, dimension(ntpltp) :: iregp, npp 
      real(double), dimension(ntpltp) :: xpinit, ypinit, dmin 
      real(double), dimension(ntplt) :: tplt 
      real(double), dimension(ntplt*ntpltp) :: pmix, rhmix, xmix, ymix, umix, &
         vmix, siemix, vortex 
      real(double), dimension(nhst) :: ta 
      real(double), dimension(nhst*nsp) :: parnrg 
      real(double), dimension(nhst) :: fldnrg, enrglost, edotj, delefld, &
         variance, ymom, xmom, qtotal, ntotal, edotjp 
      real(double), dimension(nhst,nsp) :: enrgleft, enrgrite 
      real(double), dimension(nsp) :: qout 
      real(double), dimension(nhst,nsp) :: qdust, phidust, emomdustx, emomdusty&
         , qdustp, emomdustxp, emomdustyp, fdragx, fdragy, feletx, felety &
         , eforce_partx, eforce_party &
		 , forcespx, forcespy, flux_inject_left, flux_inject_right, flux_inject_top &
		 , flux_inject_bottom, flux_emitted
      real(double) :: efld 
      real(double) :: kappa, kappap, jacob, mass, mass1, mass2, ndustc, jx1, &
         jx2, jx3, jx4, jy1, jy2, jy3, jy4, lambda, mu 
      real(double), dimension(nbinm,nsp) :: ud 
      real(double), dimension(nbinm) :: vd 
      integer :: iesp, ndistr, ntempo, nparticle, iviaver, iviaspl, iviacoa, &
         ntresh 
      real(double) :: offset, ycut, elle, vtresh, sor
      logical :: geometric_object
      logical, dimension(nsp) :: lcoal, lsplit 

! (Leo) Parameters for the subcycling
! nsub(npart) : Number of subcycling of each particle
! dtsub(npart) : past timestep of the particle
! nsubmax : maximum subcycling (Close to the probe)
      integer, dimension(npart) :: nsub
      real(double), dimension(npart) :: dtsub
      integer :: nsubmax

! (Leo) Parameters for the angular collection diagnostic. 
! thetadim : the number of angular slices of the probe (<128)
! thetadust  : an array of the number of particles, and mean vx,vy,vz
! who strike the probe
      integer :: thetadim
      real(double), dimension(4,nsp,128) :: thetadust

      end module celest2d_com_M 
	  
	  
	  module mover_data
      USE vast_kind_param
	        use celest2d_com_M 
      integer , dimension(ntmp) :: isp, inew, jnew, iold, jold, numx, numy, ls 
      integer :: ctype, ipsrch, n, ij, nh, is, j, i, ipj, ipjp, ijp, nlostr, &
         nlostl, nlost, np, lv, l, nctheta 
      real(double), dimension(ntmp) :: the, zeta, xtp0, ytp0, xpc, ypc, w1, w2&
         , w3, w4, upv1, vpv1, wpv1, qptmp, bxpar, bypar, bzpar, expar, eypar
      real(double), dimension(nsp) :: xcentre, ycentre, voldust 
      real(double) :: twopi, evfact, ntarld, mneut&
         , mratio, vthn, qdt, qomdt, x2p, z2p, upvold, &
         adust, bdust, cdust, testdust, sdust1, sdust2, sdust, radp, dnorm, &
         rld3, vexact, mfact , velmax, quale, udotb, denom, bpar2, qo2mdt,&
         dtprec,dtX,dtV,cosomdt,sinomdt,Omega,qhere
      logical , dimension(ntmp) :: discard 
      logical :: nim
      logical, dimension(ntmp) :: move
      end module mover_data
 
