      subroutine adapt 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ipjp, ijp, ijmin, ijmax, is 
      real(double) :: alfa, tinytmp, r, xc, yc
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: ismin, ismax 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/adapt.f90,v 1.2 2006/11/21 20:34:51 lapenta Exp $
!
! $Log: adapt.f90,v $
! Revision 1.2  2006/11/21 20:34:51  lapenta
! Gianni: modifications to have 2 dust particles
!
! Revision 1.1.1.1  2006/06/13 23:37:44  cfichtl
! Initial version of democritus 6/13/06
!
!
         do j = 2, nyp 
            do i = 2, nxp 
               ij = nxp*(j - 1) + i 
			   ijp = ij + nxp 
               ipj = ij + 1 
               ipjp = ijp + 1
               wadapt(ij)=0d0 
               xc=(x(ij)+x(ijp)+x(ipj)+x(ipjp))*.25d0
               yc=(y(ij)+y(ijp)+y(ipj)+y(ipjp))*.25d0
               do is=nsp_dust,nrg
                  r=sqrt((xc-xcenter(is))**2/rex(is)**2+&
                         (yc-ycenter(is))**2/rey(is)**2)
                  if (r<1d0) then
                     wadapt(ij)=wadapt(ij)+r**2
                  else
                     wadapt(ij)=wadapt(ij)+exp(-(r-1d0)/3)
                  end if
                enddo
            end do 
         end do 
      
      ijmax = ismax(nxp*nyp,wadapt,1) 
      wmax = wadapt(ijmax)
      wadapt=wadapt/wmax 
      ijmin = ismin(nxp*nyp,wadapt,1) 
      wmin = wadapt(ijmin)
      wadapt=wadapt-wmin+1d0/offset
      w=wadapt 
      write(*,*)wmin,wmax
!            write (*, *) 'wmin,wmax', wmin, wmax 
!      do i = 1, nx + 1 
!         do j = 1, ny + 1 
!            ij = (j - 1)*nxp + i 
!            w(ij)=(wadapt(ij)-wmin)/(wmax-wmin)+1d0/offset
!         end do 
!      end do 
!      wadapt=w
      ijmin = ismin(nxp*nyp,wadapt,1) 
      wmin = wadapt(ijmin) 
      ijmax = ismax(nxp*nyp,wadapt,1) 
      wmax = wadapt(ijmax) 
      write (*, *) 'wmin,wmax', wmin, wmax 
      return  
      end subroutine adapt 

