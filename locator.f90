      subroutine locator_kin(ipsrch) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(inout) :: ipsrch 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(ntmp) :: isp, inew, jnew, numx, numy, ls 
      integer :: is, nh, iplost, np, nchng, nlost, lv, ntest, n, kintcp, nlostr&
         , iv, jtry, iphold, ijnew, ij 
      real(double), dimension(ntmp) :: xpc, ypc, xtp0, ytp0, fail, testx, testy 
      real(double) :: ypcsav, ws, xtemp, ytemp, vmag, th, fi 
      logical, dimension(ntmp) :: discard, left 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: ranf 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/locator.f90,v 1.1.1.1 2006/06/13 23:37:45 cfichtl Exp $
!
! $Log: locator.f90,v $
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
!
!
!      a routine to locate a point in a computation mesh of quadrilat
!         and calculate its natural coordinates
!
!
      do is = 1, nsp 
         qout(is) = 0. 
      end do 
!     *********************************************************
!
!     reconstruct linked lists
!
!     ***********************************************
!
!
!     ***************************************************************
!
!     Stage 1: Detect intersections of the particle orbits with the boun
!              Discard particles that strike an absorbing boundary.
!
!     ***************************************************************
!
      nh = mod(ncyc,nhst) 
      nh = max(nh,1) 
      do is = 1, nsp 
         enrgleft(nh,is) = 0.0 
         enrgrite(nh,is) = 0.0 
      end do 
!
      iplost = 0 
      np = ipsrch 
      nchng = -1 
      if (np /= 0) then 
 1100    continue 
         call listmkr (np, nploc, link, lvmax, ntmp) 
!
         if (nchng /= 0) then 
            nlost = 0 
            nchng = 0 
         endif 
         do lv = 1, lvmax 
            xpc(lv) = xpf(nploc(lv)) 
            ypc(lv) = ypf(nploc(lv)) 
            xtp0(lv) = xp(nploc(lv)) 
            ytp0(lv) = yp(nploc(lv)) 
            isp(lv) = ico(nploc(lv)) 
            fail(lv) = 0.0 
            discard(lv) = .FALSE. 
         end do 
!
         nlost = nlost + lvmax 
!
!     if periodic boundary conditions in y,
!     return particle to principal interval
!
 1805    continue 
         call localev (1, nxp, 1, lvmax, xpc, ypc, dx, dy, inew, jnew, xorigin&
            , yorigin, xpv, ypv) 
         ntest = 0 
         if (periodic) then 
!
!     check first to see if any particles have left the principal
!     periodic interval
!
            do lv = 1, lvmax 
!      numy(lv)=int((ypc(lv)+yt-yb)/(yt-yb+1.e-10))
               numy(lv) = 1 
               if (ypc(lv)>yt .or. ypc(lv)<yb) numy(lv) = 0 
               numx(lv) = (inew(lv)-2)*(nx - inew(lv)) 
            end do 
!
            do lv = 1, lvmax 
               if (.not.(numy(lv)/=1 .or. numx(lv)<0 .and. .not.discard(lv))) &
                  cycle  
               ntest = ntest + 1 
               ls(ntest) = lv 
            end do 
         else 
            do lv = 1, lvmax 
               numy(lv) = 1 
               if (ypc(lv)>yt .or. ypc(lv)<yb) numy(lv) = -1 
!      numy(lv)=(jnew(lv)-2)*(ny-jnew(lv))
               numx(lv) = (inew(lv)-2)*(nx - inew(lv)) 
            end do 
!
            do lv = 1, lvmax 
               if (.not.(numy(lv)<0 .or. numx(lv)<0 .and. .not.discard(lv))) &
                  cycle  
               ntest = ntest + 1 
               ls(ntest) = lv 
            end do 
!
         endif 
!
!     restore particles which have left the principal
!     interval using reflctp
!     particles are assumed to start in the principal interval
!     if they intercept the top boundary, yt-yb must be subtracted
!     if they intercept the bottom boundary, yt-yb must be added
!
         do n = 1, ntest 
            lv = ls(n) 
            ypcsav = ypc(lv) 
            fail(lv) = 0.0 
!
!
!
!      call guessk(nx,ny,ktl,ktr,kbr,kbl,
!     &     inew(lv),jnew(lv),kstart)
!
!
!      call xreflctp(xbdy,ybdy,kmax,xtp0(lv),xpc(lv),
!     &     ytp0(lv),ypc(lv),fail(lv),nx,ny,wb,wr,wt,wl,
!     &      kbr,ktr,ktl,kbl,kstart,kintcp)
            call reflctp (xpc(lv), ypc(lv), xr, xl, yt, yb, fail(lv)) 
!     if(periodic) then
!     lwt=2
!     lwb=2
!     else
!     lwt=3
!     lwl=3
!     end if
!     if (reflctr) then
!     lwr=1
!     else
!     lwr=3
!     end if
!     if (reflctl) then
!     lwl=1
!     else
!     lwl=3
!     end if
!     call fence_box(1,1,xpc(lv),ypc(lv),xl,xr,yb,yt,
!     &   lwl,lwr,lwb,lwt,kdummy)
!
            if (fail(lv) == 4) then 
!
!     the particle has left bottom boundary
!
               if (periodic) then 
                  ytp0(lv) = ytp0(lv) + (yt - yb) 
                  ypc(lv) = ypcsav + (yt - yb) 
                  xpf(nploc(lv)) = xpc(lv) 
                  ypf(nploc(lv)) = ypc(lv) 
                  jnew(lv) = ny 
               else 
!
                  if (siepb(isp(lv)) > 0.0) then 
!     generate a particle on the bottom boundary
!     with prescribed temperature
!
                     ws = ranf() 
                     xtemp = ws*xbdy(kintcp) + (1. - ws)*xbdy(kintcp+1) 
                     ytemp = ws*ybdy(kintcp) + (1. - ws)*ybdy(kintcp+1) 
                     vmag = sqrt((-log(1. - 0.999999*ranf()))) 
                     th = pi*ranf() 
                     fi = pi*ranf() 
                     up(nploc(lv)) = siepb(isp(lv))*vmag*cos(th)*sin(fi) 
                     vp(nploc(lv)) = siepb(isp(lv))*vmag*sin(th)*sin(fi) 
                     wp(nploc(lv)) = siepb(isp(lv))*vmag*cos(fi) 
!
                     xpc(lv) = xtemp + (xpc(lv)-xtp0(lv)) 
                     ypc(lv) = ytemp + (ypc(lv)-ytp0(lv)) 
                     xtp0(lv) = xtemp 
                     ytp0(lv) = ytemp 
                  else 
!      vp(nploc(lv))=-vp(nploc(lv))
                     ytp0(lv) = ytp0(lv) + (yt - yb) 
                     ypc(lv) = ypcsav + (yt - yb) 
                     xpf(nploc(lv)) = xpc(lv) 
                     ypf(nploc(lv)) = ypc(lv) 
                     discard(lv) = .TRUE. 
                     left(lv) = .FALSE. 
                  endif 
                  xpf(nploc(lv)) = xpc(lv) 
                  ypf(nploc(lv)) = ypc(lv) 
!
 
                  jnew(lv) = 2 
               endif 
            endif 
!
            if (fail(lv) == 6) then 
!
!     the particle has left the top boundary
!
               if (periodic) then 
                  ytp0(lv) = ytp0(lv) - (yt - yb) 
                  ypc(lv) = ypcsav - (yt - yb) 
                  xpf(nploc(lv)) = xpc(lv) 
!      write(*,*)ypf(nploc(lv)),ypc(lv),yt,yb,vp(nploc(lv))
                  ypf(nploc(lv)) = ypc(lv) 
                  jnew(lv) = 2 
               else 
!
                  if (siept(isp(lv)) > 0.0) then 
!     generate a particle on the top boundary
!     with prescribed temperature
!
                     ws = ranf() 
                     xtemp = ws*xbdy(kintcp) + (1. - ws)*xbdy(kintcp+1) 
                     ytemp = ws*ybdy(kintcp) + (1. - ws)*ybdy(kintcp+1) 
!
                     vmag = sqrt((-log(1. - 0.999999*ranf()))) 
                     th = pi*(1. + ranf()) 
                     fi = pi*ranf() 
                     up(nploc(lv)) = siept(isp(lv))*vmag*cos(th)*sin(fi) 
                     vp(nploc(lv)) = siept(isp(lv))*vmag*sin(th)*sin(fi) 
                     wp(nploc(lv)) = siept(isp(lv))*vmag*cos(fi) 
!
                     xpc(lv) = xtemp + (xpc(lv)-xtp0(lv)) 
                     ypc(lv) = ytemp + (ypc(lv)-ytp0(lv)) 
                     xtp0(lv) = xtemp 
                     ytp0(lv) = ytemp 
                  else 
!      vp(nploc(lv))=-vp(nploc(lv))
                     ytp0(lv) = ytp0(lv) - (yt - yb) 
                     ypc(lv) = ypcsav - (yt - yb) 
                     xpf(nploc(lv)) = xpc(lv) 
                     ypf(nploc(lv)) = ypc(lv) 
                     discard(lv) = .TRUE. 
                     left(lv) = .FALSE. 
                  endif 
                  xpf(nploc(lv)) = xpc(lv) 
                  ypf(nploc(lv)) = ypc(lv) 
!
                  jnew(lv) = ny 
               endif 
            endif 
!
            if (fail(lv) == 5.) then 
               inew(lv) = nx 
               if (.not.reflctr) then 
                  nlostr = nlostr + 1 
                  enrglost(nh) = enrglost(nh) + 0.5*qpar(nploc(lv))*(up(nploc(&
                     lv))**2+vp(nploc(lv))**2)/qom(isp(lv)) 
!
                  discard(lv) = .TRUE. 
                  left(lv) = .FALSE. 
!
               else 
!
!     reflect the particle velocity at the right boundary
!
!
                  if (siepr(isp(lv)) > 0.0) then 
!     generate a particle on the right boundary
!     with prescribed temperature
!
!     subtract energy lost
!
                     enrgrite(nh,isp(lv)) = enrgrite(nh,isp(lv)) - 0.5*qpar(&
                        nploc(lv))/qom(isp(lv))*(up(nploc(lv))**2+vp(nploc(lv))&
                        **2+wp(nploc(lv))**2) 
!
                     ws = ranf() 
                     xtemp = ws*xbdy(kintcp) + (1. - ws)*xbdy(kintcp+1) 
                     ytemp = ws*ybdy(kintcp) + (1. - ws)*ybdy(kintcp+1) 
!
                     vmag = sqrt((-log(1. - 0.999999*ranf()))) 
                     th = pi*(0.5 + ranf()) 
                     fi = pi*ranf() 
                     up(nploc(lv)) = siepr(isp(lv))*vmag*cos(th)*sin(fi) 
                     vp(nploc(lv)) = siepr(isp(lv))*vmag*sin(th)*sin(fi) 
                     wp(nploc(lv)) = siepr(isp(lv))*vmag*cos(fi) 
!
                     enrgrite(nh,isp(lv)) = enrgrite(nh,isp(lv)) + 0.5*qpar(&
                        nploc(lv))/qom(isp(lv))*(up(nploc(lv))**2+vp(nploc(lv))&
                        **2+wp(nploc(lv))**2) 
!
                     xpc(lv) = xtemp + (xpc(lv)-xtp0(lv)) 
                     ypc(lv) = ytemp + (ypc(lv)-ytp0(lv)) 
                     xtp0(lv) = xtemp 
                     ytp0(lv) = ytemp 
                  else 
                     up(nploc(lv)) = -up(nploc(lv)) 
!      up(nploc(lv))=up(nploc(lv))
                  endif 
                  xpf(nploc(lv)) = xpc(lv) 
                  ypf(nploc(lv)) = ypc(lv) 
               endif 
            endif 
!
            if (fail(lv) /= 7.) cycle  
            inew(lv) = 2 
            if (.not.reflctl) then 
!
!     accumulate the energy lost when particles leave the domain
!
               enrglost(nh) = enrglost(nh) + 0.5*qpar(nploc(lv))*(up(nploc(lv))&
                  **2+vp(nploc(lv))**2)/qom(isp(lv)) 
!
               discard(lv) = .TRUE. 
               left(lv) = .TRUE. 
!
            else 
!
!     reflect the particle velocity in the right boundary
!
!
               if (siepl(isp(lv)) > 0.0) then 
!     generate a particle on the left boundary
!     with prescribed temperature
!
                  enrgleft(nh,isp(lv)) = enrgleft(nh,isp(lv)) - 0.5*qpar(nploc(&
                     lv))/qom(isp(lv))*(up(nploc(lv))**2+vp(nploc(lv))**2+wp(&
                     nploc(lv))**2) 
!
                  ws = ranf() 
                  xtemp = ws*xbdy(kintcp) + (1. - ws)*xbdy(kintcp+1) 
                  ytemp = ws*ybdy(kintcp) + (1. - ws)*ybdy(kintcp+1) 
!
                  vmag = sqrt((-log(1. - 0.999999*ranf()))) 
                  th = pi*(-0.5 + ranf()) 
                  fi = pi*ranf() 
                  up(nploc(lv)) = siepl(isp(lv))*vmag*cos(th)*sin(fi) 
                  vp(nploc(lv)) = siepl(isp(lv))*vmag*sin(th)*sin(fi) 
                  wp(nploc(lv)) = siepl(isp(lv))*vmag*cos(fi) 
!
                  enrgleft(nh,isp(lv)) = enrgleft(nh,isp(lv)) + 0.5*qpar(nploc(&
                     lv))/qom(isp(lv))*(up(nploc(lv))**2+vp(nploc(lv))**2+wp(&
                     nploc(lv))**2) 
!
                  xpc(lv) = xtemp + (xpc(lv)-xtp0(lv)) 
                  ypc(lv) = ytemp + (ypc(lv)-ytp0(lv)) 
                  xtp0(lv) = xtemp 
                  ytp0(lv) = ytemp 
               else 
                  up(nploc(lv)) = -up(nploc(lv)) 
!      up(nploc(lv))=up(nploc(lv))
               endif 
               xpf(nploc(lv)) = xpc(lv) 
               ypf(nploc(lv)) = ypc(lv) 
!
            endif 
         end do 
!
         if (ntest > 0) go to 1805 
!
         do lv = 1, lvmax 
            if (discard(lv)) then 
               if (left(lv)) then 
                  iv = int(1. + (up(nploc(lv))+siep(ico(nploc(lv)))*3.)/2./siep&
                     (ico(nploc(lv)))/3.*float(ndistr-1)) 
                  ud(iv,ico(nploc(lv))) = ud(iv,ico(nploc(lv))) + abs(qpar(&
                     nploc(lv))) 
               endif 
               if (istatus(nploc(lv)) == 0) iviaver = iviaver + 1 
               if (istatus(nploc(lv)) == 1) iviaspl = iviaspl + 1 
               if (istatus(nploc(lv)) == 2) iviacoa = iviacoa + 1 
               ipsrch = link(nploc(lv)) 
               link(nploc(lv)) = iphd2(1) 
               iphd2(1) = nploc(lv) 
               qout(ico(nploc(lv))) = qout(ico(nploc(lv))) + qpar(nploc(lv)) 
            else 
               ipsrch = link(nploc(lv)) 
               link(nploc(lv)) = iplost 
               iplost = nploc(lv) 
            endif 
            xp(nploc(lv)) = inew(lv) + 1.E-10 
            yp(nploc(lv)) = jnew(lv) + 1.E-10 
         end do 
!
         jtry = 0 
         np = ipsrch 
         if (np > 0) go to 1100 
!
!     *****************************************************************
!
!     Stage 2: Particles lying in the domain are assigned to cell lists.
!
!     *****************************************************************
!
         iphold = 0 
         np = iplost 
 1901    continue 
         if (np > 0) then 
!
            call listmkr (np, nploc, link, lvmax, ntmp) 
!
!
            call whereami (lvmax, nploc, nploc, 1, nxp, xn, yn, nx, ny, xpf, &
               ypf, xp, yp) 
!
            do lv = 1, lvmax 
               inew(lv) = int(xp(nploc(lv))) 
               jnew(lv) = int(yp(nploc(lv))) 
               ijnew = (jnew(lv)-1)*nxp + inew(lv) 
               iplost = link(nploc(lv)) 
               link(nploc(lv)) = iphd2(ijnew) 
               iphd2(ijnew) = nploc(lv) 
            end do 
!
            np = iplost 
            if (np > 0) go to 1901 
!
         endif 
      endif 
!
      do ij = 1, nxyp 
         iphead(ij) = iphd2(ij) 
         iphd2(ij) = 0 
      end do 
!
!
!     Is locator working all right?
!
!     iwid=1
!     jwid=nxp
!     do i=2,nx
!     do j=2,ny
!     ij=(j-1)*nxp+i
!     np=iphead(ij)
!435  if(np.ne.0) then
!     if(ifix(xp(np)).ne.i) write(*,*)'locator fail x ',i,xp(np),
!     &  np,xpf(np),x(ij),x(ij+1)
!     if(ifix(yp(np)).ne.j) write(*,*)'locator fail y ',j,yp(np),
!     &  np
!       ipj=ij+iwid
!       ipjp=ij+iwid+jwid
!       ijp=ij+jwid
!     the=xp(np)-ifix(xp(np))
!     zeta=yp(np)-ifix(yp(np))
!     w1=the*(1-zeta)
!     w2=the*zeta
!     w3=(1-the)*zeta
!     w4=(1-the)*(1-zeta)
!
!     wsx=w1*xn(ipj)+w2*xn(ipjp)+w3*xn(ijp)+w4*xn(ij)
!     wsy=w1*yn(ipj)+w2*yn(ipjp)+w3*yn(ijp)+w4*yn(ij)
!
!
!     if(abs(xpf(np)-wsx).gt.1.e-8) write(*,*)'locator fallisce!'
!     if(abs(ypf(np)-wsy).gt.1.e-8) write(*,*)'locator fallisce!'
!
!     np=link(np)
!     goto 435
!     endif
!     enddo
!     enddo
      return  
      end subroutine locator_kin 


      subroutine xreflctp(x, y, kmax, x0, x1, y0, y1, fail, nx, ny, wb, wr, wt&
         , wl, kbr, ktr, ktl, kbl, kstart, kintcp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: kmax 
      integer  :: nx 
      integer  :: ny 
      integer  :: wb 
      integer  :: wr 
      integer  :: wt 
      integer  :: wl 
      integer , intent(in) :: kbr 
      integer , intent(in) :: ktr 
      integer , intent(in) :: ktl 
      integer , intent(in) :: kbl 
      integer , intent(in) :: kstart 
      integer , intent(out) :: kintcp 
      real(double) , intent(inout) :: x0 
      real(double) , intent(inout) :: x1 
      real(double) , intent(inout) :: y0 
      real(double) , intent(inout) :: y1 
      real(double) , intent(out) :: fail 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer , dimension(32) :: ktest 
      integer :: kkk, kstop, kl, kk, k, ntest, n 
      real(double), dimension(32) :: test 
      real(double) :: eps, a11, a12, a21, a22, b1, b2, ws, xintcp, yintcp, &
         dnorm 
!-----------------------------------------------
!
!      kbr=nx
!      ktr=kbr+ny-1
!      ktl=ktr+nx-1
!      kbl=ktl+ny-1
!
      eps = -1.E-5 
      do kkk = 1, kmax - 1, 8 
         kstop = min(kkk + 7,kmax - 1) 
!
!     test for intersection
!
         kl = 1 
         do kk = kkk, kstop 
            k = mod(kk + kstart - 2,kmax - 1) + 1 
            test(kl) = ((x(k)-x0)*(y1-y(k))-(x1-x(k))*(y(k)-y0))*((x(k+1)-x1)*(&
               y0-y(k+1))-(x0-x(k+1))*(y(k+1)-y1)) 
            kl = kl + 1 
         end do 
!
         ntest = 0 
         kl = 1 
         do kk = kkk, kstop 
            if (test(kl) >= 0.) then 
               k = mod(kk + kstart - 2,kmax - 1) + 1 
               ntest = ntest + 1 
               ktest(ntest) = k 
            endif 
            kl = kl + 1 
         end do 
!
         do n = 1, ntest 
            k = ktest(n) 
!
!     polygon formed by particle trajectory segment and boundary segment
!     is convex, so trajectory intersects that segment
!
!
!     the particle has crossed the boundary during the time step
!
!     test whether outside or inside
!
!     particle intersection between k and jb+jstep
!     reflect in the normal to the line segment
!
            a11 = -(y1 - y0) 
            a12 = x1 - x0 
            a21 = -(y(k+1)-y(k)) 
            a22 = x(k+1) - x(k) 
            b1 = y0*x1 - x0*y1 
            b2 = y(k)*x(k+1) - x(k)*y(k+1) 
            ws = 1./(a11*a22 - a12*a21 + 1.E-30) 
            xintcp = (b1*a22 - b2*a12)*ws 
            yintcp = (a11*b2 - a21*b1)*ws 
            dnorm = a21*(x1 - xintcp) + a22*(y1 - yintcp) 
            if (dnorm < 0.0) then 
!
!
!     the particle  is now outside and should be reflected or deleted
!
               if ((k - 1)*(kbr - 1 - k) >= 0) fail = 4. 
!
               if ((k - kbr)*(ktr - 1 - k) >= 0) fail = 5. 
!
               if ((k - ktr)*(ktl - 1 - k) >= 0) fail = 6. 
!
               if ((k - ktl)*(kbl - 1 - k) >= 0) fail = 7. 
!
               x0 = xintcp 
               y0 = yintcp 
               dnorm = dnorm/(a21**2 + a22**2) 
               x1 = x1 - 2.*a21*dnorm 
               y1 = y1 - 2.*a22*dnorm 
               kintcp = k 
               return  
            else 
!
!     particle is inside this segment
!
               fail = 3. 
            endif 
         end do 
      end do 
!     write(59,1001) fail,x0,x1,y0,y1
 1001 format(" fail,x0,x1,y0,y1=",5(1p,e10.3)) 
      return  
      end subroutine xreflctp 
