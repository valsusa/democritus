      subroutine bc(uu, vv) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      real(double) , intent(inout) :: uu(*) 
      real(double) , intent(inout) :: vv(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, ij, ndel, i 
!-----------------------------------------------
!
!
!
!     +++
!     +++ set velocity boundary conditions for all 4 sides, where
!     +++ wl, wr, wb, and wt are input integers defined as follows-
!     +++ 0 = von-neuman boundary conditions
!     +++ 1 = dirichlet boundary conditions
!     +++
      do j = 2, nyp 
         ij = (j - 2)*nxp + 2 
!     +++
!     +++ the left edge . . .
!     +++
         if (wl == 0) then 
            vv(ij) = vv(ij+1) 
            uu(ij) = 0.0 
         else 
            uu(ij) = uu(ij+1) 
            vv(ij) = 0.0 
         endif 
         ij = ij + nx - 1 
!     +++
!     +++ the right edge . . .
!     +++
         if (wr == 0) then 
            vv(ij) = vv(ij-1) 
            uu(ij) = 0.0 
         else 
            uu(ij) = uu(ij-1) 
            vv(ij) = 0.0 
         endif 
      end do 
      ndel = (ny - 1)*(nx + 1) 
      do i = 2, nxp 
         ij = i 
!     +++
!     +++ the bottom edge . . .
!     +++
         if (periodic) then 
            uu(ij) = uu(ij+ndel) 
            vv(ij) = vv(ij+ndel) 
         else if (wb == 0) then 
            uu(ij) = uu(ij+nxp) 
            vv(ij) = 0.0 
         else 
            vv(ij) = vv(ij+nxp) 
            uu(ij) = 0.0 
         endif 
         ij = ny*nxp + i 
!     +++
!     +++ the top edge . . .
!     +++
         if (periodic) then 
            uu(ij) = uu(ij-ndel) 
            vv(ij) = vv(ij-ndel) 
         else if (wt == 0) then 
            uu(ij) = uu(ij-nxp) 
            vv(ij) = 0.0 
         else 
            vv(ij) = vv(ij-nxp) 
            uu(ij) = 0.0 
         endif 
      end do 
      return  
      end subroutine bc 

      subroutine bcc(nx, ny, p, wbloc, wtloc, wl, wr, periodic) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer , intent(in) :: wbloc 
      integer , intent(in) :: wtloc 
      integer , intent(in) :: wl 
      integer , intent(in) :: wr 
      logical , intent(in) :: periodic 
      real(double) , intent(inout) :: p(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij, j, ndelx, i, wb, wt
!-----------------------------------------------
!
!     set p in ghost cells
!     wl etc. = 0 ... Neumann
!     wl etc. = 1 ... dirichlet
!

	wb=wbloc
	wt=wtloc

!     left boundary
!
      ij = nx + 2 
      do j = 2, ny 
         p(ij) = (1. - wl)*p(ij+1) 
         ij = ij + nx + 1 
      end do 
!
!
!     right boundary
!
      ij = 2*(nx + 1) 
      do j = 2, ny 
         p(ij) = (1. - wr)*p(ij-1) 
         ij = ij + nx + 1 
      end do 
!
!
      if (periodic) then 
         wb = 0 
         wt = 0 
         ndelx = (ny - 1)*(nx + 1) 
      else 
         ndelx = nx + 1 
      endif 
!
!     bottom boundary
!
      ij = 1 
      do i = 1, nx + 1 
         p(ij) = (1. - wb)*p(ij+ndelx) 
         ij = ij + 1 
      end do 
!
!
!     top boundary
!
      ij = ny*(nx + 1) + 1 
      do i = 1, nx + 1 
         p(ij) = (1. - wt)*p(ij-ndelx) 
         ij = ij + 1 
      end do 
!
!
      return  
      end subroutine bcc 


      subroutine bccphi(nx, ny, p, wb, wt, wl, wr, t, omfield, phibdy, periodic&
         , yb, yt, y, amptud, modey) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer , intent(out) :: wb 
      integer , intent(out) :: wt 
      integer , intent(in) :: wl 
      integer , intent(in) :: wr 
      integer , intent(in) :: modey 
      real(double) , intent(in) :: t 
      real(double) , intent(in) :: omfield 
      real(double) , intent(in) :: phibdy 
      real(double) , intent(in) :: yb 
      real(double) , intent(in) :: yt 
      real(double) , intent(in) :: amptud 
      logical , intent(in) :: periodic 
      real(double) , intent(inout) :: p(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij, j, ndelx, i, ijl, ijll, ijr, ijrr 
      real(double) :: pi, fky, sint, siny 
!-----------------------------------------------
!
      pi = acos(-1.) 
      fky = 2.*pi*modey/(yt - yb) 
!     set p in ghost cells
!     wl etc. = 0 ... Neumann
!     wl etc. = 1 ... dirichlet
!
!     left boundary
!
      if (wl == 1) then 
         ij = nx + 2 
         sint = sin(omfield*t) 
         do j = 2, ny 
            p(ij) = wl*phibdy*sint + (1. - wl)*p(ij+1) 
            siny = sin(fky*(y(ij)-yb)) 
            p(ij) = p(ij)*(1. + amptud*siny) 
            ij = ij + nx + 1 
         end do 
!
      endif 
!
!     right boundary
!
      ij = 2*(nx + 1) 
      if (wr == 1) then 
         sint = sin(omfield*t + pi) 
         do j = 2, ny 
            p(ij) = wr*phibdy*sint + (1. - wr)*p(ij-1) 
            siny = sin(fky*(y(ij)-yb)) 
            p(ij) = p(ij)*(1. + amptud*siny) 
            ij = ij + nx + 1 
         end do 
      endif 
!
!
      if (periodic) then 
         wb = 0 
         wt = 0 
         ndelx = (ny - 1)*(nx + 1) 
      else 
         ndelx = nx + 1 
      endif 
      
!     bottom boundary
!
         ij = 1 
         do i = 1, nx + 1 
            p(ij) = (1. - wb)*p(ij+ndelx) 
            ij = ij + 1 
         end do 
!
!
!     top boundary
!
         ij = ny*(nx + 1) + 1 
         do i = 1, nx + 1 
            p(ij) = (1. - wt)*p(ij-ndelx) 
            ij = ij + 1 
         end do 
!

      if (wl<0 .or. wr<0) then 
         do j = 1, ny + 1 
            ijl = (j - 1)*(nx + 1) + 2 
            ijll = (j - 1)*(nx + 1) + 1 
            ijr = (j - 1)*(nx + 1) + nx 
            ijrr = (j - 1)*(nx + 1) + nx + 1 
            p(ijrr) = p(ijl) 
            p(ijll) = p(ijr) 
         end do 
      endif 
!
      return  
      end subroutine bccphi 


      subroutine bcv(nqi, nqj, i1, i2, j1, j2, u) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(inout) :: u(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ijb, ijt, i 
!-----------------------------------------------
!
      ijb = (j1 - 1)*nqj + (i1 - 1)*nqi + 1 
      ijt = (j2 - 1)*nqj + (i1 - 1)*nqi + 1 
!
      do i = i1, i2 
         u(ijb) = u(ijb) + u(ijt) 
         u(ijt) = u(ijb) 
         ijb = ijb + nqi 
         ijt = ijt + nqi 
!
      end do 
!     ijl=nqi
!     ijr=nqj
!     do i=j1-1,j2
!     u(ijl)=u(ijl)+u(ijr)
!     u(ijr)=u(ijl)
!     ijl=ijl+nqj
!     ijr=ijr+nqj
!     enddo
!
      return  
      end subroutine bcv 


      subroutine bcj(nqi, nqj, i1, i2, j1, j2, periodic, wl, wr, wb, wt, jx, jy&
         , jz) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: wl 
      integer , intent(in) :: wr 
      integer , intent(in) :: wb 
      integer , intent(in) :: wt 
      logical , intent(in) :: periodic 
      real(double) , intent(inout) :: jx(*) 
      real(double) , intent(inout) :: jy(*) 
      real(double) , intent(inout) :: jz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ijb, ijt, i, j, ijl, ijr 
!-----------------------------------------------
!
!     bottom and top boundaries
!
      if (periodic) then 
         ijb = (j1 - 1)*nqj + (i1 - 1)*nqi + 1 
         ijt = (j2 - 1)*nqj + (i1 - 1)*nqi + 1 
!
         do i = i1, i2 
            jx(ijb) = jx(ijb) + jx(ijt) 
            jx(ijt) = jx(ijb) 
            jy(ijb) = jy(ijb) + jy(ijt) 
            jy(ijt) = jy(ijb) 
            jz(ijb) = jz(ijb) + jz(ijt) 
            jz(ijt) = jz(ijb) 
!
            ijb = ijb + nqi 
            ijt = ijt + nqi 
!
         end do 
      else 
         do i = i1, i2 
!
            ijb = (j1 - 1)*nqj + (i - 1)*nqi + 1 
            jx(ijb) = (1. - wb)*jx(ijb) 
            jy(ijb) = wb*jy(ijb) 
            jz(ijb) = (1. - wb)*jz(ijb) 
!
            ijt = (j2 - 1)*nqj + (i - 1)*nqi + 1 
            jx(ijt) = (1. - wt)*jx(ijt) 
            jy(ijt) = wt*jy(ijt) 
            jz(ijt) = (1. - wt)*jz(ijt) 
!
         end do 
      endif 
!
!     left and right
!
      do j = j1, j2 
!
         ijl = (j - 1)*nqj + (i1 - 1)*nqi + 1 
         jx(ijl) = 0.0 
         jy(ijl) = (1. - wl)*jy(ijl) 
         jz(ijl) = (1. - wl)*jz(ijl) 
!
         ijr = (j - 1)*nqj + (i2 - 1)*nqi + 1 
         jx(ijr) = 0.0 
         jy(ijr) = (1. - wr)*jy(ijr) 
         jz(ijr) = (1. - wr)*jz(ijr) 
!
      end do 
!
      return  
      end subroutine bcj 

      subroutine celset(nqi, nqj, i1, i2, j1, j2, xorigin, yorigin, dx, dy, x, &
         y, xn, yn) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: xorigin 
      real(double) , intent(in) :: yorigin 
      real(double) , intent(in) :: dx 
      real(double) , intent(in) :: dy 
      real(double) , intent(inout) :: x(*) 
      real(double) , intent(inout) :: y(*) 
      real(double) , intent(out) :: xn(*) 
      real(double) , intent(out) :: yn(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, ij, i 
      real(double) :: pi 
!-----------------------------------------------
!
 
      data pi/ 3.141592654/  
 
      do j = j1, j2 
         ij = (j - 1)*nqj + 1 
         do i = i1, i2 
            x(ij) = float(i - 2)*dx + xorigin 
            y(ij) = float(j - 2)*dy + yorigin 
            xn(ij) = x(ij) 
            yn(ij) = y(ij) 
 
            ij = ij + nqi 
         end do 
      end do 
 
 
 
      return  
      end subroutine celset 


      subroutine chnglst(np, iphed0, iphed1, link) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: np 
      integer , intent(out) :: iphed0 
      integer , intent(inout) :: iphed1 
      integer , intent(inout) :: link 
!-----------------------------------------------
      iphed0 = link 
      link = iphed1 
      iphed1 = np 
!
      return  
      end subroutine chnglst 


      subroutine deriv(nqi, nqj, i1, i2, j1, j2, cx1, cx2, cx3, cx4, cr1, cr2, &
         cr3, cr4, cy1, cy2, cy3, cy4, ex, ey, dive, curle) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: ex(*) 
      real(double) , intent(in) :: ey(*) 
      real(double) , intent(out) :: dive(*) 
      real(double) , intent(out) :: curle(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ipjp, ijp 
!-----------------------------------------------
!
      do j = j1, j2 
         do i = i1, i2 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            ipj = ij + nqi 
            ipjp = ij + nqi + nqj 
            ijp = ij + nqj 
!
            dive(ij) = (cx1(ij)+cr1(ij))*ex(ipj) + cy1(ij)*ey(ipj) + (cx2(ij)+&
               cr2(ij))*ex(ipjp) + cy2(ij)*ey(ipjp) + (cx3(ij)+cr3(ij))*ex(ijp)&
                + cy3(ij)*ey(ijp) + (cx4(ij)+cr4(ij))*ex(ij) + cy4(ij)*ey(ij) 
!
            curle(ij) = cx1(ij)*ey(ipj) - cy1(ij)*ex(ipj) + cx2(ij)*ey(ipjp) - &
               cy2(ij)*ex(ipjp) + cx3(ij)*ey(ijp) - cy3(ij)*ex(ijp) + cx4(ij)*&
               ey(ij) - cy4(ij)*ex(ij) 
!
         end do 
      end do 
!
      return  
      end subroutine deriv 


      subroutine electric(nqi, nqj, i1, i2, j1, j2, cx1, cx2, cx3, cx4, cr1, &
         cr2, cr3, cr4, cy1, cy2, cy3, cy4, phi0, vvol, ex, ey, xn, yn)
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: phi0(*) 
      real(double) , intent(in) :: vvol(*) 
      real(double) , intent(out) :: ex(*) 
      real(double) , intent(out) :: ey(*) 
      real(double) , intent(in) :: xn(*) 
      real(double) , intent(in) :: yn(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, imj, imjm, ijm 
!-----------------------------------------------
!
!     calculate the electric field from the potential
!
!      E=-grad(phi0)
!
      do j = j1, j2 
         do i = i1, i2 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            imj = ij - nqi 
            imjm = ij - nqi - nqj 
            ijm = ij - nqj 
!
            ex(ij) = ((cx4(ij)+cr4(ij))*phi0(ij)+(cx1(imj)+cr1(imj))*phi0(imj)+&
               (cx2(imjm)+cr2(imjm))*phi0(imjm)+(cx3(ijm)+cr3(ijm))*phi0(ijm))/&
               vvol(ij) 
!
            ey(ij) = (cy4(ij)*phi0(ij)+cy1(imj)*phi0(imj)+cy2(imjm)*phi0(imjm)+&
               cy3(ijm)*phi0(ijm))/vvol(ij) 
               
               if(i==i1) then
            ey(ij)=(phi0(ijm)-phi0(ij))/(yn(ij)-yn(ijm))
            !   write(*,*)'phi',phi0(ij),phi0(imj),phi0(imj),phi0(imjm)
            !   write(*,*)'cy',cy4(ij),cy1(imj),cy3(imj),cy2(imjm)
               end if
!
         end do 
      end do 
!
      return  
      end subroutine electric 


      subroutine curl(nqi, nqj, i1, i2, j1, j2, cx1, cx2, cx3, cx4, cr1, cr2, &
         cr3, cr4, cy1, cy2, cy3, cy4, ex, ey, vol, curle) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double)  :: cr1(*) 
      real(double)  :: cr2(*) 
      real(double)  :: cr3(*) 
      real(double)  :: cr4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: ex(*) 
      real(double) , intent(in) :: ey(*) 
      real(double) , intent(in) :: vol(*) 
      real(double) , intent(out) :: curle(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, ipj, ipjp, ijp 
!-----------------------------------------------
!
      do j = j1, j2 
         do i = i1, i2 
!
            ij = (j - 1)*nqj + (i - 1)*nqi + 1 
            ipj = ij + nqi 
            ipjp = ij + nqj + nqi 
            ijp = ij + nqj 
!
            curle(ij) = (cx1(ij)*ey(ipj)-cy1(ij)*ex(ipj)+cx2(ij)*ey(ipjp)-cy2(&
               ij)*ex(ipjp)+cx3(ij)*ey(ijp)-cy3(ij)*ex(ijp)+cx4(ij)*ey(ij)-cy4(&
               ij)*ex(ij))/vol(ij) 
!
         end do 
      end do 
!
      return  
      end subroutine curl 


!
      subroutine geom 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij, k, i, j, ndelx, ijb, ijt 
      logical :: salta
      real(double) :: wsx, wsy, ws 
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/toolbin.f90,v 1.3 2006/07/06 16:47:18 lapenta Exp $
!
! $Log: toolbin.f90,v $
! Revision 1.3  2006/07/06 16:47:18  lapenta
! The correction on the particle mover was actually not right. The original way was, so I put it back as was.
! I also removed a test from the generator of geometric coefficients.
! Il Maestro.
!
! Revision 1.2  2006/07/05 15:42:24  lapenta
! Correction on BC for the particles and electric fields. Major revision
! Gianni
!
! Revision 1.1.1.1  2006/06/13 23:37:46  cfichtl
! Initial version of democritus 6/13/06
!
!
 
      call setzero (vvol, nxyp) 
!
!     compute geometric coefficients for cylindrical geometry
!
      call geocoef (1, nxp, 2, nx, 2, ny, cyl, x, y, cx1, cx2, cx3, cx4, cr1, &
         cr2, cr3, cr4, cy1, cy2, cy3, cy4, voll, vol, vvol) 
!
      call bcv (1, nxp, 1, nxp, 2, nyp, vvol) 
!
!     upper right corner
!
      ij = nxp*nyp 
      cr4(ij) = 0.0 
      cx4(ij) = 0.0 
      cy4(ij) = 0.0 
      vol(ij) = vol(ij-nxp) 
!
!     lower left corner
!
      cx2(1) = 0.0 
      cy2(1) = 0.0 
      cr2(1) = 0.0 
      vol(1) = vol(1+nxp) 
!
!     lower right corner
!
      cx3(nxp) = 0.0 
      cy3(nxp) = 0.0 
      cr3(nxp) = 0.0 
      vol(nxp) = vol(2*nxp) 
!
!     upper left corner
!
      ij = ny*nxp + 1 
      cx1(ij) = 0.0 
      cy1(ij) = 0.0 
      cr1(ij) = 0.0 
      vol(ij) = vol(ij-nxp) 
!
!     store boundary points in a singly indexed array
!
!
!     length of the array kmax=2(nx+ny-2)+1
!
      k = 1 
      ij = nxp + 2 
      do i = 2, nx 
         xbdy(k) = x(ij) 
         ybdy(k) = y(ij) 
         ijbdy(k) = ij 
         k = k + 1 
         ij = ij + 1 
      end do 
      k = nx 
      do j = 2, ny 
         xbdy(k) = x(ij) 
         ybdy(k) = y(ij) 
         ijbdy(k) = ij 
         k = k + 1 
         ij = ij + nxp 
      end do 
      k = nx + ny - 1 
      do i = 2, nx 
         xbdy(k) = x(ij) 
         ybdy(k) = y(ij) 
         ijbdy(k) = ij 
         k = k + 1 
         ij = ij - 1 
      end do 
      k = 2*nx + ny - 2 
      do j = 2, ny 
         ijbdy(k) = ij 
         ybdy(k) = y(ijbdy(k)) 
         xbdy(k) = x(ijbdy(k)) 
         k = k + 1 
         ij = ij - nxp 
      end do 
      xbdy(k) = xbdy(1) 
      ybdy(k) = ybdy(1) 
      ijbdy(k) = ij 
      k = 1 
      k = k + 1 
      do i = nxp + 3, nxp + nx 
         wsx = cx4(i) + cr4(i) + cx1(i-1) + cr1(i-1) 
         wsy = cy4(i) + cy1(i-1) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = wsx*ws 
         grnormy(k) = wsy*ws 
         k = k + 1 
      end do 
      k = k + 1 
      do j = 3, ny 
         in1 = j*nxp - 1 
         in2 = (j - 1)*nxp - 1 
         wsx = cx1(in1) + cx2(in2) + cr1(in1)*cr2(in2) 
         wsy = cy1(in1) + cy2(in2) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = wsx*ws 
         grnormy(k) = wsy*ws 
         k = k + 1 
      end do 
      k = k + 1 
      do i = nx, 3, -1 
         in1 = nxp*(ny - 1) + i 
         in2 = nxp*(ny - 1) + (i - 1) 
         wsx = cx3(in1) + cx2(in2) + cr3(in1) + cr2(in2) 
         wsy = cy3(in1) + cy2(in2) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = ws*wsx 
         grnormy(k) = ws*wsy 
         k = k + 1 
      end do 
      k = k + 1 
      do j = ny, 3, -1 
         in1 = (j - 1)*nxp + 2 
         in2 = (j - 2)*nxp + 2 
         wsx = cx4(in1) + cx3(in2) + cr4(in1) + cr3(in2) 
         wsy = cy4(in1) + cy3(in2) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = ws*wsx 
         grnormy(k) = ws*wsy 
         k = k + 1 
      end do 
!
      kbr = nx 
      ktr = kbr + ny - 1 
      ktl = ktr + nx - 1 
      kbl = ktl + ny - 1 
!
!     set normal vectors in corners
!
      if (.not.periodic) then 
         grnormx(kbr) = 0.0 
         grnormy(kbr) = 0.0 
         grnormx(ktr) = 0.0 
         grnormy(ktr) = 0.0 
         grnormx(ktl) = 0.0 
         grnormy(ktl) = 0.0 
         grnormx(kbl) = 0.0 
         grnormy(kbl) = 0.0 
!
      else 
!
!     top-left
!
         in4 = ny*nxp + 2 
         in3 = in4 - nxp 
         grnormx(ktl) = cx4(in4) + cx3(in3) + cr4(in4) + cr3(in3) 
         grnormy(ktl) = cy4(in4) + cy3(in3) 
!
!     bottom-left
!
         in4 = nxp + 2 
         in3 = 2 
         grnormx(kbl) = cx4(in4) + cx3(in3) + cr4(in4) + cr3(in3) 
         grnormy(kbl) = cy4(in4) + cy3(in3) 
!
!     top-right
!
         in1 = nyp*nxp - 1 
         in2 = in1 - nxp 
         grnormx(ktr) = cx1(in1) + cx2(in2) + cr1(in1) + cr2(in2) 
         grnormy(ktr) = cy1(in1) + cy2(in2) 
!
!     bottom-right
!
         in1 = 2*nyp - 1 
         in2 = in1 - nxp 
         grnormx(kbr) = cx1(in1) + cx2(in2) + cr1(in1) + cr2(in2) 
         grnormy(kbr) = cy1(in1) + cy2(in2) 
!
      endif 
!
      xl = xbdy(1) 
      xr = xbdy(1) 
      yb = ybdy(1) 
      yt = ybdy(1) 
      do k = 1, kbl 
         xl = min(xl,xbdy(k)) 
         xr = max(xr,xbdy(k)) 
         yb = min(yb,ybdy(k)) 
         yt = max(yt,ybdy(k)) 
      end do 
!

      call bcc (nx, ny, cx1, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cx2, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cx3, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cx4, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr1, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr2, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr3, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr4, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy1, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy2, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy3, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy4, wb, wt, wl, wr, periodic) 
      
      salta=.false.
      if(.not.salta) then
!
!     bottom boundary
!
      if (.not.periodic) then 
         call ghostpts (nxp, 1, kbr, x, y, ijbdy, grnormx, grnormy, xghost, &
            yghost) 
!
         call bdycoef ((-nxp), 1, kbr - 1, ijbdy, x, y, xghost, yghost, cx1, &
            cx2, cx3, cx4, cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol, vvol, &
            cyl) 
!
      endif 
!
!     right boundary
!
      call ghostpts ((-1), kbr, ktr, x, y, ijbdy, grnormx, grnormy, xghost, &
         yghost) 
!
      call bdycoef (0, kbr, ktr - 1, ijbdy, x, y, xghost, yghost, cx2, cx3, cx4&
         , cx1, cy2, cy3, cy4, cy1, cr2, cr3, cr4, cr1, vol, vvol, cyl) 
!
!     top boundary
!
      if (.not.periodic) then 
         call ghostpts ((-nxp), ktr, ktl, x, y, ijbdy, grnormx, grnormy, xghost&
            , yghost) 
!
         call bdycoef ((-1), ktr, ktl - 1, ijbdy, x, y, xghost, yghost, cx3, &
            cx4, cx1, cx2, cy3, cy4, cy1, cy2, cr3, cr4, cr1, cr2, vol, vvol, &
            cyl) 
!
      endif 
!
!     left boundary
!
      call ghostpts (1, ktl, kbl, x, y, ijbdy, grnormx, grnormy, xghost, yghost&
         ) 
!
      call bdycoef ((-nxp) - 1, ktl, kbl - 1, ijbdy, x, y, xghost, yghost, cx4&
         , cx1, cx2, cx3, cy4, cy1, cy2, cy3, cr4, cr1, cr2, cr3, vol, vvol, &
         cyl) 
!
      end if
!
      if (periodic) then 
!
!     set geometric coefficients in the corners
!
         ndelx = (ny - 1)*(nx + 1) 
!
         cx1(1) = cx1(1+ndelx) 
         cr1(1) = cr1(1+ndelx) 
         cy1(1) = cy1(1+ndelx) 
!
         cx2(1) = cx2(1+ndelx) 
         cr2(1) = cr2(1+ndelx) 
         cy2(1) = cy2(1+ndelx) 
!
         cx1(nxp+1+ndelx) = cx1(nxp+1) 
         cr1(nxp+1+ndelx) = cr1(nxp+1) 
         cy1(nxp+1+ndelx) = cy1(nxp+1) 
!
         cx2(nxp+1+ndelx) = cx2(nxp+1) 
         cr2(nxp+1+ndelx) = cr2(nxp+1) 
         cy2(nxp+1+ndelx) = cy2(nxp+1) 
!
         cx3(nxp) = cx3(nxp+ndelx) 
         cr3(nxp) = cr3(nxp+ndelx) 
         cy3(nxp) = cy3(nxp+ndelx) 
!
         cx4(nxp) = cx4(nxp+ndelx) 
         cr4(nxp) = cr4(nxp+ndelx) 
         cy4(nxp) = cy4(nxp+ndelx) 
!
         cx3(2*nxp+ndelx) = cx3(2*nxp) 
         cr3(2*nxp+ndelx) = cr3(2*nxp) 
         cy3(2*nxp+ndelx) = cy3(2*nxp) 
!
         cx4(2*nxp+ndelx) = cx4(2*nxp) 
         cr4(2*nxp+ndelx) = cr4(2*nxp) 
         cy4(2*nxp+ndelx) = cy4(2*nxp) 
!
      endif 
!     set volumes in ghost cells
!
      call bcc (nx, ny, vol, 0,0,0,0, periodic) 
!     *********************************
!     calculate the vertex volume
!     *********************************
!
      call vtxvol (nx, ny, vol, vvol) 
!
      if (periodic) then 
         ijb = nxp + 1 
         ijt = ny*nxp + 1 
         do i = 1, nxp 
            vvol(ijb) = vvol(ijb) + vvol(ijt) 
            vvol(ijt) = vvol(ijb) 
            ijb = ijb + 1 
            ijt = ijt + 1 
         end do 
      endif 
!
      return  
      end subroutine geom 

      subroutine newgeom 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij, k, i, j, ndelx, ijb, ijt, imj, imjm
      logical :: salta
      real(double) :: wsx, wsy, ws 
!-----------------------------------------------
!

 
      call setzero (vvol, nxyp) 
!
!     compute geometric coefficients for cylindrical geometry
!
      call geocoef (1, nxp, 2, nx, 2, ny, cyl, x, y, cx1, cx2, cx3, cx4, cr1, &
         cr2, cr3, cr4, cy1, cy2, cy3, cy4, voll, vol, vvol) 
!
      call bcv (1, nxp, 1, nxp, 2, nyp, vvol) 
!
!     upper right corner
!
      ij = nxp*nyp 
      cr4(ij) = 0.0 
      cx4(ij) = 0.0 
      cy4(ij) = 0.0 
      vol(ij) = vol(ij-nxp) 
!
!     lower left corner
!
      cx2(1) = 0.0 
      cy2(1) = 0.0 
      cr2(1) = 0.0 
      vol(1) = vol(1+nxp) 
!
!     lower right corner
!
      cx3(nxp) = 0.0 
      cy3(nxp) = 0.0 
      cr3(nxp) = 0.0 
      vol(nxp) = vol(2*nxp) 
!
!     upper left corner
!
      ij = ny*nxp + 1 
      cx1(ij) = 0.0 
      cy1(ij) = 0.0 
      cr1(ij) = 0.0 
      vol(ij) = vol(ij-nxp) 
!
!     store boundary points in a singly indexed array
!
!
!     length of the array kmax=2(nx+ny-2)+1
!
      k = 1 
      ij = nxp + 2 
      do i = 2, nx 
         xbdy(k) = x(ij) 
         ybdy(k) = y(ij) 
         ijbdy(k) = ij 
         k = k + 1 
         ij = ij + 1 
      end do 
      k = nx 
      do j = 2, ny 
         xbdy(k) = x(ij) 
         ybdy(k) = y(ij) 
         ijbdy(k) = ij 
         k = k + 1 
         ij = ij + nxp 
      end do 
      k = nx + ny - 1 
      do i = 2, nx 
         xbdy(k) = x(ij) 
         ybdy(k) = y(ij) 
         ijbdy(k) = ij 
         k = k + 1 
         ij = ij - 1 
      end do 
      k = 2*nx + ny - 2 
      do j = 2, ny 
         ijbdy(k) = ij 
         ybdy(k) = y(ijbdy(k)) 
         xbdy(k) = x(ijbdy(k)) 
         k = k + 1 
         ij = ij - nxp 
      end do 
      xbdy(k) = xbdy(1) 
      ybdy(k) = ybdy(1) 
      ijbdy(k) = ij 
      k = 1 
      k = k + 1 
      do i = nxp + 3, nxp + nx 
         wsx = cx4(i) + cr4(i) + cx1(i-1) + cr1(i-1) 
         wsy = cy4(i) + cy1(i-1) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = wsx*ws 
         grnormy(k) = wsy*ws 
         k = k + 1 
      end do 
      k = k + 1 
      do j = 3, ny 
         in1 = j*nxp - 1 
         in2 = (j - 1)*nxp - 1 
         wsx = cx1(in1) + cx2(in2) + cr1(in1)*cr2(in2) 
         wsy = cy1(in1) + cy2(in2) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = wsx*ws 
         grnormy(k) = wsy*ws 
         k = k + 1 
      end do 
      k = k + 1 
      do i = nx, 3, -1 
         in1 = nxp*(ny - 1) + i 
         in2 = nxp*(ny - 1) + (i - 1) 
         wsx = cx3(in1) + cx2(in2) + cr3(in1) + cr2(in2) 
         wsy = cy3(in1) + cy2(in2) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = ws*wsx 
         grnormy(k) = ws*wsy 
         k = k + 1 
      end do 
      k = k + 1 
      do j = ny, 3, -1 
         in1 = (j - 1)*nxp + 2 
         in2 = (j - 2)*nxp + 2 
         wsx = cx4(in1) + cx3(in2) + cr4(in1) + cr3(in2) 
         wsy = cy4(in1) + cy3(in2) 
         ws = 1./sqrt(wsx**2 + wsy**2 + 1.E-10) 
         grnormx(k) = ws*wsx 
         grnormy(k) = ws*wsy 
         k = k + 1 
      end do 
!
      kbr = nx 
      ktr = kbr + ny - 1 
      ktl = ktr + nx - 1 
      kbl = ktl + ny - 1 
!
!     set normal vectors in corners
!
      if (.not.periodic) then 
         grnormx(kbr) = 0.0 
         grnormy(kbr) = 0.0 
         grnormx(ktr) = 0.0 
         grnormy(ktr) = 0.0 
         grnormx(ktl) = 0.0 
         grnormy(ktl) = 0.0 
         grnormx(kbl) = 0.0 
         grnormy(kbl) = 0.0 
!
      else 
!
!     top-left
!
         in4 = ny*nxp + 2 
         in3 = in4 - nxp 
         grnormx(ktl) = cx4(in4) + cx3(in3) + cr4(in4) + cr3(in3) 
         grnormy(ktl) = cy4(in4) + cy3(in3) 
!
!     bottom-left
!
         in4 = nxp + 2 
         in3 = 2 
         grnormx(kbl) = cx4(in4) + cx3(in3) + cr4(in4) + cr3(in3) 
         grnormy(kbl) = cy4(in4) + cy3(in3) 
!
!     top-right
!
         in1 = nyp*nxp - 1 
         in2 = in1 - nxp 
         grnormx(ktr) = cx1(in1) + cx2(in2) + cr1(in1) + cr2(in2) 
         grnormy(ktr) = cy1(in1) + cy2(in2) 
!
!     bottom-right
!
         in1 = 2*nyp - 1 
         in2 = in1 - nxp 
         grnormx(kbr) = cx1(in1) + cx2(in2) + cr1(in1) + cr2(in2) 
         grnormy(kbr) = cy1(in1) + cy2(in2) 
!
      endif 
!
      xl = xbdy(1) 
      xr = xbdy(1) 
      yb = ybdy(1) 
      yt = ybdy(1) 
      do k = 1, kbl 
         xl = min(xl,xbdy(k)) 
         xr = max(xr,xbdy(k)) 
         yb = min(yb,ybdy(k)) 
         yt = max(yt,ybdy(k)) 
      end do 
!
      call bcc (nx, ny, cx1, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cx2, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cx3, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cx4, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr1, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr2, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr3, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cr4, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy1, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy2, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy3, wb, wt, wl, wr, periodic) 
      call bcc (nx, ny, cy4, wb, wt, wl, wr, periodic) 
      
!
!     bottom boundary
!
      if (.not.periodic) then 
         call ghostpts (nxp, 1, kbr, x, y, ijbdy, grnormx, grnormy, xghost, &
            yghost) 
!
         call bdycoef ((-nxp), 1, kbr - 1, ijbdy, x, y, xghost, yghost, cx1, &
            cx2, cx3, cx4, cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol, vvol, &
            cyl) 
!
      endif 
!
!     right boundary
!
      call ghostpts ((-1), kbr, ktr, x, y, ijbdy, grnormx, grnormy, xghost, &
         yghost) 
!
      call bdycoef (0, kbr, ktr - 1, ijbdy, x, y, xghost, yghost, cx2, cx3, cx4&
         , cx1, cy2, cy3, cy4, cy1, cr2, cr3, cr4, cr1, vol, vvol, cyl) 
!
!     top boundary
!
      if (.not.periodic) then 
         call ghostpts ((-nxp), ktr, ktl, x, y, ijbdy, grnormx, grnormy, xghost&
            , yghost) 
!
         call bdycoef ((-1), ktr, ktl - 1, ijbdy, x, y, xghost, yghost, cx3, &
            cx4, cx1, cx2, cy3, cy4, cy1, cy2, cr3, cr4, cr1, cr2, vol, vvol, &
            cyl) 
!
      endif 
!
!     left boundary
!
      call ghostpts (1, ktl, kbl, x, y, ijbdy, grnormx, grnormy, xghost, yghost&
         ) 
!
      call bdycoef ((-nxp) - 1, ktl, kbl - 1, ijbdy, x, y, xghost, yghost, cx4&
         , cx1, cx2, cx3, cy4, cy1, cy2, cy3, cr4, cr1, cr2, cr3, vol, vvol, &
         cyl) 
!      do j=1,nyp
!      i=2
!      ij=(j-1)*nxp+(i-1)+1
!      imj=ij-1
!      imjm=ij-1-nxp
!      cy3(imj)=-cy2(imjm)
!      enddo
!
      if (periodic) then 
!
!     set geometric coefficients in the corners
!
         ndelx = (ny - 1)*(nx + 1) 
!
         cx1(1) = cx1(1+ndelx) 
         cr1(1) = cr1(1+ndelx) 
         cy1(1) = cy1(1+ndelx) 
!
         cx2(1) = cx2(1+ndelx) 
         cr2(1) = cr2(1+ndelx) 
         cy2(1) = cy2(1+ndelx) 
!
         cx1(nxp+1+ndelx) = cx1(nxp+1) 
         cr1(nxp+1+ndelx) = cr1(nxp+1) 
         cy1(nxp+1+ndelx) = cy1(nxp+1) 
!
         cx2(nxp+1+ndelx) = cx2(nxp+1) 
         cr2(nxp+1+ndelx) = cr2(nxp+1) 
         cy2(nxp+1+ndelx) = cy2(nxp+1) 
!
         cx3(nxp) = cx3(nxp+ndelx) 
         cr3(nxp) = cr3(nxp+ndelx) 
         cy3(nxp) = cy3(nxp+ndelx) 
!
         cx4(nxp) = cx4(nxp+ndelx) 
         cr4(nxp) = cr4(nxp+ndelx) 
         cy4(nxp) = cy4(nxp+ndelx) 
!
         cx3(2*nxp+ndelx) = cx3(2*nxp) 
         cr3(2*nxp+ndelx) = cr3(2*nxp) 
         cy3(2*nxp+ndelx) = cy3(2*nxp) 
!
         cx4(2*nxp+ndelx) = cx4(2*nxp) 
         cr4(2*nxp+ndelx) = cr4(2*nxp) 
         cy4(2*nxp+ndelx) = cy4(2*nxp) 
!
      endif 
!     set volumes in ghost cells
!
      call bcc (nx, ny, vol, 0, 0, 0, 0, periodic) 
!     *********************************
!     calculate the vertex volume
!     *********************************
!
      call vtxvol (nx, ny, vol, vvol) 
!
      if (periodic) then 
         ijb = nxp + 1 
         ijt = ny*nxp + 1 
         do i = 1, nxp 
            vvol(ijb) = vvol(ijb) + vvol(ijt) 
            vvol(ijt) = vvol(ijb) 
            ijb = ijb + 1 
            ijt = ijt + 1 
         end do 
      endif 
!
      return  
      end subroutine newgeom 


      subroutine vtxvol(nx, ny, vol, vvol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      real(double) , intent(in) :: vol(*) 
      real(double) , intent(inout) :: vvol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, nyp, iwid, jwid, i, j, ij, ipj, ipjp, ijp 
!-----------------------------------------------
!
      nxp = nx + 1 
      nyp = ny + 1 
      iwid = 1 
      jwid = nxp 
!
      do i = 1, nxp 
         do j = 1, nyp 
            ij = i + (j - 1)*nxp 
            vvol(ij) = 0. 
         end do 
      end do 
 
      do i = 2, nx 
         do j = 2, ny 
            ij = i + (j - 1)*nxp 
            ipj = ij + iwid 
            ipjp = ij + iwid + jwid 
            ijp = ij + jwid 
            vvol(ij) = vvol(ij) + vol(ij)*0.25 
            vvol(ipj) = vvol(ipj) + vol(ij)*0.25 
            vvol(ipjp) = vvol(ipjp) + vol(ij)*0.25 
            vvol(ijp) = vvol(ijp) + vol(ij)*0.25 
!
         end do 
      end do 
!
      return  
      end subroutine vtxvol 


 
      subroutine gradp(nqi, nqj, i1, i2, j1, j2, cx1, cx2, cx3, cx4, cy1, cy2, &
         cy3, cy4, cr1, cr2, cr3, cr4, pixx, pixy, piyx, piyy, pixz, piyz, &
         gradpx, gradpy, gradpz) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: pixx(*) 
      real(double) , intent(in) :: pixy(*) 
      real(double) , intent(in) :: piyx(*) 
      real(double) , intent(in) :: piyy(*) 
      real(double) , intent(in) :: pixz(*) 
      real(double) , intent(in) :: piyz(*) 
      real(double) , intent(out) :: gradpx(*) 
      real(double) , intent(out) :: gradpy(*) 
      real(double) , intent(out) :: gradpz(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, ij, imj, imjm, ijm 
!-----------------------------------------------
!
      do j = j1, j2 
         do i = i1, i2 
!
            ij = nqj*(j - 1) + nqi*(i - 1) + 1 
            imj = ij - nqi 
            imjm = ij - nqj - nqi 
            ijm = ij - nqj 
!
            gradpx(ij) = -((cx4(ij)+cr4(ij))*pixx(ij)+cy4(ij)*piyx(ij)+(cx1(imj&
               )+cr1(imj))*pixx(imj)+cy1(imj)*piyx(imj)+(cx2(imjm)+cr2(imjm))*&
               pixx(imjm)+cy2(imjm)*piyx(imjm)+(cx3(ijm)+cr3(ijm))*pixx(ijm)+&
               cy3(ijm)*piyx(ijm)) 
!
            gradpy(ij) = -((cx4(ij)+cr4(ij))*pixy(ij)+cy4(ij)*piyy(ij)+(cx1(imj&
               )+cr1(imj))*pixy(imj)+cy1(imj)*piyy(imj)+(cx2(imjm)+cr2(imjm))*&
               pixy(imjm)+cy2(imjm)*piyy(imjm)+(cx3(ijm)+cr3(ijm))*pixy(ijm)+&
               cy3(ijm)*piyy(ijm)) 
!
            gradpz(ij) = -((cx4(ij)+cr4(ij))*pixz(ij)+cy4(ij)*piyz(ij)+(cx1(imj&
               )+cr1(imj))*pixz(imj)+cy1(imj)*piyz(imj)+(cx2(imjm)+cr2(imjm))*&
               pixz(imjm)+cy2(imjm)*piyz(imjm)+(cx3(ijm)+cr3(ijm))*pixz(ijm)+&
               cy3(ijm)*piyz(ijm)) 
!
         end do 
      end do 
!
      return  
      end subroutine gradp 


      subroutine xgrid(j1, j2, a, b, c, d, e, f, w, x, xn) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: j1 
      integer  :: j2 
      real(double)  :: a(*) 
      real(double)  :: b(*) 
      real(double)  :: c(*) 
      real(double)  :: d(*) 
      real(double)  :: e(*) 
      real(double)  :: f(*) 
      real(double) , intent(in) :: w(*) 
      real(double)  :: x(*) 
      real(double)  :: xn(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j 
      real(double) :: tiny 
!-----------------------------------------------
!
      tiny = 1.E-2 
!
      do j = j1, j2 
!
         a(j) = w(j-1) 
         b(j) = w(j) + w(j-1) + tiny 
         c(j) = w(j) 
         d(j) = tiny*xn(j) 
!
      end do 
!
      e(j1-1) = 0.0 
      f(j1-1) = xn(j1-1) 
      call tridiag (1, j1, j2, 0.D0, x(j2+1), a, b, c, d, e, f, xn) 
!
      return  
      end subroutine xgrid 


      subroutine guessk(nx, ny, ktl, ktr, kbr, kbl, inew, jnew, kstart) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer , intent(in) :: ktl 
      integer , intent(in) :: ktr 
      integer , intent(in) :: kbr 
      integer , intent(in) :: kbl 
      integer , intent(in) :: inew 
      integer , intent(in) :: jnew 
      integer , intent(out) :: kstart 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: kadd 
!-----------------------------------------------
      kstart = 1 
      if (inew < 2) then 
         kadd = ny - jnew - 2 
         kadd = min(ny,kadd) 
         kadd = max(0,kadd) 
         kstart = ktl + kadd 
      endif 
!
      if (jnew > ny) then 
         kadd = nx - inew - 2 
         kadd = min(nx,kadd) 
         kadd = max(0,kadd) 
         kstart = ktr + kadd 
      endif 
!
      if (inew > nx) then 
         kadd = jnew - 2 
         kadd = min(ny,kadd) 
         kadd = max(0,kadd) 
         kstart = kbr + kadd 
      endif 
!
      if (jnew < 2) then 
         kadd = inew - 4 
         kadd = min(nx,kadd) 
         kadd = max(0,kadd) 
         kstart = kbl + kadd 
      endif 
!
      if (inew<2 .and. jnew<2) then 
         kadd = ny - jnew - 4 
         kadd = min(ny,kadd) 
         kadd = max(0,kadd) 
         kstart = ktl + kadd 
      endif 
!
      return  
      end subroutine guessk 

      subroutine initpar 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, np, ij, ipsrch, j, ipj, ijp, ipjp, kr, nv, klimit, k, ir, &
         kv, l 
      real(double) :: xmean, x1, x2, x3, x4, y1, y2, y3, y4, area, parea, the, &
         eta, omt, ome, xpt, ypt, xss, yss, zvm, rvm, rvn, zvn, dr, dz, drp, &
         zrp, cxp, rse, rsi, omega, rsq, vmag, vtherml, ws, th, dist 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      real(double) , external :: ranf 
!-----------------------------------------------

!
!
!     iphead.....pointer from each cell to linked list of particles in
!     that cell
!     xp,yp......coordinates of particle in grid
!     isp........species of particle
!     link.......pointer to next particle in linked list
!     npmax......maximum number of particles for entire grid
!     nsp........number of species of particle currently allowed
!     ifrhead....head of free list of particle nodes
!
!     npcelmx...number of particles per cell initially allowed based on
!     size of grid
!     npcel......number of particles per cell requested for initializati
!     must be <= npcelmx
!
!     REGION parameters:
!     nrg........number of regions
!     nvg........number of vertices in polygon description of region
!     anpar......number of particles in region
!     rhr........density of region
!     rvi........r coordinates of vertices in region
!     zvi........z coordinates of vertices in region
!     uvi........mean r velocity of region
!     vvi........mean z velocity of region
!     icoi.......color of particles in region (specie no.)
!     siep.......internal energy/mass of region
!
!     rex........exterior r axis of ellipse for region
!     rey........exterior z axis of ellipse for region
!     rix........interior r axis of ellipse for region
!     riy........interior r axis of ellipse for region
!     xcenter....r coordinate of center of ellipse of region
!     ycenter....z coordinate of center of ellipse of region
!     INJECTION parameters
!     inj........injection flag
!     isqin......injection ...
!     icoinf.....injection color
!     riinj......inner radius of injection
!     reinj......outer radius of injection
 
      if (history) then 
         do i = 1, ntpltp 
            dmin(i) = 1.0E+08 
         end do 
      endif 
      pi = acos(-1.) 
      voldust_exact = 0. 
!
 
      null = 0 
!      isq=sqrt(float(npcel(kr)))
!
      np = 0 
      do ij = 1, nxyp 
         u(ij) = 0.0 
         v(ij) = 0.0 
         iphead(ij) = 0 
      end do 
      iphead(1) = 1 
      do np = 1, npart 
         xp(np) = 1.0 
         yp(np) = 1.0 
         ico(np) = 1 
         istatus(np) = 0 
         link(np) = np + 1 
      end do 
      xmean = 0.5*(xr - xl) 
      link(npart) = 0 
      ipsrch = 0 
      np = iphead(1) 
      do j = 2, ny 
         do i = 2, nx 
 
            ij = (j - 1)*nxp + i 
            ipj = ij + 1 
            ijp = ij + nxp 
            ipjp = ijp + 1 
 
            x1 = xn(ipj) 
            x2 = xn(ipjp) 
            x3 = xn(ijp) 
            x4 = xn(ij) 
!
            y1 = yn(ipj) 
            y2 = yn(ipjp) 
            y3 = yn(ijp) 
            y4 = yn(ij) 
!
 
            area = 0.5*((x1 - x3)*(y2 - y4) - (x2 - x4)*(y1 - y3)) 
 
            do kr = 1, nrg 
               isq = sqrt(float(npcel(kr))) 
               nv = nvg(kr) 
               parea = area/float(isq*isq) 
               klimit = isq*isq 

               l300: do k = 1, klimit 
                  ir = (k - 1)/isq 
!
!     special particle initiation.
!     for this to work, npcel must be a perfect square.
!
 
                  the = (0.5 + float(mod(k - 1,isq)))/float(isq) 
                  eta = (0.5 + float(ir))/float(isq) 
 
                  omt = 1. - the 
                  ome = 1. - eta 
 
                  xpt = i + the 
                  ypt = j + eta 
 
                  xss = the*ome*x1 + eta*the*x2 + eta*omt*x3 + ome*omt*x4 
                  yss = the*ome*y1 + eta*the*y2 + eta*omt*y3 + ome*omt*y4 
 
                  zvm = zvi(nv,kr) 
                  rvm = rvi(nv,kr) 
                  do kv = 1, nv 
                     rvn = rvi(kv,kr) 
                     zvn = zvi(kv,kr) 
                     dr = rvn - rvm 
                     dz = zvn - zvm 
                     drp = xss - rvm 
                     zrp = yss - zvm 
                     cxp = dr*zrp - dz*drp 
                     if (cxp < 0.0) cycle  l300 
                     rse = ((xss - xcenter(kr))/(rex(kr)+1.E-10))**2 + ((yss - &
                        ycenter(kr))/(rey(kr)+1.E-10))**2 
                     rsi = ((xss - xcenter(kr))/(rix(kr)+1.E-10))**2 + ((yss - &
                        ycenter(kr))/(riy(kr)+1.E-10))**2 
                     if (rsi<1. .or. rse>1.) cycle  l300 
                     zvm = zvn 
                     rvm = rvn 
                  end do 
!
!     load all particles into a holding array
!
                  call chnglst (np, iphead(1), iphead(ij), link(np)) 

                  ico(np) = icoi(kr) 
                  !if(icoi(kr).eq.3) write(*,*)'WARNING creation of dust',diem(kr)*parea*(cyl*xss + 1. - cyl)
                  qpar(np) = (cyl*xss + 1. - cyl)*parea*rhr(kr)*sign(1.,qom(ico&
                     (np))) 
!                diepar(np)=diem(kr)/npcel(kr)
                  diepar(np) = diem(kr)*parea*(cyl*xss + 1. - cyl) 
                  
                  if (diepar(np) > chimin*parea*(cyl*xss + 1. - cyl)) then
                  voldust_exact = voldust_exact + parea*(cyl*xss + 1. - cyl)
                  end if
                   
                  omega = uvi(kr)/rex(kr) 
                  rsq = (xss - xcenter(kr))**2 + (yss - ycenter(kr))**2 - rex(&
                     kr)**2 
!
!     set velocities from a maxwellian distribution
!
                  vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
                  vtherml = siep(kr) 
                  ws = vtherml*vmag 
                  th = 2*pi*ranf() 
                  up(np) = uvi(kr) + ws*cos(th) 
                  vp(np) = vvi(kr) + ws*sin(th) 
                  vmag = sqrt((-2.*log(1. - 0.999999*ranf()))) 
                  vtherml = siep(kr) 
                  ws = vtherml*vmag 
                  th = 2*pi*ranf() 
                  wp(np) = ws*cos(th) 
!
!     set positions
!
                  xpf(np) = xss + dt/2.*up(np) 
                  ypf(np) = yss + dt/2.*vp(np) 
!      xpf(np)=xss
!      ypf(np)=yss
                  xp(np) = i + the 
                  yp(np) = j + eta 
 
 
                  if (history) then 
                     do l = 1, ntpltp 
                        if (iregp(l) /= kr) cycle  
                        tplt(1) = t 
                        dist = sqrt((xpinit(l)-xss)**2+(ypinit(l)-yss)**2) 
                        dmin(l) = min(dmin(l),dist) 
                        if (dmin(l) /= dist) cycle  
                        npp(l) = np 
                     end do 
                  endif 
                  np = iphead(1) 
                  if (np == 0) go to 910 
 
 
 
               end do l300 
            end do 
 
 
         end do 
      end do 
!
      do ij = 1, nxyp 
         xn(ij) = x(ij) 
         yn(ij) = y(ij) 
      end do 
!
      iphd2(1) = iphead(1) 
!      call locator_kin(ipsrch)
!
      write (*, *) 'voldust_exact', voldust_exact 
      return  
  910 continue 
      write (59, 999) i, j, ij, iphead(ij), iphead(1), npcel, kr, k 
  999 format(' init',10i5) 
      stop  
 
      end subroutine initpar 

      subroutine inject(xp, xpv, link, np, npart, ntmp, lvmax) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(inout) :: np 
      integer , intent(in) :: npart 
      integer , intent(in) :: ntmp 
      integer , intent(in) :: lvmax 
      integer , intent(in) :: link(1) 
      real(double) , intent(out) :: xp(1) 
      real(double) , intent(in) :: xpv(1) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: lv, local, nps, nvar 
!-----------------------------------------------
!
!     a routine to transfer information from a local list
!
      do lv = 1, lvmax 
         local = lv 
         nps = np 
         do nvar = 1, 7 
            xp(nps) = xpv(local) 
            local = local + ntmp 
            nps = nps + npart 
         end do 
         np = link(np) 
      end do 
!
      return  
      end subroutine inject 


      subroutine listmkr(np, nploc, link, lvmax, ntmp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: np 
      integer , intent(out) :: lvmax 
      integer , intent(in) :: ntmp 
      integer , intent(out) :: nploc(ntmp) 
      integer , intent(in) :: link(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nps 
!-----------------------------------------------
!
!      get particles from linked list &  place indices in table nploc
!
!      INPUTS:
!
!        np      :      first particle index
!        link  :      array of links to next partcile
!        ntmp  :      dimension of nploc array
!
!      OUTPUTS:
!
!        nploc      :      array of particle indices
!        lvmax      :      no of particles in nploc
 
      lvmax = 0 
      nps = np 
      nploc(1) = nps 
    1 continue 
      if (nps > 0) then 
         lvmax = lvmax + 1 
         if (lvmax < ntmp) then 
            nps = link(nps) 
            nploc(lvmax+1) = nps 
            go to 1 
         endif 
      endif 
!
      lvmax = min0(ntmp,lvmax) 
!
      return  
      end subroutine listmkr 


      subroutine listmov(iphed0, iphed1, link, lvmax) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(inout) :: iphed0 
      integer , intent(inout) :: iphed1 
      integer , intent(in) :: lvmax 
      integer , intent(inout) :: link(1) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: lv, np 
!-----------------------------------------------
!
!     a routine to move particles from one list to another
!
      do lv = 1, lvmax 
         np = iphed0 
         iphed0 = link(np) 
         link(np) = iphed1 
         iphed1 = np 
      end do 
!
      return  
      end subroutine listmov 


!
      subroutine locale(nqi, nqj, xpc, ypc, dr, dz, ichead, jchead, inew, jnew) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(out) :: inew 
      integer , intent(out) :: jnew 
      real(double) , intent(in) :: xpc 
      real(double) , intent(in) :: ypc 
      real(double) , intent(in) :: dr 
      real(double) , intent(in) :: dz 
      integer , intent(in) :: ichead(*) 
      integer , intent(in) :: jchead(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ipar, jpar, ijpar 
!-----------------------------------------------
!
      ipar = int(xpc/dr + 2. + 1.E-10) 
      jpar = int(ypc/dz + 2. + 1.E-10) 
      ijpar = (jpar - 1)*nqj + (ipar - 1)*nqi + 1 
!
      inew = ichead(ijpar) 
      jnew = jchead(ijpar) 
!
      return  
      end subroutine locale 


      subroutine localev(nqi, nqj, lvs, lve, xpc, ypc, dr, dz, inew, jnew, &
         xorigin, yorigin, xpv, ypv) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: nqi 
      integer  :: nqj 
      integer , intent(in) :: lvs 
      integer , intent(in) :: lve 
      real(double) , intent(in) :: dr 
      real(double) , intent(in) :: dz 
      real(double) , intent(in) :: xorigin 
      real(double) , intent(in) :: yorigin 
      integer , intent(out) :: inew(*) 
      integer , intent(out) :: jnew(*) 
      real(double) , intent(in) :: xpc(*) 
      real(double) , intent(in) :: ypc(*) 
      real(double) , intent(inout) :: xpv(*) 
      real(double) , intent(inout) :: ypv(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: lv 
!-----------------------------------------------
!
      do lv = lvs, lve 
         xpv(lv) = (xpc(lv)-xorigin)/dr + 2. 
         ypv(lv) = (ypc(lv)-yorigin)/dz + 2. 
         inew(lv) = int(xpv(lv)) 
         jnew(lv) = int(ypv(lv)) 
!
      end do 
!
      return  
      end subroutine localev 


      subroutine locatx(nx, ny, lvmax, xn, yn, inew, jnew, xpc, ypc, the, zeta&
         , testx, testy) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer  :: lvmax 
      integer  :: inew(*) 
      integer  :: jnew(*) 
      real(double)  :: xn(*) 
      real(double)  :: yn(*) 
      real(double)  :: xpc(*) 
      real(double)  :: ypc(*) 
      real(double)  :: the(*) 
      real(double)  :: zeta(*) 
      real(double) , intent(out) :: testx(*) 
      real(double) , intent(out) :: testy(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, itry, lv, ichange, jchange 
      real(double) :: succes, eps 
!-----------------------------------------------
!
!      a routine to locate a point in a computation mesh of quadrilat
!         and calculate its natural coordinates
!
!      INPUTS:
!
!     inew,jnew:  arbitrary input cell indices
!     xpc,ypc:     input physical coordinates of point
!
!      OUTPUTS:
!
!     the,eta:    natural coordinates of point
!
      nxp = nx + 1 
      itry = 0 
      succes = 0. 
      eps = 1.E-3 
      do lv = 1, lvmax 
         inew(lv) = max(2,inew(lv)) 
         inew(lv) = min(nx,inew(lv)) 
         jnew(lv) = max(2,jnew(lv)) 
         jnew(lv) = min(ny,jnew(lv)) 
      end do 
!
      call mapl (xn, yn, inew, jnew, nxp, xpc, ypc, the, zeta, 1, lvmax) 
!
!     check transform
      do lv = 1, lvmax 
         testx(lv) = (the(lv)+eps)*(1. + eps - the(lv)) 
         testy(lv) = (zeta(lv)+eps)*(1. + eps - zeta(lv)) 
!
!
!        transform meaningful, indicates direction to search
!
         ichange = 0.5*int(sign(1.,the(lv)-1.-eps)+sign(1.,the(lv)+eps)) 
         jchange = 0.5*int(sign(1.,zeta(lv)-1.-eps)+sign(1.,zeta(lv)+eps)) 
!
 
         inew(lv) = inew(lv) + ichange 
         jnew(lv) = jnew(lv) + jchange 
      end do 
!
      return  
      end subroutine locatx 


      subroutine mapv(x, y, ij, nxp, xp, yp, the, zeta, lvb, lve) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ij 
      integer , intent(in) :: nxp 
      integer , intent(in) :: lvb 
      integer , intent(in) :: lve 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
      real(double) , intent(out) :: the(*) 
      real(double) , intent(out) :: zeta(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ipj, ipjp, ijp, lv 
      real(double) :: x1, x2, x3, x4, y1, y2, y3, y4, dx1, dx2, dx12, dy1, dy2&
         , dy12, xc, yc, a, area, delx, dely, b, c, discrim, wsthe 
!-----------------------------------------------
!
!      INPUT:
!            x,y:      physical cell coordinates
!            ij :      cell index
!            nxp :      number of cells in row
!            xp,yp :      arrays of particle positions
!            lvb,lve : minimum and maximum vallues of xp,yp arrays
!
!      OUTPUT:
!            the, eta :  array of natural coordinates of particles
!
!
!
!
!     calculate cell geometry
!
      ipj = ij + 1 
      ipjp = ij + nxp + 1 
      ijp = ij + nxp 
!
      x1 = x(ipj) 
      x2 = x(ipjp)
      x3 = x(ijp) 
      x4 = x(ij) 
!
      y1 = y(ipj) 
      y2 = y(ipjp) 
      y3 = y(ijp) 
      y4 = y(ij) 
!
      dx1 = 0.5*(x1 + x2 - x3 - x4) 
      dx2 = 0.5*(x2 + x3 - x4 - x1) 
      dx12 = x2 - x1 + x4 - x3 
      dy1 = 0.5*(y1 + y2 - y3 - y4) 
      dy2 = 0.5*(y2 + y3 - y4 - y1) 
      dy12 = y2 - y1 + y4 - y3 
!
      xc = 0.25*(x1 + x2 + x3 + x4) 
      yc = 0.25*(y1 + y2 + y3 + y4) 
!     new particle position in natural coordinates
!
      a = dx1*dy12 - dy1*dx12 
      area = dx1*dy2 - dx2*dy1 
      do lv = lvb, lve 
         delx = xp(lv) - xc 
         dely = yp(lv) - yc 
         b = area + dx12*dely - dy12*delx 
         c = dx2*dely - dy2*delx 
         discrim = max(0.,b**2 - 4.*a*c) 
         wsthe = 2.*c/((-b) - sign(1.,b)*sqrt(discrim)) 
         zeta(lv) = (delx + dely - wsthe*(dx1 + dy1))/((dx2 + dy2 + (dx12 + &
            dy12)*wsthe) + 1.E-10) + 0.5 
!
         the(lv) = wsthe + 0.5 
      end do 
      return  
      end subroutine mapv 



subroutine mapmove(x, y, ij, nxp, xp, yp, the, zeta, lvb, lve,move) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: ij 
      integer , intent(in) :: nxp 
      integer , intent(in) :: lvb 
      integer , intent(in) :: lve 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
      real(double) , intent(out) :: the(*) 
      real(double) , intent(out) :: zeta(*) 
      logical :: move(*)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ipj, ipjp, ijp, lv 
      real(double) :: x1, x2, x3, x4, y1, y2, y3, y4, dx1, dx2, dx12, dy1, dy2&
         , dy12, xc, yc, a, area, delx, dely, b, c, discrim, wsthe 
!-----------------------------------------------
!
!      INPUT:
!            x,y:      physical cell coordinates
!            ij :      cell index
!            nxp :      number of cells in row
!            xp,yp :      arrays of particle positions
!            lvb,lve : minimum and maximum vallues of xp,yp arrays
!
!      OUTPUT:
!            the, eta :  array of natural coordinates of particles
!
!
!
!
!     calculate cell geometry
!
      ipj = ij + 1 
      ipjp = ij + nxp + 1 
      ijp = ij + nxp 
!
      x1 = x(ipj) 
      x2 = x(ipjp)
      x3 = x(ijp) 
      x4 = x(ij) 
!
      y1 = y(ipj) 
      y2 = y(ipjp) 
      y3 = y(ijp) 
      y4 = y(ij) 
!
      dx1 = 0.5*(x1 + x2 - x3 - x4) 
      dx2 = 0.5*(x2 + x3 - x4 - x1) 
      dx12 = x2 - x1 + x4 - x3 
      dy1 = 0.5*(y1 + y2 - y3 - y4) 
      dy2 = 0.5*(y2 + y3 - y4 - y1) 
      dy12 = y2 - y1 + y4 - y3 
!
      xc = 0.25*(x1 + x2 + x3 + x4) 
      yc = 0.25*(y1 + y2 + y3 + y4) 
!     new particle position in natural coordinates
!
      a = dx1*dy12 - dy1*dx12 
      area = dx1*dy2 - dx2*dy1 
      do lv = lvb, lve
         if(.not.move(lv)) cycle
         delx = xp(lv) - xc 
         dely = yp(lv) - yc 
         b = area + dx12*dely - dy12*delx 
         c = dx2*dely - dy2*delx 
         discrim = max(0.,b**2 - 4.*a*c) 
         wsthe = 2.*c/((-b) - sign(1.,b)*sqrt(discrim)) 
         zeta(lv) = (delx + dely - wsthe*(dx1 + dy1))/((dx2 + dy2 + (dx12 + &
            dy12)*wsthe) + 1.E-10) + 0.5 
!
         the(lv) = wsthe + 0.5 
      end do 
      return  
      end subroutine mapmove 

      subroutine mapl(x, y, inew, jnew, nxp, xp, yp, the, zeta, lvb, lve) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nxp 
      integer , intent(in) :: lvb 
      integer , intent(in) :: lve 
      integer , intent(in) :: inew(*) 
      integer , intent(in) :: jnew(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
      real(double) , intent(out) :: the(*) 
      real(double) , intent(out) :: zeta(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: lv, ij, ipj, ipjp, ijp 
      real(double) :: x1, x2, x3, x4, y1, y2, y3, y4, dx1, dx2, dx12, dy1, dy2&
         , dy12, xc, yc, a, area, delx, dely, b, c, discrim, wsthe 
!-----------------------------------------------
!
!      INPUT:
!            x,y:      physical cell coordinates
!            ij :      cell index
!            nxp :      number of cells in row
!            xp,yp :      arrays of particle positions
!            lvb,lve : minimum and maximum vallues of xp,yp arrays
!
!      OUTPUT:
!            the, eta :  array of natural coordinates of particles
!
!
!
!
!     calculate cell geometry
!
      do lv = lvb, lve 
         ij = (jnew(lv)-1)*nxp + inew(lv) 
         ipj = ij + 1 
         ipjp = ij + nxp + 1 
         ijp = ij + nxp 
!
         x1 = x(ipj) 
         x2 = x(ipjp) 
         x3 = x(ijp) 
         x4 = x(ij) 
!
         y1 = y(ipj) 
         y2 = y(ipjp) 
         y3 = y(ijp) 
         y4 = y(ij) 
!
         dx1 = 0.5*(x1 + x2 - x3 - x4) 
         dx2 = 0.5*(x2 + x3 - x4 - x1) 
         dx12 = x2 - x1 + x4 - x3 
         dy1 = 0.5*(y1 + y2 - y3 - y4) 
         dy2 = 0.5*(y2 + y3 - y4 - y1) 
         dy12 = y2 - y1 + y4 - y3 
!
         xc = 0.25*(x1 + x2 + x3 + x4) 
         yc = 0.25*(y1 + y2 + y3 + y4) 
!     new particle position in natural coordinates
!
         a = dx1*dy12 - dy1*dx12 
         area = dx1*dy2 - dx2*dy1 
         delx = xp(lv) - xc 
         dely = yp(lv) - yc 
         b = area + dx12*dely - dy12*delx 
         c = dx2*dely - dy2*delx 
         discrim = max(0.,b**2 - 4.*a*c) 
         wsthe = 2.*c/((-b) - sign(1.,b)*sqrt(discrim)) 
         zeta(lv) = (delx + dely - wsthe*(dx1 + dy1))/((dx2 + dy2 + (dx12 + &
            dy12)*wsthe) + 1.E-10) + 0.5 
!
         the(lv) = wsthe + 0.5 
      end do 
      return  
      end subroutine mapl 


      subroutine moveout 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
      return  
      end subroutine moveout 


 
      subroutine nbdhood(i1, i2, j1, j2, nqi, nqj, dr, dz, x, y, ichead, jchead&
         ) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      real(double) , intent(in) :: dr 
      real(double) , intent(in) :: dz 
      integer , intent(out) :: ichead(*) 
      integer , intent(out) :: jchead(*) 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(in) :: y(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, ij, i, iref, jref, ijref 
!-----------------------------------------------
!
      do j = j1, j2 
         ij = nqj*(j - 1) + 1 
         do i = i1, i2 
!
!
            iref = int(x(ij)/dr+2.+1.E-10) 
            jref = int(y(ij)/dz+2.+1.E-10) 
            ijref = (jref - 1)*nqj + (iref - 1)*nqi + 1 
!
            ichead(ijref) = i 
            jchead(ijref) = j 
!
            ij = ij + nqi 
         end do 
      end do 
!
      return  
      end subroutine nbdhood 


      subroutine newcyc 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      real(4) :: time, tmodule, told, tleft 
!-----------------------------------------------

 
      data time/ 0./  
!     +++
!     +++ begin cycle - provide monitor print, then test for
!     +++ output and run termination.  if continuing, increment
!     +++ time and cycle number . . .
!     +++
      if (ndump == ndumpsav) then 
!
         if (idone /= 0) then 
!               call fulout(0)
            call gdone 
            call exita (4) 
            stop '4' 
         endif 
 
         if (plots .and. ncyc>=1) then 
            if (ncyc == 1) then 
               call fulout (0) 
            else 
               tmodule = mod(t,twfilm) 
!      write(*,*)tmodule
               if ((-tmodule) + twfilm<dt/2. .or. tmodule<dt/2.) then 
                  call fulout (0) 
               else 
                  call fulout (-1) 
               endif 
            endif 
         endif 
 
         if (mod(ncyc,25)==0 .or. t>=twfin) then 
            write (59, 100) ncyc, t, dt, numit, grind, iddt 
            write (6, 100) ncyc, t, dt, numit, grind, iddt 
!           write(59,120) (angmom(kr),kr=1,3), totam,angmomv
!           write(6,120) (angmom(kr),kr=1,3), totam,angmomv
  120       format("angular mom=",3(1p,e10.3)," total=",1p,e10.3,&
               "                   vertex=",1p,e10.3) 
!           write(59,130) circlate,enstroph,dkdt,effvisc,diss
!           write(6,130) circlate,enstroph,dkdt,effvisc,diss
  130       format(" circ,enstrophy,dkdt,effvisc,diss=",5(1p,e10.3)) 
         endif 
!         if(t.ge.twfilm.and.plots) call fulout(12)
         if (t>=twhist .and. history) call ptime 
         if (movie) then 
            if (t >= twmovie) call moveout 
         endif 
!        if(t.ge.twprtr) call fulout(6)
         told = time 
         call second (time) 
         grind = (time - told)*grfac 
         tleft = timlmt - time 
!        call gettr(tleft)
         if (tleft < 60) then 
            twfin = t 
            if (tlimd == 1) then 
               call tapewr 
               stop  
            else 
!               call fulout(0)
               call ptime 
               call gdone 
!               call exit(4)
               stop '4' 
            endif 
         endif 
      endif 
      if (t < twfin) then 
         t = t + dt 
         ndumpsav = ndump 
         ncyc = ncyc + 1 
         return  
      endif 
!
!     termination
      write (59, 110) 
      write (6, 110) 
!      if(t.lt.twfilm.and.plots) call fulout(12)
!     if(t.lt.twprtr) call fulout(6)
      if (history) call ptime 
      call dumpone 
      call exita (1) 
      stop '1' 
  100 format(' ncyc',i6,' t=',1p,e12.5,' dt=',e12.5,' numit=',i4,' grind=',0p,e&
         15.5,1x,a1) 
  110 format(' normal termination') 
      return  
      end subroutine newcyc 


!
! $Header: /n/cvs/democritus/toolbin.f90,v 1.8 2006/11/30 21:33:31 lapenta Exp $
      subroutine ptime 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
!     ==================================================================
!
!     purpose -
!     dump various properties to particles which have been tagged
!     and save them for future time plots
!
!     ptime is called by -
!
!     ptime calls the following routines and functions -
!     ==================================================================
!
!************
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: li, np, lp 
!-----------------------------------------------
!

!************
!     <><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
!
      twhist = twhist + thist 
      npltpt = npltpt + 1 
      tplt(npltpt) = t 
!
      do li = 1, ntpltp 
         np = npp(li) 
         lp = li 
 
         xmix(npltpt+(lp-1)*ntplt) = xpf(np) 
         ymix(npltpt+(lp-1)*ntplt) = ypf(np) 
         umix(npltpt+(lp-1)*ntplt) = up(np) 
         vmix(npltpt+(lp-1)*ntplt) = vp(np) 
 
      end do 
!
      if (npltpt==ntplt .or. t>=twfin) then 
!     call fadv(1)
!         call timplt(name,jnm,d1,c1,ntplt,ntpltp,npltpt,nstat,
!     &        xplt,yplt,zplt,npp,iregp,xmix,
!     &        ymix,umix,vmix,pmix,rhmix,siemix,vortex,
!     &        tplt,xwindo,ywindo)
         if (t < twfin) npltpt = 0 
      endif 
      return  
      end subroutine ptime 


 
      subroutine rolinear(i1, i2, xl, xr, romin, romax, x, ro) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(in) :: xl 
      real(double) , intent(in) :: xr 
      real(double) , intent(in) :: romin 
      real(double) , intent(in) :: romax 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(out) :: ro(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
      real(double) :: xc 
!-----------------------------------------------
!
      do i = i1, i2 
         xc = 0.5*(x(i+1)+x(i)) 
         ro(i) = romin + (romax - romin)*(xc - xr)/(xl - xr) 
      end do 
!
      return  
      end subroutine rolinear 


      subroutine roexp(i1, i2, xl, xr, rho0, roscal, x, ro) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(in) :: xl 
      real(double)  :: xr 
      real(double) , intent(in) :: rho0 
      real(double) , intent(in) :: roscal 
      real(double) , intent(in) :: x(*) 
      real(double) , intent(out) :: ro(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i 
      real(double) :: xc 
!-----------------------------------------------
!
      do i = i1, i2 
         xc = 0.5*(x(i+1)+x(i)) 
         ro(i) = rho0*(1. + exp((xl - xc)/roscal)) 
      end do 
!
      return  
      end subroutine roexp 

      subroutine setzero(q, nxyp) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nxyp 
      real(double) , intent(out) :: q(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ij 
!-----------------------------------------------
      do ij = 1, nxyp 
         q(ij) = 0.0 
      end do 
!
      return  
      end subroutine setzero 


      subroutine smooth(nx, ny, i1, i2, j1, j2, smth, nsmth, periodic, sc, q) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer  :: nx 
      integer  :: ny 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: nsmth 
      real(double) , intent(in) :: smth 
      logical  :: periodic 
      real(double)  :: sc(*) 
      real(double) , intent(inout) :: q(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, ns, j, i, ij, ijp, ipj, ipjp, imjp, imj, imjm, ijm, ipjm 
      real(double) :: r16 
!-----------------------------------------------
!
      nxp = nx + 1 
      r16 = 1./16. 
      do ns = 1, nsmth 
!
         do j = j1, j2 
            do i = i1, i2 
!
               ij = (j - 1)*nxp + i 
!
               sc(ij) = q(ij) 
!
            end do 
         end do 
!
         call bcc (nx, ny, sc, 0, 0, 0, 0, periodic) 
!
         do j = 2, ny 
!
            do i = 2, nx 
!
               ij = (j - 1)*nxp + i 
               ijp = ij + nxp 
               ipj = ij + 1 
               ipjp = ijp + 1 
               imjp = ij + nxp - 1 
               imj = ij - 1 
               imjm = ij - nxp - 1 
               ijm = ij - nxp 
               ipjm = ij - nxp + 1 
!
!               q(ij) = (1. - smth)*sc(ij) + 0.125*smth*(4.*sc(ij)+sc(ipj)+sc(&
!                  ijp)+sc(imj)+sc(ijm)) 
      q(ij)=(1.-smth)*sc(ij)+smth*(sc(ij) &
           +sc(ipjm)+sc(imjp)+sc(ipjp)+sc(imjm) &
           +sc(ipj)+sc(ijp)+sc(imj)+sc(ijm))/9.
!
            end do 
         end do 
!
!
      end do 
!
      return  
      end subroutine smooth 


      subroutine stencil(nqi, nqj, i1, i2, j1, j2, istart, istop, jstart, jstop&
         , nxm, tiny, x, y, bx, by, bz, diegrid, coef, coeftrns, coefpar, a, b, c, d, e&
         , af, bf, cf, df, ef, ag, bg, cg, dg, eg, ah, bh, ch, dh, eh, cx1, cx2&
         , cx3, cx4, cr1, cr2, cr3, cr4, cy1, cy2, cy3, cy4) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: nqj 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      integer , intent(in) :: istart 
      integer  :: istop 
      integer , intent(in) :: jstart 
      integer , intent(in) :: jstop 
      integer , intent(in) :: nxm 
      real(double) , intent(in) :: tiny 
      real(double)  :: x(*) 
      real(double)  :: y(*) 
      real(double) , intent(in) :: bx(*) 
      real(double) , intent(in) :: by(*) 
      real(double)  :: bz(*) 
      real(double) , intent(in) :: coef(*) 
      real(double) , intent(in) :: coeftrns(*) 
      real(double) , intent(in) :: coefpar(*) 
	  real(double) , intent(in) :: diegrid(*) 
      real(double)  :: a(*) 
      real(double) , intent(inout) :: b(*) 
      real(double) , intent(inout) :: c(*) 
      real(double) , intent(inout) :: d(*) 
      real(double) , intent(inout) :: e(*) 
      real(double)  :: af(*) 
      real(double) , intent(inout) :: bf(*) 
      real(double) , intent(inout) :: cf(*) 
      real(double) , intent(inout) :: df(*) 
      real(double) , intent(inout) :: ef(*) 
      real(double)  :: ag(*) 
      real(double)  :: bg(*) 
      real(double)  :: cg(*) 
      real(double)  :: dg(*) 
      real(double)  :: eg(*) 
      real(double)  :: ah(*) 
      real(double)  :: bh(*) 
      real(double)  :: ch(*) 
      real(double)  :: dh(*) 
      real(double)  :: eh(*) 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, i, nm, ij, ipj, ipjp, ijp, ijm, ipjm, imjm, imj, imjp, nxym&
         , nmmin, nmmax 
      real(double) :: avga, ws 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: ismin, ismax 
!-----------------------------------------------
!
!
!
!
      do j = j1, j2 
         do i = i1, i2 
!
            nm = (j - jstart)*nxm + i - istart + 1 
            ij = nqj*(j - 1) + nqi*(i - 1) + 1 
            ipj = ij + nqi 
            ipjp = ij + nqj + nqi 
            ijp = ij + nqj 
            ijm = ij - nqj 
            ipjm = ij - nqj + 1 
            imjm = ij - nqj - nqi 
            imj = ij - nqi 
            imjp = ij + nqj - 1 
!			
!     case where the potential is set
!
	    if(max(max(diegrid(ij),diegrid(ipj)),max(diegrid(ijp),diegrid(ipjp))).lt.-1d0) then
!		if(diegrid(ij).lt.-1d0.or.diegrid(ipj).lt.-1d0.or.diegrid(ijp).lt.-1d0.or.diegrid(ipjp).lt.-1d0) then

		   a(nm)= -(diegrid(ij)+diegrid(ipj)+diegrid(ijp)+diegrid(ipjp))*.25d0
		   b(nm)=0d0
		   c(nm)=0d0
		   d(nm)=0d0
		   e(nm)=0d0
		   bf(nm)=0d0
		   cf(nm)=0d0
		   df(nm)=0d0
		   ef(nm)=0d0
		   else
		   a(nm)=0d0
		   b(nm)=0d0
		   c(nm)=0d0
		   d(nm)=0d0
		   e(nm)=0d0
		   bf(nm)=0d0
		   cf(nm)=0d0
		   df(nm)=0d0
		   ef(nm)=0d0
		end if
!
!
!     i, j
!
            a(nm) = a(nm)+ ((cx1(ij)+cr1(ij))**2+cy1(ij)**2)*coef(ipj) + ((cx2(ij)+cr2&
               (ij))**2+cy2(ij)**2)*coef(ipjp) + ((cx3(ij)+cr3(ij))**2+cy3(ij)&
               **2)*coef(ijp) + ((cx4(ij)+cr4(ij))**2+cy4(ij)**2)*coef(ij) 
!
!     i+1, j
!
            b(nm) = ((cx1(ij)+cr1(ij))*(cx4(ipj)+cr4(ipj))+cy1(ij)*cy4(ipj))*&
               coef(ipj) + ((cx2(ij)+cr2(ij))*(cx3(ipj)+cr3(ipj))+cy2(ij)*cy3(&
               ipj))*coef(ipjp) 
!
!     i-1, j+1
!
            c(nm) = ((cx3(ij)+cr3(ij))*(cx1(imjp)+cr1(imjp))+cy3(ij)*cy1(imjp))&
               *coef(ijp) 
!
!     i, j+1
!
            d(nm) = ((cx2(ij)+cr2(ij))*(cx1(ijp)+cr1(ijp))+cy2(ij)*cy1(ijp))*&
               coef(ipjp) + ((cx3(ij)+cr3(ij))*(cx4(ijp)+cr4(ijp))+cy3(ij)*cy4(&
               ijp))*coef(ijp) 
!
!     i+1, j+1
!
            e(nm) = ((cx2(ij)+cr2(ij))*(cx4(ipjp)+cr4(ipjp))+cy2(ij)*cy4(ipjp))&
               *coef(ipjp) 
!
!     i-1, j
!
            bf(nm) = ((cx3(ij)+cr3(ij))*(cx2(imj)+cr2(imj))+cy3(ij)*cy2(imj))*&
               coef(ijp) + ((cx4(ij)+cr4(ij))*(cx1(imj)+cr1(imj))+cy4(ij)*cy1(&
               imj))*coef(ij) 
!
!     i+1, j-1
!
            cf(nm) = ((cx1(ij)+cr1(ij))*(cx3(ipjm)+cr3(ipjm))+cy1(ij)*cy3(ipjm)&
               )*coef(ipj) 
!
!     i, j-1
!
            df(nm) = ((cx1(ij)+cr1(ij))*(cx2(ijm)+cr2(ijm))+cy1(ij)*cy2(ijm))*&
               coef(ipj) + ((cx4(ij)+cr4(ij))*(cx3(ijm)+cr3(ijm))+cy4(ij)*cy3(&
               ijm))*coef(ij) 
!
!     i-1, j-1
!
            ef(nm) = ((cx4(ij)+cr4(ij))*(cx2(imjm)+cr2(imjm))+cy4(ij)*cy2(imjm)&
               )*coef(ij) 
!
!
            a(nm) = a(nm) + (bx(ipj)*(cx1(ij)+cr1(ij))+by(ipj)*cy1(ij))**2*&
               coefpar(ipj) + (bx(ipjp)*(cx2(ij)+cr2(ij))+by(ipjp)*cy2(ij))**2*&
               coefpar(ipjp) + (bx(ijp)*(cx3(ij)+cr3(ij))+by(ijp)*cy3(ij))**2*&
               coefpar(ijp) + (bx(ij)*(cx4(ij)+cr4(ij))+by(ij)*cy4(ij))**2*&
               coefpar(ij) 
!
            b(nm) = b(nm) + (bx(ipj)*(cx1(ij)+cr1(ij))+by(ipj)*cy1(ij))*(bx(ipj&
               )*(cx4(ipj)+cr4(ipj))+by(ipj)*cy4(ipj))*coefpar(ipj) + (bx(ipjp)&
               *(cx2(ij)+cr2(ij))+by(ipjp)*cy2(ij))*(bx(ipjp)*(cx3(ipj)+cr3(ipj&
               ))+by(ipj)*cy3(ipj))*coefpar(ipjp) 
!
            c(nm) = c(nm) + (bx(ijp)*(cx3(ij)+cr3(ij))+by(ijp)*cy3(ij))*(bx(ijp&
               )*(cx1(imjp)+cr1(imjp))+by(ijp)*cy1(imjp))*coefpar(ijp) 
!
            d(nm) = d(nm) + (bx(ipjp)*(cx2(ij)+cr2(ij))+by(ipjp)*cy2(ij))*(bx(&
               ipjp)*(cx1(ijp)+cr1(ijp))+by(ipjp)*cy1(ijp))*coefpar(ipjp) + (bx&
               (ijp)*(cx3(ij)+cr3(ij))+by(ijp)*cy3(ij))*(bx(ijp)*(cx4(ijp)+cr4(&
               ijp))+by(ijp)*cy4(ijp))*coefpar(ijp) 
!
            e(nm) = e(nm) + (bx(ipjp)*(cx2(ij)+cr2(ij))+by(ipjp)*cy2(ij))*(bx(&
               ipjp)*(cx4(ipjp)+cr4(ipjp))+by(ipjp)*cy4(ipjp))*coefpar(ipjp) 
!
            bf(nm) = bf(nm) + (bx(ijp)*(cx3(ij)+cr3(ij))+by(ijp)*cy3(ij))*(bx(&
               ijp)*(cx2(imj)+cr2(imj))+by(ijp)*cy2(imj))*coefpar(ijp) + (bx(ij&
               )*(cx4(ij)+cr4(ij))+by(ij)*cy4(ij))*(bx(ij)*(cx1(imj)+cr1(imj))+&
               by(ij)*cy1(imj))*coefpar(ij) 
!
            cf(nm) = cf(nm) + (bx(ipj)*(cx1(ij)+cr1(ij))+by(ipj)*cy1(ij))*(bx(&
               ipj)*(cx3(ipjm)+cr3(ipjm))+by(ipj)*cy3(ipjm))*coefpar(ipj) 
!
            df(nm) = df(nm) + (bx(ipj)*(cx1(ij)+cr1(ij))+by(ipj)*cy1(ij))*(bx(&
               ipj)*(cx2(ijm)+cr2(ijm))+by(ipj)*cy2(ijm))*coefpar(ipj) + (bx(ij&
               )*(cx4(ij)+cr4(ij))+by(ij)*cy4(ij))*(bx(ij)*(cx3(ijm)+cr3(ijm))+&
               by(ij)*cy3(ijm))*coefpar(ij) 
!
            ef(nm) = ef(nm) + (bx(ij)*(cx4(ij)+cr4(ij))+by(ij)*cy4(ij))*(bx(ij)&
               *(cx2(imjm)+cr2(imjm))+by(ij)*cy2(imjm))*coefpar(ij) 
!
!
!     calculate the transverse coefficients
!
            a(nm) = a(nm) 
!
!
            b(nm) = b(nm) + ((cx1(ij)+cr1(ij))*cy4(ipj)-cy1(ij)*(cx4(ipj)+cr4(&
               ipj)))*coeftrns(ipj) + ((cx2(ij)+cr2(ij))*cy3(ipj)-cy2(ij)*(cx3(&
               ipj)+cr3(ipj)))*coeftrns(ipjp) 
!
            c(nm) = c(nm) + ((cx3(ij)+cr3(ij))*cy1(imjp)-cy3(ij)*(cx1(imjp)+cr1&
               (imjp)))*coeftrns(ijp) 
!
            d(nm) = d(nm) + ((cx3(ij)+cr3(ij))*cy4(ijp)-cy3(ij)*(cx4(ijp)+cr4(&
               ijp)))*coeftrns(ijp) + ((cx2(ij)+cr2(ij))*cy1(ijp)-cy2(ij)*(cx1(&
               ijp)+cr1(ijp)))*coeftrns(ipjp) 
!
            e(nm) = e(nm) + ((cx2(ij)+cr2(ij))*cy4(ipjp)-cy2(ij)*(cx4(ipjp)+cr4&
               (ipjp)))*coeftrns(ipjp) 
!
            bf(nm) = bf(nm) + ((cx3(ij)+cr3(ij))*cy2(imj)-cy3(ij)*(cx2(imj)+cr2&
               (imj)))*coeftrns(ijp) + ((cx4(ij)+cr4(ij))*cy1(imj)-cy4(ij)*(cx1&
               (imj)+cr1(imj)))*coeftrns(ij) 
!
            cf(nm) = cf(nm) + ((cx1(ij)+cr1(ij))*cy3(ipjm)-cy1(ij)*(cx3(ipjm)+&
               cr3(ipjm)))*coeftrns(ipj) 
!
            df(nm) = df(nm) + ((cx1(ij)+cr1(ij))*cy2(ijm)-cy1(ij)*(cx2(ijm)+cr2&
               (ijm)))*coeftrns(ipj) + ((cx4(ij)+cr4(ij))*cy3(ijm)-cy4(ij)*(cx3&
               (ijm)+cr3(ijm)))*coeftrns(ij) 
!
            ef(nm) = ef(nm) + ((cx4(ij)+cr4(ij))*cy2(imjm)-cy4(ij)*(cx2(imjm)+&
               cr2(imjm)))*coeftrns(ij) 
!
!
         end do 
      end do 
!
!
!     to cope with null space, add fictitious term to operator
!     set term to tiny*average value of diagonal
!
      nxym = (jstop - jstart + 1)*nxm 
      nmmin = ismin(nxym,a,1) 
      nmmax = ismax(nxym,a,1) 
!
      avga = 0.5*(a(nmmin)+a(nmmax)) 
!
      ws = tiny*avga
!     ws=0.
!
      do j = j1, j2 
         do i = i1, i2 
!
            nm = (j - jstart)*nxm + i - istart + 1 
!
            a(nm) = a(nm) + 4.*ws 
            b(nm) = b(nm) - 2.*ws 
            bf(nm) = bf(nm) - 2.*ws 
            d(nm) = d(nm) - 2.*ws 
            df(nm) = df(nm) - 2.*ws 
            c(nm) = c(nm) + ws 
            cf(nm) = cf(nm) + ws 
            e(nm) = e(nm) + ws 
            ef(nm) = ef(nm) + ws 
!
         end do 
      end do 
!
      return  
      end subroutine stencil 


      subroutine tensgrid(nx, ny, i1, i2, j1, j2, xl, xr, yb, yt, romin, romax&
         , x, y, xn, yn, w, a, b, c, d, e, f) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer  :: ny 
      integer  :: i1 
      integer , intent(in) :: i2 
      integer , intent(in) :: j1 
      integer , intent(in) :: j2 
      real(double)  :: xl 
      real(double)  :: xr 
      real(double)  :: yb 
      real(double)  :: yt 
      real(double)  :: romin 
      real(double)  :: romax 
      real(double)  :: x(*) 
      real(double)  :: y(*) 
      real(double)  :: xn(*) 
      real(double)  :: yn(*) 
      real(double)  :: w(*) 
      real(double)  :: a(*) 
      real(double)  :: b(*) 
      real(double)  :: c(*) 
      real(double)  :: d(*) 
      real(double)  :: e(*) 
      real(double)  :: f(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: l, i, nxp, j, ij 
!-----------------------------------------------
!
      do l = 1, 3 
!
!      call rolinear(i1,i2-1,xl,xr,romin,romax,xn,w)
         call roexp (i1, i2 - 1, xl, xr, 1.D0, 20.D0, xn, w) 
!
         do i = i1, i2 - 1 
            w(i) = 1./(w(i)+1.E-10) 
         end do 
!
         call xgrid (i1 + 1, i2 - 1, a, b, c, d, e, f, w, x, xn) 
!
      end do 
      nxp = nx + 1 
      do j = j1, j2 
         do i = i1, i2 
            ij = (j - 1)*nxp + i 
            xn(ij) = xn(ij-nxp) 
         end do 
      end do 
!
      return  
      end subroutine tensgrid 


      subroutine tridiag(nqi, i1, i2, bcr, srite, a, b, c, d, e, f, s) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nqi 
      integer , intent(in) :: i1 
      integer , intent(in) :: i2 
      real(double) , intent(in) :: bcr 
      real(double) , intent(in) :: srite 
      real(double) , intent(in) :: a(*) 
      real(double) , intent(in) :: b(*) 
      real(double) , intent(in) :: c(*) 
      real(double) , intent(in) :: d(*) 
      real(double) , intent(inout) :: e(*) 
      real(double) , intent(inout) :: f(*) 
      real(double) , intent(inout) :: s(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, j 
      real(double) :: rws 
!-----------------------------------------------
!
      do i = i1, i2, nqi 
         rws = 1./(b(i)-c(i)*e(i-nqi)) 
         e(i) = a(i)*rws 
         f(i) = (d(i)+c(i)*f(i-nqi))*rws 
      end do 
!
!     solve for s(i2)
!
      if (bcr == 0.0) then 
         s(i2) = e(i2)*srite + f(i2) 
      else 
         rws = 1./(1. + (1. - 2.*bcr)*e(i2)) 
         s(i2) = f(i2)*rws 
      endif 
!
      j = i2 - nqi 
      do i = i1, i2 - nqi, nqi 
         s(j) = e(j)*s(j+nqi) + f(j) 
         j = j - nqi 
      end do 
!
      return  
      end subroutine tridiag 


      subroutine timstp 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, ij, i, ijp, ipj, ipjp, ijmax,np,lv 
      real(double) :: dtcon, relstrn, dtgrow,xcdust,ycdust,dist,rdust,dcalc 
!-----------------------------------------------
!   E x t e r n a l   F u n c t i o n s
!-----------------------------------------------
      integer , external :: ismax 
!-----------------------------------------------

! +++
! +++ compute the new time step, dt
! +++
      call setzero (sc2, nxyp) 
      dtcon = 1.E+20 
      do j = 2, ny 
         ij = (j - 1)*nxp + 2 
         do i = 2, nx 
            ijp = ij + nxp 
            ipj = ij + 1 
            ipjp = ijp + 1 
!
            sc2(ij) = ((cx1(ij)+cr1(ij))**2+cy1(ij)**2+(cx2(ij)+cr2(ij))**2+cy2&
               (ij)**2+(cx3(ij)+cr3(ij))**2+cy3(ij)**2+(cx4(ij)+cr4(ij))**2+cy4&
               (ij)**2)/vol(ij)**2 
            ij = ij + 1 
         end do 
      end do 
      ijmax = ismax(nxp*nyp,sc2,1) 
!
!     calculate a the maximum strain
!
      relstrn = sqrt(relstrn)*dt 
      dtgrow = 1.2*dt 
!
      if (ncyc == 0) dtgrow = dt 
!
!     time step set so that particle does not move more than one cell
!     in a time step
!
!     dtpdv- constrains time step so that work done by fluid in time ste
!     cannot exceed energy in cell
      if (dxmax /= 0.) dtcon = stabl*dtgrow/dxmax 
      dt = min(min(min(dtgrow,dtcon),dtmax),dtpdv) 
!      dt = min1(dt,dtin)
      if (dt == dtgrow) iddt = 'g' 
      if (dt == dtcon) iddt = 'c' 
      if (dt == dtmax) iddt = 'm' 
      if (dt == dtpdv) iddt = 'p' 
      if (ncyc == 1) dtmin = dt*1.E-10 



 
      xcdust = xcenter(nsp_dust) 
      ycdust = ycenter(nsp_dust)
      rdust=rex(nsp_dust)
! (Leo) Calculates the subcycling of each particle

      do j=2,ny
         do i=2,nx
            ij=(j-1)*nxp+i
            np=iphead(ij)
            if (np.eq.0) cycle
! Calculate half the distance in Electron debye length units
        dcalc=0.5*sqrt(abs((xn(ij)-xcdust)**2+(yn(ij)-ycdust)**2)/siep(2)**2)
	if (dcalc.gt.1) then    
		dist=dcalc
	else
		dist=1
	end if
111         continue
            call listmkr(np,nploc,link,lvmax,ntmp)
! Sort of distance determining the level of subcycling. can't be smaller than 1

            do lv=1,lvmax
               if (ncyc.eq.0) then
                  dtsub(nploc(lv))=dt
               endif
               if (ico(nploc(lv)).ge.nsp_dust) then
                  nsub(nploc(lv))=1
               elseif (ico(nploc(lv)).eq.1) then
                  nsub(nploc(lv))=1+(nsubmax-1)/dist 
                  nsub(nploc(lv))=1
               else
                  nsub(nploc(lv))=nsubmax
               endif
               np=link(nploc(lv))
            enddo
            if (np.gt.0) goto 111
            
         enddo
      enddo

      if (dt > dtmin) return  
      write (6, 100) dt, ncyc, iddt 
      write (59, 100) dt, ncyc, iddt 
      twfin = 0. 
      return  
  100 format(' dt=',1p,e12.5,' at cycle',i5,', cause is ',a1) 
  200 format(' timstp',(9i7)) 
      return  
      end subroutine timstp 


      subroutine taperd 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: ntd, nwcomd, n 
!-----------------------------------------------
!
!
! +++
! +++ restart problem from a tape dump
! +++
      ntd = ny 
      nwcomd = loc(zzz) 
      nwcomd = nwcomd - loc(aa) + 1 
      nwcomd = nwcomd 
      read (8) (aa(n),n=1,nwcomd) 
      if (ntd == ndump) then 
         write (6, 100) ndump, t, ncyc 
         write (59, 100) ndump, t, ncyc 
         ndump = ndump + 1 
         call getjtl (timlmt) 
!      call fulout(0)
         return  
      endif 
      write (6, 110) ndump, ntd 
      write (59, 110) ndump, ntd 
!      call exit
      call exita (4) 
      stop '4' 
  100 format('  restarting from td',i3,' t=',1p,e12.5,' cycle',i5) 
  110 format(' wrong dump number -',2i6) 
      return  
      end subroutine taperd 

      subroutine tapewr 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nwcomd, n 
!-----------------------------------------------

!      data ndump/0/
! +++
! +++ write a dump tape and exit
! +++
      nwcomd = loc(zzz) 
      nwcomd = nwcomd - loc(aa) + 1 
      nwcomd = nwcomd 
      write (8) (aa(n),n=1,nwcomd) 
      write (6, 100) ndump, t, ncyc 
      write (59, 100) ndump, t, ncyc 
!      call fulout(0)
      stop '3' 
!      call ptime
  100 format('  tape dump',i3,' at t=',1p,e12.5,' cycle',i5) 
      return  
      end subroutine tapewr 


      subroutine vinit 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: j, ij, i, ns, is 
      real(double) :: rvvol, teta 
!-----------------------------------------------
!
!
!     calculate the new grid positions
!
      do j = 2, nyp 
         ij = (j - 1)*nxp + 2 
         do i = 2, nxp 
            x(ij) = xn(ij) 
            y(ij) = yn(ij) 
            ij = ij + 1 
         end do 
      end do 
      call geom 
!
!     calculate the charge density at the cell centers
!
!      do 38 j=2,ny
      do j = 1, ny + 1 
!         ij=(j-1)*nxp+2
         ij = (j - 1)*nxp + 1 
!         do 35 i=2,nx
         do i = 1, nx + 1 
            qnet0(ij) = qnet0(ij)/vol(ij) 
            q_mean(ij)=q_mean(ij)*.98d0+.02d0*qnet0(ij)
!         qnet0(ij)=1.
            emxc(ij) = emxc(ij)/vol(ij) 
            emyc(ij) = emyc(ij)/vol(ij) 
            ij = ij + 1 
         end do 
         do ns = 1, nsp 
!      ij=(j-1)*nxp+2
            ij = (j - 1)*nxp + 1 
!      do 36 i=2,nx
            do i = 1, nx + 1 
               qdn(ij,ns) = qdn(ij,ns)/vol(ij) 
               ij = ij + 1 
            end do 
         end do 
      end do 
!     **********************************************
!
!     recalculate vvol with contributions from the interior of
!     the grid only
!     store in sc1
!
!     *************************************************
      call geocoef (1, nxp, 2, nx, 2, ny, cyl, x, y, a, b, c, d, af, bf, cf, df&
         , ag, bg, cg, dg, eh, residu, sc1) 
!
      if (periodic) call bcv (1, nxp, 1, nxp, 2, nyp, sc1) 
!
!     calculate the current density at the vertices
!
      do j = 2, nyp 
         ij = (j - 1)*nxp + 2 
         do i = 2, nxp 
            rvvol = 1./sc1(ij) 
!      qv0(ij)=qv0(ij)*rvvol
            jxtil(ij) = jxtil(ij)*rvvol 
            jytil(ij) = jytil(ij)*rvvol 
            jztil(ij) = jztil(ij)*rvvol 
            jx0(ij) = jx0(ij)*rvvol 
            jy0(ij) = jy0(ij)*rvvol 
            opsq(ij) = opsq(ij)*rvvol 
            dielperp(ij) = dielperp(ij)*rvvol 
            dielpar(ij) = dielpar(ij)*rvvol 
            dieltrns(ij) = dieltrns(ij)*rvvol 
            ij = ij + 1 
         end do 
      end do 
      do is = 1, nsp 
         do j = 2, nyp 
            ij = (j - 1)*nxp + 2 
            do i = 2, nxp 
               jxs(ij,is) = jxs(ij,is)/sc1(ij) 
               jys(ij,is) = jys(ij,is)/sc1(ij) 
               ij = ij + 1 
            end do 
         end do 
      end do 
!
!     for an explicit calculation, set opsq to 1
!
      if (imp == 0) then 
         do j = 2, nyp 
            do i = 2, nxp 
               ij = (j - 1)*nxp + i 
               opsq(ij) = 0. 
               jx0(ij) = 0.0 
               jy0(ij) = 0.0 
            end do 
         end do 
!
      endif 
!
!     jx0,jy0 satisfy the boundary conditions
!
      call bc (jx0, jy0) 
!
!     particle accounting
!
      numtotl = 0 
      do is = 1, nsp 
         numtot(is) = 0 
         do j = 2, nyp 
            do i = 2, nxp 
               ij = (j - 1)*nxp + i 
               numtot(is) = numtot(is) + nint(number(ij,is)) 
            end do 
         end do 
         numtotl = numtotl + numtot(is) 
      end do 
!
!     set historical velocity
!
      teta = dt*sqrt(qom(1)) 
      do is = 1, nsp 
         do i = 2, nxp 
            do j = 2, nyp 
               ij = (j - 1)*nxp + i 
               if (qvs(ij,is) <= 0.01D0) cycle  
               uhst(ij,is) = uhst(ij,is)*(1.D0 - teta) + teta*jxs(ij,is)/(qvs(&
                  ij,is)+1.D-10) 
               vhst(ij,is) = vhst(ij,is)*(1.D0 - teta) + teta*jys(ij,is)/(qvs(&
                  ij,is)+1.D-10) 
            end do 
         end do 
      end do 
!
      return  
 
      end subroutine vinit 


      subroutine weight(x, y, q, qw, nx, ny, dx, dy, nxyp, cx1, cx2, cx3, cx4, &
         cy1, cy2, cy3, cy4, cr1, cr2, cr3, cr4, vol) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: nx 
      integer , intent(in) :: ny 
      integer  :: nxyp 
      real(double) , intent(in) :: dx 
      real(double) , intent(in) :: dy 
      real(double)  :: x(1) 
      real(double)  :: y(1) 
      real(double) , intent(in) :: q(1) 
      real(double) , intent(inout) :: qw(1) 
      real(double) , intent(in) :: cx1(*) 
      real(double) , intent(in) :: cx2(*) 
      real(double) , intent(in) :: cx3(*) 
      real(double) , intent(in) :: cx4(*) 
      real(double) , intent(in) :: cy1(*) 
      real(double) , intent(in) :: cy2(*) 
      real(double) , intent(in) :: cy3(*) 
      real(double) , intent(in) :: cy4(*) 
      real(double) , intent(in) :: cr1(*) 
      real(double) , intent(in) :: cr2(*) 
      real(double) , intent(in) :: cr3(*) 
      real(double) , intent(in) :: cr4(*) 
      real(double) , intent(in) :: vol(*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: nxp, nyp, j, ij, imj, imjm, ijm, i, ijb, ijt, ijr, ijl 
      real(double) :: factor, size, dqdx, dqdy, qbar 
!-----------------------------------------------
!     real*8 jacob
!
!
!     a routine to calculate the inverse gradient length
!     scale of a cell centered quantity
!
!
      nxp = nx + 1 
      nyp = ny + 1 
      factor = (1. + (nx - 1.)*0.2)**2 
      size = (1./(nx*dx)**2 + 1./(ny*dy)**2)*factor 
      do j = 2, ny 
!
         ij = (j - 1)*nxp + 2 
         imj = ij - 1 
         imjm = ij - nxp - 1 
         ijm = ij - nxp 
!
         do i = 2, nx 
!
            dqdx = (-(cx4(ij)+cr4(ij))*q(ij)) - (cx1(imj)+cr1(imj))*q(imj) - (&
               cx2(imjm)+cr2(imjm))*q(imjm) - (cx3(ijm)+cr3(ijm))*q(ijm) 
!
            dqdy = (-cy4(ij)*q(ij)) - cy1(imj)*q(imj) - cy2(imjm)*q(imjm) - cy3&
               (ijm)*q(ijm) 
!
            qbar = 0.25*(q(ij)*vol(ij)+q(imj)*vol(imj)+q(imjm)*vol(imjm)+q(ijm)&
               *vol(ijm)) 
!
            qw(ij) = (dqdx**2 + dqdy**2)/(qbar**2 + 1.E-10) + size 
 
!
            ij = ij + 1 
            imj = imj + 1 
            imjm = imjm + 1 
            ijm = ijm + 1 
!
         end do 
      end do 
!
      ijb = 2 
      ijt = ny*nxp + 2 
      do i = 2, nx 
         qw(ijb) = qw(ijb+nxp) 
         qw(ijt) = qw(ijt-nxp) 
         ijb = ijb + 1 
         ijt = ijt + 1 
      end do 
 
      ijr = nxp 
      ijl = 1 
      do j = 1, nyp 
         qw(ijl) = qw(ijl+1) 
         qw(ijr) = qw(ijr-1) 
         ijr = ijr + nxp 
         ijl = ijl + nxp 
      end do 
!
      return  
      end subroutine weight 


!
      subroutine wates(xp, yp, nploc, lvmax, w) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: lvmax 
      integer , intent(in) :: nploc(*) 
      real(double) , intent(in) :: xp(*) 
      real(double) , intent(in) :: yp(*) 
      real(double) , intent(out) :: w(9,*) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: lv, i, j 
      real(double) :: the, zeta 
!-----------------------------------------------
!
!dir$  ivdep
      do lv = 1, lvmax 
         i = int(xp(nploc(lv))) 
         j = int(yp(nploc(lv))) 
         the = xp(nploc(lv)) - i - 0.5 
         zeta = yp(nploc(lv)) - j - 0.5 
!
!           center cell
!
         w(1,lv) = (0.75 - the**2)*(0.75 - zeta**2) 
!c
!           neighbors
!c
         w(2,lv) = 0.5*(0.5 + the)**2*(0.75 - zeta**2) 
         w(4,lv) = 0.5*(0.75 - the**2)*(0.5 + zeta)**2 
         w(6,lv) = 0.5*(0.5 - the)**2*(0.75 - zeta**2) 
         w(8,lv) = 0.5*(0.75 - the**2)*(0.5 - zeta)**2 
!c
!           corners
!c
         w(3,lv) = 0.25*((0.5 + the)*(0.5 + zeta))**2 
         w(5,lv) = 0.25*((0.5 - the)*(0.5 + zeta))**2 
         w(7,lv) = 0.25*((0.5 - the)*(0.5 - zeta))**2 
         w(9,lv) = 0.25*((0.5 + the)*(0.5 - zeta))**2 
!
      end do 
!
      return  
      end subroutine wates 
