      subroutine dumpone 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
 
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: itotal, isplit, icoal, i, j, ij, np 
!-----------------------------------------------
 
      itotal = 0 
      isplit = 0 
      icoal = 0 
      do i = 2, nx 
         do j = 2, ny 
            ij = (nx + 1)*(j - 1) + i 
            np = iphead(ij) 
 1999       continue 
            if (np == 0) cycle  
            itotal = itotal + 1 
            if (istatus(np) == 1) isplit = isplit + 1 
            if (istatus(np) == 2) icoal = icoal + 1 
            if (mod(itotal,nparticle) == 0) then 
               write (71 + istatus(np) + 3*(ico(np)-1), 777) xpf(np), ypf(np), &
                  up(np), vp(np) 
  777          format(1x,4(f10.3,3x)) 
            endif 
            np = link(np) 
            go to 1999 
         end do 
      end do 
      write (6, *) 'npart: tot,split,coal', itotal, isplit, icoal 
      write (6, *) 'npart out: ver, split,coal', iviaver, iviaspl, iviacoa 
      return  
      end subroutine dumpone 
