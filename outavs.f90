      subroutine outavs
      include 'impl.inc'
      include 'celest2d.com'
      character*32 filo1,filo2
      character*8 tempo
 
       encode(8,'(e8.2)',tempo)t
 
      filo1=tempo//'grid.avs'
      filo2=tempo//'scal.avs'
 
       open(51,file=filo1,status='unknown')
       open(52,file=filo2,status='unknown')
 
       nxp=nx+1
 
!
!       Grid Printout
!
 
       do i=2,nx+1
       do j=2,ny+1
       ij=nxp*(j-1)+i
       write(51,101)x(ij),0.,y(ij),-x(ij),1.,vol(ij)
101    format(1x,6(e12.3,3x))
       enddo
       enddo
 
!
!       Scalar Printout
!
 
       do i=2,nx
       do j=2,ny
       ij=nxp*(j-1)+i
      ipj=ij+1
      ipjp=ij+nxp+1
      ijp=ij+nxp
      chi_avg=0.25*(diegrid(ipj)+diegrid(ipjp)+diegrid(ijp)             &
     & +diegrid(ij))
      x_avg=0.25*(x(ipj)+x(ipjp)+x(ijp)+x(ij))
      y_avg=0.25*(y(ipj)+y(ipjp)+y(ijp)+y(ij))
       write(52,102)x_avg,y_avg,phi0(ij),chi_avg,qnet0(ij),             &
     &  qdn(ij,1),qdn(ij,2)
102    format(1x,7(e12.3,3x))
       enddo
       enddo
 
       close(51)
       close(52)
 
       return
       end
