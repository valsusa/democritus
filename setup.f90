      subroutine setup 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      USE vast_kind_param, ONLY:  double 
      use impl_M 
      use celest2d_com_M 
!...Translated by Pacific-Sierra Research 77to90  4.3E  08:20:13   5/ 6/06  
!...Switches: -yfv -x1            
 
!
      implicit none
!-----------------------------------------------
!   G l o b a l   P a r a m e t e r s
!-----------------------------------------------
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: i, ij, isp, ni, ijl, ijr, j, ijb, ijt, is 
      real(double) :: dymn, dyi
!-----------------------------------------------
!
! $Header: /n/cvs/democritus/setup.f90,v 1.1.1.1 2006/06/13 23:37:45 cfichtl Exp $
!
! $Log: setup.f90,v $
! Revision 1.1.1.1  2006/06/13 23:37:45  cfichtl
! Initial version of democritus 6/13/06
!
!
 
 
      call celset (1, nxp, 1, nxp, 1, nyp, xorigin, yorigin, dx, dy, x, y, xn, &
         yn) 
      write(*,*)'geom'   
      call geom 
 	  write(*,*)'initpar'
      call initpar 
      call magnetic (1, nxp, 1, nyp, 1, nxp, x, cyl, rmaj, bx0, by0, bz0, bx, &
         by, bz) 
      call parcel 
!     write(6,200) (iphead(k),k=1,nxyp)
      dymn = 1.E20 
      do i = 2, nx 
         ij = nxp + i 
         dyi = y(ij+nxp) - y(ij) 
         dymn = min(dymn,dyi) 
      end do 
      do isp = 1, nsp 
         if (inj(isp) > 0) dtin(isp) = dymn/(vinf(isp)*isqin(isp)) 
         tinf(isp) = t - dtin(isp)/2. 
      end do 
!
  200 format(' setup',(9i7)) 
 
 
 
      if (maxset == 0) return  
      do ni = 1, maxset 
!
!     generate an adaptive grid to resolve structures
!
         if (grid /= 0.) then 
!
            call adapt 
!
            firstime = .TRUE. 
            call gridmkr (nxp, nyp, 2, nxp, 2, nyp, 2, nxp, 2, nyp, firstime, &
               xn, yn, w, cx1, cx2, cx3, cx4, cy1, cy2, tau, dt, scal, eqvol, &
               nummax, a, b, c, d, e, af, bf, cf, df, ef, eg, dg, cg, bg, ag, &
               ah, bh, ch, dh, eh, soln, srce, residu, sc1, sc2) 
         endif 
         do ij = 1, nxyp 
            x(ij) = xn(ij) 
            y(ij) = yn(ij) 
 
         end do 
 
!      ijl=2*nxp+2
         ijl = 1 
         ijr = 3*nxp 
         do j = 3, ny 
            y(ijl) = y(ijl+1) 
            y(ijr) = y(ijr-1) 
            yn(ijl) = y(ijl) 
            yn(ijr) = y(ijr) 
            ijl = ijl + nxp 
            ijr = ijr + nxp 
         end do 
!
         ijb = nxp + 3 
         ijt = ny*nxp + 3 
         do i = 3, nx 
            x(ijb) = x(ijb+nxp) 
            x(ijt) = x(ijt-nxp) 
            xn(ijb) = x(ijb) 
            xn(ijt) = x(ijt) 
            ijb = ijb + 1 
            ijt = ijt + 1 
         end do 
!
!
         do is = 1, nsp 
            do i = 1, nxyp 
               uhst(i,is) = 0.D0 
               vhst(i,is) = 0.D0 
            end do 
         end do 
 
         call geom 
         call initpar 
         call parcel 
 
         q_mean = 0.D0 
 
 
      end do 
!     uncomment for non-uniform, but not-adaptive grid
!     grid=0.
      return  
      end subroutine setup 
